/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.5.50-0ubuntu0.14.04.1 : Database - mangusta_dresses
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `countries` */

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `currency` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `iso_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_cost` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `countries` */

insert  into `countries`(`id`,`name`,`is_active`,`currency`,`iso_code`,`delivery_cost`) values (1,'Bulgaria',1,'BGN','100',3.6);
insert  into `countries`(`id`,`name`,`is_active`,`currency`,`iso_code`,`delivery_cost`) values (2,'Romania',1,'LEI','642',4.99);
insert  into `countries`(`id`,`name`,`is_active`,`currency`,`iso_code`,`delivery_cost`) values (3,'Greece',1,'EUR','300',10);
insert  into `countries`(`id`,`name`,`is_active`,`currency`,`iso_code`,`delivery_cost`) values (4,'Turkey',1,'TRY','762',100.5);

/*Table structure for table `facebook_pages` */

CREATE TABLE `facebook_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `facebook_pages` */

/*Table structure for table `order_products` */

CREATE TABLE `order_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `notified` int(11) NOT NULL,
  `size_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`order_id`,`product_id`),
  KEY `IDX_5242B8EB8D9F6D38` (`order_id`),
  KEY `IDX_5242B8EB4584665A` (`product_id`),
  KEY `IDX_5242B8EB498DA827` (`size_id`),
  CONSTRAINT `FK_order_products_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `FK_order_products_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_order_products_sizes` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `order_products` */

insert  into `order_products`(`id`,`order_id`,`product_id`,`quantity`,`notified`,`size_id`) values (1,14,51,2,0,1);
insert  into `order_products`(`id`,`order_id`,`product_id`,`quantity`,`notified`,`size_id`) values (2,14,51,3,0,2);
insert  into `order_products`(`id`,`order_id`,`product_id`,`quantity`,`notified`,`size_id`) values (3,16,51,1,0,1);
insert  into `order_products`(`id`,`order_id`,`product_id`,`quantity`,`notified`,`size_id`) values (4,18,51,1,0,1);
insert  into `order_products`(`id`,`order_id`,`product_id`,`quantity`,`notified`,`size_id`) values (5,18,51,1,0,2);
insert  into `order_products`(`id`,`order_id`,`product_id`,`quantity`,`notified`,`size_id`) values (6,18,51,1,0,4);
insert  into `order_products`(`id`,`order_id`,`product_id`,`quantity`,`notified`,`size_id`) values (7,14,51,7,0,3);

/*Table structure for table `orders` */

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `customer_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `post_code` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(1500) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_cost` double NOT NULL,
  `discount` double NOT NULL,
  `date_added` datetime NOT NULL,
  `facebook_page_id` int(11) NOT NULL,
  `facebook_customer_profile_url` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E52FFDEEF92F3E70` (`country_id`),
  CONSTRAINT `FK_order_countries` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `orders` */

insert  into `orders`(`id`,`country_id`,`customer_name`,`email`,`phone`,`post_code`,`city`,`address`,`note`,`delivery_cost`,`discount`,`date_added`,`facebook_page_id`,`facebook_customer_profile_url`) values (8,1,'chavdar dimitrov 1','chavdareto@gmail.com','08888888','9000','varna','vladislav varnenchik 10','Some testing note',6,5,'2016-07-02 16:44:34',0,'');
insert  into `orders`(`id`,`country_id`,`customer_name`,`email`,`phone`,`post_code`,`city`,`address`,`note`,`delivery_cost`,`discount`,`date_added`,`facebook_page_id`,`facebook_customer_profile_url`) values (9,1,'chavdar dimitrov 2','chavdareto@gmail.com','08888888','9000','varna','vladislav varnenchik 10','Some testing note',6,5,'2016-07-02 16:45:39',0,'');
insert  into `orders`(`id`,`country_id`,`customer_name`,`email`,`phone`,`post_code`,`city`,`address`,`note`,`delivery_cost`,`discount`,`date_added`,`facebook_page_id`,`facebook_customer_profile_url`) values (10,1,'chavdar dimitro v 3','chavdareto@gmail.com','08888888','9000','varna','vladislav varnenchik 10','Some testing note',6,5,'2016-07-02 16:50:10',0,'');
insert  into `orders`(`id`,`country_id`,`customer_name`,`email`,`phone`,`post_code`,`city`,`address`,`note`,`delivery_cost`,`discount`,`date_added`,`facebook_page_id`,`facebook_customer_profile_url`) values (14,1,'23ssssssssssssssssss','chavdareto@gmail.com','sssssssssssss','ssssssssssssss','sssssssssssss','ssssssssssssss','sssssssssssssssssssss',123,123123,'0000-00-00 00:00:00',0,'');
insert  into `orders`(`id`,`country_id`,`customer_name`,`email`,`phone`,`post_code`,`city`,`address`,`note`,`delivery_cost`,`discount`,`date_added`,`facebook_page_id`,`facebook_customer_profile_url`) values (15,1,'Chavdar Dimtirov Test me','chavdareto@gmail.com','0898474607','9000','varna','vladislav varnenchik 100','asdasdasd',5,0,'0000-00-00 00:00:00',0,'');
insert  into `orders`(`id`,`country_id`,`customer_name`,`email`,`phone`,`post_code`,`city`,`address`,`note`,`delivery_cost`,`discount`,`date_added`,`facebook_page_id`,`facebook_customer_profile_url`) values (16,1,'Chavdar Dimtirov Test me','chavdareto@gmail.com','08984744444','9000','varna','vladislav varnenchik 100','asdasd',10,0,'0000-00-00 00:00:00',0,'');
insert  into `orders`(`id`,`country_id`,`customer_name`,`email`,`phone`,`post_code`,`city`,`address`,`note`,`delivery_cost`,`discount`,`date_added`,`facebook_page_id`,`facebook_customer_profile_url`) values (17,1,'Chavdar Dimtirov Test me','chavdareto@gmail.cmo','0895848110','9000','varna','vladislav varnenchik 100','asdasda',0,0,'0000-00-00 00:00:00',0,'');
insert  into `orders`(`id`,`country_id`,`customer_name`,`email`,`phone`,`post_code`,`city`,`address`,`note`,`delivery_cost`,`discount`,`date_added`,`facebook_page_id`,`facebook_customer_profile_url`) values (18,1,'Chavdar Dimtirov Test me','chavdareto@gmail.com','08984746','9000','varna','vladislav varnenchik 100','asdasd',0,0,'0000-00-00 00:00:00',0,'');

/*Table structure for table `product_prices` */

CREATE TABLE `product_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` double NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_86B72CFD4584665A` (`product_id`),
  KEY `IDX_86B72CFDF92F3E70` (`country_id`),
  CONSTRAINT `FK_86B72CFD4584665A` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_86B72CFDF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `product_prices` */

insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (16,10,51,1);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (17,39.9,51,2);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (18,5,51,3);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (19,100,51,4);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (21,100,53,1);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (22,400,53,2);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (23,50,53,3);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (24,1000,53,4);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (25,10,54,1);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (26,40,54,2);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (27,18,54,3);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (28,1000,54,4);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (29,0,55,1);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (30,0,55,2);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (31,0,55,3);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (32,0,55,4);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (33,0,56,1);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (34,0,56,2);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (35,0,56,3);
insert  into `product_prices`(`id`,`price`,`product_id`,`country_id`) values (36,0,56,4);

/*Table structure for table `product_size` */

CREATE TABLE `product_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `min_quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7A2806CBD34A04AD` (`product_id`),
  KEY `IDX_7A2806CBF7C0246A` (`size_id`),
  CONSTRAINT `product_size_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `product_size_ibfk_2` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `product_size` */

insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (175,1,51,1,1);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (176,1,51,2,1);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (177,1,51,3,1);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (178,2,51,4,1);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (185,200,53,1,20);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (186,200,53,2,20);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (187,200,53,3,20);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (188,150,53,4,20);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (189,10,53,5,20);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (190,32,54,1,5);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (191,12,54,2,3);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (192,0,54,3,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (193,0,54,4,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (194,0,54,5,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (195,0,55,1,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (196,0,55,2,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (197,0,55,3,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (198,0,55,4,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (199,0,55,5,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (200,234,56,1,432);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (201,0,56,2,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (202,0,56,3,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (203,0,56,4,0);
insert  into `product_size`(`id`,`quantity`,`product_id`,`size_id`,`min_quantity`) values (204,0,56,5,0);

/*Table structure for table `product_size_description` */

CREATE TABLE `product_size_description` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `size_id` (`size_id`),
  CONSTRAINT `product_size_description_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `product_size_description_ibfk_2` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `product_size_description` */

insert  into `product_size_description`(`id`,`product_id`,`size_id`,`description`) values (1,51,1,'small 30x60');
insert  into `product_size_description`(`id`,`product_id`,`size_id`,`description`) values (2,51,3,'60x90 some additional text here');

/*Table structure for table `products` */

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `is_preorder` int(10) DEFAULT NULL,
  `is_stock_available` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B3BA5A5A9B2A6C7E` (`supplier_id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`name`,`code`,`date_added`,`supplier_id`,`is_preorder`,`is_stock_available`) values (51,'name ssssasd','sssssssssssssssssss','2016-07-05 22:14:00',6,0,1);
insert  into `products`(`id`,`name`,`code`,`date_added`,`supplier_id`,`is_preorder`,`is_stock_available`) values (53,'fffffffffffffffff__edited','1j32hg13213j2__edited','2016-07-05 22:30:00',5,0,1);
insert  into `products`(`id`,`name`,`code`,`date_added`,`supplier_id`,`is_preorder`,`is_stock_available`) values (54,'product а;а;сд а;ксдалс дйк','615234','2016-06-13 22:47:00',5,1,0);
insert  into `products`(`id`,`name`,`code`,`date_added`,`supplier_id`,`is_preorder`,`is_stock_available`) values (55,'nnnnnnnnnnnn','1652341623','2016-07-12 22:54:00',5,0,0);
insert  into `products`(`id`,`name`,`code`,`date_added`,`supplier_id`,`is_preorder`,`is_stock_available`) values (56,'nnnnnnnnnnnn','1652341623','2016-06-17 22:54:00',5,0,1);

/*Table structure for table `roles` */

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B63E2EC757698A6A` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role`) values (3,'ROLE_ADMIN');
insert  into `roles`(`id`,`role`) values (2,'ROLE_SUPER_ADMIN');
insert  into `roles`(`id`,`role`) values (1,'ROLE_USER');

/*Table structure for table `sizes` */

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `sizes` */

insert  into `sizes`(`id`,`name`) values (1,'s');
insert  into `sizes`(`id`,`name`) values (2,'m');
insert  into `sizes`(`id`,`name`) values (3,'l');
insert  into `sizes`(`id`,`name`) values (4,'xl');
insert  into `sizes`(`id`,`name`) values (5,'xs');

/*Table structure for table `suppliers` */

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AC28B95CE7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `suppliers` */

insert  into `suppliers`(`id`,`name`,`email`,`phone`) values (5,'Chavdar','chavdar2@abv.bg','08888888888');
insert  into `suppliers`(`id`,`name`,`email`,`phone`) values (6,'Ivan','vankata@abv.bg','022222222222');

/*Table structure for table `user` */

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`email`,`isActive`,`group_id`) values (1,'admin','$2a$08$jHZj/wJfcVKlIwr5AvR78euJxYK7Ku5kURNhNx.7.CSIJ3Pq6LEPC','admin@example.com',1,1);

/*Table structure for table `user_groups` */

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `identifier` varchar(30) DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `user_groups` */

insert  into `user_groups`(`id`,`title`,`identifier`,`is_hidden`) values (1,'Root','root',1);
insert  into `user_groups`(`id`,`title`,`identifier`,`is_hidden`) values (2,'Administrator','admin',0);
insert  into `user_groups`(`id`,`title`,`identifier`,`is_hidden`) values (3,'Staff','staff',0);

/*Table structure for table `user_roles` */

CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `IDX_54FCD59FA76ED395` (`user_id`),
  KEY `IDX_54FCD59FD60322AC` (`role_id`),
  CONSTRAINT `FK_54FCD59FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_54FCD59FD60322AC` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user_roles` */

insert  into `user_roles`(`user_id`,`role_id`) values (1,1);
insert  into `user_roles`(`user_id`,`role_id`) values (1,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
