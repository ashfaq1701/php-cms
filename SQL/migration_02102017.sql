CREATE TABLE IF NOT EXISTS `jarafph4_cms`.`common_options`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `val` text NOT NULL,
  PRIMARY KEY(`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE = InnoDB;
