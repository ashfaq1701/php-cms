ALTER TABLE `mangusta_dresses`.`orders`   
  ADD COLUMN `total` DOUBLE NULL AFTER `status`,
  ADD COLUMN `total_history` TEXT NULL AFTER `total`;

CREATE TABLE `facebook_pages_users` (
  `id_user` int(11) DEFAULT NULL,
  `id_page` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;