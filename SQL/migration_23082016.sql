ALTER TABLE `orders` ADD COLUMN `created_by_id` INT(11) NOT NULL AFTER `facebook_customer_profile_url`; 

ALTER TABLE `orders` ADD COLUMN `status` TINYINT(1) DEFAULT 1 NOT NULL AFTER `created_by_id`;
UPDATE orders SET status = 1;

CREATE TABLE `todo_list`(
	`id` INT(11) NOT NULL AUTO_INCREMENT, 
	`title` VARCHAR(255) NOT NULL, 
	`description` TEXT, 
	`assigned_to_id` INT(11) NOT NULL, 
	`status` TINYINT(1) NOT NULL DEFAULT 1, 
	`created_at` DATETIME NOT NULL, 
	PRIMARY KEY (`id`)
); 