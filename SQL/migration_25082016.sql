CREATE TABLE `orders_comments`
	(`id` INT(11) NOT NULL AUTO_INCREMENT, 
	`order_id` INT(11) NOT NULL, 
	`user_id` INT(11) NOT NULL, 
	`comment` TEXT NOT NULL, 
	`created_at` DATETIME NOT NULL, 
	PRIMARY KEY (`id`)
); 
