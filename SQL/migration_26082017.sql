CREATE TABLE IF NOT EXISTS `jarafph4_cms`.`album_categories`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE = InnoDB;
INSERT
INTO
  `album_categories`(`name`)
VALUES('New collection'),('Summer Sale'),('Winter Sale'), ('XL-discount');
CREATE TABLE IF NOT EXISTS `jarafph4_cms`.`facebook_albums`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(200) NOT NULL,
  `subtitle` VARCHAR(400) NULL,
  `fb_url` VARCHAR(300) NULL,
  `fb_id` VARCHAR(20) NULL,
  `fb_page_id` INT UNSIGNED NULL,
  `category_id` INT UNSIGNED NULL,
  PRIMARY KEY(`id`),
  INDEX `fk_fb_albums_1_idx`(`fb_page_id` ASC),
  INDEX `fk_fb_albums_2_idx`(`category_id` ASC),
  FOREIGN KEY(`fb_page_id`) REFERENCES `jarafph4_cms`.`facebook_pages`(`id`),
  FOREIGN KEY(`category_id`) REFERENCES `jarafph4_cms`.`album_categories`(`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `jarafph4_cms`.`facebook_photos`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT NULL,
  `image_id` INT NULL,
  `fb_id` VARCHAR(20) NULL,
  `additional_notes` TEXT NULL,
  `fb_album_id` INT UNSIGNED NULL,
  `fb_post_id` INT UNSIGNED NULL,
  'description_format' TEXT NULL,
  PRIMARY KEY(`id`),
  INDEX `fk_facebook_photos_1_idx`(`image_id` ASC),
  INDEX `fk_facebook_photos_2_idx`(`fb_album_id` ASC),
  INDEX `fk_facebook_photos_3_idx`(`fb_post_id` ASC),
  INDEX `fk_facebook_photos_4_idx`(`product_id` ASC),
  FOREIGN KEY(`image_id`) REFERENCES `jarafph4_cms`.`images`(`id`),
  FOREIGN KEY(`fb_album_id`) REFERENCES `jarafph4_cms`.`facebook_albums`(`id`),
  FOREIGN KEY(`fb_post_id`) REFERENCES `jarafph4_cms`.`facebook_posts`(`id`),
  FOREIGN KEY(`product_id`) REFERENCES `jarafph4_cms`.`products`(`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

ALTER TABLE facebook_photos
ADD COLUMN `uploaded_by` INT(11) NULL,
ADD COLUMN `uploaded_at` DATETIME NULL,
ADD INDEX `fk_facebook_photos_1_idx1` (`uploaded_by` ASC),
ADD  CONSTRAINT `fk_facebook_photos_1` FOREIGN KEY (`uploaded_by`) REFERENCES `jarafph4_cms`.`user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `images` CHANGE `code` `code` VARCHAR(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `images` CHANGE `type` `type` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `images` CHANGE `side_content` `side_content` VARCHAR(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '';
ALTER TABLE `images` CHANGE `brand` `brand` SMALLINT(5) UNSIGNED NULL;
ALTER TABLE `images` CHANGE `priority` `priority` INT(11) NULL COMMENT 'lower number means higher priority';
ALTER TABLE `images` CHANGE `additional_codes` `additional_codes` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT 'underscore separated additional codes in the image';
ALTER TABLE `images` CHANGE `times_posted` `times_posted` INT(11) NULL DEFAULT '0';

ALTER TABLE `facebook_photos` ADD `photo_order` INT(10) NULL;
ALTER TABLE `facebook_photos` ADD `batch_no` VARCHAR(50) NULL AFTER `uploaded_at`;
ALTER TABLE `facebook_photos` ADD `processed_at` DATETIME NULL AFTER `batch_no`;
ALTER TABLE facebook_photos ADD description_format TEXT NULL AFTER fb_post_id;
