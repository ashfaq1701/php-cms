ALTER TABLE products
ADD weight FLOAT AFTER `supplier_id`;

ALTER TABLE order_products
ADD is_shipped TINYINT AFTER size_id;