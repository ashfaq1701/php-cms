ALTER TABLE `mangusta_dresses`.`facebook_pages`
  ADD COLUMN `app_id` TEXT NULL AFTER `url`,
  ADD COLUMN `secret_id` TEXT NULL AFTER `app_id`,
  ADD COLUMN `token` TEXT NULL AFTER `secret_id`;

ALTER TABLE mangusta_dresses.facebook_pages ADD country INT NULL;