CREATE TABLE mangusta_dresses.facebook_pages_descriptions
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    description TEXT NOT NULL,
    page_id INT
);
CREATE UNIQUE INDEX facebook_pages_descriptions_id_uindex ON mangusta_dresses.facebook_pages_descriptions (id);