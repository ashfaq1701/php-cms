<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['maintenance'] = 'maintenance';

$route['admin'] = 'admin/home';

$route['cli/API/getVoucherLink/(:any)/(:any)/(:any)'] = 'cli/API/getVoucherLink/$1/$2/$3';
$route['cli/API/syncCoupons'] = 'cli/API/syncCoupons';
$route['cli/API/coupon_update'] = 'cli/API/coupon_update';
$route['cli/API/webhookOrderCreate'] = 'cli/API/webhookOrderCreate';
$route['cli/API/testjson'] = 'cli/API/testjson';

//order routes
$route['admin/orders/(:num)'] = 'admin/order';
$route['admin/orders'] = 'admin/order';
$route['admin/orders/create'] = 'admin/order/create';
$route['admin/orders/findById'] = 'admin/order/findById';
$route['admin/orders/GiveDeliveryPriceByFClist'] = 'admin/order/GiveDeliveryPriceByFClist';
$route['admin/orders/changestatus/(:num)/(:num)'] = 'admin/order/changeOrderStatus/$1/$2';
$route['admin/orders/edit/(:any)'] = 'admin/order/edit/$1';
$route['admin/products/ppost'] = 'admin/product/ppost';
$route['admin/orders/edit/(:any)/remprod/(:num)/(:num)'] = 'admin/order/removeProduct/$1/$2/$3';
$route['admin/orders/edit/(:any)/chprod/(:num)/(:num)'] = 'admin/order/changeProduct/$1/$2/$3';
$route['admin/orders/orders_autocomplete_customer'] = 'admin/order/order_customer_autocomplete';
$route['admin/orders/orders_autocomplete'] = 'admin/order/order_autocomplete';
$route['admin/orders/order_add_row'] = 'admin/order/order_add_row';
$route['admin/orders/handle_user_comment'] = 'admin/order/handle_user_comment';
$route['admin/orders/handle_pagination'] = 'admin/order/handle_pagination';
$route['admin/orders/handle_delivery_cost'] = 'admin/order/handle_delivery_cost';
$route['admin/orders/load_comments/(:num)'] = 'admin/order/load_comments/$1';

//product routes
$route['admin/products'] = 'admin/product';
$route['admin/products/(:num)'] = 'admin/product';
$route['admin/products/p/(:any)'] = 'admin/product/getproduct/$1';
$route['admin/products/productinfo/(:any)'] = 'admin/product/productinfo/$1';
$route['admin/products/edit/(:any)'] = 'admin/product/edit/$1';
$route['admin/products/handle_user_comment'] = 'admin/product/handle_user_comment';
$route['admin/products/deleteimage/(:any)'] = 'admin/product/deleteImage/$1';
$route['admin/products/setmainimage/(:any)'] = 'admin/product/setMainImage/$1';
$route['admin/products/changeavailability/(:num)/(:num)'] = 'admin/product/changeAvailability/$1/$2';
$route['admin/products/exchangecurrency'] = 'admin/product/exchangeCurrency';
$route['admin/products/load_comments/(:num)'] = 'admin/product/load_comments/$1';
$route['admin/products/recalculateprices/(:num)'] = 'admin/product/recalculateprices/$1';

$route['admin/products/expected/(:any)/(:any)'] = 'admin/product/expected'; // if pagination is on first page
$route['admin/products/expected/(:any)/(:any)/(:num)'] = 'admin/product/expected'; // if pagination is on first page
$route['admin/products/(:any)/(:any)'] = 'admin/product'; // if pagination is on first page
$route['admin/products/(:any)/(:any)/(:any)'] = 'admin/product';
$route['admin/products/create'] = 'admin/product/create';

//productcategories routes
$route['admin/productcategories'] = 'admin/productcategories';
$route['admin/productcategories/create'] = 'admin/productcategories/create';
$route['admin/productcategories/edit/(:any)'] = 'admin/productcategories/edit/$1';


//$route['admin/products/expected'] = 'admin/product/expected';

//size routes
$route['admin/sizes'] = 'admin/size';
$route['admin/sizes/create'] = 'admin/size/create';
$route['admin/sizes/edit/(:any)'] = 'admin/size/edit/$1';

//supplier routes
$route['admin/suppliers'] = 'admin/supplier';
$route['admin/suppliers/create'] = 'admin/supplier/create';
$route['admin/suppliers/edit/(:any)'] = 'admin/supplier/edit/$1';

//user routes
$route['admin/users'] = 'admin/user';
$route['admin/users/create'] = 'admin/user/create';
$route['admin/users/edit/(:any)'] = 'admin/user/edit/$1';

//country routes
$route['admin/countries'] = 'admin/country';
$route['admin/countries/create'] = 'admin/country/create';
$route['admin/countries/edit/(:any)'] = 'admin/country/edit/$1';

//group routes
$route['admin/groups'] = 'admin/group';
$route['admin/groups/create'] = 'admin/group/create';
$route['admin/groups/edit/(:any)'] = 'admin/group/edit/$1';

// tags routes
$route['admin/tags'] = 'admin/tag';
$route['admin/tags/create'] = 'admin/tag/create';
$route['admin/tags/edit/(:any)'] = 'admin/tag/edit/$1';

// todo routes
$route['admin/todo'] = 'admin/todo';
$route['admin/todo/create'] = 'admin/todo/create';
$route['admin/todo/edit/(:any)'] = 'admin/todo/edit/$1';

//temp routes
$route['admin/temp/replaceimages'] = 'admin/temp/replaceimages';

//images routes
$route['admin/images/bulkupload'] = 'admin/images/bulkupload';
$route['admin/images/brand/upload'] = 'admin/images/brandImageUpload';
$route['admin/images/brand/list'] = 'admin/images/getBrandList';
$route['admin/images/brand/save'] =  'admin/images/saveBrand';

//reports routes
$route['admin/reports'] = 'admin/report/ordercount';
$route['admin/reports/ordercount'] = 'admin/report/ordercount';
$route['admin/reports/productsold'] = 'admin/report/productsold';
$route['admin/reports/userprofit'] = 'admin/report/userprofit';
$route['admin/reports/words'] = 'admin/report/words';

//facebook routes
$route['admin/facebook'] = 'admin/facebook';
$route['admin/facebook/list'] = 'admin/facebook/pageList';
$route['admin/facebook/grouped'] = 'admin/facebook/getGroupedCollection';
$route['admin/facebook/page/(:num)'] = 'admin/facebook/get/$1';
$route['admin/facebook/create'] = 'admin/facebook/create';
$route['admin/facebook/delete/(:num)'] = 'admin/facebook/delete/$1';
$route['admin/facebook/deleteFBImage/(:num)'] = 'admin/facebook/deleteFBImage/$1';
$route['admin/facebook/edit/(:any)'] = 'admin/facebook/edit/$1';
$route['admin/facebook/refresh/all'] = 'admin/facebook/refreshAllPhotos';
$route['admin/facebook/refresh/(:num)'] = 'admin/facebook/refreshAlbums/$1';
$route['admin/facebook/entity/test'] = 'admin/facebook/facebookEntityTest';
$route['admin/facebook/uploadPhotos'] = 'admin/facebook/getUploadPhotosIntoAlbums';
$route['admin/facebook/uploadPhotos/post'] = 'admin/facebook/postUploadPhotosIntoAlbums';
$route['admin/facebook/photos'] = 'admin/facebook/managePhotos';
$route['admin/facebook/batch-no'] = 'admin/facebook/getBatchNos';
$route['admin/fbc/ads'] = 'admin/fbc/ads';


$route['admin/facebook/fap'] = 'admin/facebook/fap_index';
$route['admin/facebook/fap/pages'] = 'admin/facebook/fap_pages';
$route['admin/facebook/fap/create'] = 'admin/facebook/fap_create';
$route['admin/facebook/fap/edit/(:num)'] = 'admin/facebook/fap_edit/$1';
$route['admin/facebook/fap/delete/(:num)'] = 'admin/facebook/fap_delete/$1';
$route['admin/facebook/fap_find_product'] = 'admin/facebook/getProductInfo';
$route['admin/facebookads/'] = 'admin/facebookads/index';

// facebook manage page description
$route['admin/facebook/manage-text/(:num)'] = 'admin/facebookDescriptions/index/$1';
$route['admin/facebook/manage-text/(:num)/create'] = 'admin/facebookDescriptions/create/$1';
$route['admin/facebook/manage-text/edit/(:num)'] = 'admin/facebookDescriptions/edit/$1';
$route['admin/facebook/manage-text/delete/(:num)'] = 'admin/facebookDescriptions/delete/$1';

$route['admin/facebook/manage-text/get-descriptions'] = 'admin/facebookDescriptions/getDescriptions';

$route['admin/facebook/auth'] = 'admin/facebook/getFacebookLogin';
$route['admin/facebook/store-auth'] = 'admin/facebook/facebookAuthInfo';
$route['admin/facebook/redirect'] = 'admin/facebook/facebookRedirect';
$route['admin/facebook/image/upload'] = 'admin/facebook/imageUpload';
$route['admin/facebook/photos/search'] = 'admin/facebook/searchPhotos';
$route['admin/facebook/photo/delete/(:num)'] = 'admin/facebook/deletePhoto/$1';

// album routes
$route['admin/album/page/(:num)'] = 'admin/album/pageAlbums/$1';
$route['admin/album/page/(:num)/create'] = 'admin/album/create/$1';
$route['admin/album/edit/(:num)'] = 'admin/album/edit/$1';
$route['admin/album/delete/(:num)'] = 'admin/album/delete/$1';
$route['admin/album/refresh/(:num)'] = 'admin/album/refreshPhotos/$1';

//album categories routes
$route['admin/album-category'] = 'admin/albumCategory/albumCategories';

// statistic routes
$route['admin/statistics'] = 'admin/statistic';

//fbc routes
$route['admin/fbc/unreadmessages'] = 'admin/facebook/unreadmessages';
$route['admin/fbc/change_album_id'] = 'admin/facebook/change_album_id';

//Woo routes
$route['admin/woo/addproduct'] = 'admin/woo/addproduct';
$route['admin/woo/productslist'] = 'admin/woo/productslist';
$route['admin/woo/updatecategories'] = 'admin/woo/updatecategories';
$route['admin/woo/addarray'] = 'admin/woo/addarray';
$route['admin/woo/displaycategories'] = 'admin/woo/displaycategories';
$route['admin/woo/updateproduct/(:any)'] = 'admin/woo/updateproduct/$1';
$route['admin/woo/deletewooproduct/(:any)'] = 'admin/woo/deletewooproduct/$1';

//fapimages
$route['admin/fapimages'] = 'admin/fapimages';
$route['admin/fapimages/getall'] = 'admin/fapimages/getall';

//API
$route['cli/p/all'] = 'cli/API/allproducts';
$route['cli/p/(:any)'] = 'cli/API/product/$1';
$route['cli/facebook_auto_post/paradise_pp'] = 'cli/facebook_auto_post/paradise_pp';
$route['cli/facebook_auto_post/gettestpostsdata'] = 'cli/facebook_auto_post/gettestpostsdata';
$route['cli/facebook_auto_post/updatetestposts'] = 'cli/facebook_auto_post/updatetestposts';
$route['cli/facebook_auto_post/flame_pp'] = 'cli/facebook_auto_post/flame_pp';
$route['cli/facebook_auto_post/paradise_cc'] = 'cli/facebook_auto_post/paradise_cc';
$route['cli/facebook_auto_post/flame_cc'] = 'cli/facebook_auto_post/flame_cc';
$route['cli/facebook_auto_post/deleteTestPosts'] = 'cli/facebook_auto_post/deleteTestPosts';
$route['cli/facebook_auto_post/markBadTestPosts'] = 'cli/facebook_auto_post/markBadTestPosts';
$route['cli/schedule_cron/resetConfirm'] = 'cli/schedule_cron/resetConfirm';


$route['cli/facebook_auto_post/createalbum/(:num)'] = 'cli/facebook_auto_post/createalbum/$1';
$route['cli/general/generatecalendar'] = 'cli/general/generatecalendar';
$route['cli/album/refresh/all'] = 'cli/album/refreshAllPhotos';
$route['cli/facebook/photos/process'] = 'cli/facebook/processPhotos';
$route['cli/facebook/reorder'] = 'cli/facebook/reorderPhotos';
