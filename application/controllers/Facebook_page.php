<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook_page extends Frontend_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $fb = new Facebook\Facebook([
            'app_id' => '958640260934477',
            'app_secret' => 'febe5cb8925df85df11e014cca598dfd',
            'default_graph_version' => 'v2.8',
        ]);


        //Post property to Facebook
        $linkData = [
            'link' => 'www.example.com',
            'message' => 'Your message here test'
        ];
//        $pageAccessToken = '981417565335748|tW5LZelNLC829goytMzf-iRN0M0';
        $pageAccessToken = '958640260934477|CxMkqyNwp3n5baL81ThTNO5p6dQ';

        try {
            $response = $fb->post('/1623934163/feed', $linkData, $pageAccessToken);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $graphNode = $response->getGraphNode();


        exit();
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['manage_pages', 'publish_pages']; // optional

        try {
            if (isset($_SESSION['facebook_access_token'])) {
                $accessToken = $_SESSION['facebook_access_token'];
            } else {
                $accessToken = $helper->getAccessToken();
            }
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        if (isset($accessToken)) {
            if (isset($_SESSION['facebook_access_token'])) {
                $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
            } else {
                // getting short-lived access token
                $_SESSION['facebook_access_token'] = (string)$accessToken;
                // OAuth 2.0 client handler
                $oAuth2Client = $fb->getOAuth2Client();
                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
                $_SESSION['facebook_access_token'] = (string)$longLivedAccessToken;
                // setting default access token to be used in script
                $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
            }
            // redirect the user back to the same page if it has "code" GET variable
            if (isset($_GET['code'])) {
                header('Location: ./');
            }
            // getting basic info about user
            try {
                $profile_request = $fb->get('/me');
                $profile = $profile_request->getGraphNode()->asArray();
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                session_destroy();
                // redirecting user back to app login page
                header("Location: ./");
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            // post on behalf of page
            $pages = $fb->get('/me/accounts');
            $pages = $pages->getGraphEdge()->asArray();
            foreach ($pages as $key) {
                if ($key['name'] == 'Funny Demons') {
                    $post = $fb->post('/' . $key['id'] . '/feed', array('message' => 'just for testing...'), $key['access_token']);
                    $post = $post->getGraphNode()->asArray();
                    print_r($post);
                }
            }
            // Now you can redirect to another page and use the access token from $_SESSION['facebook_access_token']
        } else {
            // replace your website URL same as added in the developers.facebook.com/apps e.g. if you used http instead of https and you used non-www version or www version of your website then you must add the same here
            $loginUrl = $helper->getLoginUrl('http://sohaibilyas.com/APP_DIR/', $permissions);
            echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
        }
    }

    public function v2()
    {
        // initialize Facebook class using your own Facebook App credentials
// see: https://developers.facebook.com/docs/php/gettingstarted/#install
        $config = array();
        $config['app_id'] = '958640260934477';
        $config['app_secret'] = 'febe5cb8925df85df11e014cca598dfd';
//        $config['fileUpload'] = false; // optional
//        echo $appsecret_proof= hash_hmac('sha256', '958640260934477|CxMkqyNwp3n5baL81ThTNO5p6dQ"', $config['app_secret'] ); 
        $fb = new Facebook\Facebook($config);


// define your POST parameters (replace with your own values)
        $params = array(
            "access_token" => "EAANn4NQ4a00BANfI8gv1NoPwAoyS6WrPZCzHJ1QQIEI8u1PwUhRhYSgXSZCUIFDql75JZCSQQ5PXUXXuk86UVTVKZAaK0rWCTBFfAoPybvGKwwJx8KIqHZB4mw94FD1KfSYaiFe9MfjCG4vZAbZCWpV4dMsZAV1mwcw919W5lS3w1gZDZD", // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
//            "access_token" => "EAANn4NQ4a00BABiMvZCd43YsSqj0WNZCqzCCiqLjpsUaPFxPActW39nxTziLD4Rf3UMpMPr7G96PqZCG8DbJe1MW3MFcTl803xw8NTlZCcqnMExwSs06g4sRlsKFQQdfYnGxHiic5ZCggCOCkhku8ZCJivPsugZA1AZD", // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
//            "access_token" => "EAANn4NQ4a00BABx8IgcrTdXecb1fj8rqHFS7CdZAmZC8DmBs2dnd2xn3r32fhSnaZBqdWO27dGcwafU9SHjernZBSIK4pN4UD43HZBjcITo9IvXV70F9b4BS0800yKzWR7F2GRH3FH1JxsgEbmE8GAWtBuRY92Wt5FkGetZCf3JgZDZD", // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
//            "access_token" => "958640260934477|febe5cb8925df85df11e014cca598dfd", // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
//            "access_token" => "EAANn4NQ4a00BAL2WNz7FVVN0JSilcwvf9997sKLuzc5uqQZCZC44q3jqGKpRQV7wIeHrVrGlvWnNZCW6E4G5q42MWSCbcYZB4L88uPz0UeYVJSYB6YXYSLuWUEh6o1QkNueZCBZAHndEnyC0UlPTyMd95NgzPXh8cZD", // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
//            "access_token" => "EAANn4NQ4a00BAES9W3I8Wahpw9bntRYcj46c57FKqSuXNoy86SSMEN3MxH8bgkVj6dfjYLOc2Ii3ZBGggTe77HENB15o1gbfRgxFNCTGRk3womjQnydv3ZCcfiN2UJmUdf3nTe7L3lSlmu4vfgFd58b3GzCH4ZDEAANn4NQ4a00BAES9W3I8Wahpw9bntRYcj46c57FKqSuXNoy86SSMEN3MxH8bgkVj6dfjYLOc2Ii3ZBGggTe77HENB15o1gbfRgxFNCTGRk3womjQnydv3ZCcfiN2UJmUdf3nTe7L3lSlmu4vfgFd58b3GzCH4ZD", // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
//            "access_token" => "Invalid appsecret_proof provided in the API argument", // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
            "message" => "Is this real OMG ",
//            "link" => "ubuntu.com",
//            "picture" => "http://www.gloriana.com/sites/g/files/g2000004666/f/Sample-image10-highres.jpg",
            "name" => " my name",
            'source' => $fb->fileToUpload('application/controllers/subi.jpg'),
            "caption" => "hm this is strange phpoto",
            "description" => "text text text"
        );

// post to Facebook
// see: https://developers.facebook.com/docs/reference/php/facebook-api/

//        $fp=  file_exists('application/controllers/subi.jpg'); 
//        var_dump($fp);
//        exit();

        try {
//            $fb->fileToUpload('application/controllers/subi.jpg');
            $ret = $fb->post('/913724815428624/photos', $params);
//            $ret = $fb->post('/351661148537917/photos', $params);
//            $ret = $fb->post('/351661148537917/feed', $params);
            echo 'Successfully posted to Facebook';
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function get_token()
    {
        $scope = array('manage_pages, read_stream');
        $helper = new \Facebook\FacebookRedirectLoginHelper('https://www.mydomain.com/after_login.php');
        $loginUrl = $helper->getLoginUrl($scope);
        echo '<a href="' . $loginUrl . '">Login</a>';
    }

    public function login_facebook()
    {
        $fb = new Facebook\Facebook([
            'app_id' => '958640260934477',
            'app_secret' => 'febe5cb8925df85df11e014cca598dfd',
            'default_graph_version' => 'v2.8'
        ]);
//        $fb = new Facebook\Facebook([
//            'app_id' => '1848049545471939',
//            'app_secret' => '60a7c3fbf130abd33ada6479dfe48822',
//            'default_graph_version' => 'v2.8'
//        ]);

        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['manage_pages','publish_actions', 'publish_pages']; // optional
        $loginUrl = $helper->getLoginUrl('http://mangusta-ci.local/facebook_page/login_redirect', $permissions);

        echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
    }

    public function login_redirect() {
        if(!session_id()) {
            session_start();
        }
        $fb = new Facebook\Facebook([
            'app_id' => '958640260934477',
            'app_secret' => 'febe5cb8925df85df11e014cca598dfd',
            'default_graph_version' => 'v2.8'
        ]);

        $helper = $fb->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (isset($accessToken)) {
            // Logged in!
//            $_SESSION['facebook_access_token'] = (string) $accessToken;
            $face_token = (string) $accessToken;;
            var_dump($face_token);
            // Now you can redirect to another page and use the
            // access token from $_SESSION['facebook_access_token']
        }

    }


}

//351661148537917

//1623934163