<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends Frontend_Controller {

    public function __construct() {
        parent::__construct();
        if (!$maintenance_mode = $this->config->item('maintenance_mode')) {
            $this->load->helper('url');
            redirect('admin/');
        }
    }

    public function index() {
        $this->render('maintenance', array());
    }

}
