<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album extends Admin_Controller
{

  protected
    $load_models = array('album_model', 'facebook_model', 'product_model', 'images_model'),
    $load_helpers = array('form', 'url'),
    $load_libraries = array('facebooklib', 'facebookspider');

  public function __construct() {
      parent::__construct();
  }

  public function pageAlbums($pageId) {
    if (!$this->input->is_ajax_request())
    {
      $this->render('pageAlbums', array(
			  'title' => 'Albums',
        'pageId' => $pageId,
        'albums' => $this->album_model->getPageAlbums($pageId)
		  ));
    }
    else
    {
      header('Content-Type: application/json');
      echo json_encode($this->album_model->getPageAlbums($pageId));
    }
  }

  public function create($pageId)
  {
		if ($this->isPost()){
      $object = $this->input->post();
      $page = $this->facebook_model->findPageById($pageId);
      $createdAlbum = $this->facebooklib->createAlbum($page, $object['title'], $object['subtitle']);
      $createdAlbumId = $createdAlbum['id'];
      $object['fb_id'] = $createdAlbumId;
      $object['fb_url'] = 'https://business.facebook.com/'.$createdAlbumId.'/photos';
      if ($this->album_model->insertRow($object)){
        if (!$this->input->is_ajax_request())
        {
				  redirect('admin/album/page/'.$pageId);
        }
        else
        {
          header('Content-Type: application/json');
          echo json_encode(['status' => true, 'message' => 'Album created successfully']);
          return;
        }
			}
		}
    $page = $this->facebook_model->findPageById($pageId);
    $this->render('form', array(
      'title' => 'Create Album',
      'page' => $page
    ));
  }

  public function edit($albumId)
  {
    if (! $object = $this->album_model->findAlbumById($albumId)){
      if (!$this->input->is_ajax_request())
      {
			     show_404();
      }
      else
      {
        header('Content-Type: application/json');
        echo json_encode(['status' => false, 'message' => 'Album doesnot exist']);
        return;
      }
		}
    if ($this->isPost())
    {
			$object = array_merge($object, $this->input->post());
			if ($this->album_model->updateRow($albumId, $object)){
        if (!$this->input->is_ajax_request())
        {
				  redirect('admin/album/page/'.$object['fb_page_id']);
        }
        else
        {
          header('Content-Type: application/json');
          echo json_encode(['status' => true, 'message' => 'Album edited successfully']);
          return;
        }
			}
		}

    $album = $this->album_model->findAlbumById($albumId);
    $page = $this->facebook_model->findPageById($album['fb_page_id']);
    $this->render('form', array(
      'title' => 'Edit Album',
      'page' => $page,
      'album' => $album
    ));
  }

  public function delete($albumId)
  {
    $object = $this->album_model->findAlbumById($albumId);
    if($this->album_model->deleteRow($albumId))
    {
      if (!$this->input->is_ajax_request())
      {
        redirect('admin/album/page/'.$object['fb_page_id']);
      }
      else
      {
        header('Content-Type: application/json');
        echo json_encode(['status' => true, 'message' => 'Album deleted successfully']);
        return;
      }
    }
  }

  public function refreshPhotos($albumId)
  {
    $album = $this->album_model->findAlbumById($albumId);
    $page = $this->facebook_model->findPageById($album['fb_page_id']);
    $photos = $this->facebooklib->getPhotos($album, $page);
    $series = $this->product_model->getProductCodeSeries();
		$photos = $this->facebooklib->addProductCodes($photos, $series);
    $uploadsDir = __DIR__.'/../../../uploads/';
    $i = 0;
    foreach($photos as $photo)
    {
      $fbPhotoId = $photo['id'];
      $facebookImages = $this->images_model->searchFacebookPhotosByAll(['fb_id' => $fbPhotoId]);
      if(count($facebookImages) == 0)
      {
        $product = null;
        if(!empty($photo['product_code']))
        {
          $product = $this->product_model->findByCode($photo['product_code']);
          if(!is_dir($uploadsDir.$product['code']))
          {
            mkdir($uploadsDir.$product['code']);
          }
          $this->facebooklib->copyImageToLocal($photo['src'], $uploadsDir.$product['code'].'/');
          $photoUrl = $this->config->item('base_url').'/uploads/'.$product['code'].'/'.$this->facebooklib->getFileName($photo['src']);
        }
        else
        {
          if(!is_dir($uploadsDir.'unspecified'))
          {
            mkdir($uploadsDir.'unspecified');
          }
          $this->facebooklib->copyImageToLocal($photo['src'], $uploadsDir.'unspecified'.'/');
          $photoUrl = $this->config->item('base_url').'/uploads/unspecified/'.$this->facebooklib->getFileName($photo['src']);
        }
        $imageData = [
          'date_added' => date('Y-m-d H:i:s'),
          'url' => $photoUrl,
          'file_name' => $this->facebooklib->getFileName($photo['src']),
          'file_ext' => pathinfo($photo['src'], PATHINFO_EXTENSION)
        ];
        if(!empty($product))
        {
          $imageData['code'] = $product['code'];
        }
        $imageId = $this->images_model->insertImageRow($imageData);
        $facebookImageData = [
          'product_id' => $product['id'],
          'image_id' => $imageId,
          'fb_id' => $photo['id'],
          'fb_album_id' => $albumId,
          'photo_order' => ++$i
        ];
        if(!empty($photo['name']))
        {
          $facebookImageData['additional_notes'] = $photo['name'];
        }
        if(!empty($product))
        {
          $facebookImageData['product_id'] = $product['id'];
        }
        $facebookImageId = $this->images_model->insertFacebookPhotoRow($facebookImageData);
      }
      else
      {
        $facebookImage = $facebookImages[0];
        $facebookImageId = $facebookImage['id'];
        $facebookImageData = [
          'photo_order' => ++$i
        ];
        $this->images_model->updateFacebookPhotoRow($facebookImageId, $facebookImageData);
      }
    }
  }
}
