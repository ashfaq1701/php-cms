<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlbumCategory extends Admin_Controller{

  protected
    $load_models = array('albumcategory_model'),
    $load_helpers = array('form', 'url');

  public function __construct() {
      parent::__construct();
  }

  public function albumCategories()
  {
    header('Content-Type: application/json');
    echo json_encode($this->albumcategory_model->getAlbumcategories());
  }
}
