<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends Admin_Controller {

	protected
		$load_models = array('country_model'),
		$load_helpers = array('form'),
		$load_libraries = array('form_validation'),
		$has_filter = true;

	public function index() {
		if (!$this->input->is_ajax_request())
    {
			$this->render('index', array(
				'title' => 'List countries',
				'countries' => $this->country_model->getCollection()
			));
		}
		else
		{
			header('Content-Type: application/json');
			echo json_encode($this->country_model->getCollection());
		}
	}

	public function create() {
		$object = $this->input->post();

		if ($this->isPost() && $this->form_validation->run('country')) {
			if ($this->country_model->insertRow($object)) {
				redirect('admin/countries');
			}
		}

		//MISSING ADDING NEW ROWS FOR PRODUCTS!!!
		$this->render('form', array(
			'title' => 'Create country',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}

	public function edit($id) {
		if (! $object = $this->country_model->findById($id)) {
			show_404();
		}

		if ($this->isPost() && $this->form_validation->run('country')) {
			$object = array_merge($object, $this->input->post());

			if ($this->country_model->updateRow($id, $object)) {
				redirect('admin/countries');
			}
		}
		else {
			$object = array_merge($object, $this->input->post());
		}

		$this->render('form', array(
			'title' => 'Edit country',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}

	public function delete($id) {

	}
}
