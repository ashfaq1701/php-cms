<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook extends Admin_Controller{

	protected
		$load_models = array('facebook_model', 'user_model', 'country_model', 'images_model', 'order_model', 'product_model', 'images_model', 'album_model', 'albumcategory_model', 'commonoption_model'),
		$load_helpers = array('form', 'url'),
		$load_libraries = array('form_validation', 'image', 'money', 'pagination', 'facebooklib', 'facebookspider');

    public function __construct(){
        parent::__construct();
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;
		if($user_confirmed == 0 || $user_confirmed == null){
			redirect('admin/schedule');
		}
    }

	//TO DO insert the whole FBC controller
	//inserted so far - populate pages
	//Stefan

    /**
	 * by Chavdar, updated by Stefan
     * Lists Facebook pages (manually added)
     * The whole controller needs to be a lot more automated
     */

	public function get($pageId) {
		header('Content-Type: application/json');
		echo json_encode($this->facebook_model->findPageById($pageId));
	}

	public function index(){
		$pages = $this->facebook_model->getCollection();
		if (!$this->input->is_ajax_request())
    {
			$this->render('index', array(
				'title' => 'Facebook pages',
				'facebook_pages' => $pages
			));
		}
		else
		{
			header('Content-Type: application/json');
			echo json_encode($pages);
		}
	}

	public function pageList()
	{
		$pages = $this->facebook_model->getCollection();
		header('Content-Type: application/json');
		echo json_encode($pages);
	}

	public function getBatchNos() {
		$page = $_GET['page'];
		$album = $_GET['album'];
		$batchNos = $this->facebook_model->getBatchNos($page, $album);
		header('Content-Type: application/json');
		echo json_encode($batchNos);
	}

	public function getGroupedCollection()
	{
		$pages = $this->facebook_model->getGroupedCollection();
		header('Content-Type: application/json');
		echo json_encode($pages);
	}

	public function getFacebookLogin()
	{
		$loginUrl = $this->facebooklib->authUrl();
		$this->render('facebook_auth', array(
			'title' => 'Authorize facebook accounts',
			'loginUrl' => $loginUrl
		));
	}

	public function facebookRedirect()
	{
		$this->config->load('facebook');
		$accessToken = $this->facebooklib->getAccessToken();
		$llToken = $this->facebooklib->getLongLivedToken($accessToken);
		$pageTokens = $this->facebooklib->pageAccessToken($llToken);
		$this->facebook_model->addTokenToPages($this->config->item('fb_app_id'), $this->config->item('fb_app_secret'), $pageTokens);
		redirect('/admin/facebook/auth');
	}

	public function refreshAlbums($pageId)
	{
		$page = $this->facebook_model->findPageById($pageId);
		$albums = $this->facebooklib->getAlbums($page);
		$this->album_model->syncAlbums($albums, $page);
	}

	public function facebookEntityTest()
	{
		/*$fbPhotos = ["916702495153277","916702555153271","916702811819912","916702488486611","916702768486583","916702785153248","916702508486609","916702541819939","916702231819970","916702228486637","916702258486634","916702571819936","916702275153299","916702235153303","916702675153259","916702655153261","916702281819965","916702311819962","916702318486628","916702351819958","916702345153292","916702711819922","916702361819957","916702748486585","916702411819952","916702418486618","916702431819950","916702685153258"];
		$fbCookies = $this->commonoption_model->getFacebookCookies();
		$this->facebookspider->setCookies(json_decode($fbCookies, true));
		$this->facebookspider->updateBusinessPagePhotoOrderings('627363430753853', '916702218486638', $fbPhotos);*/
		$page = [
			'fb_id' => '627363430753853',
			'app_id' => '1848049545471939',
			'secret_id' => '60a7c3fbf130abd33ada6479dfe48822',
			'access_token' => 'EAAaQypCvZA8MBAFoAKQJ9JB317JWicjTZAc8IMxfF1ZAai7yYAxkDhgQfWCbqzEtKdnkjKgnlYul81kNBfgWWQocIIjBaQShmskc4ahZA8ZAnVCDy581edAVuVNJbCkySlNW0bpsSCDkoXSnGkZCQOKpu4lpMhzmBa2FsYlsBMewZDZD'
		];
		$info = $this->facebooklib->getPageInformation($page);
		print_r($info);
	}

	public function searchPhotos()
	{
		$page = $_GET['page'];
		$album = $_GET['album'];
		$batch = $_GET['batch'];
		$start = $_GET['start'];
		$limit = $_GET['length'];
		$offset = (empty($offset) ? 0 : intval($offset));
    $limit = (empty($limit) ? 0 : intval($limit));
		$res = $this->facebook_model->getAlbumPhotos($page, $album, $batch, $start, $limit);
		header('Content-Type: application/json');
		echo json_encode(array(
			'draw' => $_GET['draw'],
      'recordsTotal' => $res['count'],
      'recordsFiltered' => $res['count'],
      'data' => $res['results']
		));
	}

	public function deletePhoto($photoId) {
		$facebookPhotos = $this->images_model->searchFacebookPhotosByAll(array(
			'id' => $photoId
		));
		$facebookPhoto = $facebookPhotos[0];
		$albumId = $facebookPhoto['fb_album_id'];
		$album = $this->album_model->findAlbumById($albumId);
		$page = $this->facebook_model->findById($album['fb_page_id']);
		$this->facebooklib->deletePhoto($facebookPhoto, $page);
		$this->images_model->deleteFacebookPhoto($facebookPhoto['id']);
	}

	public function managePhotos()
	{
		$this->render('manage_photos', array(
			'title' => 'Manage Photos'
		));
	}

	public function getUploadPhotosIntoAlbums()
	{
		$this->render('album_photo_upload', array(
			'title' => 'Upload Photos'
		));
	}

	public function postUploadPhotosIntoAlbums() {
		$params = json_decode(file_get_contents('php://input'),true);
		$files = $params['files'];
		$pages = $params['pages'];
		$albumCategories = $params['albumCategories'];
		$description = $params['description'];
		$isBranded = boolval($params['isBranded']);
		$overlayProductCode = boolval($params['overlayProductCode']);
		$brandId = intval($params['brandId']);
		$brand = null;
		if (!empty($brandId)) {
			$brand = $this->images_model->findBrandByIdAsArr($brandId);
		}
		$batchNo = $this->facebook_model->getBatchNo();
		foreach($files as $file)
		{
			$file = str_replace(' ', '', $file);
			$parts = explode('_', $file);
			$code = $parts[0];
			$product = $this->product_model->findByCode(trim($code));
			$productId = $product['id'];
			$filePath = __DIR__.'/../../../uploads/'.$code.'/'.$file;
			if (!empty($isBranded)) {
				$this->image->overlaySpecificImageWithWatermark($filePath, $brand);
			}
			if (!empty($overlayProductCode)) {
				$this->image->overlaySpecificImageTextWatermark($filePath, $code);
			}
			$fileUrl = $this->config->item('base_url').'/uploads/'.$code.'/'.$file;
			$fileName = $this->facebooklib->getFileName($fileUrl);
			$fileExt = pathinfo($fileUrl, PATHINFO_EXTENSION);

			$sizes = '';
			$priceEur = '';

			$filenameMeta = $this->facebook_model->testImageFile(strtolower($file));
			if(!empty($filenameMeta['sizes']))
			{
				$sizes = $filenameMeta['sizes'];
			}
			if(!empty($filenameMeta['price_eur']))
			{
				$priceEur = $filenameMeta['price_eur'];
			}

			$photoObj = [
				'file_name' => $fileName,
        'file_ext' => $fileExt
			];
			if(!empty($code))
      {
        $photoObj['code'] = $code;
      }
			$images = $this->images_model->searchByAll($photoObj);
			if(count($images) > 0)
			{
				$imageId = $images[0]['id'];
			}
			else
			{
				$photoObj['date_added'] = date('Y-m-d H:i:s');
        $photoObj['url'] = $fileUrl;
				if (!empty($brandId)) {
					$photoObj['brand'] = $brandId;
				}
				$imageId = $this->images_model->insertImageRow($photoObj);
			}
			$fileObj = [
				'product_id' => $productId,
				'image_id' => $imageId
			];
			foreach($pages as $page)
			{
				$pageObj = $this->facebook_model->findPageById($page);
				$pageAlbumObj = array('page' => $pageObj);
				$descStr = '';
				if(empty($pageObj['country']))
				{
					$descStr = $this->facebook_model->parseTestsDescription($description, $sizes, $priceEur, 3, $code);
				}
				else
				{
					$descStr = $this->facebook_model->parseTestsDescription($description, $sizes, $priceEur, $pageObj['country'], $code);
				}
				foreach($albumCategories as $albumCategory)
				{
					$albums = $this->album_model->findAlbumsByPageAndCategory($pageObj['id'], $albumCategory);
					foreach($albums as $album)
					{
						$fbImageObj = $fileObj;
						$fbImageObj['description_format'] = $description;
						$fbImageObj['additional_notes'] = $descStr;
						$fbImageObj['fb_album_id'] = $album['id'];
						$fbImageObj['uploaded_by'] = session('admin_user')['id'];
						$fbImageObj['uploaded_at'] = date('Y-m-d H:i:s');
						$fbImageObj['batch_no'] = $batchNo;
						$this->images_model->insertFacebookPhotoRow($fbImageObj);
					}
				}
			}
		}
		echo json_encode(['status' => true]);
	}

	public function imageUpload()
	{
		$filename = $_FILES['photo']['name'];
		$filename = str_replace(' ', '', $filename);
		$parts = explode('_', $filename);
		$code = $parts[0];
		$uploadDir = __DIR__.'/../../../uploads/'.$code.'/';
		if(!is_dir($uploadDir))
		{
			mkdir($uploadDir);
		}
		$targetFile = $uploadDir.$filename;
		$targetUrl = $this->config->item('base_url').'/uploads/'.$code.'/'.$filename;
		if(file_exists($targetFile))
		{
			unlink($targetFile);
		}
		move_uploaded_file($_FILES["photo"]["tmp_name"], $targetFile);
		header('Content-Type: application/json');
		echo json_encode(['url'=> $targetUrl]);
	}

	public function facebookAuthInfo()
	{
		if ($this->isPost())
    {
			$email = $_POST['fb-email'];
			$password = $_POST['fb-password'];
			$this->commonoption_model->setFacebookEmail($email);
			$this->commonoption_model->setFacebookPassword($password);
			$this->facebookspider->setEmail($email);
			$this->facebookspider->setPassword($password);
			$cookies = $this->facebookspider->login();
			$this->commonoption_model->setFacebookCookies(json_encode($cookies));
		}
		$this->render('store_facebook_auth', array(
			'title' => 'Facebook Auth',
			'email' => $this->commonoption_model->getFacebookEmail()
		));
	}

	public function profiles(){
		$sql = "SELECT replace(facebook_customer_profile_url, 'https://business', 'https://www') as fb_profile
					FROM orders
					where country_id = 3
				";
		$profiles = $this->db->query($sql)->result_array();
		$this->render('profiles', array(
			'title' => 'Customers',
			'profiles' => $profiles
		));
	}

	public function change_album_id(){

		$page_id = $_GET['page'];
		$album_id = $_GET['album'];

		$sql = "UPDATE facebook_pages
				SET album_id = '$album_id'
				WHERE id = '$page_id' ;
		";
		$this->db->query($sql);

		echo($page_id);
		echo($album_id);

		echo("TEST");

	}

	/*
	 * by Stefan
	 * gets user profile pics from facebook for schedule
	 */
	public function getUsersProfilePic(){
		//NOT WORKING
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		$app_id = '1848049545471939';
		$app_secret = '60a7c3fbf130abd33ada6479dfe48822';
		$mytoken = 'EAAaQypCvZA8MBAEM3r21VQZAEEyKG3B5gNPRJKemqbuzKXnaFHlxnOdckQ0sdHGYjaHV6991QUyjMCJfq9io0ZBnYUeD3mcQnqFcNtFtFEPkx8qdqoZC9vY9oWzrhxo2ubUut8DRhZA8WVlsbqddJfV73ibPTvhiCSRZCYKLS3I8nPOysdXkyQUG7eUkSehBHzf7t34EIA7LZCJGVXaoYfnmbDMjtjWbSEZD';

		$users = $this->user_model->getAllUsersFBIDs();
		foreach($users as $user){
			$fb = new Facebook\Facebook(array('app_id'=>$app_id, 'app_secret'=>$app_secret, 'default_graph_version'=>'v2.9'));
			$fbApp = $fb->getApp();
			$request = new Facebook\FacebookRequest($fbApp, $mytoken, 'GET', '/'.$user['fb_id'].'/picture?fields=redirect');
			try{
				$response = $fb->getClient()->sendRequest($request);
			}catch(Facebook\Exceptions\FacebookResponseException $e){
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			$user_pic = $response->getGraphNode();
			echo "<pre>";
			var_dump($response->getBody());
			echo "</pre>";
		}
		/*$ch = curl_init('http://example.com/image.php');
		$fp = fopen('/my/folder/flower.gif', 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);*/
	}

	/*
	 * by Stefan
	 * List Facebook posts with tests
	 */
	public function listTest(){

		$current_page = $this->uri->segment(4);
		$this->pagination->initialize($this->facebook_model->getConfigForPagination());

		$current_page = ($current_page == null OR $current_page < 1) ? 1 : $current_page;

		$tests = $this->facebook_model->getTestPosts($current_page, false);

		foreach($tests as &$test){
			$test['impressions_initial'] = 0;
			$test['reactions_initial'] = 0;
			$test['comments_initial'] = 0;
			$test['impressions_deleted'] = 0;
			$test['reactions_deleted'] = 0;
			$test['comments_deleted'] = 0;
			$test['impressions_1'] = 0;
			$test['reactions_1'] = 0;
			$test['comments_1'] = 0;
			$test['impressions_2'] = 0;
			$test['reactions_2'] = 0;
			$test['comments_2'] = 0;
			$test['impressions_3'] = 0;
			$test['reactions_3'] = 0;
			$test['comments_3'] = 0;
			$test_id = $test['test_id'];
			$test['status_pending'] = 0;
			$test['status_up'] = 0;
			$test['status_deleted'] = 0;
			$sql2 = "select *,
						facebook_tests_posts.id as post_id
						from facebook_tests_posts
						left join facebook_pages on facebook_pages.id = facebook_tests_posts.page_id
						where facebook_tests_posts.test_id = '$test_id'
					";
			$test['posts'] = $this->db->query($sql2)->result_array();
			foreach($test['posts'] as &$post){
				switch($post['status']){
					case '0':{
						$test['status_pending'] ++;
						break;
					}
					case '1':{
						$test['status_up'] ++;
						break;
					}
					case '3':{
						$test['status_deleted'] ++;
						break;
					}
					default:
						break;
				}

				$test['impressions_initial'] += $post['impressions_initial'];
				$test['reactions_initial'] += $post['reactions_initial'];
				$test['comments_initial'] += $post['comments_initial'];
				$test['impressions_deleted'] += $post['impressions_deleted'];
				$test['reactions_deleted'] += $post['reactions_deleted'];
				$test['comments_deleted'] += $post['comments_deleted'];
				$test['impressions_1'] += $post['impressions_1'];
				$test['reactions_1'] += $post['reactions_1'];
				$test['comments_1'] += $post['comments_1'];
				$test['impressions_2'] += $post['impressions_2'];
				$test['reactions_2'] += $post['reactions_2'];
				$test['comments_2'] += $post['comments_2'];
				$test['impressions_3'] += $post['impressions_3'];
				$test['reactions_3'] += $post['reactions_3'];
				$test['comments_3'] += $post['comments_3'];

				$post['coeff_initial'] = ($post['impressions_initial'] == 0) ? 0 : ($post['reactions_initial'] / $post['impressions_initial']);
				$post['coeff_deleted'] = ($post['impressions_deleted'] == 0) ? 0 : ($post['reactions_deleted'] / $post['impressions_deleted']);
				$post['coeff_1'] = ($post['impressions_1'] == 0) ? 0 : ($post['reactions_1'] / $post['impressions_1']);
				$post['coeff_2'] = ($post['impressions_2'] == 0) ? 0 : ($post['reactions_2'] / $post['impressions_2']);
				$post['coeff_3'] = ($post['impressions_3'] == 0) ? 0 : ($post['reactions_3'] / $post['impressions_3']);

				$post_id = $post['post_id'];
				$sql2 = "select *
							from facebook_tests_images
							where post_id = '$post_id'";
				$post['images'] = $this->db->query($sql2)->result_array();
			}

			$test['coeff_initial'] = ($test['impressions_initial'] == 0) ? 0 : ($test['reactions_initial'] / $test['impressions_initial']);
			$test['coeff_deleted'] = ($test['impressions_deleted'] == 0) ? 0 : ($test['reactions_deleted'] / $test['impressions_deleted']);
			$test['coeff_1'] = ($test['impressions_1'] == 0) ? 0 : ($test['reactions_1'] / $test['impressions_1']);
			$test['coeff_2'] = ($test['impressions_2'] == 0) ? 0 : ($test['reactions_2'] / $test['impressions_2']);
			$test['coeff_3'] = ($test['impressions_3'] == 0) ? 0 : ($test['reactions_3'] / $test['impressions_3']);
		}

		$this->render('facebook_test_index', array(
			'title' => 'Test Posts List',
			'pagination' => $this->pagination->create_links(),
			'tests' => $tests
		));
	}

	/*
	 * by Stefan
	 * Creates Facebook posts with tests
	 */
	public function createTest(){
		if($this->input->post()){
			$data = $this->input->post();
			$files = $_FILES;
			$images_count = count($_FILES['userfile']['name']);

			$sql = "SELECT test_id
						FROM facebook_tests_posts
						ORDER BY test_id DESC
						LIMIT 1
					";
			$test_id_array = $this->db->query($sql)->result_array();
			if(!empty($test_id_array))
				$test_id = $test_id_array[0]['test_id'] + 1;
			else
				$test_id = 1;

			foreach($data['pages'] as $page){
				//insert post
				$post['page_id'] = $page;
				$post['test_id'] = $test_id;
				$post['description'] = $data['description'];
				$post['date_schedule'] = date("Y-m-d H:i:s");
				$fb_page = $this->facebook_model->findById($page);
				$post_id = $this->facebook_model->insertTestRow($post);
				//insert images rows
				if($images_count > 0 && strlen($_FILES['userfile']['name'][0])){
					for($i = 0; $i < $images_count; $i++){
						//analyze filename
						$image = $this->facebook_model->testImageFile(strtolower($files['userfile']['name'][$i]));
						if(isset($image['error'])){
							echo "error: ";
							var_dump($image['error']);
							return;
						}else{
							$filename = rtrim($files['userfile']['name'][$i], '.'.pathinfo($files['userfile']['name'][$i], PATHINFO_EXTENSION));
							$_FILES['userfile']['name'] = strtolower($files['userfile']['name'][$i]);
							$_FILES['userfile']['type'] = $files['userfile']['type'][$i];
							$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
							$_FILES['userfile']['error'] = $files['userfile']['error'][$i];
							$_FILES['userfile']['size'] = $files['userfile']['size'][$i];
							if(is_uploaded_file($_FILES['userfile']['tmp_name'])){
								$upload_response = $this->image->uploadTestImage(); //?!
								if(!isset($upload_response['error'])){
									$ins['post_id'] = $post_id;
									$ins['file_name'] = rtrim($upload_response['upload_data']['file_name'], $upload_response['upload_data']['file_ext']);
									$ins['file_ext'] = $upload_response['upload_data']['file_ext'];
									if(isset($image['code'])){
										$ins['code'] = $image['code'];
									}elseif(isset($data['code'])){
										$ins['code'] = $data['code'];
									}
									if(isset($image['supplier']))
										$ins['supplier'] = $image['supplier'];
									if(isset($image['price_eur']))
										$ins['price_eur'] = $image['price_eur'];
									if(isset($image['sizes']))
										$ins['sizes'] = $image['sizes'];
									//copy file to filename_source.fileext
									copy('./uploads/tests/'.$ins['file_name'].$ins['file_ext'], './uploads/tests/'.$ins['file_name'].'_source'.$ins['file_ext']);
									$this->facebook_model->insertTestImageRow($ins);
								}else{
									echo "<pre>";
									var_dump($upload_response);
									echo "</pre>";
								}
							}
							$images[] = $image;
						}
					}
				}
				if(isset($images[0]['price_eur']))
					$price_eur = $images[0]['price_eur'];
				$sizes = '';
				if(isset($images[0]['sizes']))
					$sizes = $images[0]['sizes'];
				$description = $this->facebook_model->parseTestsDescription($data['description'], $sizes, $price_eur, $fb_page['country']);
				$sql = "update facebook_tests_posts
							set description = '$description', status = '0'
							where id = '$post_id'
						";
				$this->db->query($sql);
			}
			redirect('admin/facebook/listtest');
		}

		//don't you dare to laugh, it works!!!
		//move to one function in model
		$sql = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
					order by title
				";
		$pages = $this->db->query($sql)->result_array();

		$sql_f = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and gender = 'F'
				";
		$female_pages = $this->db->query($sql_f)->result_array();

		$sql_m = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and gender = 'M'
				";
		$male_pages = $this->db->query($sql_m)->result_array();

		$sql_n = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and gender = 'N'
				";
		$neutral_pages = $this->db->query($sql_n)->result_array();

		$sql_ro = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and country = '2'
				";
		$romanian_pages = $this->db->query($sql_ro)->result_array();

		$sql_gr = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and country = '3'
				";
		$greek_pages = $this->db->query($sql_gr)->result_array();

		$sql_bg = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and country = '1'
				";
		$bulgarian_pages = $this->db->query($sql_bg)->result_array();

		$sql_other = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and country not in ('1', '2', '3')
				";
		$other_pages = $this->db->query($sql_other)->result_array();

		$sql_fbg = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and gender = 'F'
						and country = '1'
				";
		$fbg_pages = $this->db->query($sql_fbg)->result_array();

		$sql_fro = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and gender = 'F'
						and country = '2'
				";
		$fro_pages = $this->db->query($sql_fro)->result_array();

		$sql_fgr = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and gender = 'F'
						and country = '3'
				";
		$fgr_pages = $this->db->query($sql_fgr)->result_array();

		$this->render('facebook_test_create', array(
			'title' => 'Facebook pages',
			'facebook_pages' => $pages,
			'pages_m' => $male_pages,
			'pages_n' => $neutral_pages,
			'pages_f' => $female_pages,
			'pages_ro' => $romanian_pages,
			'pages_gr' => $greek_pages,
			'pages_bg' => $bulgarian_pages,
			'pages_other' => $other_pages,
			'pages_fbg' => $fbg_pages,
			'pages_fro' => $fro_pages,
			'pages_fgr' => $fgr_pages,
			'series' => $this->product_model->getAllSeries()
		));
	}

	public function createProductFromTest($test_id){
		//get info from test
		$sql1 = "SELECT *
					from facebook_tests_posts
					left join facebook_tests_images as fbi on facebook_tests_posts.id = fbi.post_id
					where test_id = '$test_id'
				";
		$res1 = $this->db->query($sql1)->result_array();
		echo "<pre>";
		var_dump($res1);
		echo "</pre>";
	}

	public function deleteTestPostManually($post_id){
		$sql1 = "select *
					from facebook_tests_posts
					left join facebook_pages on facebook_pages.id = facebook_tests_posts.page_id
					where facebook_tests_posts.id = '$post_id'
					limit 1
				";
		$post_array = $this->db->query($sql1)->result_array();

		if(isset($post_array[0])){
			$post = $post_array[0];

			$config = array();
			$config['app_id'] = $post['app_id'];
			$config['app_secret'] = $post['secret_id'];
			$config['access_token'] = $post['access_token'];
			$config['default_graph_version'] = 'v2.9';
			$fb = new Facebook\Facebook($config);
			$fbApp = $fb->getApp();

			$params[0]['method'] = "GET";
			$params[0]['relative_url']  = $post['fb_post_id'].'/insights/post_impressions';
			$params[1]['method'] = "GET";
			$params[1]['relative_url']  = $post['fb_post_id'].'/insights/post_reactions_by_type_total';
			$params[2]['method'] = "GET";
			$params[2]['relative_url']  = $post['fb_post_id'].'/comments?limit=50';

			$request = new Facebook\FacebookRequest($fbApp, $post['access_token'], 'POST', '?batch='.urlencode(json_encode($params)));
			try{
				$response = $fb->getClient()->sendRequest($request);
			}catch(Facebook\Exceptions\FacebookResponseException $e){
				echo 'Graph returned an error: '.$e->getMessage();
				exit;
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				echo 'Facebook SDK returned an error: '.$e->getMessage();
				exit;
			}
			$fb_data = $response->getBody();
			$fb_data_array = json_decode($fb_data);

			$impressions = json_decode($fb_data_array[0]->body)->data[0]->values[0]->value;

			$reactions = json_decode($fb_data_array[1]->body)->data[0]->values[0]->value;
			$total_reactions = 0;
			foreach($reactions as $react){
				$total_reactions += $react;
			}

			$comments = json_decode($fb_data_array[2]->body)->data;
			$total_comments = count($comments);

			$sql2 = "select *
						from facebook_tests_images
						where post_id = '$post_id'
					";

			$post['images'] = $this->db->query($sql2)->result_array();

			$i = 0;
			foreach($post['images'] as $image){
				$params[$i]['method'] = "DELETE";
				$params[$i]['relative_url']  = '/'.$image['fb_image_id'];
				$i++;
			}

			$request = new Facebook\FacebookRequest($fbApp, $post['access_token'], 'POST', '?batch='.urlencode(json_encode($params)));
			try{
				$response = $fb->getClient()->sendRequest($request);
			}catch(Facebook\Exceptions\FacebookResponseException $e){
				echo 'Graph returned an error: '.$e->getMessage();
				exit;
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				echo 'Facebook SDK returned an error: '.$e->getMessage();
				exit;
			}
			$fb_data = $response->getBody();

			$sql3 = "update facebook_tests_posts
						set impressions_deleted = '$impressions',
							reactions_deleted = '$total_reactions',
							comments_deleted = '$total_comments',
							status = 3,
							date_deleted = NOW()
						where id = '$post_id'
					";
			$this->db->query($sql3);
		}
		redirect('admin/facebook/listtest');
	}

	public function deleteTestManually($test_id){
		$sql1 = "select *
					from facebook_tests_posts
					where test_id = '$test_id'
				";
		$posts = $this->db->query($sql1)->result_array();

		foreach($posts as $post){
			$post_id = $post['id'];

			$sql2 = "select *
						from facebook_tests_posts
						left join facebook_pages on facebook_pages.id = facebook_tests_posts.page_id
						where facebook_tests_posts.id = '$post_id'
						limit 1
						";
			$post = $this->db->query($sql2)->row_array();
			if(isset($post['fb_post_id'])){
				$config = array();
				$config['app_id'] = $post['app_id'];
				$config['app_secret'] = $post['secret_id'];
				$config['access_token'] = $post['access_token'];
				$config['default_graph_version'] = 'v2.9';
				$fb = new Facebook\Facebook($config);
				$fbApp = $fb->getApp();

				$params[0]['method'] = "GET";
				$params[0]['relative_url']  = $post['fb_post_id'].'/insights/post_impressions';
				$params[1]['method'] = "GET";
				$params[1]['relative_url']  = $post['fb_post_id'].'/insights/post_reactions_by_type_total';
				$params[2]['method'] = "GET";
				$params[2]['relative_url']  = $post['fb_post_id'].'/comments?limit=50';

				$request = new Facebook\FacebookRequest($fbApp, $post['access_token'], 'POST', '?batch='.urlencode(json_encode($params)));
				try{
					$response = $fb->getClient()->sendRequest($request);
				}catch(Facebook\Exceptions\FacebookResponseException $e){
					echo 'Graph returned an error: '.$e->getMessage();
					exit;
				}catch(Facebook\Exceptions\FacebookSDKException $e){
					echo 'Facebook SDK returned an error: '.$e->getMessage();
					exit;
				}
				$fb_data = $response->getBody();
				$fb_data_array = json_decode($fb_data);

				$impressions = json_decode($fb_data_array[0]->body)->data[0]->values[0]->value;

				$reactions = json_decode($fb_data_array[1]->body)->data[0]->values[0]->value;
				$total_reactions = 0;
				foreach($reactions as $react){
					$total_reactions += $react;
				}

				$comments = json_decode($fb_data_array[2]->body)->data;
				$total_comments = count($comments);

				$sql2 = "select *
							from facebook_tests_images
							where post_id = '$post_id'
						";

				$post['images'] = $this->db->query($sql2)->result_array();

				$i = 0;
				foreach($post['images'] as $image){
					$params[$i]['method'] = "DELETE";
					$params[$i]['relative_url']  = '/'.$image['fb_image_id'];
					$i++;
				}

				$request = new Facebook\FacebookRequest($fbApp, $post['access_token'], 'POST', '?batch='.urlencode(json_encode($params)));
				try{
					$response = $fb->getClient()->sendRequest($request);
				}catch(Facebook\Exceptions\FacebookResponseException $e){
					echo 'Graph returned an error: '.$e->getMessage();
					exit;
				}catch(Facebook\Exceptions\FacebookSDKException $e){
					echo 'Facebook SDK returned an error: '.$e->getMessage();
					exit;
				}
				$fb_data = $response->getBody();

				$sql3 = "update facebook_tests_posts
							set impressions_deleted = '$impressions',
								reactions_deleted = '$total_reactions',
								comments_deleted = '$total_comments',
								status = 3,
								date_deleted = NOW()
							where id = '$post_id'
						";
				$this->db->query($sql3);
			}else{
				$sql4 = "update facebook_tests_posts
							set status = 3,
								date_deleted = NOW()
							where id = '$post_id'
						";
				$this->db->query($sql4);
			}
		}
		redirect('admin/facebook/listtest');
	}

	/*
	 * Test function
	 */
	public function ch24(){
		$this->facebook_model->changeProfilePic(24);
	}

	//will be moved to automated controller
	public function populateRating(){
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');

		$app_id = '1848049545471939';
		$app_secret = '60a7c3fbf130abd33ada6479dfe48822';

		$pages = $this->facebook_model->getAllPages();
		foreach($pages as $page){
			$page_id = '239682463042533';
			$fb = new Facebook\Facebook(array('app_id'=>$page['app_id'], 'app_secret'=>$page['secret_id'], 'default_graph_version'=>'v2.9'));
			$fbApp = $fb->getApp();
			$request = new Facebook\FacebookRequest($fbApp, $page['access_token'], 'GET', 'me?fields=rating_count,overall_star_rating');
			try{
				$response = $fb->getClient()->sendRequest($request);
			}catch(Facebook\Exceptions\FacebookResponseException $e){
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			$fb_page = $response->getGraphNode();
			$fb_rating = $fb_page['overall_star_rating'];
			$fb_rating_count = $fb_page['rating_count'];
			$page_id = $page['id'];
			$sql1 = "update facebook_pages
						set fb_rating = $fb_rating
						where id = '$page_id'";
			$this->db->query($sql1);
			$sql2 = "update facebook_pages
						set fb_rating_count = $fb_rating_count
						where id = '$page_id'";
			$this->db->query($sql2);
		}
		redirect('admin/facebook/pagesrating');
	}

	public function pagesRating(){
		$sql = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
						and fb_rating > 0
					order by fb_title";
		$res = $this->db->query($sql)->result_array();
        $this->render('pages_rating', array(
            'pages' => $res
			));
	}

    /**
	 * by Stefan
     * Populate all facebook pages
     * The whole controller needs to be a lot more automated
     */
	public function populatepages(){
		//downloads /me/accounts from FB and puts it in DB
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		//this data should be updated manually, for security measures!
		//facebook app id and secret
		$app_id = '1848049545471939';
		$app_secret = '60a7c3fbf130abd33ada6479dfe48822';
		//one of our pages id, it needs to be any
		//$page_id = '239682463042533'; //not required anymore
		//user access token (mangusto), this is never-expiring access token on Mr. Mangusto
		$mytoken = 'EAAaQypCvZA8MBAHtAqwzHBnukwtgxgv1fmZAM2rsMEBV6ULlk2qavLYtLjpYXJPJvZCJF8nnE8ZCJrNdKxHlLUDKbyCnkH2ZC4NfRVQEPZCtrg7LGAE57kBNkjveBYcMbFFVzYJw9hEQZAYiZB3NZCAsljYkO91ZAl8tMZD';
		//create FB controller
		$fb = new Facebook\Facebook(array('app_id'=>$app_id, 'app_secret'=>$app_secret, 'default_graph_version'=>'v2.8'));
		//initiate controllers app
		echo "Before creating App<br />"; //control text
		$fbApp = $fb->getApp();
		//what this function requests from facebook
		$request = new Facebook\FacebookRequest($fbApp, $mytoken, 'GET', '/me/accounts');
		echo "Facebook App was made<br />"; //control text
		//try to check if it will work out
		try{
			//gets the request we send to FB
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			//catches the error with FB response if any, it is usefull for debuging purpouses
			//in future can be put in log files
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			//catches the error with SDK if any, it is usefull for debuging purpouses
			//in future can be put in log files
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		echo "Request Successful<br />"; //control text
		//handle the result
		$myPages = $response->getGraphEdge();
		$maxPages = 500; //why on earth would you like to have that many pages
		$pagesCount = 0;
		$i = 0;
		$fb_pages = array();
		//facebook pagination
		do{
			foreach ($myPages as $page){
				$fb_pages[$i]['fb_id'] = $page->getField('id');
				$fb_pages[$i]['fb_title'] = $page->getField('name');
				$fb_pages[$i]['access_token'] = $page->getField('access_token');
				$fb_pages[$i]['app_id'] = "1848049545471939";
				$fb_pages[$i]['secret_id'] = "60a7c3fbf130abd33ada6479dfe48822";
				$fb_pages[$i]['url'] = "https://www.facebook.com/".$fb_pages[$i]['fb_id'];
				$i++;
			}
			$pagesCount++;
		}while($pagesCount < $maxPages && $myPages = $fb->next($myPages));
		for($j=0; $j<$i; $j++){
			//insert each page data into our DB
			$this->facebook_model->populatePageRow($fb_pages[$j]);
		}
	}

	/*
	 * by Stefan
	 * Gets unread messages by pages
	 */
	public function unreadmessages(){
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		$app_id = '1848049545471939';
		$app_secret = '60a7c3fbf130abd33ada6479dfe48822';
		$page_id = '239682463042533';
		$mytoken = 'EAAaQypCvZA8MBAFIsyaqGk4pJ4MhhNc5hSYFFCv1LYhtLQHdZBt3QZBhmcei0alkpZBhzc3lC4fEdDaQZC8MKlZAwIZAVuluF7KcmZA2ze79WEmAu4uB6A1WxzjV8PCczpqVOPnY48A5ESwEk4tO9HWxaX01QrLOOhMZD';
		$fb = new Facebook\Facebook(array('app_id'=>$app_id, 'app_secret'=>$app_secret, 'default_graph_version'=>'v2.8'));
		$fbApp = $fb->getApp();
		$request = new Facebook\FacebookRequest($fbApp, $mytoken, 'GET', '/me/accounts?fields=id,unseen_message_count,unread_message_count&limit=50');
		try{
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			echo 'Graph returned an error: '.$e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			echo 'Facebook SDK returned an error: '.$e->getMessage();
			exit;
		}
		$wtf = $response->getGraphEdge();
		$i=0;
		$fb_pages = array();
		foreach($wtf as $page){
			$fb_pages[$i]['id'] = $page['id'];
			$fb_pages[$i]['unread_message_count'] = $page['unread_message_count'];
			$fb_pages[$i]['unseen_message_count'] = $page['unseen_message_count'];
			$i++;
		}
		$sql1 = "select id from facebook_pages where active = '1' and fb_id is not null";
		$res1 = $this->db->query($sql1)->result();
		$pages = array();
		$i=0;
		foreach($res1 as $row){
			$pid = $row->id;
			$page = $this->facebook_model->findPageById($pid);
			foreach($fb_pages as $fb_page){
				if($fb_page['id'] == $page['fb_id']){
					$pages[$i]['fb_title'] = $page['fb_title'];
					$pages[$i]['fb_id'] = $page['fb_id'];
					$pages[$i]['url'] = $page['url'];
					$pages[$i]['unread_message_count'] = $fb_page['unread_message_count'];
					$pages[$i]['unseen_message_count'] = $fb_page['unseen_message_count'];
					$pages[$i]['album_id'] = $page['album_id'];
					$pages[$i]['id'] = $page['id'];
					$fb_page_id = $page['id'];
					$sql2 = "select *
								from fb_pages_users
								where page_id = '$fb_page_id'
									and role = 'Primary'
							";
					$res2 = $this->db->query($sql2)->row_array();
					$pages[$i]['primary'] = $this->facebook_model->getusernamebyid($res2['user_id']);
					$sql3 = "select *
								from fb_pages_users
								where page_id = '$fb_page_id'
									and role = 'Secondary'
							";
					$res3 = $this->db->query($sql3)->result();
					$pages[$i]['secondary'] = '';
					foreach($res3 as $rs3){
						$pages[$i]['secondary'] .= $this->facebook_model->getusernamebyid($rs3->user_id);
						$pages[$i]['secondary'] .= ', ';
					}
					$i++;
				}
			}
		}
		usort($pages,'sortArrayByUnseenMessageCount');
		$pages = array_reverse($pages);
        $this->render('unreadmessages_index', array(
            'pages' => $pages
			));
	}

	public function unreadMessagesNoAgents(){
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		$app_id = '1848049545471939';
		$app_secret = '60a7c3fbf130abd33ada6479dfe48822';
		$page_id = '239682463042533';
		$mytoken = 'EAAaQypCvZA8MBAHtAqwzHBnukwtgxgv1fmZAM2rsMEBV6ULlk2qavLYtLjpYXJPJvZCJF8nnE8ZCJrNdKxHlLUDKbyCnkH2ZC4NfRVQEPZCtrg7LGAE57kBNkjveBYcMbFFVzYJw9hEQZAYiZB3NZCAsljYkO91ZAl8tMZD';
		$fb = new Facebook\Facebook(array('app_id'=>$app_id, 'app_secret'=>$app_secret, 'default_graph_version'=>'v2.8'));
		$fbApp = $fb->getApp();
		//request all pages on Mr Mangusto with unread_messages_count
		$request = new Facebook\FacebookRequest($fbApp, $mytoken, 'GET', '/me/accounts?fields=id,unseen_message_count,unread_message_count&limit=50');
		try{
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			echo 'Graph returned an error: '.$e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			echo 'Facebook SDK returned an error: '.$e->getMessage();
			exit;
		}
		$fb_res = $response->getGraphEdge();
		$i=0;
		$fb_pages = array();
		foreach($fb_res as $page){
			$fb_pages[$i]['id'] = $page['id'];
			$fb_pages[$i]['unread_message_count'] = $page['unread_message_count'];
			$fb_pages[$i]['unseen_message_count'] = $page['unseen_message_count'];
			$i++;
		}

		$sql1 = "select id from facebook_pages where active = '1' and fb_id is not null";
		$res1 = $this->db->query($sql1)->result();

		$pages = array();
		$i=0;
		foreach($res1 as $row){
			$pid = $row->id;
			$page = $this->facebook_model->findPageById($pid);
			foreach($fb_pages as $fb_page){
				if($fb_page['id'] == $page['fb_id']){
					if($pid == 44 || $pid == 46 || $pid == 47 || $pid == 48 ||$pid == 50){
						$pages[$i]['fb_title'] = $page['fb_title'];
						$pages[$i]['fb_id'] = $page['fb_id'];
						$pages[$i]['url'] = $page['url'];
						$pages[$i]['unread_message_count'] = $fb_page['unread_message_count'];
						$pages[$i]['unseen_message_count'] = $fb_page['unseen_message_count'];
						switch($pid){
							case 44:{
								$pages[$i]['google_translate'] = 'https://translate.google.com/#hr/en/Dobar%20dan';
								$pages[$i]['language'] = 'Croatian';
								$pages[$i]['currency_link'] = 'https://www.google.bg/search?q=1+eur+in+kuna';
								break;
							}
							case 46:{
								$pages[$i]['google_translate'] = 'https://translate.google.com/#sl/en/Dober%20dan';
								$pages[$i]['language'] = 'Slovenian';
								$pages[$i]['currency_link'] = 'EUR';
								break;
							}
							case 47:{
								$pages[$i]['google_translate'] = 'https://translate.google.com/#sk/en/Dobr%C3%BD%20den';
								$pages[$i]['language'] = 'Slovakian';
								$pages[$i]['currency_link'] = 'EUR';
								break;
							}
							case 48:{
								$pages[$i]['google_translate'] = 'https://translate.google.com/#cs/en/Dobr%C3%BD%20den';
								$pages[$i]['language'] = 'Czech';
								$pages[$i]['currency_link'] = 'https://www.google.bg/search?q=1+eur+in+czk';
								break;
							}
							case 50:{
								$pages[$i]['google_translate'] = 'https://translate.google.com/#hu/en/J%C3%B3%20nap';
								$pages[$i]['language'] = 'Hungarian';
								$pages[$i]['currency_link'] = 'https://www.google.bg/search?q=1+eur+in+huf';
								break;
							}
						}
						$i++;
					}
				}
			}
		}
		usort($pages,'sortArrayByUnseenMessageCount');
		$pages = array_reverse($pages);
        $this->render('new_pages', array(
            'pages' => $pages
			));
	}

	/**
	 * by Stefan
	 * Updates Agent roles in DB for pages reference
	 */
	public function agentroles(){
		//query all active pages
		$sql1 = "select * from facebook_pages where active = '1' and fb_id is not null";
		$pages = $this->db->query($sql1)->result();
		//query all users who have FB ID - agents
		$sql1 = "SELECT id, fb_id, username, name FROM user WHERE fb_id IS NOT NULL";
		$db_agents = $this->db->query($sql1)->result();
		$agents = array();
		$i = 0;
		foreach($db_agents as $agent){
			$agents[$i]['id'] = $agent->id;
			$agents[$i]['fb_id'] = $agent->fb_id;
			$agents[$i]['name'] = $this->getAgentNamesFromFB($agent->fb_id);
			$i++;
		}
		//truncate fb_pages_users
		$sql1 = "truncate fb_pages_users";
		$this->db->query($sql1);
		$i = 0;
		//for each page check facebook roles
		echo "<pre>";
		foreach($pages as $fb_page){
			var_dump($fb_page);
			//create the app
			$fb = new Facebook\Facebook(array('app_id'=>$fb_page->app_id, 'app_secret'=>$fb_page->secret_id, 'default_graph_version'=>'v2.8'));
			//initiate controllers app
			$fbApp = $fb->getApp();
			$fbp = $fb_page->fb_id;
			//what this function requests from facebook
			$request = new Facebook\FacebookRequest($fbApp, $fb_page->access_token, 'GET', '/'.$fbp.'?fields=roles{id,name,role}');
			//try to check if it will work out
			try{
				//gets the request we send to FB
				$response = $fb->getClient()->sendRequest($request);
			}catch(Facebook\Exceptions\FacebookResponseException $e){
				//catches the error with FB response if any, it is usefull for debuging purpouses
				//in future can be put in log files
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				//catches the error with SDK if any, it is usefull for debuging purpouses
				//in future can be put in log files
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			//handle the result
			$wtf = $response->getGraphNode();
			$fb_pages[$i] = $wtf;
			$i++;
		}
		echo "</pre>";
		$data = array();
		//don't laugh, it works fast enough!
		foreach($fb_pages as $fb_page){
			//all pages
			foreach($fb_page as $fb_users){
				//all users in the page
				if(!is_string($fb_users)){
					foreach($fb_users as $fb_user){
						//each user individually
						foreach($agents as $agent){
							if($agent['name'] == $fb_user['name']){
								$data['user_id'] = $agent['id'];
								$page = $this->facebook_model->findPageByFBId($fb_page['id']);
								$pid = $page['id'];
								$data['page_id'] = $pid;
								switch($fb_user['role']){
									case 'Editor':
										$data['role'] = 'Primary';
										break;
									case 'Moderator':
										$data['role'] = 'Secondary';
										break;
									default:
										break;
								}
								//insert/update fb_pages_users
								$this->facebook_model->populateFBPagesUsersRow($data);
							}
						}
					}
				}
			}
		}
	}

	public function getAgentNamesFromFB($fb_id){
		$fb_page = $this->facebook_model->findPageByFBId('217940225331582');
		//create the app
		$fb = new Facebook\Facebook(array('app_id'=>$fb_page['app_id'], 'app_secret'=>$fb_page['secret_id'], 'default_graph_version'=>'v2.8'));
		//initiate controllers app
		$fbApp = $fb->getApp();
		$fbu = $fb_id;
		//what this function requests from facebook
		$request = new Facebook\FacebookRequest($fbApp, $fb_page['access_token'], 'GET', '/'.$fbu.'?fields=name');
		//try to check if it will work out
		try{
			//gets the request we send to FB
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			//catches the error with FB response if any, it is usefull for debuging purpouses
			//in future can be put in log files
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			//catches the error with SDK if any, it is usefull for debuging purpouses
			//in future can be put in log files
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		//handle the result
		$wtf = $response->getGraphNode();
		return $wtf['name'];
	}

	//it doesn't work!!!
	public function lastreadmessage($wtf){
		$conversations = array();
		$i=0;
		foreach($wtf as $convo){
			$conversations[$i] = $convo;
			$i++;
		}
		for($j=$i-1; $j>=0; $j--){
			if(isset($conversations[$j]->unread_count))
				break;
		}
		if($i == 0)
			$lar = "no messages yet :(";
		else{
			if(isset($conversations[$j+1])){
				$clar = $conversations[$j+1]->updated_time;
				$lar1 = new DateTime($clar);
				$lar1->add(new DateInterval('PT2H'));
				$lar = $lar1->format("F-d H:i");
			}else
				$lar = "Can't find it";
		}
		return $lar;
	}


    /**
	 * by Chavdar, updated by Stefan
     * Manually creates Facebook Page in our system
     * Needs to be changed to be automatically generate
     */
	public function create(){
		$object = $this->input->post();
		if ($this->isPost() && $this->form_validation->run('facebook'))
		{
			$pageInfo = $this->facebooklib->getPageInfo($object['url']);
			if(!empty($pageInfo))
			{
				$object['fb_title'] = $pageInfo['name'];
				$object['fb_id'] = $pageInfo['id'];
				$object['active'] = 1;
				$object['app_id'] = '';
				$object['secret_id'] = '';
				if ($this->facebook_model->insertRow($object))
				{
					redirect('admin/facebook');
				}
			}
			else
			{
					redirect('admin/facebook');
			}
		}
		$this->render('form', array(
			'title' => 'Create facebook page',
			'object' => $object,
            'countries' => $this->country_model->getSelectCollection(),
			'validation_errors' => validation_errors()
		));
	}

    /**
	 * by Chavdar, updated by Stefan
     * Manually edit Facebook Page in our system
     * It requires more specific controls like active or inactive
     */
	public function edit($id){
		if (! $object = $this->facebook_model->findPageById($id)){
			if (!$this->input->is_ajax_request())
      {
			     show_404();
      }
      else
      {
        header('Content-Type: application/json');
        echo json_encode(['status' => false, 'message' => 'Page doesnot exist']);
        return;
      }
		}
		if (!$this->input->is_ajax_request())
		{
			if ($this->isPost() && $this->form_validation->run('facebook'))
			{
				$object = array_merge($object, $this->input->post());
				if ($this->facebook_model->updateRow($id, $object))
				{
					redirect('admin/facebook');
				}
			}
			else{
				$object = array_merge($object, $this->input->post());
			}
		}
		else
		{
			if ($this->isPost())
			{
				$object = array_merge($object, $this->input->post());
				if ($this->facebook_model->updateRow($id, $object))
				{
					header('Content-Type: application/json');
          echo json_encode(['status' => true, 'message' => 'Page edited successfully']);
          return;
				}
			}
		}
		$this->render('form', array(
			'title' => 'Edit facebook page',
			'object' => $object,
			'users' => $this->user_model->getStaffGroupUsers(),
            'countries' => $this->country_model->getSelectCollection(),
            'selected_country' => array($object['country']),
			'validation_errors' => validation_errors()
		));
	}

    /**
	 * by Chavdar, updated by Stefan
     * Manages Facebook page, gives users permissions
     * We don't really need such function?! And it doesn't do anything much atm
     */
	public function manage($id){
		if(! $object = $this->facebook_model->findById($id)){
			show_404();
		}
		if($this->isPost()){
			$users = (array) $this->input->post('users');
			$users_origin = $this->facebook_model->getUsersAccess($id);
			$users_to_add = array_diff($users, $users_origin);
			$users_to_remove = array_diff($users_origin, $users);
			foreach($users_to_add as $user_to_add){
				$facebook_page_manage_access = $this->facebook_model->findManageById($user_to_add, $id);
				if(!$facebook_page_manage_access){
					$data = array(
						'id_user' => $user_to_add,
						'id_page' => $id
					);
					$this->facebook_model->insertManageRow($data);
				}
			}
			foreach($users_to_remove as $user_to_remove){
				$facebook_page_manage_access = $this->facebook_model->findManageById($user_to_remove, $id);
				if($facebook_page_manage_access){
					$data = array(
						'id_user' => $user_to_remove,
						'id_page' => $id
					);
					$this->facebook_model->deleteManageRow($user_to_remove, $id);
				}

			}
		}
		$this->render('manage', array(
			'title' => 'Edit facebook page access',
			'object' => $object,
			'users' => $this->user_model->getStaffGroupUsers(),
			'users_selected' => $this->facebook_model->getUsersAccess($id)
		));
	}

    /**
	 * by Chavdar, updated by Stefan
     * Delete Facebook page from our system
     * MAKE IT WORK ONLY IF THERE ARE USER SAFELY REMOVE MESSAGES
     */
	public function delete($id){
		$object = $this->facebook_model->findPageById($id);
    if($this->facebook_model->deleteRow($id))
    {
      if (!$this->input->is_ajax_request())
      {
        redirect('admin/facebook');
      }
      else
      {
        header('Content-Type: application/json');
        echo json_encode(['status' => true, 'message' => 'Page deleted successfully']);
        return;
      }
    }
	}

    /**
	 * by Chavdar
     * No idea what is this about, but it contains Chavdars never-expiring token :D
	 * We use Mr Mangusto access token now
     */
	public function fap_pages(){
	    $data  = file_get_contents('https://graph.facebook.com/v2.8/me/accounts?access_token=EAANn4NQ4a00BAP7xg0ur14s8Ly4QtSwa1z0FAt6iDVBGHqmgwXZAX9ZBhZAhtwTL61aZC1lOuww5jvAmiZCteXEftZCZAIjOCDZCEPehO1Io4IfbHjkHhktc1ZCFo3VuOUhzIn9MMfdkXYTxOlqvA6ZA9eNbQOk4WGXiUZD');
        $pages = json_decode($data);
        $pages_whitelist = array(
            'Oxa-test Community2',
            'Oxa-test shop page'
        );
        $valid_pages = array_filter($pages->data, function($item) use ($pages_whitelist){
            if(in_array($item->name, $pages_whitelist)){
                return $item;
            }
        });
        var_dump($valid_pages);
    }

	/**
	 * by Chavdar
	 * edited by Georgi 12042017 - added facebook pages filter
     * List facebook posts
     */
	public function fap_index()
	{
	    // TODO SEARCH FIX TO WORK NOT RESPONDING
				if (!empty($_POST['fb_page_filter'])){
					$des_page = $this->input->post('fb_page_filter');
					//var_dump($des_page);
					$fap_lists = $this->facebook_model->get_fap_lists();
					$facebook_posts = $this->facebook_model->getAllFacebookPosts($des_page);
				} else {
					$facebook_posts = $this->facebook_model->getAllFacebookPosts();
					$fap_lists = $this->facebook_model->get_fap_lists();
				}

				//var_dump($fap_lists['fb_pages_list']);
				$data['fb_pages_list'] = $fap_lists['fb_pages_list'];
				//$data['description_list'] = $fap_lists['description_list'];
				//$data['products_list'] = $fap_lists['products_list'];
				//$data['products_series'] = $fap_lists['products_series'];
				//$data['time_interval_list'] = $fap_lists['time_interval_list'];

        // TODO PAGINATION
        $this->render('fap_index', array(
            'facebook_posts' => $facebook_posts,
            'fb_pages_list' => $fap_lists['fb_pages_list'],
        ));
  }

	/**
	 * by Stefan (adopted from Chavdars code)
	 * Creates Facebook post, as a schedule
	 */
	public function fap_create(){
        $description = '';
        if($this->isPost()){
            $description = $this->input->post('description');
			$page_id = $this->input->post('facebook_page');
			$pid = $this->input->post('product_id');
			//$descriptionWithValues = $this->parseDescription($description, $pid, $page_id);
            //TODO validation facebook auto post
            $data = array(
                'product_id' => $this->input->post('product_id'),
                'facebook_page_id' => $this->input->post('facebook_page'),
                'description' => $description,
                //'description' => utf8_encode($descriptionWithValues),
                'date_schedule' => $this->input->post('schedule_date'),
                'is_uploaded' => 0
            );
            $this->facebook_model->insertFacebookPostsRow($data);
            redirect("admin/facebook/fap");
        }
		$countries = $this->country_model->getSelectCollection();
        $this->render('fap_form',array(
            'title' => 'Create facebook post',
            'facebook_pages' => $this->facebook_model->getSelectCollectionFAP(),
            'description' => $description
        ));
    }

    /**
	 * by Stefan (adopted from Chavdars code)
	 * Edits Facebook post
     */
	public function fap_edit($post_id){
	    $test = $this->facebook_model->AP_getAllfacebookPosts();
	    $facebook_post = $this->facebook_model->findFacebookPost($post_id);
	    $productImage = $this->product_model->getMainImage($facebook_post->product_id);
	    // TODO VALIDATION
	    if($this->isPost()){
            $description = $this->input->post('description');
			$page_id = $this->input->post('facebook_page');
			$pid = $facebook_post->product_id;
			//$descriptionWithValues = $this->parseDescription($description, $pid, $page_id);
			$updateData = array(
                'facebook_page_id' => $this->input->post('facebook_page'),
                'description' => $description,
                //'description' => utf8_encode($descriptionWithValues),
                'date_schedule' => $this->input->post('schedule_date')
            );
			$response = $this->facebook_model->updateFacebookPost($post_id, $updateData);
            redirect("admin/facebook/fap");
        }
        $this->render('fap_form',array(
            'title' => 'Edit facebook post',
            'editAction' => true,
            'facebook_pages' => $this->facebook_model->getSelectCollectionFAP(),
            'selected_facebook_page' => $facebook_post->facebook_page_id,
            'description' => $facebook_post->description,
            //'description' => utf8_decode($facebook_post->description),
            'date_schedule' => $facebook_post->date_schedule,
            'product_image' => $productImage,
            'product_id' => $facebook_post->product_id
        ));
    }

    /**
     * by Chavdar
     * Deletes Facebook post, From our CMS only!
     */
	public function fap_delete($post_id) {
        $facebook_post = $this->facebook_model->findFacebookPost($post_id);
        if($facebook_post === null){
            show_404(); // TODO SHOW MORE USER FRIENDLY MESSAGE TO THE END USER
        }
        $this->facebook_model->deleteFacebookPost($post_id);
        // TODO CHECK IF DELETE IS SUCCESSFULL AND SHOW USER FRIENDLY MESSAGE
        redirect("admin/facebook/fap");
    }

    /**
	 * by Stefan
	 * Changes {$vars} in description with real values
	 * Returns the new description
     */
	public function parseDescription($description, $product_id, $page_id){
		//gets the product object
		$product = $this->product_model->findById($product_id);
		//gets the available sizes
		$product_sizes = $this->product_model->getProductSizes($product_id);
		$available_sizes = '';
		foreach($product_sizes as $product_size){
			if($product_size->quantity>0)
				$available_sizes .= $product_size->name.', ';
		}
		$available_sizes = rtrim($available_sizes,', ');
		//gets product price
		$page = $this->facebook_model->findPageById($page_id);
		$country = $this->country_model->findById($page['country']);
		$currency = $country['currency'];
		if($currency == 'BGN')
			$currency = 'лв';
		$prices = $this->product_model->getProductPrices($product_id);
		foreach($prices as $price){
			if($price->country_id == $page['country']){
				$real_price = $price->price;
				break;
			}
		}
		//words to be replaced
		$vars = array(
			'{$code}' => $product['code'],
			'{$currency}' => $currency,
			'{$price}' => $real_price,
			'{$sizes}' => $available_sizes
		);
		//result description
		$descriptionWithValues = strtr($description, $vars);
		return $descriptionWithValues;
	}

	public static function getActions() {
        return array(
            'index',
            'create',
            'edit',
            'delete',
            'fap_index',
            'fap_create',
            'fap_edit',
            'fap_delete',
            'fap_pages',
        );
    }

    public function getProductInfo() {
        if(!$this->input->is_ajax_request()) {
            show_404();
        }

        $term = $this->input->post('term');

        if (empty($term)) {
            $response = array('success '=> false);
        }else{
            $term = filter_var($term, FILTER_SANITIZE_STRING);
            $country = $this->input->post('country');
            $products = $this->order_model->findProductsForAutocomplete($term, $country);

            $dataProducts = array();

            foreach($products as $product){
                $dataProducts[$product['id']] = $product;
            }
            $response = $dataProducts;
        }
        echo json_encode($response);
        die();
    }

		public function auto_fap()
		{
			//echo '$_POST: ';
		  //print_r($_POST);
			//echo '<br/>';

			$this->form_validation->set_rules('sel_products[]', 'Products', 'required');
			$this->form_validation->set_rules('sel_fb_pages[]', 'Facebook pages', 'required');
			$this->form_validation->set_rules('sel_descriptions[]', 'Descriptions', 'required');
			$this->form_validation->set_rules('startdatetime', 'Autoposting start', 'required');
			$this->form_validation->set_rules('timeinterval', 'Time interval between posts', 'required');

			if ($this->form_validation->run() == FALSE) {
				//echo "Form validation FAILED <br/>"; // to be commented after testing
			//LOADS THE AUTOFAP VIEW
				//Queries the db for the list required for the multiselect form

				//$test = $this->facebook_model->unique_product_series();
			  //var_dump($test);

				if (!empty($_POST['sel_serie'])){
					$products_ser = $this->input->post('products_ser');
					//var_dump($this->input->post('sel_serie'));
					//var_dump($this->input->post('products_ser'));
					$serie_filter = $products_ser[$this->input->post('sel_serie')];
					$fap_lists = $this->facebook_model->get_fap_lists($serie_filter);
				} else {
					$fap_lists = $this->facebook_model->get_fap_lists();
				}


				$data['fb_pages_list'] = $fap_lists['fb_pages_list'];
				$data['description_list'] = $fap_lists['description_list'];
				$data['products_list'] = $fap_lists['products_list'];
				$data['products_series'] = $fap_lists['products_series'];
				$data['time_interval_list'] = $fap_lists['time_interval_list'];

				//$this->load->view('admin/facebook/auto_fap', $data);
				$data['title'] = 'Auto generate multiple posts';
				$this->render('auto_fap_form',$data);

			}
			else{
				//echo "Form validation SUCCESSFULL <br/>";

				//Queries the db for the list required for the multiselect form
				$fap_lists = $this->facebook_model->get_fap_lists();

				$data['fb_pages_list'] = $fap_lists['fb_pages_list'];
				$data['description_list'] = $fap_lists['description_list'];
				$data['products_list'] = $fap_lists['products_list'];
				$data['time_interval_list'] = $fap_lists['time_interval_list'];

				//Assigns the posted values to vars
				$products = $this->input->post('sel_products');
				$fb_pages = $this->input->post('sel_fb_pages');
				$description = $this->input->post('sel_descriptions');
				$startdatetime = $this->input->post('startdatetime');

				$timeinterval = $fap_lists['time_interval_list'][$this->input->post('timeinterval')];
				$descriptions_text = array();
				foreach ($description as $value) {
					$descriptions_text[] = $fap_lists['description_list'][$value];
				}

				//var_dump($fap_lists['description_list']);
				//var_dump($descriptions_text);

				//ADD POST_INTERVAL_MAX
				/*
				var_dump($products);
				var_dump($fb_pages);
				var_dump($description);
				var_dump($startdatetime);
				*/
				$this->facebook_model->product_fap($products, $fb_pages, $descriptions_text, $startdatetime, $timeinterval);
				//$data['sel_products'] = $this->input->post('sel_products');

				//REDIRECT TO FAP TABLE VIEW
				redirect(base_url() . 'admin/facebook/fap');

				//LOADS THE AUTOFAP VIEW - to be removed after testing
				//$this->load->view('admin/facebook/auto_fap', $data);

			}

		}

		public function auto_fap_st()
		{
			//echo '$_POST: ';
			//print_r($_POST);
			//echo '<br/>';

			$this->form_validation->set_rules('sel_products[]', 'Products', 'required');
			$this->form_validation->set_rules('sel_fb_pages[]', 'Facebook pages', 'required');
			$this->form_validation->set_rules('sel_descriptions[]', 'Descriptions', 'required');
			$this->form_validation->set_rules('startdatetime', 'Autoposting start', 'required');
			$this->form_validation->set_rules('timeinterval', 'Time interval between posts', 'required');

			if ($this->form_validation->run() == FALSE) {
				//echo "Form validation FAILED <br/>"; // to be commented after testing
			//LOADS THE AUTOFAP VIEW
				//Queries the db for the list required for the multiselect form

				//$test = $this->facebook_model->unique_product_series();
				//var_dump($test);

				if (!empty($_POST['sel_serie'])){
					$products_ser = $this->input->post('products_ser');
					//var_dump($this->input->post('sel_serie'));
					//var_dump($this->input->post('products_ser'));
					$serie_filter = $products_ser[$this->input->post('sel_serie')];
					$fap_lists = $this->facebook_model->get_fap_lists($serie_filter);
				} else {
					$fap_lists = $this->facebook_model->get_fap_lists();
				}

				$data['fb_pages_list'] = $fap_lists['fb_pages_list'];
				$data['description_list'] = $fap_lists['description_list'];
				$data['products_list'] = $fap_lists['products_list'];
				$data['products_series'] = $fap_lists['products_series'];
				$data['time_interval_list'] = $fap_lists['time_interval_list'];

				//$this->load->view('admin/facebook/auto_fap', $data);
				$data['title'] = 'Auto generate multiple posts';
				$this->render('auto_fap_form_st',$data);

			}
			else{
				//echo "Form validation SUCCESSFULL <br/>";

				//Queries the db for the list required for the multiselect form
				$fap_lists = $this->facebook_model->get_fap_lists();

				$data['fb_pages_list'] = $fap_lists['fb_pages_list'];
				$data['description_list'] = $fap_lists['description_list'];
				$data['products_list'] = $fap_lists['products_list'];
				$data['time_interval_list'] = $fap_lists['time_interval_list'];

				//Assigns the posted values to vars
				$products = $this->input->post('sel_products');
				$fb_pages = $this->input->post('sel_fb_pages');
				$description = $this->input->post('sel_descriptions');
				$startdatetime = $this->input->post('startdatetime');

				$timeinterval = $fap_lists['time_interval_list'][$this->input->post('timeinterval')];
				$descriptions_text = array();
				foreach ($description as $value) {
					$descriptions_text[] = $fap_lists['description_list'][$value];
				}

				$this->facebook_model->product_fap($products, $fb_pages, $descriptions_text, $startdatetime, $timeinterval);

				//REDIRECT TO FAP TABLE VIEW
				redirect(base_url() . 'admin/facebook/fap');

			}

		}

	public function addDescription()
	{
		$this->form_validation->set_rules('description', 'Description', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			//echo "Form validation FAILED <br/>";
			$data = array();
			$data['title'] = 'Add descriptions';

			$this->render('add_descriptions', $data);
		}	else {
			//echo "Form validation SUCCESSFULL <br/>";
			$data = array(
							'description' => $this->input->post('description')
						);
			$this->facebook_model->insertFacebookPageDescription($data);
			$this->render('add_descriptions', $data);
			//echo "Description added";
			//redirect(base_url() . 'admin/facebook/auto_fap');
		}

	}

	public function fap_sql_requests()
	{
		//Echoes added products by brand
		if (isset($_POST['brand_filter']) === true && empty($_POST['brand_filter']) === false) {
			$serie_filter = $_POST['brand_filter'];
			//var_dump($serie_filter);
			$brand_result = $this->facebook_model->index_brand_product($serie_filter);
			$added_products =json_encode($brand_result);

			echo ($added_products);
		}

		//Echoes products by product serie
		if (isset($_POST['product_serie']) === true && empty($_POST['product_serie']) === false) {
			$pr_serie = $_POST['product_serie'];
			//var_dump($serie_filter);
			$series_codes = $this->facebook_model->unique_product_series();
			$brand_result = $this->facebook_model->get_stocked_products($series_codes[$pr_serie]);
			$json_result =json_encode($brand_result);

			//echo ($brand_result);
			echo ($json_result);
		}
	}

	public function fap_sql_requests_st()
	{
		//Echoes added products by brand
		if (isset($_POST['brand_filter']) === true && empty($_POST['brand_filter']) === false) {
			$serie_filter = $_POST['brand_filter'];
			//var_dump($serie_filter);
			$brand_result = $this->facebook_model->index_brand_product($serie_filter);
			$added_products =json_encode($brand_result);

			echo ($added_products);
		}

		//Echoes products by product serie
		if (isset($_POST['product_serie']) === true && empty($_POST['product_serie']) === false) {
			$pr_serie = $_POST['product_serie'];
			//var_dump($serie_filter);
			$series_codes = $this->facebook_model->unique_product_series();
			$brand_result = $this->facebook_model->get_stocked_products_st($series_codes[$pr_serie]);
			$json_result =json_encode($brand_result);

			//echo ($brand_result);
			echo ($json_result);
		}
	}
}
