<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FacebookDescriptions extends Admin_Controller {

    protected
        $load_models = array('facebook_model'),
        $load_helpers = array('form'),
        $load_libraries = array('form_validation');

    //TODO facebook page description FIX SEARCH

    /**
     * List facebook descriptions for certain page
     *
     * @param $facebook_page_id facebook page id
     */
    public function index($facebook_page_id) {
        $this->render('index', array(
            'title' => 'Manage facebook page descriptions',
            'facebook_page_id' => $facebook_page_id,
            'facebook_page_descriptions' => $this->facebook_model->getFacebookPagesDescriptions($facebook_page_id)
        ));
    }

    /**
     * Create new facebook page description
     *
     * @param $facebook_page_id
     */
    public function create($facebook_page_id) {
        if($this->isPost()){
			/*echo "<pre>";
			var_dump($this->input->post('description'));
			echo "</pre>";*/
			//check if it is utf8
            $insertData = array(
                'page_id' => $facebook_page_id,
                'description' => $this->input->post('description')
            );
            if ($this->facebook_model->insertFacebookPageDescription($insertData)) {
                redirect("admin/facebook/manage-text/{$facebook_page_id}");
            }
        }

        $this->render('form', array(
            'title' => 'Create facebook page description',
            'facebook_page_id' => $facebook_page_id,
        ));
    }

    /**
     * Edit facebook page description
     *
     * @param $facebook_page_description_id
     */
    public function edit($facebook_page_description_id) {
        // TODO MAY BE ADD BUTTON TO GO BACK TO FACEBOOK PAGE DESCRIPTION LISTING
        $facebook_page_description = $this->facebook_model->findFacebookPageDescription($facebook_page_description_id);
        if($facebook_page_description === null){
            show_404(); // TODO SHOW MORE USER FRIENDLY MESSAGE TO THE END USER
        }

        // TODO VALIDATION
        if($this->isPost()){
            $description = $this->input->post('description');
            $updateData = array(
                'description' => $description
            );
            $response = $this->facebook_model->updateFacebookPageDescription($facebook_page_description_id, $updateData);
            // TODO SHOW USER FRIENDLY MESSAGE TO THE USER AFTER UPDATE
            redirect("admin/facebook/manage-text/edit/{$facebook_page_description_id}");
        }

        $this->render('form', array(
            'title' => 'Edit facebook page description',
            'object' => $facebook_page_description,
            'facebook_page_id' => $facebook_page_description->page_id,
        ));
    }

    // TODO FACEBOOK PAGE DESCRIPTION LISTING CONFIRM ON DELETE
    /**
     * Delete facebook page description
     *
     * @param $facebook_page_description_id
     */
    public function delete($facebook_page_description_id) {
        $facebook_page_description = $this->facebook_model->findFacebookPageDescription($facebook_page_description_id);
        if($facebook_page_description === null){
            show_404(); // TODO SHOW MORE USER FRIENDLY MESSAGE TO THE END USER
        }

        $this->facebook_model->deleteFacebookPageDescription($facebook_page_description->id);
        // TODO CHECK IF DELETE IS SUCCESSFULL AND SHOW USER FRIENDLY MESSAGE
        redirect("admin/facebook/manage-text/{$facebook_page_description->page_id}");
    }

    /**
     * Ajax return descriptions for certain facebook page
     */
    public function getDescriptions() {
        $response = array();
        if(!$this->input->is_ajax_request()) {
            show_404();
        }

        $page_id = $this->input->post('page_id'); // facebook page id
        if(empty($page_id)){
            $response = array('success '=> false);
        } else {
            // TODO move the view file in partials folder will be a good idea probably
            $pageDescriptions = $this->facebook_model->findFacebookPageDescriptions($page_id);
            $response['content'] = $this->twig->render('admin/facebookdescriptions/ajax_descriptions',
                array(
                    'pageDescriptions'=>$pageDescriptions
                )
            );
        }

        echo json_encode($response);
        die();
    }
}