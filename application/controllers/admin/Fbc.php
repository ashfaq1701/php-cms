<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
	
function sortArrayByUnreadMessageCount($a, $b)
{
	$a = $a['unread_message_count'];
	$b = $b['unread_message_count'];

	if ($a == $b)
	{
		return 0;
	}

	return ($a < $b) ? -1 : 1;
}

class Fbc extends Admin_Controller {
	
	protected
		$load_models = array('fbc_model', 'user_model'),
		$load_helpers = array('file', 'url');
		
	//not finished at all
	public function ads(){

		$fb = new Facebook([
		  'app_id' => '1848049545471939',
		  'app_secret' => '60a7c3fbf130abd33ada6479dfe48822',
		  'default_graph_version' => 'v2.8'
		]);

		$helper = $fb->getRedirectLoginHelper();

		if (!isset($_SESSION['facebook_access_token'])) {
		  $_SESSION['facebook_access_token'] = null;
		}

		if (!$_SESSION['facebook_access_token']) {
		  $helper = $fb->getRedirectLoginHelper();
		  try {
			$_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
		  } catch(FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		  } catch(FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		  }
		}

		if ($_SESSION['facebook_access_token']) {
		  echo "You are logged in!";
		} else {
		  $permissions = ['ads_management'];
		  $loginUrl = $helper->getLoginUrl('http://localhost:8888/marketing-api/', $permissions);
		  echo '<a href="' . $loginUrl . '">Log in with Facebook</a>';
		}

		// Add after echo "You are logged in "

		// Initialize a new Session and instantiate an Api object
		Api::init(
		  '233923320407573', // App ID
		  '{your-app-secret}',
		  $_SESSION['facebook_access_token'] // Your user access token
		);
 
	}
		
	public function lastreadmessage($wtf){		
		$conversations = array();
		$i=0;
		foreach($wtf as $convo){
			$conversations[$i] = $convo;
			$i++;
		}
		for($j=$i-1; $j>=0; $j--){
			if(isset($conversations[$j]->unread_count))
				break;
		}
		if($i == 0)
			$lar = "no messages yet :(";
		else{
			if(isset($conversations[$j+1])){
				$clar = $conversations[$j+1]->updated_time;
				$lar1 = new DateTime($clar);
				$lar1->add(new DateInterval('PT2H'));
				$lar = $lar1->format("F-d H:i");
			}else
				$lar = "Can't find it";
		}
		return $lar;
	}
	
	public function nextPage($id = '1'){
		//starts updating page with fb_id = $fb_id
		//after finishing redirects to nextPage with id from the db
		
		$ids = array();
		$i = 0;
		$file = fopen(APPPATH."pages.txt","r");

		while(!feof($file)){
			$ids[$i] = fgets($file);
			$i++;
		}
		fclose($file);
		
		$page = $this->fbc_model->findPageByDBId($ids[0]);
		$this->updateconversations($page['fb_id']);
		//have to make sure this one is finished before continuing
		
		echo "<br />".$ids[0]."<br />";
		
		$file = fopen(APPPATH."pages.txt", "w");
		
		for($j=1; $j<$i-1; $j++){
			fwrite($file, $ids[$j]);
		}
	}
	
	public function nextConversation($id = '1'){
		//starts updating conversation with id = $id
		//after finishing redirects to nextConversation with id from the db
		
		$ids = array();
		$i = 0;
		$file = fopen(APPPATH."conversations.txt", "r");

		while(!feof($file)){
			$ids[$i] = fgets($file);
			$i++;
		}
		fclose($file);
		
		echo "<br />".$ids[0]."/".$ids[$i-2]."<br />";
		
		$conversation = $this->fbc_model->findConversationByDBId($ids[0]);
		$page = $this->fbc_model->findPageByDBId($conversation['page_id']);
		
		$this->getMessagesByConversationId($page['fb_id'], $conversation['conversation_id']);
		
		$file = fopen(APPPATH."conversations.txt","w");
		
		for($j=1; $j<$i-1; $j++){
			fwrite($file, $ids[$j]);
		}
		
		if($i > 1){		
			echo '<meta http-equiv="refresh" content="1; url=http://cms.paradiseshop.eu/admin/fbc/nextconversation">';
		}
	}
	
	public function nextConversationBackwards($id = '37433'){
		//starts updating conversation with id = $id
		//after finishing redirects to nextConversation with id from the db
		
		$ids = array();
		$i = 0;
		$file = fopen(APPPATH."conversationsbackwards.txt", "r");

		while(!feof($file)){
			$ids[$i] = fgets($file);
			$i++;
		}
		fclose($file);
		
		echo "<br />".$ids[0]."/".$ids[$i-2]."<br />";
		
		$conversation = $this->fbc_model->findConversationByDBId($ids[0]);
		$page = $this->fbc_model->findPageByDBId($conversation['page_id']);
		
		$this->getMessagesByConversationId($page['fb_id'], $conversation['conversation_id']);
		
		$file = fopen(APPPATH."conversationsbackwards.txt","w");
		
		for($j=1; $j<$i-1; $j++){
			fwrite($file, $ids[$j]);
		}
		
		if($i > 1){		
			echo '<meta http-equiv="refresh" content="1; url=http://cms.paradiseshop.eu/admin/fbc/nextconversationbackwards">';
		}
	}
	
	public function updateconversations($fb_page_id = '735754616600702'){
		//doesnt work as intended yet
		//print a specific conversation, will go for print all conversations
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		
		$page = $this->fbc_model->findByPageId($fb_page_id);
		
		$fb = new Facebook\Facebook(array('app_id'=>$page['app_id'], 'app_secret'=>$page['secret_id'], 'default_graph_version'=>'v2.8'));
		
		$fbApp = $fb->getApp();
		
		$to_request = "/".$page['fb_id']."/conversations";
		$request = new Facebook\FacebookRequest($fbApp, $page['access_token'], 'GET', $to_request);
		
		try{
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		$myConversations = $response->getGraphEdge();
		
		$maxConversations = 50000;
		$convoCount = 0;
		$i = 0;
		
		//$fb_convo = array();
		
		do{
			foreach($myConversations as $convo){
				$convo_id = $convo->getField('id');
				$convo_link = $convo->getField('link');
				$allConversations[$i]['conversation_link'] = $convo_link;
				$allConversations[$i]['conversation_id'] = $convo_id;
				$allConversations[$i]['page_id'] = $page['id'];
				$i++;
			}
			$convoCount++;
		}while ($convoCount < $maxConversations && $myConversations = $fb->next($myConversations));
		
		for($j=0; $j<$i; $j++){
			$this->fbc_model->populateConversationRow($allConversations[$j]);
		}		
	}
	
	public function getMessagesByConversationId($page_id, $convo_id){
		//get all messages for page_id in our DB
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		
		$page = $this->fbc_model->findByPageId($page_id);
		$fb = new Facebook\Facebook(array('app_id'=>$page['app_id'], 'app_secret'=>$page['secret_id'], 'default_graph_version'=>'v2.8'));
		
		$fbApp = $fb->getApp();
		
		$to_request = "/".$convo_id."/messages";
		$to_request = str_replace('/','%2F',$to_request);
		$request = new Facebook\FacebookRequest($fbApp, $page['access_token'], 'GET', $to_request);
		
		try{
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		$myMessages = $response->getGraphEdge();
		
		$maxMessages = 50000;
		$messagesCount = 0;
		$i = 0;
		
		do{
			foreach ($myMessages as $message){
				$message_id = $message->getField('id');
				$allMessageIds[$i] = $message_id;
				$i++;
			}
			$messagesCount++;
		}while ($messagesCount < $maxMessages && $myMessages = $fb->next($myMessages));
		
		$amessages = array();
		//get the actuall messages
		for($j=0; $j<$i; $j++){
			$to_request = "/".$allMessageIds[$j];
			$to_request = str_replace('/','%2F',$to_request);
			echo $to_request."<br />";
			$request = new Facebook\FacebookRequest($fbApp, $page['access_token'], 'GET', $to_request, array('fields' => 'id,created_time,from,message'));
			
			try{
				$response = $fb->getClient()->sendRequest($request);
			}catch(Facebook\Exceptions\FacebookResponseException $e){
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			
			$myMessages = $response->getGraphNode();
			
			$some_date = $myMessages->getField('created_time');
			
			$message_from = $myMessages->getField('from');
			
			$amessages[$j]['created_time'] = $some_date->format('Y-m-d H:i:s');
			$amessages[$j]['conversation_id'] = $convo_id;
			$amessages[$j]['message_id'] = $myMessages->getField('id');
			$amessages[$j]['message_from_id'] = $message_from['id'];
			$amessages[$j]['message_from_text'] = $message_from['name'];
			$amessages[$j]['message_text'] = $myMessages->getField('message');
			$amessages[$j]['fb_page_id'] = $page_id;
			$amessages[$j]['conversation_link'] = "";
		}
		
		for($j=0; $j<$i; $j++){
			$this->fbc_model->populateMessageRow($amessages[$j]);
		}		
	}
	
	public function responsetime($fb_page_id = '735754616600702'){
		//calculate the response time of our pages
		//for each customer
		
		//time zone 1: working hours 08:00 - 23:59
		//formula (tc-ta)/nl
		//time zone 2: sleep time 24:00 - 07:59
		//formula "response before 8:15" or something like 9:00
		
		//page to look in, find by fb id get our db id
		$page = $this->fbc_model->findByPageId($fb_page_id);
		$page_id = $page['id'];
		
		//get all conversations in array
		$sql1 = "select *
					from fb_conversations
					where page_id = '$page_id'";
		$result1 = $this->db->query($sql1)->result_array();
		$allconversations = array();
		$k=0;
		foreach($result1 as $conversation){
			$allconversations[$k] = $conversation;
			$k++;
		}
		
		$countg = 0;
		$countb = 0;
		$counts = 0;
			
		$countasymbols = 0;
		$countcsymbols = 0;
		
		for($z=0; $z<$k; $z++){
			//keep one conversation to check for responses
			$conversation_id = $allconversations[$z]['conversation_id'];
			$conversation_array = $this->fbc_model->findConversationById($conversation_id);
			
			//get all messages for this conversation
			$sql = "select *
						from fb_messages
						where conversation_id = '$conversation_id'";
			$result = $this->db->query($sql)->result_array();
			$allmessages = array();
			$i = 0;
			foreach($result as $message){
				$allmessages[$i] = $message;
				$i++;
			}
			
			//reverse messages feed
			$allmessages = array_reverse($allmessages);
			
			//initialize
			$agent = $page['fb_id'];
			
			$client = "0";
			for($j=0; $j<$i; $j++){
				if($allmessages[$j]['message_from_id'] != $agent){
					$client = $allmessages[$j]['message_from_id'];
					break;
				}
			}
			
			$ganswers = 0;
			$banswers = 0;
			$sanswers = 0;
			
			$asymbols = 0;
			$csymbols = 0;
			
			for($j=0; $j<$i; $j++){
				$message_len = strlen($allmessages[$j]['message_text']);
				if($allmessages[$j]['message_from_id'] == $client){
					$csymbols += $message_len;
				}else{
					$asymbols += $message_len;
				}
				
				echo $allmessages[$j]['created_time']." ".$allmessages[$j]['message_from_text']."len-".$message_len.": ".$allmessages[$j]['message_text']."<br />";
			}
			
			//count <1 as sanswers, <5 as ganswers and rest as banswers
			for($j=0; $j<$i-1; $j++){				
				if($allmessages[$j]['message_from_id'] == $client && $allmessages[$j+1]['message_from_id'] == $agent){
					$datetime1 = strtotime($allmessages[$j]['created_time']);
					$datetime2 = strtotime($allmessages[$j+1]['created_time']);
					if(($datetime2 - $datetime1) < 301){
						$ganswers++;
						if(($datetime2 - $datetime1) < 61){
							$sanswers++;
						}
					}else{
						$banswers++;
					}
					echo ($datetime2 - $datetime1)." seconds <br />";
				}
			}
			echo "<br />";
			
			$countg += $ganswers;
			$countb += $banswers;
			$counts += $sanswers;
			
			$countasymbols += $asymbols;
			$countcsymbols += $csymbols;
			
			$tanswers = $ganswers + $banswers;
			if($tanswers == 0){
				$tanswers = 1;
			}
			$value = $ganswers / $tanswers;
			echo "fast answers: ".$ganswers."<br />";
			echo "Super-fast answers: ".$sanswers."<br />";
			echo "slow answers: ".$banswers."<br />";
			echo "ratio: ".$value."<br />";
			echo "<br />";
			echo "agent text: ".$asymbols."<br />";
			echo "client text: ".$csymbols."<br />";
			echo "-------------------------------------------------------------<br />";
		}
		
		$countt = $countg + $countb;
		$valuet = $countg / $countt;
		echo "Total fast answers: ".$countg."<br />";
		echo "Total Super-fast answers: ".$counts."<br />";
		echo "Total slow answers: ".$countb."<br />";
		echo "ratio: ".$valuet."<br />";
		echo "<br />";
		echo "Total agent text: ".$countasymbols."<br />";
		echo "Total client text: ".$countcsymbols."<br />";
		echo "-------------------------------------------------------------<br />";
	}
	
	public function chatweight($fb_id = ''){
		//returns chat weight between a page and client
	}
	
	public function pagesinfile(){
		//get a list of all pages
		//creates a file with simple list of pages ids
		//cms/application/pages.txt
		$sql1 = "select id
					from fb_pages";
		$result1 = $this->db->query($sql1)->result_array();
		$txt = "";
		
		foreach($result1 as $page){
			echo $page['id'];
			$txt .= $page['id']."\r\n";
		}
		
		if(!write_file(APPPATH."/pages.txt", $txt)){
			echo "error";
		}else{
			echo "success";
		}
	}
	
	public function conversationsinfile(){
		//get a list of all pages
		//creates a file with simple list of pages ids
		//cms/application/pages.txt
		$sql1 = "select id
					from fb_conversations";
		$result1 = $this->db->query($sql1)->result_array();
		$txt = "";
		
		foreach($result1 as $conversation){
			echo $conversation['id'];
			$txt .= $conversation['id']."\r\n";
		}
		
		if(!write_file(APPPATH."/conversations.txt", $txt)){
			echo "error";
		}else{
			echo "success";
		}
	}
	
	public function conversationsinfilebackwards(){
		//get a list of all pages
		//creates a file with simple list of pages ids
		//cms/application/pages.txt
		$sql1 = "select id
					from fb_conversations";
		$result1 = $this->db->query($sql1)->result_array();
		$txt = "";
		$result1 = array_reverse($result1);
		
		foreach($result1 as $conversation){
			echo $conversation['id'];
			$txt .= $conversation['id']."\r\n";
		}
		
		if(!write_file(APPPATH."/conversationsbackwards.txt", $txt)){
			echo "error";
		}else{
			echo "success";
		}
	}
}