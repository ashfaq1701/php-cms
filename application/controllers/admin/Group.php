<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends Admin_Controller {
	
	protected
		$load_models = array('group_model'),
		$load_helpers = array('form', 'directory', 'inflector'),
		$load_libraries = array('form_validation');
	
	public function index() {
		return $this->render('index', array(
			'title' => 'List groups',
			'groups' => $this->group_model->getCollection()
		));
	}
	
	public function edit($id) {
		if (! $object = $this->group_model->findById($id)) {
			show_404();
		}
		
		if ($this->isPost() && $this->form_validation->run('group')) {
			$object = array_merge($object, $this->input->post());
			
			if ($this->group_model->updateRow($id, $object)) {
				redirect('admin/groups');
			}
		}
		else {
			$object = array_merge($object, $this->input->post());
		}
		
		$this->render('form', array(
			'title' => 'Edit group',
			'object' => $object,
			'permissions' => $this->getPermissionsToArray(),
			'selected_permissions' => array_filter(explode('|', $object['permissions'])),
			'validation_errors' => validation_errors()
		));
	}
	
	/**
	 * 
	 * @return array
	 */
	private function getPermissionsToArray() {
		$result = array();
		
		foreach (directory_map(APPPATH . 'controllers/admin') as $file) {
			require_once $file;
			
			$controller = pathinfo($file, PATHINFO_FILENAME);
			
			if (class_exists($controller)) {
				if ($actions = forward_static_call(array($controller, 'getActions'))) {
					$result[strtolower($controller)] = $actions;
				}
			}
		}

		if ($result) {
			ksort($result);
			$result = array_chunk($result, 3, true);
		}

		return $result;
	}
	
}