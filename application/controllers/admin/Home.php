<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author chavdar
 * @updates Stefan
 */
class Home extends Admin_Controller {
	
	protected
		$load_models = array('user_model', 'facebook_model', 'schedule_model'),
		$load_helpers = array('url');

    public function __construct(){
        parent::__construct();
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;
		if($user_confirmed == 0 || $user_confirmed == null){
			redirect('admin/schedule');
		}
    }
	
	/**
	 * @return void
	 */
	public static function getActions() {
		return array('index');
	}
	
	public function index() {
		$user_data = UserHelper::getCurrentUserData();
		
		$this->render('index.html', [
			'title' => 'Dashboard',
			'orders_collection' => $this->user_model->getCurrentUserOrders($user_data['id']),
			'schedule' => $this->schedule_model->getuserschedule($user_data['id']),
			'pages' => $this->facebook_model->getUnseenMessages()
		]);
	}
}