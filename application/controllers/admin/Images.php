<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends Admin_Controller {

	protected
		$load_models = array('images_model', 'product_model','facebook_model'),
		$load_helpers = array('directory', 'url'),
		$load_libraries = array('image');

	public function index(){
		//design some sort of a gallery
	}
	/*
	public function testpickimage(){
		$product_id = 589;
		$page_id = 5;
		echo $this->facebook_model->chooseImageForPost($product_id, $page_id);
	}*/

	public function brandImageUpload() {
		$file = $_FILES['file'];
		$fileName = $file['name'];
		$tmpName = $file['tmp_name'];
		$targetName = str_replace(' ', '_', $fileName);
		move_uploaded_file($tmpName, __DIR__ . '/../../../uploads/brands/' . $targetName);
		echo json_encode(['filename' => $targetName]);
	}

	public function getBrandList() {
		$brands = $this->images_model->getBrandList();
		header('Content-Type: application/json');
		echo json_encode($brands);
	}

	public function saveBrand() {
		$params = json_decode(file_get_contents('php://input'),true);
		$brandData = array(
			'full_name' => $params['full_name'],
			'short_name' => $params['short_name'],
			'abbreviation' => $params['abbreviation'],
			'active' => 1,
			'image' => '/uploads/brands/' . $params['brand_image']
		);
		$brands = $this->images_model->searchBrandsByAll($brandData);
		if (count($brands) > 0) {
			$brand = $brands[0];
			$brandId = $brand['id'];
		} else {
			$brandData['created_at'] = date('Y-m-d H:i:s');
			$brandId = $this->images_model->insertBrand($brandData);
		}
		header('Content-Type: application/json');
		echo json_encode(['brand_id' => $brandId]);
	}

	public function batchUpload(){
		//set path
		$path_to_unsorted = APPPATH.'../uploads/unsorted/batch';

		//get all frolders and files
		$files = scandir($path_to_unsorted);
		$files = array_slice($files, 2);

		$i = 0;
		$images = array();

		$cnt = count($files);
		echo $cnt;

		var_dump($files);
		foreach($files as $file){
			if(substr($file, 0, 1) == '_')
				$file = ltrim($file, '_');

			$images[$i] = array();

			//extract code, type, brand and all other commands
			$file1 = strtolower($file);
			$file1 = str_replace(' ', '', $file1); //sometimes there is a space before priority
			$file1 = str_replace('(', '_(', $file1); //in case there is no underscore before priority
			$file1 = preg_replace('/_+/', '_', $file1); //in case there are multiple underscores

			//file extension
			$file_ext = pathinfo($file1, PATHINFO_EXTENSION);
			$images[$i]['file_ext'] = $file_ext;
			if($images[$i]['file_ext'] != 'gif' && $images[$i]['file_ext'] != 'png' && $images[$i]['file_ext'] != 'jpg' && $images[$i]['file_ext'] != 'jpeg'){
				//proper format
				$images[$i]['error'][] = "wrong extension: ".$images[$i]['file_ext'];
			}else{
				//wrong format
			}

			//file_name
			$file_name = pathinfo($file1, PATHINFO_BASENAME);
			$file_name = rtrim($file_name, $file_ext);
			$file_name = rtrim($file_name, '.');
			$images[$i]['file_name'] = $file_name;

			$file_name_array = explode('_', $file_name);

			$images[$i]['additional_codes'] = '';
			foreach($file_name_array as $cmd){
				echo "test: ". $cmd." ";
				//check what text you have in the filename array
				switch($cmd){
					case substr($cmd, 0, 2) === "br":{
						//brand
						if(isset($images[$i]['brand'])){
							//multiple brands
							$images[$i]['error'][] = "multiple brands";
						}else{
							$brand = substr($cmd, 2);
							$brand_id = $this->images_model->findBrandIDByName($brand)->id;
							if($brand_id){
								$images[$i]['brand'] = $brand_id;
							}else{
								$images[$i]['error'][] = "not existing brand";
							}
						}
						break;
					}
					case substr($cmd, 0, 1) === '(':{
						//priority
						if(isset($images[$i]['priority'])){
							$images[$i]['error'][] = "multiple priorities";
						}else{
							$images[$i]['priority'] = ltrim(rtrim($cmd, ')'), '(');
						}
						break;
					}
					case $this->images_model->cmdCheck($cmd):{
						//command
						if(isset($images[$i]['type'])){
							$images[$i]['error'][] = "multiple types";
						}else{
							$images[$i]['type'] = $cmd;
						}
						break;
					}
					case $this->images_model->sideContentCheck($cmd):{
						//command
						if(isset($images[$i]['side_content'])){
							$images[$i]['error'][] = "multiple side content tags";
						}else{
							$images[$i]['side_content'] = $cmd;
						}
						break;
					}
					case $this->images_model->codeCheck($cmd):{
						//code
						if(isset($images[$i]['code'])){
							$images[$i]['additional_codes'] .= '_';
							$images[$i]['additional_codes'] .= $cmd;
							//it has to trigger another function call for this particular image
							//$images[$i]['error'][] = "multiple codes";
						}else{
							$images[$i]['code'] = $cmd;
						}
						break;
					}
					case substr($cmd, -2) === 'eu':{
						//price euro as eu
						if(isset($images[$i]['price_eur'])){
							$images[$i]['error'][] = 'multiple prices';
						}else{
							$images[$i]['price_eur'] = substr($cmd, 0, -2);
						}
						break;
					}
					case substr($cmd, -3) === 'eur':{
						//price euro as eur
						if(isset($images[$i]['price_eur'])){
							$images[$i]['error'][] = 'multiple prices';
						}else{
							$images[$i]['price_eur'] = substr($cmd, 0, -3);
						}
						break;
					}
					case substr($cmd, -2) === 'bg':{
						//stock price euro as bg
						if(isset($images[$i]['stock_price_bg'])){
							$images[$i]['error'][] = 'multiple stock prices';
						}else{
							$images[$i]['stock_price_bg'] = substr($cmd, 0, -2);
						}
						break;
					}
					case substr($cmd, -3) === 'bgn':{
						//stock price euro as bgn
						if(isset($images[$i]['stock_price_bg'])){
							$images[$i]['error'][] = 'multiple stock prices';
						}else{
							$images[$i]['stock_price_bg'] = substr($cmd, 0, -3);
						}
						break;
					}
					case $this->images_model->sizecheck($cmd):{
						//sizes command
						if(isset($images[$i]['sizes'])){
							$images[$i]['error'][] = 'multiple sizes';
						}else{
							$images[$i]['sizes'] = $cmd;
						}
						break;
					}
					case $this->images_model->suppliercheck($cmd):{
						//supplier command
						if(isset($images[$i]['supplier'])){
							$images[$i]['error'][] = 'multiple suppliers';
						}else{
							$images[$i]['supplier'] = $cmd;
						}
						break;
					}
					default :{
						$images[$i]['error'][] = "unknown symbols: ".$cmd;
						break;
					}
				}
			}
			$i ++;
		}

		//check all images for code
		foreach($images as &$image){
			if(isset($image['code'])){
				//there is code set
				if(!isset($image['type'])){
					$image['error'][] = "missing type";
				}
				if(!isset($image['brand'])){
					//no brand = 11 in DB
					$image['brand'] = 11;
				}
			}else{
				if(!isset($image['side_content']) && !isset($image['error'])){
					$image['test'] = true;
					if(!isset($image['price_eur'])){
						$image['error'][] = "missing price";
					}
					if(!isset($image['supplier'])){
						$image['error'][] = "missing supplier";
					}
					if(!isset($image['sizes'])){
						$image['error'][] = "missing sizes";
					}
				}
			}
			if(empty($image['error'])){
				if(isset($image['test'])){
					$next_image = $this->images_model->lastTestImageID()->cn + 1;
					$next_post = $this->images_model->lastTestImageID()->tn + 1;
					$new_file = 'test';
					$new_file .= '_'.$image['price_eur'];
					$new_file .= '_'.$image['supplier'];
					$new_file .= '_'.$image['sizes'];

					//add image to test images
				}else{
					if(isset($image['side_content'])){
						//should have some rules as well
						$next_image = $this->images_model->lastImageID()->cn + 1;
					}else{
						$next_image = $this->images_model->lastImageID()->cn + 1;
						$new_file = $image['code'];
						if(isset($image['additional_codes'])){
							$new_file .= $image['additional_codes'];
						}
						$new_file .= '_'.$image['type'];
						if(isset($image['brand'])){
							$new_file .= '_BR'.$brand;
						}
						if(isset($image['priority'])){
							$new_file .= '_('.$image['priority'].')';
						}else{
							$new_file .= '_(0)';
						}
					}
				}

				$new_file .= '_'.$next_image;
				$image['file_name'] = $new_file;

				$new_file .= '.'.$image['file_ext'];
				/*
				$path_to_copy = APPPATH.'../uploads/'.$image['code'];
				if(!file_exists($path_to_copy)){
					mkdir($path_to_copy, 0777, true);
				}
				copy($path_to_unsorted.'/'.$file, $path_to_copy.'/'.$new_file);*/

				//url
				$image['url'] = base_url().'uploads/'.$image['code'].'/'.$new_file;

				if(!isset($image['brand'])){
					$image['brand'] = 11;
				}
				//insert image into the db
				//$this->images_model->insertImageRow($image);

				//delete the original file
				//unlink(realpath($path_to_unsorted).'/'.$file);
			}else{
				/*echo "<pre>";
				var_dump($image);
				echo "</pre>";*/
			}
		}

		echo "<pre>";
		var_dump($images);
		echo "</pre>";
	}

	public function getfewimages(){
		$sql1 = "select *
					from images
					order by id DESC
					limit 35
				";
		return $this->db->query($sql1)->result();
	}

	public function getbackimages(){
		$images = $this->getfewimages();
		/*echo "<pre>";
		var_dump($images);
		echo "</pre>";*/
		$i=0;
		foreach($images as &$image){
			if($image->brand == 11){
				$brand = $this->images_model->findBrandById(1);
		/*echo "<pre>";
		var_dump($brand);
		echo "</pre>";*/
				//copy
				$data['file_path'] = APPPATH.'../uploads/'.$image->code.'/';
				$data['file_name'] = $image->file_name;
				$data['file_ext'] = $image->file_ext;

				$data2['file_path'] = APPPATH.'../uploads/temp/';
				$data2['file_name'] = $image->code.'_'.$image->type.'_BR'.$brand->abbreviation.'_('.$image->priority.')_'.$image->id;
				$data2['file_ext'] = $image->file_ext;

				$file_from = $data['file_path'].$data['file_name'].'.'.$data['file_ext'];
				$file_to = $data2['file_path'].$data2['file_name'].'.'.$data2['file_ext'];
				copy($file_from, $file_to);
				//brand it
				$this->image->brandImage2($data2, $image->code, $brand);
			}
			$i++;
		}
	}

	/*
	 * by Stefan
	 * Uploads all images and insert them into the DB
	 * TO DO ADD VALIDATIONS, requires a lot complex validation on filenames - somewhat achieved
	 * TO DO Multiple codes, it drops them currently
	 */
	public function bulkUpload(){
		//set path
		$path_to_unsorted = APPPATH.'../uploads/unsorted/standart';

		//get all frolders and files
		$files = scandir($path_to_unsorted);

		$i = 0;
		$images = array();

		foreach($files as $file){
			if($file != '.' && $file != '..'){ //skip first '.' and '..'
				$images[$i] = array();

				//extract code, type, brand and all other commands
				$file1 = strtoupper($file);
				$file1 = str_replace(' ', '', $file1); //sometimes there is a space before priority
				$file1 = str_replace('(', '_(', $file1); //in case there is no underscore before priority
				$file1 = preg_replace('/_+/', '_', $file1); //in case there are multiple underscores

				$error = array();

				//file extension
				$file_ext = pathinfo($file1, PATHINFO_EXTENSION);
				$images[$i]['file_ext'] = $file_ext;
				if($images[$i]['file_ext'] == 'GIF' || $images[$i]['file_ext'] == 'PNG' || $images[$i]['file_ext'] == 'JPG' || $images[$i]['file_ext'] == 'JPEG'){
					//proper format
				}else{
					//wrong format
					$error[] = "wrong extension";
				}

				//file_name
				$file_name = pathinfo($file1, PATHINFO_BASENAME);
				$file_name = rtrim($file_name, $file_ext);
				$file_name = rtrim($file_name, '.');
				$images[$i]['file_name'] = $file_name;

				$file_name_array = explode('_', $file_name);

				$images[$i]['additional_codes'] = '';
				foreach($file_name_array as $cmd){
					//check what text you have in the filename array
					switch($cmd){
						case substr($cmd, 0, 2) === "BR":{
							//brand
							if(isset($images[$i]['brand'])){
								//multiple brands
								$error[] = "multiple brands";
							}else{
								$brand = substr($cmd, 2);
								$brand_id = $this->images_model->findBrandIDByName($brand)->id;
								if($brand_id){
									$images[$i]['brand'] = $brand_id;
								}else{
									$error[] = "not existing brand";
								}
							}
							break;
						}
						case substr($cmd, 0, 1) === '(':{
							//priority
							if(isset($images[$i]['priority'])){
								$error[] = "multiple priorities";
							}else{
								$images[$i]['priority'] = ltrim(rtrim($cmd, ')'), '(');
							}
							break;
						}
						case $this->images_model->cmdCheck($cmd):{
							//command
							if(isset($images[$i]['type'])){
								$error[] = "multiple commands";
							}else
								$images[$i]['type'] = $cmd;
							break;
						}
						case $this->images_model->sideContentCheck($cmd):{
							//command
							if(isset($images[$i]['side_content'])){
								$error[] = "multiple side content tags";
							}else
								$images[$i]['side_content'] = $cmd;
							break;
						}
						case $this->images_model->codeCheck($cmd):{
							//code
							if(isset($images[$i]['code'])){
								$images[$i]['additional_codes'] .= '_';
								$images[$i]['additional_codes'] .= $cmd;
								//it has to trigger another function call for this particular image
								$error[] = "multiple codes";
							}else
								$images[$i]['code'] = $cmd;
							break;
						}
						default :{
							$error[] = "unknown symbols<br />";
							break;
						}
					}
				}

				//check fields
				if(!isset($images[$i]['code']))
					$error[] = "missing code";
				if(!isset($images[$i]['type']))
					$error[] = "missing type";

				if(empty($error)){
					//copy the image into the appropriate folder
					//gets lattest id, so we wont have duplicates
					$next_image = $this->images_model->lastImageID()->cn + 1;
					//create file name, RECREATE IT TO STANDARTIZE IT
					$new_file = $images[$i]['file_name'].'_'.$next_image.'.'.$images[$i]['file_ext'];

					$new_file = $images[$i]['code'];
					if($images[$i]['additional_codes'] != '')
						$new_file .= $images[$i]['additional_codes'];
					$new_file .= '_'.$images[$i]['type'];
					if(isset($images[$i]['brand']))
						$new_file .= '_BR'.$brand;
					if(isset($images[$i]['priority']))
						$new_file .= '_('.$images[$i]['priority'].')';
					else
						$new_file .= '_(0)';

					$new_file .= '_'.$next_image;
					$images[$i]['file_name'] = $new_file;

					$new_file .= '.'.$images[$i]['file_ext'];

					$path_to_copy = APPPATH.'../uploads/'.$images[$i]['code'];
					if(!file_exists($path_to_copy)){
						mkdir($path_to_copy, 0777, true);
					}
					copy($path_to_unsorted.'/'.$file, $path_to_copy.'/'.$new_file);

					//url
					$images[$i]['url'] = base_url().'uploads/'.$images[$i]['code'].'/'.$new_file;

					if(!isset($images[$i]['brand'])){
						$images[$i]['brand'] = 11;
					}
					//insert image into the db
					$this->images_model->insertImageRow($images[$i]);

					//delete the original file
					unlink(realpath($path_to_unsorted).'/'.$file);

				}else{
					echo "<pre>";
					var_dump($images[$i]);
					echo "error types<br />";
					var_dump($error);
					echo "</pre>";
				}

				$i++;
			}
		}
	}

	/*
	 * by Stefan
	 * Uploads all images and insert them into the DB
	 * TO DO ADD VALIDATIONS, requires a lot complex validation on filenames - somewhat achieved
	 * TO DO Multiple codes, it drops them currently
	 */
	public function maxBranding(){
		//set path
		$path_to_unsorted = APPPATH.'../uploads/unsorted/maxbrand';

		//get all frolders and files
		$files = scandir($path_to_unsorted);

		$i = 0;
		$images = array();

		foreach($files as $file){
			if($file != '.' && $file != '..'){ //skip first '.' and '..'
				$images[$i] = array();

				//extract code, type, brand and all other commands
				$file1 = strtoupper($file);
				$file1 = str_replace(' ', '', $file1); //sometimes there is a space before priority
				$file1 = str_replace('(', '_(', $file1); //in case there is no underscore before priority
				$file1 = preg_replace('/_+/', '_', $file1); //in case there are multiple underscores

				$error = array();

				//file extension
				$file_ext = pathinfo($file1, PATHINFO_EXTENSION);
				$images[$i]['file_ext'] = $file_ext;
				if($images[$i]['file_ext'] == 'GIF' || $images[$i]['file_ext'] == 'PNG' || $images[$i]['file_ext'] == 'JPG'){
					//proper format
				}else{
					//wrong format
					$error[] = "wrong extension";
				}

				//file_name
				$file_name = pathinfo($file1, PATHINFO_BASENAME);
				$file_name = rtrim($file_name, $file_ext);
				$file_name = rtrim($file_name, '.');
				$images[$i]['file_name'] = $file_name;

				$file_name_array = explode('_', $file_name);

				$images[$i]['additional_codes'] = '';
				foreach($file_name_array as $cmd){
					//check what text you have in the filename array
					switch($cmd){
						case substr($cmd, 0, 2) === "BR":{
							//brand
							if(isset($images[$i]['brand'])){
								//multiple brands
								$error[] = "multiple brands";
							}else{
								$brand = substr($cmd, 2);
								$brand_id = $this->images_model->findBrandIDByName($brand)->id;
								if($brand_id){
									$images[$i]['brand'] = $brand_id;
								}else{
									$error[] = "not existing brand";
								}
							}
							break;
						}
						case substr($cmd, 0, 1) === '(':{
							//priority
							if(isset($images[$i]['priority'])){
								$error[] = "multiple priorities";
							}else{
								$images[$i]['priority'] = ltrim(rtrim($cmd, ')'), '(');
							}
							break;
						}
						case $this->images_model->cmdCheck($cmd):{
							//command
							if(isset($images[$i]['type'])){
								$error[] = "multiple commands";
							}else
								$images[$i]['type'] = $cmd;
							break;
						}
						case $this->images_model->sideContentCheck($cmd):{
							//command
							if(isset($images[$i]['side_content'])){
								$error[] = "multiple side content tags";
							}else
								$images[$i]['side_content'] = $cmd;
							break;
						}
						case $this->images_model->codeCheck($cmd):{
							//code
							if(isset($images[$i]['code'])){
								$images[$i]['additional_codes'] .= '_';
								$images[$i]['additional_codes'] .= $cmd;
								//it has to trigger another function call for this particular image
								$error[] = "multiple codes";
							}else
								$images[$i]['code'] = $cmd;
							break;
						}
						default :{
							$error[] = "unknown symbols<br />";
							break;
						}
					}
				}

				//check fields
				if(!isset($images[$i]['code']))
					$error[] = "missing code";
				if(!isset($images[$i]['type']))
					$error[] = "missing type";

				if(empty($error)){
					//get product by code
					$product = $this->product_model->findByCode($images[$i]['code']);
					$product_id = $product['id'];
					$sql = "select *
								from product_prices
								where product_id = '$product_id'
									and country_id = '3'";
					$price_array = $this->db->query($sql)->result_array();
					$price = $price_array[0]['price'];


					$new_file = '_'.$price.'eu';
					$new_file .= '_'.$images[$i]['code'];

					if(isset($images[$i]['priority']))
						$new_file .= '_'.$images[$i]['priority'];
					else
						$new_file .= '_0';
					$images[$i]['file_name'] = $new_file;

					$new_file .= '.'.$images[$i]['file_ext'];

					$path_to_copy = APPPATH.'../uploads/unsorted/maxbrand/down/';
					if(!file_exists($path_to_copy)){
						mkdir($path_to_copy, 0777, true);
					}
					copy($path_to_unsorted.'/'.$file, $path_to_copy.'/'.$new_file);

					$source_image = $path_to_copy.'/'.$new_file;

					$brand = $this->images_model->findBrandIDByShortName('maximiliano');
					$this->image->putBrandMaximiliano($source_image, $product['code'], $brand);
					$this->image->putCode($source_image, $product['code']);

					//url
					$images[$i]['url'] = base_url().'uploads/'.$images[$i]['code'].'/'.$new_file;

					if(!isset($images[$i]['brand'])){
						$images[$i]['brand'] = 11;
					}

					//delete the original file
					unlink(realpath($path_to_unsorted).'/'.$file);
				}else{
					echo "<pre>";
					var_dump($images[$i]);
					echo "error types<br />";
					var_dump($error);
					echo "</pre>";
				}
				$i++;
			}
		}
	}

	public function gallery(){
		$sql1 = "select *
					from images
				";
		$images = $this->db->query($sql1)->result_array();
		echo "<pre>";
		var_dump($images);
		echo "</pre>";
	}

	public function testupload(){
		//set path
		$path_to_unsorted = APPPATH.'../uploads/test';

		//get all frolders and files
		$files = scandir($path_to_unsorted);

		$i = 0;
		$images = array();

		echo "<pre>";
		var_dump($files);
		echo "</pre>";
	}

	public function brandTest(){
		//rewrite this
		//get images from the new table
		$product_id = 1;
		$brand_name = '420BG';


		$product = $this->product_model->findById($product_id);
		$pid = $product['id'];
		$code = $product['code'];
		//$brand_abbreviation = $post->brand_name;
		$brand = $this->images_model->findBrandByName($brand_name);

		//1. list all front images for this product
		$sql1 = "select *
					from images
					where code = '$code'
						and type = 'F'
						and brand = 11
					order by times_posted";

		$res1 = $this->db->query($sql1)->result();
		//2. pick an image
		$image = $res1[0]; //image with leasts posts
		//will be upgraded for using priority
		//copy image with new name
		$data['file_path'] = APPPATH.'../uploads/'.$code.'/';
		$data['file_name'] = $image->file_name;
		$data['file_ext'] = $image->file_ext;

		$file_from = $data['file_path'].$data['file_name'].'.'.$data['file_ext'];
		$file_to = $data['file_path'].$data['file_name'].'_temp.'.$data['file_ext'];
		copy($file_from, $file_to);

		$this->image->brandImage($data, $code, $brand);
	}
}
