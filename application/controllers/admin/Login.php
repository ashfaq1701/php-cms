<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_Controller {
	
	protected 
		$load_models = array('user_model'),
		$load_helpers = array('form'),
		$load_libraries = array('form_validation');
	
	/**
	 * @return void
	 */
	public static function getActions() {}
	
	public function index() {
		$errors = '';
		
		if ($this->isPost()) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_auth_user');
			
			if ($this->form_validation->run()) {
				redirect('admin/home');
			}
				
			$errors = validation_errors();
		}
		
		return $this->render('form', array(
			'title' => 'Control panel | Sign-in',
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'validation_errors' => $errors
		));
	}
	
	public function logout() {
		$this->session->unset_userdata('admin_user');
		session_destroy();
		
		redirect('admin/login');
	}
	
	public function auth_user($password) {
		$username = $this->input->post('username');
		
		if ($data = $this->user_model->findUserForLogin($username, $password)) {
			$this->session->set_userdata('admin_user', $data);
			
			return true;
		}
		else {
			$this->form_validation->set_message('auth_user', 'Invalid user or password');
		
			return false;
		}
	}
	
}
