<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Order
 *
 * @author chavdar
 */
class Order extends Admin_Controller {

	protected
		$load_models = array('order_model', 'country_model', 'size_model', 'product_model', 'facebook_model', 'user_model'),
		$load_helpers = array('form', 'url'),
		$load_libraries = array('form_validation', 'pagination');

	// //added by danny test
	public function findById() {
		$id = $_GET['id'];

				$FacebookPage = $this->facebook_model->findById($id);
				$Cid = $FacebookPage['country'];

			$CountryAr = $this->country_model->findById($Cid);

			$CoName = $CountryAr['id'];
			echo($CoName);

		//return $tt;
		}

	public function GiveDeliveryPriceByFClist(){
		$id = $_GET['id'];

			$FacebookPage = $this->facebook_model->findById($id);
			$Cid = $FacebookPage['country'];

			$CountryAr = $this->country_model->findById($Cid);

			$CoDeliveryPrice = $CountryAr['delivery_cost'];

		echo($CoDeliveryPrice);
	}

    public function __construct(){
        parent::__construct();
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;
		if($user_confirmed == 0 || $user_confirmed == null){
			redirect('admin/schedule');
		}
    }

	public function index(){
		if($this->input->is_ajax_request() && ($params = $this->input->post())){
			$response = array();

			$current_page = isset($params['current_page']) ? intval($params['current_page']) : 0;
			$term = $this->input->post('term');
			$this->pagination->initialize($this->order_model->getConfigForPagination());

			$pagination = $this->pagination->create_links();
			$response['success'] = true;
			$response['pagination'] = $pagination;
			$response['content'] = $this->twig->render('admin/order/order_table', array(
				'is_ajax' => true,
				'orders' => $this->order_model->getCollection($current_page),
				'pagination' => $pagination,
				'_order_filter' => $term
			));

			print json_encode($response);
		}else{
			$user_data = UserHelper::getCurrentUserData();
			$this->pagination->initialize($this->order_model->getConfigForPagination());

			$users_select = array('all_users' => 'All users');
			$users_select = $users_select + $this->user_model->findUsersForSelect();

			$this->render('index', array(
				'title' => 'List orders',
				'orders' => $this->order_model->getCollection($this->uri->segment(3)),
				'pagination' => $this->pagination->create_links(),
				'users_select' => $users_select,
				'user_data' => $user_data
			));
		}
	}

	public function create(){
		$errors = '';
		$user_data = UserHelper::getCurrentUserData();
		$initial_order_time = new DateTime();

		$return_order_id = (int)$this->input->get('return_order');
		if($return_order_id) {
			if (! $object = $this->order_model->findById($return_order_id)) {
				show_404();
			}
		}else{
			$object = new stdClass();
		}

		if($this->isPost()){
			$initial_order_time = $this->input->post('initial_order_time');
			if($this->form_validation->run('order')){
				$initial_order_time_object = new Datetime($initial_order_time);
				$current_time_object = new Datetime();

				$create_order_time = $initial_order_time_object->diff($current_time_object);
				$minutes = $create_order_time->days * 24 * 60;
				$minutes += $create_order_time->h * 60;
				$minutes += $create_order_time->i;

				$data['customer_name'] = $this->input->post('customer_name');
				$data['country_id'] = $this->input->post('country');
				$data['email'] = $this->input->post('email');
				$data['phone'] = $this->input->post('phone');
				$data['post_code'] = $this->input->post('post_code');
				$data['city'] = $this->input->post('city');
				$data['address'] = $this->input->post('address');
				$data['note'] = $this->input->post('note');
				$data['delivery_cost'] = $this->input->post('delivery_cost');
				$data['discount'] = $this->input->post('discount');
				$data['discount_percentage'] = $this->input->post('discount_percentage');
				$data['created_by_id'] = $user_data['id'];
				$data['status'] = 1;
				$data['date_added'] = $initial_order_time_object->format("Y-m-d H:i:s");
				$data['facebook_page_id'] = $this->input->post('facebook_page');
				$data['facebook_customer_profile_url'] = $this->input->post('customer_facebook_url');
				$data['create_order_time'] = $minutes;
				if($return_order_id){
					$data['return_order_id'] = $object->id;
				}

				if($order_id = $this->order_model->insertRow($data)){
					$total = 0;
					$products = $this->input->post('order')['products'];
					$dataProducts = array();
					foreach($products as $product){
						$dataProducts[] = array(
							'order_id' => $order_id,
							'product_id' => $product['product_id'],
							'quantity' => $product['quantity'],
							'size_id' => $product['size'],
							'notified' => 0,
							'is_shipped' => 0
						);
						$total = $total + $product['price'] * $product['quantity'];
					}

					foreach($dataProducts as $dataProduct){
						$this->order_model->insertProductRow($dataProduct);
					}
					$total = ($total - ($total * ($data['discount_percentage'] / 100))) - $data['discount'];
					$total = $total + $data['delivery_cost'];

					$total_history = new stdClass();
					$total_history->total = $total;
					$total_history->data = $data;
					$total_history->products = $dataProducts;

					$updateData = array(
						'total' => $total,
						'total_history' => json_encode($total_history)
						);

					$this->order_model->updateRow($order_id, $updateData);

					$this->recalculateTotal($order_id);

					redirect('admin/orders');
				}
			}else{
				$errors = validation_errors();
			}
		}else{
			$object->discount = 0;
			$object->discount_percentage = 0;
		}

		$this->render('form', array(
			'object' => $object,
			'title' => 'Create order',
			'countries' => $this->country_model->getSelectCollection(),
			'validation_errors' => $errors,
			'edit_action' => false,
			'facebook_pages' => $this->facebook_model->getSelectCollection(),
			'initial_order_time' => $initial_order_time
		));
	}

	public function edit($id){
		$errors = '';
		if(!$object = $this->order_model->findById($id)){
			show_404();
		}

		if($this->isPost()){
			if ($this->form_validation->run('order')) {
				$data['customer_name'] = $this->input->post('customer_name');
				$data['country_id'] = $this->input->post('country');
				$data['email'] = $this->input->post('email');
				$data['phone'] = $this->input->post('phone');
				$data['post_code'] = $this->input->post('post_code');
				$data['city'] = $this->input->post('city');
				$data['address'] = $this->input->post('address');
				$data['note'] = $this->input->post('note');
				$data['delivery_cost'] = $this->input->post('delivery_cost');
				$data['discount'] = $this->input->post('discount');
				$data['discount_percentage'] = $this->input->post('discount_percentage');
				$data['facebook_page_id'] = $this->input->post('facebook_page');
				$data['facebook_customer_profile_url'] = $this->input->post('customer_facebook_url');

				if ($this->order_model->updateRow($id, $data)){
					if(isset($this->input->post('order')['products'])){
						$products = $this->input->post('order')['products'];

						$dataProducts = array();
						foreach($products as $product){
							$dataProducts[] = array(
								'order_id' => $id,
								'product_id' => $product['product_id'],
								'quantity' => $product['quantity'],
								'size_id' => $product['size'],
								'notified' => 0,
								'is_shipped' => 0
							);
						}

						foreach($dataProducts as $dataProduct){
							$this->order_model->insertProductRow($dataProduct);
						}
					}

					// recalculate total
					$this->recalculateTotal($id);
					redirect('admin/orders');
				}
			}else{
				$errors = validation_errors();
			}
		}
		$products = $this->order_model->getOrderProducts($id, $object->country_id);
		$sizes = $this->size_model->getSelectCollection();
		$this->render('form', array(
			'title' => 'Edit order',
			'object' => $object,
			'products' => $products,
			'countries' => $this->country_model->getSelectCollection(),
			'countries_selected' => array($object->country_id),
			'status_choices' => Order::getAvailableStatusChoices(),
			'sizes' => $sizes,
			'edit_action' => true,
			'validation_errors' => $errors,
			'order_comments' => $this->order_model->getComments($id),
			'facebook_pages' => $this->facebook_model->getSelectCollection(),
			'facebook_page_selected' => $object->facebook_page_id
		));
	}

	private function recalculateTotal($id) {
		$order = $this->order_model->findById($id);
		$order_products = $this->order_model->getOrderProducts($id, $order->country_id);
		$total = 0;
		foreach ($order_products as $o_product) {
			$total = $total + $o_product['price'] * $o_product['quantity'];
		}
		$total = ($total - ($total * ($order->discount_percentage / 100))) - $order->discount;
		$total = $total + $order->delivery_cost;
		$data = array(
			'total' => $total
		);

		$this->order_model->updateRow($id, $data);
	}

	public function removeProduct($order_id, $product_id, $size_id) {
		$order_product_row = $this->order_model->getOrderProduct($order_id, $product_id, $size_id);
		$quantity = $order_product_row->quantity;
		$this->order_model->removeOrderProduct($order_id, $product_id, $size_id);
		$this->recalculateTotal($order_id);
//		$product_size_row = $this->product_model->getProductSizeRowByPS($product_id, $size_id);
//		$data = array(
//			'quantity' => $product_size_row->quantity + $quantity
//		);
//
//		$this->product_model->updateProductSizeRow($product_size_row->id, $data);

		redirect('admin/orders/edit/'.$order_id);
	}

	public function changeProduct($order_id, $product_id, $size_id) {
		$order_product_row = $this->order_model->getOrderProduct($order_id, $product_id, $size_id);
		$sizes = $this->size_model->getSelectCollection();
		$errors = '';

		if($this->isPost()){
			$quantity = $this->input->post('quantity');
			if(!is_numeric($quantity)){
				$errors .= "Quantity is not a number<br />";
			}
			if($quantity < 1) {
				$errors .= "Quantity cannot be less than 1<br />";
			}
			if($this->form_validation->run('order_change_product') && strlen($errors) == 0) {
				$data = array(
					'quantity' => $quantity,
					'size_id' => $this->input->post('size')
				);

				$this->order_model->updateOrderProductRow($order_product_row->id, $data);
				$this->recalculateTotal($order_id);
				redirect("admin/orders/edit/$order_id");
			} else {
				$errors .= validation_errors();
			}
		}
		$this->render('product_form', array(
			'sizes' => $sizes,
			'size_selected' => $order_product_row->size_id,
			'object' => $order_product_row,
			'validation_errors' => $errors
		));
	}

	public function delete($id) {
		echo $id;
	}

	public function order_autocomplete() {
		if(!$this->input->is_ajax_request()) {
			show_404();
		}

		$term = $this->input->post('term');
		$country = $this->input->post('country');
		if (empty($term)) {
			$response = array('success '=> false);
		}
		else {
			$term = filter_var($term, FILTER_SANITIZE_STRING);
			$sizes = $this->size_model->getCollection();
			$products = $this->order_model->findProductsForAutocomplete($term, $country);

			$dataProducts = array();

			foreach($products as &$product){
				$reserved_quantity_response = $this->order_model->getProductReservedQuantity($product['id'], $product['size_id']);
					if($reserved_quantity_response->reserved_quantity == null){
						$reserved_quantity = 0;
					} else {
						$reserved_quantity = $reserved_quantity_response->reserved_quantity;
					}


				$product['sizes'] = $sizes;
				if(array_key_exists($product['id'], $dataProducts)){
					$dataProducts[$product['id']]['sizes'][] = array(
						'size_id' => $product['size_id'],
						'size_name' => $product['size_name'],
						'quantity' => $product['quantity'],
						'reserved_quantity' => $reserved_quantity
					);
				} else {
//					$reserved_quantity_response = $this->order_model->getProductReservedQuantity($product['id'], $product['size_id']);
//					if($reserved_quantity_response->reserved_quantity == null){
//						$reserved_quantity = 0;
//					} else {
//						$reserved_quantity = $reserved_quantity_response->reserved_quantity;
//					}
					$prepareProduct = array(
						'id' => $product['id'],
						'name' => $product['name'],
						'code' => $product['code'],
						'supplier_id' => $product['supplier_id'],
                        'currency' => $product['currency'],
						'sizes' => array(
							array(
								'size_id' => $product['size_id'],
								'size_name' => $product['size_name'],
								'quantity' => $product['quantity'],
								'reserved_quantity' => $reserved_quantity
							),
						),
						'price' => $product['price']
 					);
					$dataProducts[$product['id']] = $prepareProduct;
//					$dataProducts[$product['id']] = array('sizes' => array('1'));
				}

			}

			$response = $dataProducts;
		}

		echo json_encode($response);
		die();
	}

	public function order_add_row() {
		if(!$this->input->is_ajax_request()) {
			show_404();
		}

		$product = $this->input->post('product');

		$product_sizes_description = $this->product_model->getSizesDescription($product['id']);
		$product_image = $this->product_model->getMainImage($product['id']);

		$data = array(
			'item' => $product,
			'table_rows' => $this->input->post('table_rows'),
			'product_sizes_description' => $product_sizes_description,
			'image' => $product_image
		);
		$tableRow = $this->twig->render('admin/order/product_table_row', $data);
		echo $tableRow;
		die();
	}

	public function handle_user_comment() {
		$response = array();

		if ($this->input->is_ajax_request() && ($params = $this->input->post()) && isset($params['id'])) {
			$user_data = UserHelper::getCurrentUserData();
			$data = array(
				'order_id' => intval($params['id']),
				'user_id' => $user_data['id'],
				'comment' => $params['comment'],
				'created_at' => date('Y-m-d H:i:s')
			);

			if ($this->order_model->saveComment($data)) {
				$response['success'] = true;
				$response['content'] = $this->twig->render('admin/order/_user_comments_block', array(
					'order_comments' => $this->order_model->getComments($params['id'])
				));
			}
		}

		print json_encode($response);
	}

	public function handle_delivery_cost() {
		$response = array();

		if($this->input->is_ajax_request() && ($params = $this->input->post()) && isset($params['country_id'])){
			$country = $this->country_model->findById($params['country_id']);
			if($country){
				$response['success'] = true;
				$response['content'] = $country['delivery_cost'];
			}
		}

		print json_encode($response);
	}

	public function load_comments($order_id) {
		$response = array();

		if ($this->input->is_ajax_request() && !empty($order_id)) {
			$response['comments'] = $this->twig->render('admin/partials/_list_table_comments_block.twig', array(
				'comments' => $this->order_model->getComments(intval($order_id))
			));
		}

		exit(json_encode($response));
	}

	/**
	 *
	 * @return array
	 */
	public static function getAvailableStatusChoices() {
		return array(
			1 => 'Pending',
			2 => 'Completed',
			3 => 'Rejected',
			4 => 'Cancelled'
		);
	}

	public function changeOrderStatus($orderId, $statusId) {
		switch ($statusId) {
			case 1: break;
			case 2:
				// Completed
				$this->changeOrderStatusToComplete($orderId);
				break;
			case 3:
				// Rejected
				$this->changeOrderStatusToRejected($orderId);
				break;
			case 4:
				// Canceled
				$this->changeOrderStatusToCanceled($orderId);
				break;
			case 5:
				// Missing Product
				$this->changeOrderStatusToMiisingProduct($orderId);
				break;
			default:
				break;
		}
		redirect('admin/orders');
	}

    private function changeOrderStatusToComplete($orderId) {
        $productsVariations = $this->order_model->getOrderProducts($orderId);
        foreach ($productsVariations as $productVariation) {
            if ($productVariation['is_shipped'] == 0) {
                $variation_quantity = $productVariation['quantity'];
                $sizeRow = $this->product_model->getProductSizeRow($productVariation['product_id'], $productVariation['size_id']);
                $data = array(
                  'quantity' => $sizeRow->quantity - $variation_quantity
                );
                $this->product_model->updateSizeRow($sizeRow->id, $data);

                $orderProductData = array(
                  'is_shipped' => 1
                );
                $this->order_model->updateOrderProductRow($productVariation['id'], $orderProductData);
            }
        }
        $orderData = array(
          'status' => 2
        );
        $this->order_model->updateRow($orderId, $orderData);
    }

    private function changeOrderStatusToRejected($orderId) {
        // each product variation is a represented from product
        // contained in the order
        $productVariations = $this->order_model->getOrderProducts($orderId);
        foreach ($productVariations as $productVariation) {
            if ($productVariation['is_shipped'] == 1) {
                $productSizeRow = $this->product_model->getProductSizeRow($productVariation['product_id'], $productVariation['size_id']);
                $oldQuantity = $productSizeRow->quantity;
                $newQuantity = $oldQuantity + $productVariation['quantity'];
                $data = array(
                  'quantity' => $newQuantity
                );
                $this->product_model->updateSizeRow($productSizeRow->id, $data);
            }
        }
        $orderData = array(
          'status' => 3
        );
        $this->order_model->updateRow($orderId, $orderData);
		/*
		$orderProductData = array(
		  'is_shipped' => 1
		);
		$this->order_model->updateOrderProductRow($productVariation['id'], $orderProductData);
		*/
    }

    private function changeOrderStatusToCanceled($orderId) {
        // each product variation is a represented from product
        // contained in the order
        $productVariations = $this->order_model->getOrderProducts($orderId);
        foreach ($productVariations as $productVariation) {
            if ($productVariation['is_shipped'] == 1) {
                $productSizeRow = $this->product_model->getProductSizeRow($productVariation['product_id'], $productVariation['size_id']);
                $oldQuantity = $productSizeRow->quantity;
                $newQuantity = $oldQuantity + $productVariation['quantity'];
                $data = array(
                  'quantity' => $newQuantity
                );
                $this->product_model->updateSizeRow($productSizeRow->id, $data);
            }
        }
        $orderData = array(
          'status' => 4
        );
        $this->order_model->updateRow($orderId, $orderData);

    }

    private function changeOrderStatusToMiisingProduct($orderId) {
        // each product variation is a represented from product
        // contained in the order
        $productVariations = $this->order_model->getOrderProducts($orderId);
        foreach ($productVariations as $productVariation) {
            if ($productVariation['is_shipped'] == 1) {
                $productSizeRow = $this->product_model->getProductSizeRow($productVariation['product_id'], $productVariation['size_id']);
                $oldQuantity = $productSizeRow->quantity;
                $newQuantity = $oldQuantity + $productVariation['quantity'];
                $data = array(
                  'quantity' => $newQuantity
                );
                $this->product_model->updateSizeRow($productSizeRow->id, $data);
            }
        }
        $orderData = array(
          'status' => 5
        );
        $this->order_model->updateRow($orderId, $orderData);

    }

    public function order_customer_autocomplete() {
		if(!$this->input->is_ajax_request()) {
			show_404();
		}

		$term = $this->input->post('term');

		if (empty($term)) {
			$response = array('success '=> false);
		}
		else {
			$term = filter_var($term, FILTER_SANITIZE_STRING);
			$customer_names = $this->order_model->findCustomerNamesForAutocomplete($term);
			$response = $customer_names;
		}

		echo json_encode($response);
		die();
	}

	//temp - deprecated
	public function cleanuporders()
	{
		$sql1 = "SELECT *
					FROM orders
					WHERE status = '5'
			";

		$res1 = $this->db->query($sql1)->result();

		echo "<pre>";
		var_dump($res1);
		echo "</pre>";

		foreach($res1 as $row1)
		{
			$order_id = $row1->id;
			$sql2 = "SELECT *
						FROM orders_comments
						WHERE order_id = '$order_id'
							AND comment LIKE CONCAT('%', 'returned', '%')
				";
			$res2 = $this->db->query($sql2)->result();

			echo "<pre>";
			if($res2)
			{
				var_dump($res2);
				//$sql3 = "UPDATE orders SET status='3' WHERE id='$order_id'";
				//$this->db->query($sql3);
			}
			else
			{
				//change order status to 4
				//$sql4 = "UPDATE orders SET status='4' WHERE id='$order_id'";
				//$this->db->query($sql4);
			}
			echo "</pre>";
		}
	}
}
