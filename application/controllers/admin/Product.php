<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Description of Product
 *
 * @author chavdar
 */
class Product extends Admin_Controller {

	protected
		$load_models = array('product_model', 'size_model', 'country_model', 'images_model', 'user_model', 'facebook_model'),
		$load_helpers = array('form', 'url'),
		$load_libraries = array('form_validation', 'image', 'money', 'pagination', 'facebooklib'),
		$has_filter = true;

        protected $action = '';

    public function __construct(){
        parent::__construct();
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;
		if($user_confirmed == 0 || $user_confirmed == null){
			redirect('admin/schedule');
		}
    }

	public function index(){
		if(strpos($_SERVER['QUERY_STRING'], '&submit=1') !== false){
			$query_string = str_replace('&submit=1', '', $_SERVER['QUERY_STRING']);
			redirect(base_url('admin/products').'/date_added/asc?'. $query_string);
		}

		$current_page = $this->uri->segment(5);
		$this->pagination->initialize($this->product_model->getConfigForPagination(false));

		$current_page = ($current_page == null OR $current_page < 1) ? 1 : $current_page;
		$order_seg = $this->uri->segment(4,"asc");
		if($order_seg == "asc") $order_res = "desc"; else $order_res = "asc";

		$categories_array = $this->product_model->getCategories();
		$i = 0;
		foreach($categories_array as $category){
			$categories[$i] = $category->slug;
			$i++;
		}

		if(!$this->session->userdata["admin_user"]["super_admin"]){
			return $this->render('index', array(
				'title' => 'List products',
				'suppliers' => $this->product_model->findSuppliersForSelect(),
				'categories' => $categories,
				'products' => $this->product_model->getCollection($current_page),
				'pagination' => $this->pagination->create_links(),
				'current_page' => $current_page,
				'order_res' => $order_res
			));
		}else{
			return $this->render('admin_index', array(
				'title' => 'List products',
				'suppliers' => $this->product_model->findSuppliersForSelect(),
				'categories' => $categories,
				'products' => $this->product_model->getCollection($current_page),
				'pagination' => $this->pagination->create_links(),
				'current_page' => $current_page,
				'order_res' => $order_res
			));
		}
	}

	/*
	 * by Stefan
	 * Just for info on Product (not editable)
	 * Displaying Single product (Pinder)
	 * Needs optimizations on next and previous products
	 * Needs moving function to the model
	 */
	public function productInfo($product_id){
		if($this->input->post()){
			$data = $this->input->post();
			//get product by code
			$product = $this->product_model->findByCode($data['code']);
			if($product['id']){
				$product_id = $product['id'];
				redirect('admin/product/productinfo/'.$product_id);
			}else{
				redirect('admin/product/productinfo/1');
				show_404();
			}
		}
		$pr = $this->product_model->findById($product_id);
		if($pr){
			$prices = $this->product_model->getProductPrices($product_id);
			$img = $this->product_model->getMainImage($product_id);
			$product['id'] = $product_id;
			$product['code'] = strtoupper($pr['code']);
			$images = $this->product_model->getAllImagesByCode($product['code']);
			$i=0;
			foreach($images as &$image){
				if($image->brand == 11){
					//copy
					$data['file_path'] = APPPATH.'../uploads/'.$product['code'].'/';
					$data['file_name'] = $image->file_name;
					$data['file_ext'] = $image->file_ext;

					$file_from = $data['file_path'].$data['file_name'].'.'.$data['file_ext'];
					$file_to = $data['file_path'].$data['file_name'].'_temp.'.$data['file_ext'];
					//if file exists
					if(!file_exists($file_to)){
						copy($file_from, $file_to);
						//brand it
						$brand = $this->images_model->findBrandById(3);
						$this->image->brandImage($data, $product['code'], $brand);
						//replace link
					}
					$images[$i]->url = base_url().'uploads/'.$image->code.'/'.$data['file_name'].'_temp.'.$data['file_ext'];
				}
				$i++;
			}

			foreach($prices as $price){
				if($price->country_id == 1){
					$product['price_bgn'] = $price->price;
					$product['price_stock_bgn'] = $price->price_stock;
					$product['currency_bgn'] = $price->currency;
				}
				if($price->country_id == 3){
					$product['price_eur'] = $price->price;
					$product['price_stock_eur'] = $price->price_stock;
					$product['currency_eur'] = $price->currency;
				}
				if($price->country_id == 2){
					$product['price_lei'] = $price->price;
					$product['price_stock_lei'] = $price->price_stock;
					$product['currency_lei'] = $price->currency;
				}
			}

			$sizes = $this->product_model->getProductSizes($product_id);

			$categories_array = $this->product_model->getCategories();
			$i = 0;
			foreach($categories_array as $category){
				$categories[$i] = $category->slug;
				$i++;
			}
			$fabrics = [
						'100% polyester',
						'95% viscouse 5% elastan',
						'95% cotton 5% elastan',
						'100% cotton',
						'70% cotton 5% lycra 25% polyester'
						];
			$fit = [
					'slim',
					'normal'
					];
			$cuffs = [
					'elastic ribbon',
					'hem',
					'none'
					];
			$size_incorrect = [
					'shorter',
					'smaller',
					'shorter-and-smaller',
					'normal size'
					];
			$quality = [
						'first',
						'second',
						'third'
						];

			$product['imagefile'] = @$img->filename;
			$product['imagefile_ext'] = @$img->ext;
			$product['name'] = $pr['name'];
			$product['category'] = $pr['category'];
			$product['weight'] = $pr['weight'];
			$product['cweight'] = $pr['weight'].' kg';
			$product['location'] = $pr['location'];
			$product['fit'] = $pr['fit'];
			$product['fabric'] = $pr['fabric'];
			$product['quality'] = $pr['quality'];
			$product['colors'] = $pr['colors'];
			$product['cuffs'] = $pr['cuffs'];
			$product['size_incorrect'] = $pr['size_incorrect'];
			$i = 0;
			foreach($sizes as $size){
				$product['sizes'][$i]['size_id'] = $size->size_id;
				$product['sizes'][$i]['name'] = $size->name;
				$product['sizes'][$i]['description'] = $size->description;
				if($size->description){
					$product['sizes'][$i]['cdescription'] = 'bust '.str_replace('/', ' cm / ', $size->description).' cm';
					$product['sizes'][$i]['cdescription'] = str_replace('...', ' cm ... ', $product['sizes'][$i]['cdescription']);
				}
				$product['sizes'][$i]['quantity'] = $size->quantity;
				$i++;
			}

			//get next and previous products
			$sql1 = "select id from products where id = (select min(id) from products where id > '$product_id')";
			$res1 = $this->db->query($sql1)->row();
			$sql2 = "select id from products where id = (select max(id) from products where id < '$product_id')";
			$res2 = $this->db->query($sql2)->row();
			$sql3 = "select MIN(id) as first, MAX(id) as last FROM products";
			$res3 = $this->db->query($sql3)->row();

			if($res2)
				$previous = $res2->id;
			else
				$previous = $res3->last;
			if($res1)
				$next = $res1->id;
			else
				$next = $res3->first;

			//get next and previous from series
			$series = substr($product['code'], 0, 2);

			$sql4 = "select id from products where id = (select min(id) from products where id > '$product_id' and left(code, 2) = '$series')";
			$res4 = $this->db->query($sql4)->row();
			$sql5 = "select id from products where id = (select max(id) from products where id < '$product_id' and left(code, 2) = '$series')";
			$res5 = $this->db->query($sql5)->row();
			$sql6 = "select MIN(id) as first, MAX(id) as last FROM products where left(code, 2) = '$series'";
			$res6 = $this->db->query($sql6)->row();

			if($res5)
				$previous_series = $res5->id;
			else
				$previous_series = $res6->last;
			if($res4)
				$next_series = $res4->id;
			else
				$next_series = $res6->first;

			$sql7 = "select id, code from products where left(code, 2) = '$series'";
			$all = $this->db->query($sql7)->result();
			$all_temp = array_slice($all, -6);
			array_splice($all, 0, 0, $all_temp);
			array_push($all, $all[5], $all[6], $all[7], $all[8], $all[9], $all[10]);
			$i = 0;
			$n = 0;
			foreach($all as $r){
				$result[$i] = $r->id;
				$n++;
				$i++;
			}
			for($i=6; $i<$n; $i++){
				if($result[$i] == $product_id){
					break;
				}
			}
			$next_six = array();
			$previous_six = array();

			for($j=0; $j<6; $j++){
				$next_six[$j]['id'] = $result[$i + $j +1];
				$previous_six[$j]['id'] = $result[$i -6 + $j];
			}
			for($i=0; $i<6; $i++){
				$next_six[$i] = $this->product_model->findById($next_six[$i]['id']);
				$nf_img = $this->product_model->getMainImage($next_six[$i]['id']);
				$next_six[$i]['imagefile'] = @$nf_img->filename;
				$next_six[$i]['imagefile_ext'] = @$nf_img->ext;
			}
			for($i=0; $i<6; $i++){
				$previous_six[$i] = $this->product_model->findById($previous_six[$i]['id']);
				$pf_img = $this->product_model->getMainImage($previous_six[$i]['id']);
				$previous_six[$i]['imagefile'] = @$pf_img->filename;
				$previous_six[$i]['imagefile_ext'] = @$pf_img->ext;
			}

			//get the HRK and HUF currencies values
			$product['price_hrk'] = ceil($this->money->convertAmount($product['price_eur'], 2 , 'EUR', 'HRK') * 1.02);
			$product['price_huf'] = ceil($this->money->convertAmount($product['price_eur'], 2 , 'EUR', 'HUF') * 1.02);

			//check if admin
			return $this->render('single_product_public', array(
					'title' => 'Product',
					'product' => $product,
					'next' => $next,
					'previous' => $previous,
					'next_series' => $next_series,
					'next_six' => $next_six,
					'series' => $series,
					'previous_series' => $previous_series,
					'previous_six' => $previous_six,
					'categories' => $categories,
					'fabrics' => $fabrics,
					'fit' => $fit,
					'cuffs' => $cuffs,
					'size_incorrect' => $size_incorrect,
					'quality' => $quality,
					'images' => $images
				));
		}

		redirect('admin/product/productinfo/1');
	}

	/*
	 * by Stefan
	 * Displaying Single product (Pinder)
	 */
	public function getProduct($product_id){
		if($this->input->post()){
			$data = $this->input->post();
			//get product by code
			$product = $this->product_model->findByCode($data['code']);
			$product_id = $product['id'];
			redirect('admin/products/p/' . $product_id);
		}
		$pr = $this->product_model->findById($product_id);
		if($pr){
			$prices = $this->product_model->getProductPrices($product_id);
			$img = $this->product_model->getMainImage($product_id);
			$product['id'] = $product_id;
			$product['code'] = strtoupper($pr['code']);
			$images = $this->product_model->getAllImagesByCode($product['code']);

			foreach($prices as $price){
				if($price->country_id == 1){
					$product['price_bgn'] = $price->price;
					$product['price_stock_bgn'] = $price->price_stock;
					$product['currency_bgn'] = $price->currency;
				}
				if($price->country_id == 3){
					$product['price_eur'] = $price->price;
					$product['price_stock_eur'] = $price->price_stock;
					$product['currency_eur'] = $price->currency;
				}
				if($price->country_id == 2){
					$product['price_lei'] = $price->price;
					$product['price_stock_lei'] = $price->price_stock;
					$product['currency_lei'] = $price->currency;
				}
			}

			$sizes = $this->product_model->getProductSizes($product_id);

			$categories_array = $this->product_model->getCategories();
			$i = 0;
			foreach($categories_array as $category){
				$categories[$i] = $category->slug;
				$i++;
			}
			$fabrics = [
						'100% polyester',
						'95% viscouse 5% elastan',
						'95% cotton 5% elastan',
						'100% cotton',
						'70% cotton 5% lycra 25% polyester'
						];
			$fit = [
					'slim',
					'normal'
					];
			$cuffs = [
					'elastic ribbon',
					'hem',
					'none'
					];
			$size_incorrect = [
					'shorter',
					'smaller',
					'shorter-and-smaller',
					'normal size'
					];
			$quality = [
						'first',
						'second',
						'third'
						];

			$product['imagefile'] = @$img->filename;
			$product['imagefile_ext'] = @$img->ext;
			$product['name'] = $pr['name'];
			$product['category'] = $pr['category'];
			$product['weight'] = $pr['weight'];
			$product['cweight'] = $pr['weight'].' kg';
			$product['location'] = $pr['location'];
			$product['fit'] = $pr['fit'];
			$product['fabric'] = $pr['fabric'];
			$product['quality'] = $pr['quality'];
			$product['colors'] = $pr['colors'];
			$product['cuffs'] = $pr['cuffs'];
			$product['size_incorrect'] = $pr['size_incorrect'];
			$i = 0;
			foreach($sizes as $size){
				$product['sizes'][$i]['size_id'] = $size->size_id;
				$product['sizes'][$i]['name'] = $size->name;
				$product['sizes'][$i]['description'] = $size->description;
				if($size->description){
					$product['sizes'][$i]['cdescription'] = 'bust '.str_replace('/', ' cm / ', $size->description).' cm';
					$product['sizes'][$i]['cdescription'] = str_replace('...', ' cm ... ', $product['sizes'][$i]['cdescription']);
				}
				$product['sizes'][$i]['quantity'] = $size->quantity;
				$i++;
			}

			//get next and previous products
			$sql1 = "select id from products where id = (select min(id) from products where id > '$product_id')";
			$res1 = $this->db->query($sql1)->row();
			$sql2 = "select id from products where id = (select max(id) from products where id < '$product_id')";
			$res2 = $this->db->query($sql2)->row();
			$sql3 = "select MIN(id) as first, MAX(id) as last FROM products";
			$res3 = $this->db->query($sql3)->row();

			if($res2)
				$previous = $res2->id;
			else
				$previous = $res3->last;
			if($res1)
				$next = $res1->id;
			else
				$next = $res3->first;

			//get next and previous from series
			$series = substr($product['code'], 0, 2);

			$sql4 = "select id from products where id = (select min(id) from products where id > '$product_id' and left(code, 2) = '$series')";
			$res4 = $this->db->query($sql4)->row();
			$sql5 = "select id from products where id = (select max(id) from products where id < '$product_id' and left(code, 2) = '$series')";
			$res5 = $this->db->query($sql5)->row();
			$sql6 = "select MIN(id) as first, MAX(id) as last FROM products where left(code, 2) = '$series'";
			$res6 = $this->db->query($sql6)->row();

			if($res5)
				$previous_series = $res5->id;
			else
				$previous_series = $res6->last;
			if($res4)
				$next_series = $res4->id;
			else
				$next_series = $res6->first;

			$sql7 = "select id, code from products where left(code, 2) = '$series'";
			$all = $this->db->query($sql7)->result();
			if($series != 'MD'){ //there is only one model MD
				$all_temp = array_slice($all, -6);
				array_splice($all, 0, 0, $all_temp);
				array_push($all, $all[5], $all[6], $all[7], $all[8], $all[9], $all[10]);
				$i = 0;
				$n = 0;
				foreach($all as $r){
					$result[$i] = $r->id;
					$n++;
					$i++;
				}
				for($i=6; $i<$n; $i++){
					if($result[$i] == $product_id){
						break;
					}
				}
				$next_six = array();
				$previous_six = array();

				for($j=0; $j<6; $j++){
					$next_six[$j]['id'] = $result[$i + $j +1];
					$previous_six[$j]['id'] = $result[$i -6 + $j];
				}
				for($i=0; $i<6; $i++){
					$next_six[$i] = $this->product_model->findById($next_six[$i]['id']);
					$nf_img = $this->product_model->getMainImage($next_six[$i]['id']);
					$next_six[$i]['imagefile'] = @$nf_img->filename;
					$next_six[$i]['imagefile_ext'] = @$nf_img->ext;
				}
				for($i=0; $i<6; $i++){
					$previous_six[$i] = $this->product_model->findById($previous_six[$i]['id']);
					$pf_img = $this->product_model->getMainImage($previous_six[$i]['id']);
					$previous_six[$i]['imagefile'] = @$pf_img->filename;
					$previous_six[$i]['imagefile_ext'] = @$pf_img->ext;
				}
			}else{
				$next_six = array();
				$previous_six = array();
			}

			//get the HRK and HUF currencies values
			$product['price_hrk'] = ceil($this->money->convertAmount($product['price_eur'], 2 , 'EUR', 'HRK') * 1.02);
			$product['price_huf'] = ceil($this->money->convertAmount($product['price_eur'], 2 , 'EUR', 'HUF') * 1.02);

			//check if admin
			if(!$this->session->userdata['admin_user']['super_admin']){
				return $this->render('single_product_public', array(
						'title' => 'Product',
						'product' => $product,
						'next' => $next,
						'previous' => $previous,
						'next_series' => $next_series,
						'next_six' => $next_six,
						'series' => $series,
						'previous_series' => $previous_series,
						'previous_six' => $previous_six,
						'categories' => $categories,
						'fabrics' => $fabrics,
						'fit' => $fit,
						'cuffs' => $cuffs,
						'size_incorrect' => $size_incorrect,
						'quality' => $quality,
						'images' => $images
					));
			}else{
				return $this->render('single_product', array(
						'title' => 'Product',
						'product' => $product,
						'next' => $next,
						'previous' => $previous,
						'categories' => $categories,
						'fabrics' => $fabrics,
						'fit' => $fit,
						'cuffs' => $cuffs,
						'size_incorrect' => $size_incorrect,
						'quality' => $quality,
						'images' => $images
					));
			}
		}

		redirect('admin/products/p/1');
		//show_404();
	}

	public function rearangeimages(){
		//look into the folder and figure out the brand

		//for each image get the file name
		//and extract the code from it
		//then figure out the product id
		//copy the new file as standart name
		//and add the brand as finish
	}

	public function expected(){
		$current_page = $this->uri->segment(6);
		$this->pagination->initialize($this->product_model->getConfigForPagination($expected = true));

		$current_page = ($current_page == null OR $current_page < 1) ? 1 : $current_page;
		$order_seg = $this->uri->segment(5,"asc");
		if($order_seg == "asc") $order_res = "desc"; else $order_res = "asc";

		return $this->render('index', array(
			'title' => 'List products',
			'products' => $this->product_model->getCollection($current_page, $expected = true),
			'pagination' => $this->pagination->create_links(),
			'order_res' => $order_res,
			'expected' => true
		));
	}

	public function create(){
		$this->action = 'create';
		$object = $this->input->post();
		if(empty($object['weight'])){
			$object['weight'] = 0;
		}
		$sizes = $this->size_model->getCollection();
		$countries = $this->country_model->getCollectionOrdered();

		$this->config->load("form_validation");
		$product_rules = $this->config->item('product');
		$product_rules = array_merge($product_rules, $this->getRulesForFieldPrices($countries));
		$product_rules = array_merge($product_rules, $this->getRulesForFieldSizes($sizes));
		$this->form_validation->set_rules($product_rules);

		$categories_array = $this->product_model->getCategories();
		$i = 0;
		foreach($categories_array as $category){
			$categories[$i] = $category->slug;
			$i++;
		}

		if($this->isPost()){
			if($this->form_validation->run() == false){
				$product_prices = $this->input->post('product')['prices'];
				foreach($countries as &$country){
					$price_index = $this->searchForCountryId($country->id, $product_prices);
					$country->price = $product_prices[$price_index]['price'];
					$country->price_stock = $product_prices[$price_index]['price_stock'];
				}
				$product_sizes = $this->input->post('product')['sizes'];
				foreach($sizes as &$size){
					$size_index =  $this->searchForSizeId($size->id, $product_sizes);
					$size->quantity = $product_sizes[$size_index]['quantity'];
					$size->description = $product_sizes[$size_index]['description'];
					$size->min_quantity = $product_sizes[$size_index]['minQuantity'];
				}
			}else{
				$product = array();
				$product['name'] = $this->input->post('name');
				$product['code'] = strtoupper($this->input->post('code'));
				$product['supplier_id'] = $this->input->post('supplier_id');
				$product['date_added'] = date('Y-m-d H:i:s');
				$product['weight'] = $this->input->post('weight');
				$product['tags'] = $this->input->post('tags');
				$product['colors'] = $this->input->post('colors');
				$product['category'] = $this->input->post('category');

				$is_preorder = $this->input->post('is_preorder');
				if(isset($is_preorder)){
					$product['is_preorder'] = 1;
				} else {
					$product['is_preorder'] = 0;
				}

				$product['is_stock_available'] = 1;

				$is_branded = $this->input->post('is_branded');
				if(isset($is_branded)){
					$product['is_branded'] = 1;
				} else {
					$product['is_branded'] = 0;
				}

				$product_sizes_post = $this->input->post('product')['sizes'];
				$product_prices_post = $this->input->post('product')['prices'];
				$product_id = $this->product_model->insertRow($product);
				if($product_id){
					foreach($product_sizes_post as $product_size){
						$data = array(
							'quantity' => $product_size['quantity'],
							'size_id' => $product_size['size'],
							'min_quantity' => $product_size['minQuantity'],
							'product_id' => $product_id
						);
						$this->product_model->insertSizeRow($data);
						unset($data);

						$size_description_data = array(
							'size_id' => $product_size['size_id'],
							'product_id' => $product_id,
							'description' => $product_size['description']
						);
						$this->product_model->insertSizeDescriptionRow($size_description_data);
					}

					foreach ($product_prices_post as $product_price) {
						$data = array(
							'price' => $product_price['price'],
							'price_stock' => $product_price['price_stock'],
							'product_id' => $product_id,
							'country_id' => $product_price['country']
						);
						$this->product_model->insertPriceRow($data);
						unset($data);
					}
					$product_data_object = $this->product_model->findById($product_id);

					// upload photo
					$files = $_FILES;
					$cpt = count ( $_FILES ['userfile'] ['name'] );
					if($cpt > 0 && strlen($_FILES ['userfile'] ['name'] [ 0 ])){
						for($i = 0; $i < $cpt; $i++) {
							$_FILES ['userfile'] ['name'] = $files ['userfile'] ['name'] [$i];
							$_FILES ['userfile'] ['type'] = $files ['userfile'] ['type'] [$i];
							$_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [$i];
							$_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [$i];
							$_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [$i];
							if(is_uploaded_file($_FILES['userfile']['tmp_name'])){
								$upload_response = $this->image->upload($product_id, $product_data_object['code']);
								if(!isset($upload_response['error'])){
									// show success message
									// write image to database
									$is_main = ($i == 0) ? 1 : 0;
									$data = array(
											'product_id' => $product_id,
											'filename' => $upload_response['upload_data']['raw_name'],
											'ext' => $upload_response['upload_data']['file_ext'],
											'is_main' => $is_main
									);
									$this->product_model->insertImageRow($data);
								}else{
									// show message with errors
								}
							}
						}
					}
					redirect('admin/products/edit/' . $product_id);
				}
			}
		}

		$this->render('form', array(
			'title' => 'Add product',
			'suppliers' => $this->product_model->findSuppliersForSelect(),
			'object' => $object,
			'validation_errors' => validation_errors(),
			'categories' => $categories,
			'sizes' => $sizes,
			'countries' => $countries,
			'series' => $this->product_model->getAllSeries()
		));
	}

	public function edit($id) {
                $this->action = 'edit';
		if (! $object = $this->product_model->findById($id)) {
			show_404();
		}

		$sizes = $this->product_model->getProductSizes($id);
		$countries = $this->country_model->getCollectionOrdered();
		$prices = $this->product_model->getProductPrices($id);

		$this->config->load("form_validation");
		$product_rules = $this->config->item('product');

		$product_rules = array_merge($product_rules, $this->getRulesForFieldPrices($countries));
		$product_rules = array_merge($product_rules, $this->getRulesForFieldSizes($sizes));

		$this->form_validation->set_rules($product_rules);

		if($this->isPost()){
			if($this->form_validation->run() == FALSE){
				$product_prices = $this->input->post('product')['prices'];
				foreach($countries as &$country) {
					$price_index = $this->searchForCountryId($country->id, $product_prices);
					$country->price = $product_prices[$price_index]['price'];
				}

				$product_sizes = $this->input->post('product')['sizes'];
				foreach($sizes as &$size) {
					$size_index =  $this->searchForSizeId($size->size_id, $product_sizes);
					$size->quantity = $product_sizes[$size_index]['quantity'];
					$size->description = $product_sizes[$size_index]['description'];
					$size->min_quantity = $product_sizes[$size_index]['minQuantity'];
				}
			} else {
				$object = array_merge($object, $this->input->post());

				$product_data = array();
				$product_data['name'] = $this->input->post('name');
				$product_data['code'] = $this->input->post('code');
				$product_data['supplier_id'] = $this->input->post('supplier_id');
				$product_data['weight'] = $this->input->post('weight');
				$product_data['tags'] = $this->input->post('tags');
				$product_data['colors'] = $this->input->post('colors');

				$is_preorder = $this->input->post('is_preorder');
				if(isset($is_preorder)){
					$product_data['is_preorder'] = 1;
				} else {
					$product_data['is_preorder'] = 0;
				}

				$is_branded = $this->input->post('is_branded');
				if(isset($is_branded)){
					$product['is_branded'] = 1;
				} else {
					$product['is_branded'] = 0;
				}

				$product_sizes_post = $this->input->post('product')['sizes'];
				$product_prices_post = $this->input->post('product')['prices'];

				if ($this->product_model->updateRow($id, $product_data)) {

					foreach ($product_sizes_post as $size) {
						$size_data = array(
							'quantity' => $size['quantity'],
							'min_quantity' => $size['minQuantity'],
							'product_id' => $id
						);
						$this->product_model->updateSizeRow($size['size'], $size_data);

						$size_description = array(
							'description' => $size['description']
						);
						$this->product_model->updateSizeDescriptionRow($id, $size['size_id'], $size_description);
					}

					foreach($product_prices_post as $product_price){
						$price_data = array(
							'price' => $product_price['price'],
							'price_stock' => $product_price['price_stock'],
						);
						$country_id = $product_price['country'];
						$this->product_model->updatePriceRow($id, $country_id, $price_data);
					}

					$files = $_FILES;
					$cpt = count ( $_FILES ['userfile'] ['name'] );
					if($cpt > 0 && strlen($_FILES ['userfile'] ['name'] [ 0 ])){
						for($i = 0; $i < $cpt; $i++){
							$_FILES ['userfile'] ['name'] = $files ['userfile'] ['name'] [$i];
							$_FILES ['userfile'] ['type'] = $files ['userfile'] ['type'] [$i];
							$_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [$i];
							$_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [$i];
							$_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [$i];
							if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
								$upload_response = $this->image->upload($id, $product_data['code']);
								if(!isset($upload_response['error'])){
									// show success message
									// write image to database
									// $is_main = ($i == 0) ? 1 : 0;
									$data = array(
											'product_id' => $id,
											'filename' => $upload_response['upload_data']['raw_name'],
											'ext' => $upload_response['upload_data']['file_ext'],
											'is_main' => 0
									);
									$this->product_model->insertImageRow($data);
								}else{
									// show message with errors
								}
							}
						}
					}
					redirect('admin/products/date_added/asc');
				}
			}
			$object = array_merge($object, $this->input->post());
		}

		$images = $this->getImages($id);

		$this->render('form', array(
			'title' => 'Edit product',
			'suppliers' => $this->product_model->findSuppliersForSelect(),
			'object' => $object,
			'validation_errors' => validation_errors(),
			'sizes' => $sizes,
			'countries' => $prices,
			'images' => $images,
			'product_comments' => $this->product_model->getComments($id),
            'action' => 'edit'
		));
	}

	/*
	 * by Stefan
	 * Updates product data by x-editable request
	 */
	public function ppost(){
		$data = $this->input->post();

		$pid = $data['pk']; //gets the product id
		$field_name = $data['name']; //gets the field/column to be updated
		$field_name = trim(substr($field_name, strpos($field_name, '-') + 1)); //cuts product.id and '-'
		$pvalue = $data['value']; //gets the value to be updated
		$price_data = array(
				'price' => $pvalue
			);

		//$product_data_object = $this->product_model->findById($pid);
		//$product_prices_object = $this->product_model->getProductPrices($pid);

		//field switch
		switch ($field_name){
			case 'price_eur' :{
				$country_id = '3';
				$this->product_model->updatePriceRow($pid, $country_id, $price_data);
				break;
			}
			case 'price_bgn' :{
				$country_id = '1';
				$this->product_model->updatePriceRow($pid, $country_id, $price_data);
				break;
			}
			case 'price_lei' :{
				$country_id = '2';
				$this->product_model->updatePriceRow($pid, $country_id, $price_data);
				break;
			}
			case 'supplier' :{
				$supplier_object = $this->product_model->findSupplierByName($pvalue);
				$sid = $supplier_object['id'];
				$data = array(
					'supplier_id' => $sid
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'location' :{
				$location = $pvalue;
				$data = array(
					'location' => $location
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'size_incorrect' :{
				$size_incorrect = $pvalue;
				$data = array(
					'size_incorrect' => $size_incorrect
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'quantity_xs' :{
				$size_data = array(
					'quantity' => $pvalue,
					'product_id' => $pid
				);
				$sid = $this->returnproductsizeid($pid, 1);
				$this->product_model->updateSizeRow($sid, $size_data);
				break;
			}
			case 'quantity_s' :{
				$size_data = array(
					'quantity' => $pvalue,
					'product_id' => $pid
				);
				$sid = $this->returnproductsizeid($pid, 2);
				$this->product_model->updateSizeRow($sid, $size_data);
				break;
			}
			case 'quantity_m' :{
				$size_data = array(
					'quantity' => $pvalue,
					'product_id' => $pid
				);
				$sid = $this->returnproductsizeid($pid, 3);
				$this->product_model->updateSizeRow($sid, $size_data);
				break;
			}
			case 'quantity_l' :{
				$size_data = array(
					'quantity' => $pvalue,
					'product_id' => $pid
				);
				$sid = $this->returnproductsizeid($pid, 4);
				$this->product_model->updateSizeRow($sid, $size_data);
				break;
			}
			case 'quantity_xl' :{
				$size_data = array(
					'quantity' => $pvalue,
					'product_id' => $pid
				);
				$sid = $this->returnproductsizeid($pid, 5);
				$this->product_model->updateSizeRow($sid, $size_data);
				break;
			}
			case 'quantity_xxl' :{
				$size_data = array(
					'quantity' => $pvalue,
					'product_id' => $pid
				);
				$sid = $this->returnproductsizeid($pid, 6);
				$this->product_model->updateSizeRow($sid, $size_data);
				break;
			}
			case 'quantity_xxxl' :{
				$size_data = array(
					'quantity' => $pvalue,
					'product_id' => $pid
				);
				$sid = $this->returnproductsizeid($pid, 7);
				$this->product_model->updateSizeRow($sid, $size_data);
				break;
			}
			case 'category' :{
				$category = $pvalue;
				$data = array(
					'category' => $category
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'onsale' :{
				$onsale = $pvalue;
				$data = array(
					'onsale' => $onsale
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'fit' :{
				$fit = $pvalue;
				$data = array(
					'fit' => $fit
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'colors' :{
				$colors = $pvalue;
				$data = array(
					'colors' => $colors
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'fabric' :{
				$fabric = $pvalue;
				$data = array(
					'fabric' => $fabric
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'cuffs' :{
				$cuffs = $pvalue;
				$data = array(
					'cuffs' => $cuffs
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'quality' :{
				$quality = $pvalue;
				$data = array(
					'quality' => $quality
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'weight' :{
				$weight = $pvalue;
				$data = array(
					'weight' => $weight
				);
				$this->product_model->updateRow($pid, $data);
				break;
			}
			case 'price_stock_bgn' :{
				$country_id = '1';
				$price_stock = $pvalue;
				$price_data1 = array(
					'price_stock' => $price_stock
				);
				$this->product_model->updatePriceRow($pid, $country_id, $price_data1);
				break;
			}
			case 'price_stock_eur' :{
				$country_id = '3';
				$price_stock = $pvalue;
				$price_data1 = array(
					'price_stock' => $price_stock
				);
				$this->product_model->updatePriceRow($pid, $country_id, $price_data1);
				break;
			}
			case 'price_stock_lei' :{
				$country_id = '2';
				$price_stock = $pvalue;
				$price_data1 = array(
					'price_stock' => $price_stock
				);
				$this->product_model->updatePriceRow($pid, $country_id, $price_data1);
				break;
			}
			case substr($field_name, 0, 8) == 'sizedesc' :{
				$size_id = ltrim($field_name, 'sizedesc-');
				$size_data = array(
					'description' => $pvalue
				);
				$this->product_model->updateSizeDescriptionRow($pid, $size_id, $size_data);
				break;
			}
		}

		$photos = $this->facebook_model->getFacebookPhotos($pid);
		foreach($photos as $photo) {
			$this->facebooklib->updatePhotoDesc($pid, $photo);
		}
	}

	public function getProductSizeDescriptionID($product_id, $size_id){
		$sql1 = "select id
					from product_size_description
					where product_id = '$product_id'
						and size_id = '$size_id'";
		$res1 = $this->db->query($sql1)->row();

		return $res1->id;
	}

	public function returnproductsizeidbyname($product_id, $size_name){
		$sql1 = "select id
					from product_size
					where product_id = '$product_id'
						and size_id = '$size_name'";
		$res1 = $this->db->query($sql1)->row();

		return $res1->id;
	}

	public function returnproductsizeid($product_id, $size_id){
		$sql1 = "select id
					from product_size
					where product_id = '$product_id'
						and size_id = '$size_id'";
		$res1 = $this->db->query($sql1)->row();

		return $res1->id;
	}

	public function delete($id) {

	}

	public function product_price_check($value, $index) {
		$countries = Country_model::getCountriesCollection();
		$selected_country = isset($countries[$index]) ? $countries[$index]->name : '';

		if(empty($value) || $value == '') {
			$this->form_validation->set_message('product_price_check', 'The {field} field can not be 0 or empty for country '. $selected_country);
			return FALSE;
		}
		else {
			return TRUE;
		}

	}

	public function product_size_check($value, $size_name) {
		if($value < 0) {
			$this->form_validation->set_message('product_size_check', 'The {field} for field Size |'.$size_name.'|  can not be less than 0.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	private function searchForCountryId($id, $array) {
		foreach ($array as $key => $val) {
			if ($val['country'] === $id) {
				return $key;
			}
		}
		return null;
	}

	private function searchForSizeId($id, $array) {
		foreach ($array as $key => $val) {
			if ($val['size_id'] === $id) {
				return $key;
			}
		}
		return null;
	}

	private function getRulesForFieldPrices($countries) {
		$data = array();
		foreach ($countries as $key => $country) {
			$data[] = array(
				'field' => "product[prices][$key][price]",
				'label' => 'Prices',
				'rules' => 'trim|required|callback_product_price_check[' . $key . ']');
		}

		return $data;
	}

	private function getRulesForFieldSizes($sizes){
		$data = array();
		foreach($sizes as $key => $size) {
			$data[] = array(
				'field' => "product[sizes][$key][quantity]",
				'label' => 'Quantity',
				'rules' => 'trim|required|numeric'
//				'rules' => 'trim|required|callback_product_size_check[' . $size->name .']'
			);
		}
		return $data;
	}

	private function getImages($id) {
		$images = $this->product_model->getImages($id);

		$data = array();

		foreach($images as $image) {
			$data[] = array(
				'id' => $image->id,
				'product_id' => $image->product_id,
				'original' => base_url() . "uploads/$id/" . $image->filename . $image->ext,
				'resized' => base_url() . "uploads/$id/" . $image->filename . '_resized' . $image->ext,
				'thumb' => base_url() . "uploads/$id/" . $image->filename.'_thumb' . $image->ext,
				'is_main' => $image->is_main
			);
		}

		return $data;
	}

	public function deleteImage($imageId) {
		$image = $this->product_model->findImageById($imageId);
		if($image){
			$project_path = FCPATH;
			$file_original_path = $project_path.'/uploads/'.$image->product_id.'/'.$image->filename.$image->ext;
			$file_resized_path = $project_path.'/uploads/'.$image->product_id.'/'.$image->filename.'_resized'.$image->ext;
			$file_thumb_path = $project_path.'/uploads/'.$image->product_id.'/'.$image->filename.'_thumb'.$image->ext;

			$result = $this->product_model->deleteImageById($imageId);
			if($result) {
				unlink($file_original_path);
				unlink($file_thumb_path);
				unlink($file_resized_path);
			}

			redirect('admin/products/edit/' . $image->product_id);

		} else {
			show_404();
		}
	}

	public function setMainImage($imageId) {
		$image = $this->product_model->findImageById($imageId);
		$currentProductId = $image->product_id;
		if($image) {
			$allProductImages = $this->product_model->findAllMainImagesForProduct($currentProductId);
			foreach($allProductImages as $item) {
				$data = array(
					'is_main' => 0
				);
				$this->product_model->updateMainImage($item->id, $data);
			}

			$data = array(
				'is_main' => 1
			);
			$this->product_model->updateMainImage($image->id, $data);
		}

		redirect('admin/products/edit/' . $image->product_id);
	}

	public function changeAvailability($product_id, $status) {
		$data = array(
			'is_stock_available' => $status
		);
		$this->product_model->updateRow($product_id, $data);

		redirect('admin/products/date_added/asc');
	}

	public function exchangeCurrency() {
		if(!$this->input->is_ajax_request()) {
			show_404();
		}

		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$price = $this->input->post('price');

		$result = $this->money->convertAmount($price, 2 , $from, $to);

		$response = array(
			'status' => 'ok',
			'convertedPrice' => trim($result)
		);

		echo json_encode($response);
		die();
	}

	public function recalculatePrices($product_id)
	{
		//query the db for product_id
		$res1 = $this->product_model->getProductPrices($product_id);

		foreach($res1 as $row)
		{
			if($row->currency == 'EUR')
				$price_eur = $row->price;
		}

		$price_bgn = ceil($this->money->convertAmount($price_eur, 2 , 'EUR', 'BGN'));
		$price_ron = ceil($this->money->convertAmount($price_eur, 2 , 'EUR', 'RON'));
		/*
		echo "<pre>";
		var_dump($price_bgn);
		var_dump($price_ron);
		echo "</pre>";*/

		$price_data = array(
			'price' => $price_bgn
		);
		$this->product_model->updatePriceRow($product_id, 1, $price_data);
		$price_data = array(
			'price' => $price_ron
		);
		$this->product_model->updatePriceRow($product_id, 2, $price_data);

		redirect('admin/products/date_added/asc');
	}

	public function handle_user_comment() {
		$response = array();

		if ($this->input->is_ajax_request() && ($params = $this->input->post()) && isset($params['id'])) {
			$user_data = UserHelper::getCurrentUserData();
			$data = array(
				'product_id' => intval($params['id']),
				'user_id' => $user_data['id'],
				'comment' => $params['comment'],
				'created_at' => date('Y-m-d H:i:s')
			);

			if ($this->product_model->saveComment($data)) {
				$response['success'] = true;
				$response['content'] = $this->twig->render('admin/product/_user_comments_block', array(
					'product_comments' => $this->product_model->getComments($params['id'])
				));
			}
		}

		print json_encode($response);
	}

	public function load_comments($order_id) {
		$response = array();

		if ($this->input->is_ajax_request() && !empty($order_id)) {
			$response['comments'] = $this->twig->render('admin/partials/_list_table_comments_block.twig', array(
				'comments' => $this->product_model->getComments(intval($order_id))
			));
		}

		exit(json_encode($response));
	}


	public function _unique_code($code) {
		$codeResult = $this->product_model->checkCode($code);
		$post_code = $this->input->post('code');
                if($this->action == 'edit'){

                    if ($code == $codeResult->code) {

			return true;
                    }

                    if($codeResult){
			$this->form_validation->set_message('_unique_code', 'The code already exists in the system');
			return false;
                    } else {
                        return true;
                    }
                } else {

                    if($codeResult){
			$this->form_validation->set_message('_unique_code', 'The code already exists in the system');
			return false;
                    } else {
                            return true;
                    }

                }
	}

	public function product_speed() {
		$SQL = 'SELECT * FROM `monthly_pr_speed_view` WHERE sold_last_month is not null';
		$products_query = $this->db->query($SQL);
		$pr_speed_array = $products_query->result_array();
		/*
		echo "<pre>";
		var_dump ($result);
		echo "</pre>";
		*/

		$this->render('speed_table', array(
				'title' => 'Monthly product speed',
				'pr_speed_array' => $pr_speed_array,
		));

	}
}
