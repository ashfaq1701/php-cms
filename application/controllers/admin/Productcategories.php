<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Description of Productcategories
 *
 * @author Stefan
 */
class Productcategories extends Admin_Controller {
	
	protected
		$load_models = array('product_model', 'productcategories_model'),
		//$load_helpers = array('form'),
		$load_libraries = array('form_validation');
		//$has_filter = true;
	
	public function index(){
		return $this->render('index', array(
			'title' => 'Categories',
			'categories' => $this->productcategories_model->getCollection()
		));
	}
	
	public function create(){
		$object = $this->input->post();
		
		if ($this->isPost()) {
			//add form validation -  && $this->form_validation->run('productcategories')
			if ($this->productcategories_model->insertRow($object)) {
				redirect('admin/productcategories');
			}
		}
		
		$this->render('form', array(
			'title' => 'Add Category',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}
	
	public function edit($id){
		if (!$object = $this->productcategories_model->findById($id)) {
			echo "wtf";
			//show_404();
		}
		//add form validation -  && $this->form_validation->run('productcategories')
		if ($this->isPost()) {
			$object = array_merge($object, $this->input->post());
			
			if ($this->productcategories_model->updateRow($id, $object)) {
				redirect('admin/productcategories');
			}
		}
		else {
			$object = array_merge($object, $this->input->post());
		}
		
		$this->render('form', array(
			'title' => 'Edit Category',
			'object' => $object
		));
	}
	
	public function slugs(){
		$this->productcategories
	}
}