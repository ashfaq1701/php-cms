<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Admin_Controller {
	
    protected 
		$load_models = array('statistic_model', 'order_model', 'country_model', 'size_model', 'product_model', 'facebook_model', 'user_model', 'report_model'),
		$load_helpers = array('form', 'url'),
		$load_libraries = array('form_validation', 'pagination');
		//right now we don't use pagination for this particular page
		//in future it is possible to have a lot more sales agents to be listed in here

	/*public function __construct(){
	/	//echo "this is just a test, dont freak out :P";
	}*/

    public function __construct(){
        parent::__construct();
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;
		if($user_confirmed == 0 || $user_confirmed == null){
			redirect('admin/schedule');
		}
    }

	
	public function index() {
		
	}
	
	public function storeValue(){
		$values = $this->report_model->getStoreValue();
		
		
		echo "<pre>";
		var_dump($values);
		echo "</pre>";
		$this->render('store_value', array(
			'title' => 'Store Value',
			'values' => $values
		));
	}
	
	public function salesByCountry(){
		if($this->input->post('from_date')){
			$date_from = $this->input->post('from_date');
		}else{
			$date_from = new DateTime('first day of this month');
			$date_from = $date_from->format('d-m-Y');
		}
		if($this->input->post('date_to')){
			$date_to = $this->input->post('date_to');
		}else{
			$date_to = new DateTime('last day of this month');
			$date_to = $date_to->format('d-m-Y');
		}
		
		$orders = $this->report_model->salesByCountry($date_from, $date_to);
		
        $this->render('country_orders', array(
            'orders' => $orders,
			'date_from' => $date_from,
			'date_to' => $date_to
			));
	}
	
	public function words(){
		ini_set('memory_limit', '-1');
		$chat = array();
		$word = '';
		
		if($this->isPost()){
			$object = $this->input->post();
			$word = $object['word'];
			if($word != ''){
				$sql1 = "select *
							from fb_messages
							where message_text like '%$word%'";
				
				$res1 = $this->db->query($sql1)->result();
				$i = 0;
				foreach($res1 as $row1){
					$chat[$i]['link'] = $row1->conversation_link;
					$chat[$i]['conversation_id'] = $row1->conversation_id;
					$chat[$i]['created_time'] = $row1->created_time;
					$chat[$i]['from_id'] = $row1->message_from_id;
					$chat[$i]['from_name'] = $row1->message_from_text;
					$chat[$i]['text'] = $row1->message_text;
					$page = $this->facebook_model->findByFBId($row1->fb_page_id);
					$chat[$i]['page'] = $page['fb_title'];
					$chat[$i]['page_url'] = $page['url'];
					$i++;
				}
			}
		}
		
		$this->render('words', array(
			'title' => 'Search FB for word',
			'word' => $word,
			'chats' => $chat
		));
	}
	
	public function ordercount() {
		if($this->input->post('from_date')){
			$date_from = $this->input->post('from_date');
		}else{
			$date_from = new DateTime('first day of this month');
			$date_from = $date_from->format('d-m-Y');
		}
		if($this->input->post('date_to')){
			$date_to = $this->input->post('date_to');
		}else{
			$date_to = new DateTime('last day of this month');
			$date_to = $date_to->format('d-m-Y');
		}
		
        $users = $this->report_model->getSalesOrders($date_from, $date_to);
		
        $this->render('ordercount_index', array(
            'users' => $users,
			'date_from' => $date_from,
			'date_to' => $date_to
			));
	}
	
	public function productsold() {
		if($this->input->post('from_date')){
			$date_from = $this->input->post('from_date');
		}else{
			$date_from = new DateTime('first day of this month');
			$date_from = $date_from->format('d-m-Y');
		}
		if($this->input->post('date_to')){
			$date_to = $this->input->post('date_to');
		}else{
			$date_to = new DateTime('last day of this month');
			$date_to = $date_to->format('d-m-Y');
		}
		
        $users = $this->report_model->getSoldProducts($date_from, $date_to);
		
        $this->render('productsold_index', array(
            'users' => $users,
			'date_from' => $date_from,
			'date_to' => $date_to
			));
	}
	
	public function userprofit() {
		if($this->input->post('from_date')){
			$date_from = $this->input->post('from_date');
		}else{
			$date_from = new DateTime('first day of this month');
			$date_from = $date_from->format('d-m-Y');
		}
		if($this->input->post('date_to')){
			$date_to = $this->input->post('date_to');
		}else{
			$date_to = new DateTime('last day of this month');
			$date_to = $date_to->format('d-m-Y');
		}
		
        $users = $this->report_model->getUserProfit($date_from, $date_to);
		
        $this->render('userprofit_index', array(
            'users' => $users,
			'date_from' => $date_from,
			'date_to' => $date_to
			));
	}
	
	public function fbpproductsold(){
		if($this->input->post('from_date')){
			$date_from = $this->input->post('from_date');
		}else{
			$date_from = new DateTime('first day of this month');
			$date_from = $date_from->format('d-m-Y');
		}
		if($this->input->post('date_to')){
			$date_to = $this->input->post('date_to');
		}else{
			$date_to = new DateTime('last day of this month');
			$date_to = $date_to->format('d-m-Y');
		}
		
        $pages = $this->report_model->getSoldProductsByPage($date_from, $date_to);
		
        $this->render('fbp_productsold_index', array(
            'pages' => $pages,
			'date_from' => $date_from,
			'date_to' => $date_to
			));
	}
	
	public function topproducts(){
		if($this->input->post('from_date')){
			$date_from = $this->input->post('from_date');
		}else{
			$date_from = new DateTime('first day of this month');
			$date_from = $date_from->format('d-m-Y');
		}
		if($this->input->post('date_to')){
			$date_to = $this->input->post('date_to');
		}else{
			$date_to = new DateTime('last day of this month');
			$date_to = $date_to->format('d-m-Y');
		}
		
        $products = $this->report_model->getTopProducts($date_from, $date_to);
		
        $this->render('topproducts_index', array(
            'products' => $products,
			'date_from' => $date_from,
			'date_to' => $date_to
			));
	}

	public function revenuebg(){
		if($this->input->post('from_date')){
			$date_from = $this->input->post('from_date');
		}else{
			$date_from = new DateTime('first day of this month');
			$date_from = $date_from->format('d-m-Y');
		}
		if($this->input->post('date_to')){
			$date_to = $this->input->post('date_to');
		}else{
			$date_to = new DateTime('last day of this month');
			$date_to = $date_to->format('d-m-Y');
		}
		
        $revenue = $this->report_model->getRevenueBG($date_from, $date_to);
		
        $this->render('revenue_index', array(
            'revenue' => $revenue,
			'date_from' => $date_from,
			'date_to' => $date_to
			));
	}
}
