<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends Admin_Controller{

	protected
		$load_models = array('schedule_model', 'facebook_model', 'user_model', 'country_model', 'order_model', 'product_model', 'images_model'),
		$load_helpers = array('form'),
		$load_libraries = array('form_validation', 'image', 'money', 'pagination');

	public function index(){

		$startDate = date('Y-m-d', strtotime('monday this week'));
		$endDate = date('Y-m-d', strtotime('sunday this week'));
		
		$schedule = $this->schedule_model->getCollection($startDate, $endDate);
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;

		$this->render('index', array(
			'title' => 'Schedule',
			'user' => $user,
			'schedule' => $schedule,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'confirmed' => $user_confirmed
		));
	}

	public function userClick($data){
		if(!$this->input->is_ajax_request()) {
			show_404();
		}

		$dates = $this->input->post();
		$schedule = $this->schedule_model->getCollection($dates['startDate'], $dates['endDate']);
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;
		
		/*echo "<pre>";
		var_dump($dates);
		echo "</pre>";*/

		$data_array = explode('-', $data);
		$r = $data_array[0];
		$c = $data_array[1];

		$date = $schedule[0][$c];
		$hours = $r;
		$user_id = $user['id'];

		$sql1 = "select *
					from schedule
					where date = '$date'
						and hours = '$hours'
						and user_id = '$user_id'
					limit 1
				";
		$res1 = $this->db->query($sql1)->row_array();

		if(empty($res1)){
			$sql2 = "insert into schedule (date, hours, user_id)
								values ('$date', '$hours', '$user_id')
					";
			$this->db->query($sql2);
		}else{
			$id = $res1['id'];
			$sql3 = "delete from schedule
						where id = '$id'
					";
			$this->db->query($sql3);
		}

		$schedule = $this->schedule_model->getCollection($dates['startDate'], $dates['endDate']);

		/*echo "<pre>";
		var_dump($dates);
		echo "</pre>";*/

		echo $this->render('index', array(
			'title' => 'Schedule',
			'user' => $user,
			'schedule' => $schedule,
			'startDate' => $dates['startDate'],
			'endDate' => $dates['endDate'],
			'confirmed' => $user_confirmed
		));
	}

	public function pickweek(){
		if(!$this->input->is_ajax_request()){
			show_404();
		}

		$dates = $this->input->post();
		$schedule = $this->schedule_model->getCollection($dates['startDate'], $dates['endDate']);
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;

		/*echo "<pre>";
		var_dump($dates);
		echo "</pre>";*/

		echo $this->render('index', array(
			'title' => 'Schedule',
			'user' => $user,
			'schedule' => $schedule,
			'startDate' => $dates['startDate'],
			'endDate' => $dates['endDate'],
			'confirmed' => $user_confirmed
		));
	}

	public function confirm_schedule(){
		$user = $this->session->userdata['admin_user'];
		$user_id = $user['id'];
		$sql = "update user
					set schedule_confirmed = 1
					where id = '$user_id'
				";
		$this->db->query($sql);
		redirect('/admin/schedule/');
	}
}