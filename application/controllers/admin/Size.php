<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Size
 *
 * @author chavdar
 */
class Size extends Admin_Controller {
	
	protected
		$load_models = array('size_model'),
		$load_helpers = array('form'),
		$load_libraries = array('form_validation'),
		$has_filter = true;
	
	public function index() {
		$this->render('index', array(
			'title' => 'List sizes',
			'sizes' => $this->size_model->getCollection()
		));
	}
	
	public function create() {
		$object = $this->input->post();
		
		if ($this->isPost() && $this->form_validation->run('size')) {
			if ($this->size_model->insertRow($object)) {
				redirect('admin/sizes');
			}
		}
		
		$this->render('form', array(
			'title' => 'Add size',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}
	
	public function edit($id) {
		if (! $object = $this->size_model->findById($id)) {
			show_404();
		}
		
		if ($this->isPost() && $this->form_validation->run('size')) {
			$object = array_merge($object, $this->input->post());
			
			if ($this->size_model->updateRow($id, $object)) {
				redirect('admin/sizes');
			}
		}
		else {
			$object = array_merge($object, $this->input->post());
		}
		
		$this->render('form', array(
			'title' => 'Edit size',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}
	
	public function delete($id) {
		
	}
}
