<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Description of Statistic
 *
 * @author chavdar
 */
class Statistic extends Admin_Controller {
    
    protected 
		$load_models = array('statistic_model','order_model', 'country_model', 'size_model', 'product_model', 'facebook_model', 'user_model'),
		$load_helpers = array('form', 'url'),
		$load_libraries = array('form_validation', 'pagination');

    public function __construct(){
        parent::__construct();
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;
		if($user_confirmed == 0 || $user_confirmed == null){
			redirect('admin/schedule');
		}
    }
    
    public function index() {
		if($this->input->post('from_date')){
			$date_from = $this->input->post('from_date');
		}else{
			$date_from = new DateTime('first day of this month');
			$date_from = $date_from->format('d-m-Y');
		}
		if($this->input->post('date_to')){
			$date_to = $this->input->post('date_to');
		}else{
			$date_to = new DateTime('last day of this month');
			$date_to = $date_to->format('d-m-Y');
		}
		
		//get all agents
        $rows = $this->statistic_model->getUserProfitTotal($date_from, $date_to);
		$agents = array();
		foreach($rows as $row){
			$agents[$row['user_id']]['user_name'] = $row['username'];
			$agents[$row['user_id']]['user_id'] = $row['user_id'];
			$agents[$row['user_id']]['orders_total'] = 0;
			$agents[$row['user_id']]['products_total'] = 0;
			switch($row['status']){
				case '2':{
					//paid
					$agents[$row['user_id']]['orders_paid'] = $row['orders_total'];
					$agents[$row['user_id']]['products_paid'] = $row['products_total'];
					break;
				}
				case '3':{
					//returned
					$agents[$row['user_id']]['orders_returned'] = $row['orders_total'];
					$agents[$row['user_id']]['products_returned'] = $row['products_total'];
				}
				case '4':{
					//problem
					$agents[$row['user_id']]['orders_problem'] = $row['orders_total'];
					$agents[$row['user_id']]['products_problem'] = $row['products_total'];
				}
				case '5':{
					//missing
					$agents[$row['user_id']]['orders_missing'] = $row['orders_total'];
					$agents[$row['user_id']]['products_missing'] = $row['products_total'];
				}
			}
		}
		
		foreach($rows as $row){
			if($row['status'] == '2' || $row['status'] == '3'){
				$agents[$row['user_id']]['orders_total'] += $row['orders_total'];
				$agents[$row['user_id']]['products_total'] += $row['products_total'];
			}
		}
		
		foreach($rows as $row){
			if(isset($agents[$row['user_id']]['orders_problem'])){
				if($agents[$row['user_id']]['orders_total'] == 0){
					$tempy = 1;
					$agents[$row['user_id']]['orders_problem_percent'] = round(($agents[$row['user_id']]['orders_problem'] / $tempy), 2) * 100;
				}else
					$agents[$row['user_id']]['orders_problem_percent'] = round(($agents[$row['user_id']]['orders_problem'] / $agents[$row['user_id']]['orders_total']), 2) * 100;
			}
			if(isset($agents[$row['user_id']]['orders_paid'])){
				if($agents[$row['user_id']]['orders_total'] == 0){
					$tempy = 1;
					$agents[$row['user_id']]['orders_paid_percent'] = round(($agents[$row['user_id']]['orders_paid'] / $tempy), 2) * 100;
				}else
					$agents[$row['user_id']]['orders_paid_percent'] = round(($agents[$row['user_id']]['orders_paid'] / $agents[$row['user_id']]['orders_total']), 2) * 100;
			}
			if(isset($agents[$row['user_id']]['orders_returned'])){
				if($agents[$row['user_id']]['orders_total'] == 0){
					$tempy = 1;
					$agents[$row['user_id']]['orders_returned_percent'] = round(($agents[$row['user_id']]['orders_returned'] / $tempy), 2) * 100;
				}else
					$agents[$row['user_id']]['orders_returned_percent'] = round(($agents[$row['user_id']]['orders_returned'] / $agents[$row['user_id']]['orders_total']), 2) * 100;
			}
			if(isset($agents[$row['user_id']]['orders_missing'])){
				if($agents[$row['user_id']]['orders_total'] == 0){
					$tempy = 1;
					$agents[$row['user_id']]['orders_missing_percent'] = round(($agents[$row['user_id']]['orders_missing'] / $tempy), 2) * 100;
				}else
					$agents[$row['user_id']]['orders_missing_percent'] = round(($agents[$row['user_id']]['orders_missing'] / $agents[$row['user_id']]['orders_total']), 2) * 100;
			}
			
			if(isset($agents[$row['user_id']]['products_problem'])){
				if($agents[$row['user_id']]['orders_total'] == 0){
					$tempy = 1;
					$agents[$row['user_id']]['products_problem_percent'] = round(($agents[$row['user_id']]['products_problem'] / $tempy), 2) * 100;
				}else
					$agents[$row['user_id']]['products_problem_percent'] = round(($agents[$row['user_id']]['products_problem'] / $agents[$row['user_id']]['products_total']), 2) * 100;
			}
			if(isset($agents[$row['user_id']]['products_paid'])){
				$agents[$row['user_id']]['products_paid_percent'] = round(($agents[$row['user_id']]['products_paid'] / $agents[$row['user_id']]['products_total']), 2) * 100;
			}
			if(isset($agents[$row['user_id']]['products_returned'])){
				$agents[$row['user_id']]['products_returned_percent'] = round(($agents[$row['user_id']]['products_returned'] / $agents[$row['user_id']]['products_total']), 2) * 100;
			}
			if(isset($agents[$row['user_id']]['products_missing'])){
				if($agents[$row['user_id']]['orders_total'] == 0){
					$tempy = 1;
					$agents[$row['user_id']]['products_missing_percent'] = round(($agents[$row['user_id']]['products_missing'] / $tempy), 2) * 100;
				}else
					$agents[$row['user_id']]['products_missing_percent'] = round(($agents[$row['user_id']]['products_missing'] / $agents[$row['user_id']]['products_total']), 2) * 100;
			}
			
			if(isset($agents[$row['user_id']]['products_paid'])){
				$agents[$row['user_id']]['orders_paid_average_size'] = round(($agents[$row['user_id']]['products_paid'] / $agents[$row['user_id']]['orders_paid']), 2);
			}
			if(isset($agents[$row['user_id']]['products_returned'])){
				$agents[$row['user_id']]['orders_returned_average_size'] = round(($agents[$row['user_id']]['products_returned'] / $agents[$row['user_id']]['orders_returned']), 2);
			}
			if(isset($agents[$row['user_id']]['products_paid'])){
				$agents[$row['user_id']]['order_success_rate'] = round(($agents[$row['user_id']]['orders_paid'] / $agents[$row['user_id']]['orders_total']), 2);
			}
		}
		
        $this->render('index', array(
            'agents' => $agents,
			'date_from' => $date_from,
			'date_to' => $date_to
			));
    }
    
    
}