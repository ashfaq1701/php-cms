<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Supplier
 *
 * @author chavdar
 */
class Supplier extends Admin_Controller {
	
	protected
		$load_models = array('supplier_model'),
		$load_helpers = array('form'),
		$load_libraries = array('form_validation'),
		$has_filter = true;
	
	public function index() {
		$this->render('index', array(
			'title' => 'List suppliers',
			'suppliers' => $this->supplier_model->get_all(),
			'filter_fields' => $this->config->item('supplier', 'filter')
		));
	}
	
	public function create() {
		$object = $this->input->post();
		
		if ($this->isPost() && $this->form_validation->run('supplier')) {
			
			if ($this->supplier_model->insertRow($object)) {
				redirect('admin/suppliers');
			}
		}
		
		$this->render('form', array(
			'title' => 'Create supplier',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}
	
	public function edit($id) {
		if (! $object = $this->supplier_model->findById($id)) {
			show_404();
		}
		
		if ($this->isPost() && $this->form_validation->run('supplier')) {
			$object = array_merge($object, $this->input->post());
			
			if ($this->supplier_model->updateRow($id, $object)) {
				redirect('admin/suppliers');
			}
		}
		else {
			$object = array_merge($object, $this->input->post());
		}
		
		$this->render('form', array(
			'title' => 'Edit supplier',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}
	
	public function delete($id) {
		
	}
}
