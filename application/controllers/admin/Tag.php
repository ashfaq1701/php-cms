<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends Admin_Controller {
	
	protected
		$load_models = array('tag_model'),
		$load_helpers = array('form'),
		$load_libraries = array('form_validation'),
		$has_filter = true;
	
	public function index() {
		$this->render('index', array(
			'title' => 'List tags',
			'tags' => $this->tag_model->getCollection()
		));
	}
	
	public function create() {
		$object = $this->input->post();
		
		if ($this->isPost() && $this->form_validation->run('tag')) {
			
			if ($this->tag_model->insertRow($object)) {
				redirect('admin/tags');
			}
		}
		
		$this->render('form', array(
			'title' => 'Create tag',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}
	
	public function edit($id) {
		if (! $object = $this->tag_model->findById($id)) {
			show_404();
		}
		
		if ($this->isPost() && $this->form_validation->run('tag')) {
			$object = array_merge($object, $this->input->post());
			
			if ($this->tag_model->updateRow($id, $object)) {
				redirect('admin/tags');
			}
		}
		else {
			$object = array_merge($object, $this->input->post());
		}
		
		$this->render('form', array(
			'title' => 'Edit tag',
			'object' => $object,
			'validation_errors' => validation_errors()
		));
	}
	
	public function delete($id) {
		
	}
}
