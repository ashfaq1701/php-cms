<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function cmp($a, $b) {
    if ($a['pid'] == $b['pid']) {
        return 0;
    }
    return ($a['pid'] < $b['pid']) ? -1 : 1;
}

class Temp extends Admin_Controller {
	
	protected
		$load_models = array('temp_model', 'product_model'),
		$load_helpers = array('form', 'directory'),
		$load_libraries = array('form_validation', 'image', 'money', 'pagination');
		
	/*
	 * by Stefan
	 * Gets batch of images and replaces our current product images
	 */
	public function replaceimages(){
		//list all images
		$path_to_new = APPPATH.'../uploads/new';
		$map1 = directory_map($path_to_new, FALSE, TRUE);
		
		//already changed
		$str = "MS159, MS208, MS209, MS210, MS211, MS212, MS213, MS214, MS190, MS195, MS196, MS197, MS163, MS169, MS173, MS180, MS183, MS184, MS185, MS186, MS187, MS188, MS189, MS193, MS194, MS198, MS199, MS200, MS201, MS202, MS203, MS204, MS205, SC154, SC168, SC178, SC179, SC182, SC254, SC255, SC256, SD313, SD312, SD311, SD309, SD308, SD307, SD306, SD305, SD304, SD302, SD301, SD298, SD297, SD275, SD274, SD256, SD255, SD250, SD247, SD237, SD225, SD221, SD206, SD205, SD204, SD203, SD202, SD201, SD187, SD186, SD167, SD166, SD128";
		$list = explode(', ',$str,100);
		
		$i = 0;
		foreach($map1 as $file){
			//prepare the names
			$filename = str_replace(' ', '', strtolower($file));
			$filename = rtrim($filename, '.jpg');
			$code = substr($filename,0,5);
			$filename = substr_replace($filename, '_', 5, 0);
			$filename = str_replace('(', '', $filename);
			$filename = str_replace(')', '', $filename);
			$filename = strtoupper($filename);
			$product = $this->product_model->findByCode($code);
			if($product != NULL){
				$product_images = $this->product_model->getImages($product['id']);
				if(!in_array(strtoupper($code),$list,FALSE)){
					//delete old images from the website!!!!!!!!
					//test everything else first
					
					//prepare filenames to replace
					$files[$i]['name'] = $filename; //filename
					$files[$i]['ext'] = '.jpg'; //extension
					$files[$i]['pid'] = $product['id']; //use it to sort the array
					$files[$i]['is_branded'] = 0;
					$files[$i]['is_main'] = 0;
					
					//insert data in DB
					//$this->temp_model->insertImage($files[$i]); ?!?!
					$i++;
				}
			}
		}
		
		usort($files, "cmp");
		
		$pids = array();
		$i = 0;
		foreach($files as $file){
			$pids[$i] = $file['pid'];
			$i++;
		}
		$pids = array_unique($pids);
		sort($pids);
		
		foreach($pids as $pid){
			echo "<pre>";
			var_dump($this->product_model->getImages($pid));
			echo "</pre>";
		}
		//loop all names
			//foreach file check if the product exists
			//if it does:
			//delete all images from uploads for this product
			//upload the new files
			//insert them in DB
	}
	
	public function productprices(){
		//cicle all products
			//foreach cicle all countries
	}
	
	/*
	
	//TEMP BLOCK
	public function initBefore13(){
		//posts albums to FB
		$pages = $this->postsBefore13();
		foreach($pages as $page){
			//if($page->id == '24'){
				//create album
				$album_id = $this->createAlbum($page->id);
				
				$posts = $this->postsBefore13byID($page->id); // posts to upload
				foreach($posts as $post){
					$this->post = $post;
					$response = $this->postToFacebook2($post, $album_id);
					if($response){
						$this->facebook_model->updateFacebookPost($post->fid, array('is_uploaded'=>1));
					}
					
					sleep(rand(7,20));
				}
			//}
		}
	}
	
	public function initAfter14(){
		//posts albums to FB
		$pages = $this->postsAfter14();
		foreach($pages as $page){
			//if($page->id == '24'){
				//create album
				$album_id = $this->createAlbum($page->id);
				
				$posts = $this->postsAfter14byID($page->id); // posts to upload
				foreach($posts as $post){
					$this->post = $post;
					$response = $this->postToFacebook2($post, $album_id);
					if($response){
						$this->facebook_model->updateFacebookPost($post->fid, array('is_uploaded'=>1));
					}
					
					sleep(rand(7,20));
				}
			//}
		}
	}
	
	public function createAlbum($page_id){
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		
		$page = $this->facebook_model->findById($page_id);
		
        $app_id = $page['app_id'];
        $app_secret = $page['secret_id'];
        
		if(date('H') > 14){
			$params = array(
				"access_token" => $page['access_token'],
				"message" => 'Happy '.date("Y-m-d").' evening!',
				"name" => 'Happy '.date("Y-m-d").' evening!'
			);
		}else{
			$params = array(
				"access_token" => $page['access_token'],
				"message" => 'Happy '.date("Y-m-d").' morning!',
				"name" => 'Happy '.date("Y-m-d").' morning!'
			);
		}
		
		$req = '/'.$page['fb_id'].'/albums';
		
		$fb = new Facebook\Facebook(array('app_id'=>$app_id, 'app_secret'=>$app_secret, 'default_graph_version'=>'v2.9'));
		$fbApp = $fb->getApp();
		$request = new Facebook\FacebookRequest($fbApp, $page['access_token'], 'POST', $req, $params);
		try{
			//gets the request we send to FB
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			//catches the error with FB response if any, it is usefull for debuging purpouses
			//in future can be put in log files
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			//catches the error with SDK if any, it is usefull for debuging purpouses
			//in future can be put in log files
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		$res = $response->getGraphNode();
		return $res['id'];
	}
	
	/*
	 * by Stefan
	 * Temp Function, shortcut on FAP before 13h
	 */
	 /*
    public function postsBefore13byID($page_id){
		$sql1 = "select *, fbp.*, fbp.fb_id as fb_id, facebook_posts.id as fid
					from facebook_posts
					left join facebook_pages as fbp on fbp.id = facebook_posts.facebook_page_id
					where is_uploaded = 0
						and date_schedule <= CONCAT(CURDATE(), ' 14:00:01')
						and date_schedule > CONCAT(CURDATE(), ' 07:00:00')
						and facebook_posts.facebook_page_id = '$page_id'";
		return $this->db->query($sql1)->result();
    }
	
	/*
	 * by Stefan
	 * Temp Function, shortcut on FAP after 14h
	 */
	 /*
    public function postsAfter14byID($page_id){
		$sql1 = "select *, fbp.*, fbp.fb_id as fb_id, facebook_posts.id as fid
					from facebook_posts
					left join facebook_pages as fbp on fbp.id = facebook_posts.facebook_page_id
					where is_uploaded = 0
						and date_schedule >= CONCAT(CURDATE(), ' 14:00:01')
						and date_schedule < CONCAT(CURDATE(), ' 23:59:59')
						and facebook_posts.facebook_page_id = '$page_id'";
		return $this->db->query($sql1)->result();
    }
	
	/*
	 * by Stefan
	 * Temp Function, shortcut on FAP before 13h
	 */
	 /*
    public function postsBefore13(){
		$sql1 = "select *, fbp.*, fbp.fb_id as fb_id, facebook_posts.id as fid
					from facebook_posts
					left join facebook_pages as fbp on fbp.id = facebook_posts.facebook_page_id
					where is_uploaded = 0
						and date_schedule <= CONCAT(CURDATE(), ' 14:00:01')
						and date_schedule > CONCAT(CURDATE(), ' 07:00:00')
					group by facebook_posts.facebook_page_id";
		return $this->db->query($sql1)->result();
    }
	
	/*
	 * by Stefan
	 * Temp Function, shortcut on FAP after 14h
	 */
	 /*
    public function postsAfter14(){
		$sql1 = "select *, fbp.*, fbp.fb_id as fb_id, facebook_posts.id as fid
					from facebook_posts
					left join facebook_pages as fbp on fbp.id = facebook_posts.facebook_page_id
					where is_uploaded = 0
						and date_schedule >= CONCAT(CURDATE(), ' 14:00:01')
						and date_schedule < CONCAT(CURDATE(), ' 23:59:59')
					group by facebook_posts.facebook_page_id";
		return $this->db->query($sql1)->result();
    }

    private function postToFacebook2($post, $album_id) {
		//rewrite this
		//get images from the new table
		$product = $this->product_model->findById($post->product_id);
		
		
		$pid = $product['id'];
		$brand = $post->brand_name;
		
		$sql1 = "select *
					from fap_images
					where product_id = '$pid'
						and brand_name = '$brand'
					order by RAND()
					limit 1";
		
		$res1 = $this->db->query($sql1)->row();
		
		$path = APPPATH."../uploads/fap/";
		if($res1){
			$image = $path.$res1->filename.'_'.$res1->brand_name.$res1->fileext;
			
			$page = $this->facebook_model->findPageById($post->facebook_page_id);
			
			//$product = $this->getProduct($post);
			$config = array();
			$config['app_id'] = $post->app_id;
			$config['app_secret'] = $post->secret_id;
			$config['default_graph_version'] = 'v2.8';
			$fb = new Facebook\Facebook($config);
			$description = $post->description;
			$description = $this->parseDescription($description, $post->product_id, $post->facebook_page_id);

			$params = array(
				"access_token" => $post->access_token,
				"message" => $description,
				"name" => $description,
				"source" => $fb->fileToUpload($image),
				"description" => $description
			);
			
			echo "<br />";
			$place = '/'.$album_id.'/photos';
			echo $place."<br />";

			try{
				$ret = $fb->post($place, $params);
				return $ret->id;
			}catch(Exception $e) {
				echo $e->getMessage();
			}
			//proccess the response to get FB photo ID for future reference
		}
        return false;
    }
	
	//END OF TEMP BLOCK
	*/
}