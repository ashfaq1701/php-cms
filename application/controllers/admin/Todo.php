<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todo extends Admin_Controller {
	
	protected
		$load_models = array('todo_model', 'user_model'),
		$load_helpers = array('form'),
		$load_libraries = array('form_validation'),
		$has_filter = true;
	
	public function index() {
		$this->render('index', array(
			'title' => 'List todo',
			'todo_collection' => $this->todo_model->getCollection()
		));
	}
	
	public function create() {
		$object = $this->input->post();
		
		if ($this->isPost() && $this->form_validation->run('todo')) {
			$object['created_at'] = date('Y-m-d H:i:s');
			
			if ($this->todo_model->insertRow($object)) {
				redirect('admin/todo');
			}
		}
		
		$this->render('form', array(
			'title' => 'Add todo',
			'object' => $object,
			'validation_errors' => validation_errors(),
			'users_choices' => $this->user_model->findUsersForSelect(),
			'status_choices' => Todo::getAvailableStatusChoices()
		));
	}
	
	public function edit($id) {
		if (! $object = $this->todo_model->findById($id)) {
			show_404();
		}
		
		if ($this->isPost() && $this->form_validation->run('todo')) {
			$object = array_merge($object, $this->input->post());
			
			if ($this->todo_model->updateRow($id, $object)) {
				redirect('admin/todo');
			}
		}
		else {
			$object = array_merge($object, $this->input->post());
		}
		
		$this->render('form', array(
			'title' => 'Edit todo',
			'object' => $object,
			'validation_errors' => validation_errors(),
			'users_choices' => $this->user_model->findUsersForSelect(),
			'status_choices' => Todo::getAvailableStatusChoices()
		));
	}
	
	public function idelete($todo_id) {
        $todo_item = $this->todo_model->findById($todo_id);
        if($todo_item === null){
            show_404(); // TODO SHOW MORE USER FRIENDLY MESSAGE TO THE END USER
        }		
		//user check
		
        $this->todo_model->deleteRow($todo_id);
        // TODO CHECK IF DELETE IS SUCCESSFULL AND SHOW USER FRIENDLY MESSAGE
        redirect("admin/todo");
		
	}
	
	/**
	 * 
	 * @return array
	 */
	public static function getAvailableStatusChoices() {
		return array(
			1 => 'Pending',
			2 => 'Completed',
			3 => 'Rejected',
			4 => 'Cancelled'
		);
	}
}
