<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {
	
	protected
		$load_models = array('user_model'),
		$load_helpers = array('form'),
		$has_filter = true;
	
	public function index() {
		$this->render('index', array(
			'title' => 'List users',
			'users' => $this->user_model->getCollection(),
			'groups' => $this->user_model->findGroupsForSelect()
		));
	}
	
	public function create() {
		if ($this->isPost()) {
			$data['username'] = $this->input->post('username');
			$data['email'] = $this->input->post('email');
			$data['password'] = password_hash(trim($this->input->post('password')), PASSWORD_BCRYPT);
			$data['isActive'] = $this->input->post('isActive');
			$data['group_id'] = $this->input->post('group_id');
			$data['is_super'] = $this->input->post('is_super');

			//check if exists before inserting
			if ($this->user_model->insertRow($data)) {
				redirect('admin/users');
			}
		}
		
		$this->render('form', array(
			'title' => 'Create user',
			'groups' => $this->user_model->findGroupsForSelect(),
			'object' => null
		));
	}
	
	public function edit($id) {
		if (! $object = $this->user_model->findById($id)) {
			show_404();
		}
		
		if ($this->isPost()) { 
			$data['username'] = $this->input->post('username');
			$data['email'] = $this->input->post('email');
                       
			
                        if(strlen(trim($this->input->post('password'))) > 0) {
                             $data['password'] = trim($this->input->post('password'));
                        }
                        
			!empty($data['password']) && $data['password'] = password_hash(trim($this->input->post('password')), PASSWORD_BCRYPT);
			
			$data['isActive'] = $this->input->post('isActive');
			$data['group_id'] = $this->input->post('group_id');
			$data['is_super'] = $this->input->post('is_super');
                        
			if ($this->user_model->updateRow($id, $data)) {
				redirect('admin/users');
			}
		}
		
		$this->render('form', array(
			'title' => 'Edit user',
			'groups' => $this->user_model->findGroupsForSelect(),
			'object' => $object
		));
	}
	
	public function idelete($user_id) {
        $user_row = $this->user_model->findById($user_id);
        if($user_row === null){
            show_404(); // TODO SHOW MORE USER FRIENDLY MESSAGE TO THE END USER
        }		
		//user check
		show_404();
		
        //$this->user_model->deleteUser($user_id);
        // TODO CHECK IF DELETE IS SUCCESSFULL AND SHOW USER FRIENDLY MESSAGE
        //redirect("admin/users");
	}
}
