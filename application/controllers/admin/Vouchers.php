<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vouchers extends Admin_Controller {
	
	protected
		$load_models = array('images_model', 'product_model', 'vouchers_model', 'woo_model', 'user_model'),
		$load_helpers = array('directory', 'url', 'form'),
		$load_libraries = array('image');

    public function __construct(){
        parent::__construct();
		$user = $this->session->userdata['admin_user'];
		$user_array = $this->user_model->findbyid($user['id']);
		$user_confirmed = $user_array->schedule_confirmed;
		if($user_confirmed == 0 || $user_confirmed == null){
			redirect('admin/schedule');
		}
    }
	
	/*
	 * by Stefan
	 * Displays list of current vouchers
	 */
	public function index(){
		$vouchers = $this->vouchers_model->getAllVouchers();
		$this->render('index', array(
			'title' => 'All Vouchers',
			'vouchers' => $vouchers
		));
	}
	
	/*
	 * by Stefan
	 * for creating vouchers
	 */
	public function create(){
		$facebook_pages = $this->vouchers_model->getActiveFacebookPages();
		$i = 0;
		foreach($facebook_pages as $fb_page){
			$pages[$i] = $fb_page['title'];
			$i++;
		}
		
		$vouchers_types = $this->vouchers_model->getVouchersTypes();
		$i = 0;
		$types = array();
		foreach($vouchers_types as $voucher_type){
			$types[$i] = '';
			if($voucher_type['discount']){
				$types[$i] = $voucher_type['discount'].'% '; //in case of "free shipping etc."
			}
			$types[$i] .= $voucher_type['name'];
			$i++;
		}
		
		if($this->input->post()){
			$data = $this->input->post();
			
			$rnd_code = $this->vouchers_model->getRandomCode();
			$code = sprintf('%05d', $rnd_code->rnd_code);
			
			$voucher_image = APPPATH.'../uploads/vouchers/'.$vouchers_types[$data['type']]['file']; //with full path
			$new_voucher_image = APPPATH.'../uploads/vouchers/'.$code.'.png';
			copy($voucher_image, $new_voucher_image);
			
			$ins['code'] = $code;
			$ins['name'] = $data['customer_name'];
			$ins['email'] = $data['customer_email'];
			$ins['fb_page_id'] = $facebook_pages[$data['page']]['id'];
			$ins['expiry_date'] = date('Y-m-d', strtotime("+7 days"));
			$ins['type_id'] = $vouchers_types[$data['type']]['id'];
			$ins['link'] = base_url().'uploads/vouchers/'.$code.'.png';
			$expiry_date = date('d M Y', strtotime("+7 days"));
		
			if($vouchers_types[$data['type']]['id'] == '7'){
				$ins['ref_name'] = $data['refered_name'];
				$ins['ref_email'] = $data['refered_email'];
				$this->image->makeGiftCard($new_voucher_image, $expiry_date, $code, $data['customer_name'], $data['refered_name']);
			}else{
				$this->image->makeVoucher($new_voucher_image, $expiry_date, $code);
			}
			
			$this->vouchers_model->insertRow($ins);
			
			$link = $ins['link'];
			
			redirect('admin/vouchers');
			
		}else{
			$this->render('create', array(
				'title' => 'Create Voucher',
				'pages' => $pages,
				'types' => $types
			));
		}
	}
	
	public function useVoucher($id){
		//update voucher row
		$data['used'] = 1;
		$this->vouchers_model->updateRow($id, $data);
		redirect('admin/vouchers');
	}
}