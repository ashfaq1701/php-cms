<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Woo extends Admin_Controller{

	protected
		$load_models = array('woo_model', 'product_model', 'productcategories_model', 'vouchers_model'),
		$load_helpers = array('form'),
		$load_libraries = array('form_validation', 'image'),
		$has_filter = true;
		
	public function index(){
		$this->render('index', array(
			'title' => 'List countries',
			'countries' => $this->country_model->getCollection()
		));
	}
	
	public function couponUpdate(){
		$woocommerce = $this->woo_model->wooInit();
		
		$data['name'] = 'Order Create';
		$data['topic'] = 'order.created';
		$data['secret'] = 'lo834ymrlqiu4rldmaw4iu5bql34uiylsqiw4ytm';
		$data['delivery_url'] = 'https://cms.flameflame.eu/API/webhookOrderCreate';
		
		echo "<pre>";
		//print_r($woocommerce->get('webhooks'));
		print_r($woocommerce->post('webhooks', $data));
		echo "</pre>";
	}
	
	public function updateprices(){
		$woocommerce = $this->woo_model->wooInit();
		
		//get all products woo ids
		$sql = "SELECT products.*,
					sum(ps.quantity) as total
				FROM products
				left join product_size as ps on products.id = ps.product_id
				WHERE left(products.code, 2) = 'LB'
				group by code  
				ORDER BY `products`.`code` ASC
				";
				
		$products = $this->db->query($sql)->result_array();
		
		foreach($products as $product){
			$product_prices = $this->product_model->getProductPrices($product['id']);
			foreach($product_prices as $pp){
				if($pp->country_id == '3'){
					$product_price = $pp->price;
					break;
				}
			}
			$data['regular_price'] = $product_price;
			echo "<pre>";
			//var_dump($data);
			print_r($woocommerce->put('products/'.$product['woo_id'], $data));
			echo "</pre>";
		}
	}
	
	public function testjason(){
		$this->vouchers_model->updateUseVoucherByCode('36822');
		echo "<pre>";
		//var_dump($voucher);
		/*echo '{"id":60076,
		"parent_id":0,
		"number":"60076",
		"order_key":"wc_order_592be1f67544c",
		"created_via":"checkout",
		"version":"3.0.5",
		"status":"processing",
		"currency":"EUR",
		"date_created":"2017-05-29T08:55:18",
		"date_created_gmt":"2017-05-29T08:55:18",
		"date_modified":"2017-05-29T08:55:18",
		"date_modified_gmt":"2017-05-29T08:55:18",
		"discount_total":"165.30",
		"discount_tax":"0.00",
		"shipping_total":"4.00",
		"shipping_tax":"0.00",
		"cart_tax":"0.00",
		"total":"389.70",
		"total_tax":"0.00",
		"prices_include_tax":false,
		"customer_id":1,
		"customer_ip_address":"46.10.114.252",
		"customer_user_agent":"mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/58.0.3029.110 safari/537.36",
		"customer_note":"asdfxcv",
		"billing":{"first_name":"Stefan","last_name":"Stratiev","company":"","address_1":"ul. D-r Jelezkova 21","address_2":"asd12","city":"Varna","state":"BG-03","postcode":"9010","country":"BG","email":"rabotastefan25@gmail.com","phone":"+359894578883"},
		"shipping":{"first_name":"Stefan","last_name":"Stratiev","company":"","address_1":"ul. D-r Jelezkova 21","address_2":"asd12","city":"Varna","state":"BG-03","postcode":"9010","country":"BG"},
		"payment_method":"cod",
		"payment_method_title":"Cash on Delivery",
		"transaction_id":"",
		"date_paid":"2017-05-29T08:55:18",
		"date_paid_gmt":"2017-05-29T08:55:18",
		"date_completed":null,
		"date_completed_gmt":null,
		"cart_hash":"0e27c6bea6642f21adcea0fea68e3514",
		"meta_data":[{"id":1080248,"key":"wpml_language","value":"en"},{"id":1080249,"key":"mailchimp_woocommerce_is_subscribed","value":"1"},{"id":1080250,"key":"_download_permissions_granted","value":"yes"}],
		"line_items":[{"id":169,"name":"WW128","product_id":46656,"variation_id":46657,"quantity":9,"tax_class":"","subtotal":"261.00","subtotal_tax":"0.00","total":"182.70","total_tax":"0.00","taxes":[],"meta_data":[{"id":1313,"key":"pa_size","value":"xs"}],"sku":"WW128","price":20.3},{"id":170,"name":"WW127","product_id":46965,"variation_id":46968,"quantity":10,"tax_class":"","subtotal":"290.00","subtotal_tax":"0.00","total":"203.00","total_tax":"0.00","taxes":[],"meta_data":[{"id":1323,"key":"pa_size","value":"xl"}],"sku":"WW127","price":20.3}],
		"tax_lines":[],
		"shipping_lines":[{"id":171,"method_title":"Flat Rate","method_id":"flat_rate:2","total":"4.00","total_tax":"0.00","taxes":[{"id":"total","total":[],"subtotal":""}],"meta_data":[{"id":1328,"key":"Items","value":"WW128 × 9, WW127 × 10"}]}],
		"fee_lines":[],
		"coupon_lines":[{"id":172,"code":"98146","discount":"165.3","discount_tax":"0","meta_data":[]}],
		"refunds":[]}';*/
		echo "</pre>";
	}
	
	//this one is for manual use
	//there is another in API controller
	public function syncCoupons(){
		$woocommerce = $this->woo_model->wooInit();
		$vouchers = $this->vouchers_model->getAllVouchers();
		//grab all coupons from woocommerce
		//compare if that code exists in our DB
		
		foreach($vouchers as $voucher){
			$data['code'] = $voucher['code'];
			$data['discount_type'] = 'percent';
			$data['amount'] = $voucher['discount'];
			$data['date_expires'] = $voucher['expiry_date'];
			$data['individual_use'] = true;
			$data['exclude_sale_items'] = true;
			$data['minimum_amount'] = $voucher['min_amount'];
			$data['email_restrictions'] = $voucher['email'];
			$data['usage_limit_per_user'] = 1;
			$data['usage_limit'] = 1;
			if($voucher['type_id'] == 5 || $voucher['type_id'] == 8){
				$data['free_shipping'] = true;
			}
			
			if($voucher['woo_id']){
				//print_r($this->woo_model->updateCoupon($voucher['id'], $voucher['woo_id'], $data));
				//print_r($this->woo_model->deleteCoupon($voucher['woo_id'], $voucher['id']));
			}else{
				print_r($this->woo_model->createCoupon($voucher['id'], $data));
			}
		}
	}
	
	public function updatecategories(){
		$woocommerce = $this->woo_model->wooInit();
		
		$woocats = $woocommerce->get('products/categories', ['per_page' => 50]);
		echo "<pre>";
		var_dump($woocats);
		echo "</pre>";
	}
	
	public function getallproducts(){
		$woocommerce = $this->woo_model->wooInit();
		
		//with pagination example !!!
		$i = 1;
		$wooproducts = array();
		do{
			$temp = $woocommerce->get('products', ['per_page' => 100, 'page' => $i]);
			$wooproducts = array_merge($wooproducts, $temp);
			$i++;
		}while($temp && $i<10);
		echo "<pre>";
		var_dump($wooproducts);
		echo "</pre>";
	}
	
	public function getallproductswithvariations(){
		$woocommerce = $this->woo_model->wooInit();
		
		//with pagination example !!!
		$i = 1;
		$wooproducts = array();
		do{
			$temp = $woocommerce->get('products', ['per_page' => 100, 'page' => $i]);
			$wooproducts = array_merge($wooproducts, $temp);
			$i++;
		}while($temp && $i<10);
		
		foreach($wooproducts as $wooproduct){
			$variations = $woocommerce->get('products/'.$wooproduct['id'].'/variations');
			foreach($variations as $variation){
				echo "<pre>--------";
				//var_dump($wooproduct);
				echo "<br/>vars<br/>";
				var_dump($variation['attributes']);
				echo "-------</pre>";
			}
		}
	}
	
	public function deleteCategory($woo_id){
		$woocommerce = $this->woo_model->wooInit();
		
		$result = $woocommerce->delete('products/categories/'.$woo_id, ['force' => true]);
		echo "<pre>";
		var_dump($result);
		echo "</pre>";
	}
	
	/*
	 * by Stefan
	 * Handle categories
	 */
	public function populatecategories(){
		$woocommerce = $this->woo_model->wooInit();
		
		//delete all categories - will be don once only
		//$woocategories = $woocommerce->get('products/categories');
		//$woocategories = $woocommerce->get('');
		
		$sql1 = "select *
					from product_categories";
					
		$categories = $this->db->query($sql1)->result();
		
		$i = 0;
		foreach($categories as $category){
			$data['create'][$i]['slug'] = $category->slug;
			$data['create'][$i]['name'] = $category->name_en;
			$maincat = explode("-", $category->slug);
			switch($maincat[0]){
				case 'women':{
					$data['create'][$i]['parent'] = '662';
					break;
				}
				case 'men':{
					$data['create'][$i]['parent'] = '661';
					break;
				}
				case 'gifts':{
					$data['create'][$i]['parent'] = '663';
					break;
				}
				default:
					break;
			}
			$i++;
		}
		$result = $woocommerce->post('products/categories/batch', $data);
		$i = 0;
		echo "<pre>";
		var_dump($result);
		echo "</pre>";
		foreach($result as $res2){
			foreach($res2 as $res1){
				$woocats[$i]['woo_id'] = $res1['id'];
				$woocats[$i]['slug'] = $res1['slug'];
				$i++;
			}
		}
		
		foreach($woocats as $wcat){
			$data = array();
			$data['woo_id'] = $wcat['woo_id'];
			$res3 = $this->productcategories_model->findBySlug($wcat['slug']);
			$cid = $res3['id'];
			echo "<pre>";
			var_dump($data);
			echo "</pre>";
			print_r($this->productcategories_model->updateRow($cid, $data));
		}
	}
	
	public function translateCategories(){
		$woocommerce = $this->woo_model->wooInit();
		$data = [
					'name' => 'Жени',
					'lang' => 'bg',
					'translation_of' => '662'
				];
		
		$product = $woocommerce->post('products/categories', $data);
	}
	
	public function specialupload(){
		$woocommerce = $this->woo_model->wooInit();
		
		//select all products and exclude some of them
		$sql = "SELECT products.*,
						sum(ps.quantity) as total
					FROM products
					left join product_size as ps on products.id = ps.product_id
					WHERE code not in ('WW126', 'SD279', 'SD127', 'SD185', 'WW129', 'SD154', 'SC156', 'SC260', 'SC258', 'MS223')
						and ps.quantity > 0
						and code not in ('SC263', 'SC167', 'MS159', 'LD139', 'SD310')
						and left(products.code, 2) != 'KC'
						and left(products.code, 2) != 'LB'
					group by code  
					ORDER BY total ASC
				";
		$sql2 = "SELECT products.*,
					sum(ps.quantity) as total
				FROM products
				left join product_size as ps on products.id = ps.product_id
				WHERE code not in ('WW126', 'SD279', 'SD127', 'SD185', 'WW129', 'SD154', 'SC156', 'SC260', 'SC258', 'MS223')
					and ps.quantity > 0
					and code not in ('SC263', 'SC167', 'MS159', 'LD139', 'SD310')
					and (left(products.code, 2) = 'KC'
						or left(products.code, 2) = 'LB')
				group by code  
				ORDER BY `products`.`code` ASC
				";
		$allproducts = $this->db->query($sql2)->result();
		//$allproducts = $this->db->query($sql)->result();
		$j = 0;
		foreach($allproducts as $product){
			$pid = $product->id;
			$psizes = $this->product_model->getProductSizes($pid);
			$total = 0;
			foreach($psizes as $psize){
				$total += $psize->quantity;
			}
			if($total > 0){
				if($j >= 600 && $j < 700){
					$cat = $this->productcategories_model->findBySlug($product->category);
					$cat_id = $cat['woo_id'];
					$sizes = $this->product_model->getProductSizes($product->id);
					$data = array();
					$data['name'] = strtoupper($product->code); //name of the product, its how it appears on website afterwards
					$data['sku'] = strtoupper($product->code); //product code
					$data['type'] = 'variable'; //variable - it has variations on sizes
					//cicle through sizes for price only
					$product_prices = $this->product_model->getProductPrices($product->id);
					foreach($product_prices as $pp){
						if($pp->country_id == '3'){
							$product_price = $pp->price;
							break;
						}
					}
					$data['regular_price'] = $product_price;
					//handle categories
					
					$maincat = explode("-", $product->category);
					switch($maincat[0]){
						case 'women':{
							$data['categories'][0]['id'] = '662';
							break;
						}
						case 'men':{
							$data['categories'][0]['id'] = '661';
							break;
						}
						case 'gifts':{
							$data['categories'][0]['id'] = '663';
							break;
						}
						case 'kids':{
							$data['categories'][0]['id'] = '881';
							break;
						}
						default:
							break;
					}
					$data['categories'][1]['id'] = $cat_id;
					//hanlde images
					$images = $this->product_model->getImages($product->id);
					$i = 0;
					foreach($images as $image){
						//array of the image links and their positions
						$p_image['file_path'] = APPPATH.'../uploads/'.$product->id.'/';
						$p_image['file_name'] = $image->filename;
						$p_image['file_ext'] = $image->ext;
						//create a new image for posting
						$this->image->prepareImage($p_image, strtoupper($product->code));
						//get the url of the new image
						$data['images'][$i]['src'] = base_url('uploads').'/'.$product->id.'/'.$image->filename.'_prepared'.$image->ext;
						$data['images'][$i]['position'] = $i;
						$i++;
					}
					$data['attributes'][0]['id'] = 6;
					$data['attributes'][0]['position'] = 0;
					$data['attributes'][0]['visible'] = true;
					$data['attributes'][0]['variation'] = true;
					//get only available sizes
					$i = 0;
					foreach($sizes as $size){
						if($size->quantity > 0){
							$data['attributes'][0]['options'][$i] = $size->name;
							$i++;
						}
					}
					//check if product exists
					//check for fieald woo_id in products table
					
					$woo_product = @$woocommerce->post('products', $data);
					echo 'product: '.$product->code.' was uploaded with woo_id: '.$woo_product['id'].'!<br />';
					
					$woo_id = $woo_product['id'];
					//update product in DB table products with woo_id
					$sql1 = "UPDATE products SET woo_id = '$woo_id' WHERE id = '$product->id'";
					$this->db->query($sql1);
					
					//cicle through all available sizes and create product variations
					$i=0;
					foreach($data['attributes'][0]['options'] as $size){
						$var_data['regular_price'] = $product_price;
						$var_data['attributes'][0]['id'] = 6;
						$var_data['attributes'][0]['option'] = $size;
						$var_product = @$woocommerce->post('products/'.$woo_id.'/variations', $var_data);
						$i++;
					}
				}
				$j++;
			}
		}
	}
	
	public function productslist(){
		$woocommerce = $this->woo_model->wooInit();
		$sql1 = "select *
					from products";
		$allproducts = $this->db->query($sql1)->result();
		
		$j = 0;
		foreach($allproducts as $product){
			$pid = $product->id;
			$psizes = $this->product_model->getProductSizes($pid);
			$total = 0;
			foreach($psizes as $psize){
				$total += $psize->quantity;
			}
			if($total > 10){
				if($j >= 143 && $j < 600){
					$cat = $this->productcategories_model->findBySlug($product->category);
					$cat_id = $cat['woo_id'];
					$sizes = $this->product_model->getProductSizes($product->id);
					$data = array();
					$data['name'] = strtoupper($product->code); //name of the product, its how it appears on website afterwards
					$data['sku'] = strtoupper($product->code); //product code
					$data['type'] = 'variable'; //variable - it has variations on sizes
					//cicle through sizes for price only
					$product_prices = $this->product_model->getProductPrices($product->id);
					foreach($product_prices as $pp){
						if($pp->country_id == '3'){
							$product_price = $pp->price;
							break;
						}
					}
					$data['regular_price'] = $product_price;
					//handle categories
					
					$maincat = explode("-", $product->category);
					switch($maincat[0]){
						case 'women':{
							$data['categories'][0]['id'] = '662';
							break;
						}
						case 'men':{
							$data['categories'][0]['id'] = '661';
							break;
						}
						case 'gifts':{
							$data['categories'][0]['id'] = '663';
							break;
						}
						default:
							break;
					}
					$data['categories'][1]['id'] = $cat_id;
					//hanlde images
					$images = $this->product_model->getImages($product->id);
					$i = 0;
					foreach($images as $image){
						//array of the image links and their positions
						$p_image['file_path'] = APPPATH.'../uploads/'.$product->id.'/';
						$p_image['file_name'] = $image->filename;
						$p_image['file_ext'] = $image->ext;
						//create a new image for posting
						$this->image->prepareImage($p_image, strtoupper($product->code));
						//get the url of the new image
						$data['images'][$i]['src'] = base_url('uploads').'/'.$product->id.'/'.$image->filename.'_prepared'.$image->ext;
						$data['images'][$i]['position'] = $i;
						$i++;
					}
					$data['attributes'][0]['id'] = 6;
					$data['attributes'][0]['position'] = 0;
					$data['attributes'][0]['visible'] = true;
					$data['attributes'][0]['variation'] = true;
					//get only available sizes
					$i = 0;
					foreach($sizes as $size){
						if($size->quantity > 0){
							$data['attributes'][0]['options'][$i] = $size->name;
							$i++;
						}
					}
					//cicle through all available sizes and create product variations
					
					$woo_product = @$woocommerce->post('products', $data);
					
					$i=0;
					foreach($data['attributes'][0]['options'] as $size){
						$var_data['regular_price'] = $product_price;
						$var_data['attributes'][0]['id'] = 6;
						$var_data['attributes'][0]['option'] = $size;
						$var_product = @$woocommerce->post('products/'.$woo_product['id'].'/variations', $var_data);
						$i++;
					}
				}
				$j++;
			}
		}
		
	}
	
	//rework it!!!
	public function addWooProduct($code, $category_string){
		$woocommerce = $this->woo_model->wooInit();
		$product = $this->product_model->findByCode($code);
		
		$sizes = $this->product_model->getProductSizes($product['id']);
		$data['name'] = strtoupper($product['code']); //name of the product, its how it appears on website afterwards
		$data['sku'] = strtoupper($product['code']); //product code
		$data['type'] = 'variable'; //variable - it has variations on sizes
		//handle categories
		$categories = explode(',', $category_string);
		$data['categories'] = array(); //should be array of ids of the categories from the website
		$i = 0;
		foreach($categories as $category)
		{
			$data['categories'][$i]['id'] = $category;
			$i++;
		}
		//hanlde images
		$images = $this->product_model->getImages($product['id']);
		$i = 0;
		foreach($images as $image){
			//array of the image links and their positions
			$p_image['file_path'] = APPPATH.'../uploads/'.$product['id'].'/';
			$p_image['file_name'] = $image->filename;
			$p_image['file_ext'] = $image->ext;
			//create a new image for posting
			$this->image->prepareImage($p_image, strtoupper($product['code']));
			//get the url of the new image
			$data['images'][$i]['src'] = base_url('uploads').'/'.$product['id'].'/'.$image->filename.'_prepared'.$image->ext;
			$data['images'][$i]['position'] = $i;
			$i++;
		}
		$data['attributes'][0]['name'] = 'Size';
		$data['attributes'][0]['position'] = 0;
		$data['attributes'][0]['visible'] = true;
		$data['attributes'][0]['variation'] = true;
		//cicle through sizes
		$product_prices = $this->product_model->getProductPrices($product['id']);
		foreach($product_prices as $pp){
			if($pp->country_id == '3')
				$product_price = $pp->price;
		}
		//get only available sizes
		foreach($sizes as $size){
			if($size->quantity > 0){
				$data['attributes'][0]['options'][] = $size->name;
			}
		}
		//default data, no idea why it does exists
		$data['default_attributes'][] = [
						'name' => 'Size',
						'option' => $data['attributes'][0]['options'][0] //gets the first size
					];
		//cicle through all available sizes and create product variations
		$i=0;
		foreach($data['attributes'][0]['options'] as $size){
			$data['variations'][$i]['regular_price'] = $product_price;
			$data['variations'][$i]['image']['src'] = ''; //don't need to add images to each variations if the product is the same
			$data['variations'][$i]['image']['position'] = 0;
			$data['variations'][$i]['attributes']['name'] = 'Size';
			$data['variations'][$i]['attributes']['option'] = $size;
			$i++;
		}
	}
	
	//rework it!!!
	public function updateProduct($code){
		$woocommerce = $this->woo_model->wooInit();
		//updates images and quantity of the product
		$product = $this->product_model->findByCode($code);
		$products = array();
		$data = array();
		if($product == NULL){
			echo "wrong code";
		}else{
			$woo_id = $this->woo_model->findWooProductByWooID($code);
			$wooproduct = $woocommerce->get('products/'.$woo_id);
			$woopvars = $wooproduct['variations'];
			foreach($woopvars as $var_id){
				var_dump($var_id);
				//print_r($woocommerce->delete('products/'.$woo_id.'/variations/'.$var_id, ['force' => true]));
			}
			echo "<br />------------<pre>";
			var_dump($woopvars);
			echo "</pre>";
			
			$categories = $wooproduct['categories'];
			$sizes = $this->product_model->getProductSizes($product['id']);
			
			//prepare $data to send
			$data = array();
			$data['name'] = strtoupper($product['code']); //name of the product, its how it appears on website afterwards
			$data['sku'] = strtoupper($product['code']); //product code
			$data['type'] = 'variable'; //variable - it has variations on sizes
			
			$data['categories'] = $categories; //should be array of ids of the categories from the website
			
			//hanlde images
			$images = $this->product_model->getImages($product['id']);
			$i = 0;
			foreach($images as $image){
				//array of the image links and their positions
				$p_image['file_path'] = APPPATH.'../uploads/'.$product['id'].'/';
				$p_image['file_name'] = $image->filename;
				$p_image['file_ext'] = $image->ext;
				//create a new image for posting
				$this->image->prepareImage($p_image, strtoupper($product['code']));
				//get the url of the new image
				$data['images'][$i]['src'] = base_url('uploads').'/'.$product['id'].'/'.$image->filename.'_prepared'.$image->ext;
				$data['images'][$i]['position'] = $i;
				$i++;
			}
			
			$data['attributes'][0]['name'] = 'Size';
			$data['attributes'][0]['id'] = 6;
			$data['attributes'][0]['position'] = 1;
			$data['attributes'][0]['visible'] = true;
			$data['attributes'][0]['variation'] = true;
			
			//cicle through prices
			//gets only euro
			$product_prices = $this->product_model->getProductPrices($product['id']);
			foreach($product_prices as $pp){
				if($pp->country_id == '3')
					$product_price = $pp->price;
			}
			$data['regular_price'] = $product_price;
			//get only available sizes
			$i = 0;
			foreach($sizes as $size){
				if($size->quantity > 0){
					$data['attributes'][0]['options'][$i] = $size->name;
					$i++;
				}
			}
			
			//default data, no idea why it does exists
			$data['default_attributes'][] = [
							'name' => 'Size',
							'option' => $data['attributes'][0]['options'][0] //gets the first size
						];
						
			//cicle through all available sizes and create product variations
			$i=0;
			foreach($sizes as $size){
				if($size->quantity > 0){
					//$data['variations'][$i]['image']['src'] = ''; //don't need to add images to each variations if the product is the same
					//$data['variations'][$i]['image']['position'] = 0;
					$data['variations'][$i]['attributes'][0]['id'] = 6;
					//$data['variations'][$i]['attributes'][0]['name'] = 'Size';
					$data['variations'][$i]['attributes'][0]['option'] = $size->name;
					/*$data['variations'][$i]['attributes']['id'] = 6;
					$data['variations'][$i]['attributes']['name'] = strtolower($size->name);
					$data['variations'][$i]['attributes']['option'] = $size->name;*/
					$i++;
				}
			}
			
			//print_r($woocommerce->put('products/'.$woo_id, $data));
			echo "<br />------------<pre>";
			var_dump($wooproduct);
			echo "</pre>";
		}
	}
	
	public function deleteWooProduct($woo_id){
		$woocommerce = $this->woo_model->wooInit();
		
		print_r($woocommerce->delete('products/'.$woo_id, ['force' => true]));
	}
	
	public function getWooProduct($woo_id){
		$woocommerce = $this->woo_model->wooInit();
		echo "<pre>";
		print_r($wooproduct = $woocommerce->get('products/'.$woo_id));
		echo "</pre>";
	}
	
	public function getWooProductByCode($code){
		$woocommerce = $this->woo_model->wooInit();
		//you need to use search here
		/*echo "<pre>";
		print_r($wooproduct = $woocommerce->get(''));
		echo "</pre>";*/
	}
	
	//rework it!!!
	public function addarray(){
		$woocommerce = $this->woo_model->wooInit();
		
		$category_string = "35"; //categories for products to add
		$code_string = "MS181,MS171"; //product codes to be added ',' sepparated
		$codes_array = explode(',', $code_string);
		
		foreach($codes_array as $code){ //move it to another function
			$product = $this->product_model->findByCode($code);
			$sizes = $this->product_model->getProductSizes($product['id']);
			$data = array();
			$data['name'] = strtoupper($product['code']); //name of the product, its how it appears on website afterwards
			$data['sku'] = strtoupper($product['code']); //product code
			$data['type'] = 'variable'; //variable - it has variations on sizes
			//handle categories
			$categories = explode(',', $category_string);
			$data['categories'] = array(); //should be array of ids of the categories from the website
			$i = 0;
			foreach($categories as $category)
			{
				$data['categories'][$i]['id'] = $category;
				$i++;
			}
			//hanlde images
			$images = $this->product_model->getImages($product['id']);
			$i = 0;
			foreach($images as $image){
				//array of the image links and their positions
				$p_image['file_path'] = APPPATH.'../uploads/'.$product['id'].'/';
				$p_image['file_name'] = $image->filename;
				$p_image['file_ext'] = $image->ext;
				//create a new image for posting
				$this->image->prepareImage($p_image, strtoupper($product['code']));
				//get the url of the new image
				$data['images'][$i]['src'] = base_url('uploads').'/'.$product['id'].'/'.$image->filename.'_prepared'.$image->ext;
				$data['images'][$i]['position'] = $i;
				$i++;
			}
			$data['attributes'][0]['name'] = 'Size';
			$data['attributes'][0]['position'] = 0;
			$data['attributes'][0]['visible'] = true;
			$data['attributes'][0]['variation'] = true;
			//cicle through sizes
			$product_prices = $this->product_model->getProductPrices($product['id']);
			foreach($product_prices as $pp){
				if($pp->country_id == '3')
					$product_price = $pp->price;
			}
			//get only available sizes
			foreach($sizes as $size){
				if($size->quantity > 0){
					$data['attributes'][0]['options'][] = $size->name;
				}
			}
			//default data, no idea why it does exists
			$data['default_attributes'][] = [
							'name' => 'Size',
							'option' => $data['attributes'][0]['options'][0]
						];
			//cicle through all available sizes and create product variations
			$i=0;
			foreach($data['attributes'][0]['options'] as $size){
				$data['variations'][$i]['regular_price'] = $product_price;
				$data['variations'][$i]['image']['src'] = ''; //cicle through all images?!?! it works for some reason as is
				$data['variations'][$i]['image']['position'] = 0; //cicle through all images?!?! it works for some reason as is
				$data['variations'][$i]['attributes']['name'] = 'Size';
				$data['variations'][$i]['attributes']['option'] = $size;
				$i++;
			}
			/*echo "<pre>";
			var_dump($data);
			echo "</pre>";*/
			$woocommerce->post('products', $data);
		}
	}
	
	/*
	 * by Stefan
	 * Manually adds a product to paradiseshop.eu
	 * not working
	 */
	public function AddProduct(){
		$woocommerce = $this->woo_model->wooInit();
		
		$object = $this->input->post();
		if($this->isPost()){
			//move it to another function!!!
			$product = $this->product_model->findByCode($object['code']);
			$sizes = $this->product_model->getProductSizes($product['id']);
			$data['name'] = $product['code']; //name of the product, its how it appears on website afterwards
			$data['sku'] = $product['code']; //product code
			$data['type'] = 'variable'; //variable - it has variations on sizes
			//description
			$data['description'] = $object['description']; //product description
			$data['short_description'] = $object['short_description']; //product short description
			//handle categories
			$categories = explode(',', $object['categories']);
			$data['categories'] = array(); //should be array of ids of the categories from the website
			$i = 0;
			foreach($categories as $category){
				$data['categories'][$i]['id'] = $category;
				$i++;
			}
			//hanlde images
			$images = $this->product_model->getImages($product['id']);
			$i = 0;
			foreach($images as $image){
				//array of the image links and their positions
				$p_image['file_path'] = APPPATH.'../uploads/'.$product['id'].'/';
				$p_image['file_name'] = $image->filename;
				$p_image['file_ext'] = $image->ext;
				//create a new image for posting
				$this->image->prepareImage($p_image, strtoupper($product['code']));
				//get the url of the new image
				$data['images'][$i]['src'] = base_url('uploads').'/'.$product['id'].'/'.$image->filename.'_prepared'.$image->ext;
				$data['images'][$i]['position'] = $i;
				$i++;
			}
			$data['attributes'][0]['name'] = 'Size';
			$data['attributes'][0]['position'] = 0;
			$data['attributes'][0]['visible'] = true;
			$data['attributes'][0]['variation'] = true;
			//cicle through prices
			$product_prices = $this->product_model->getProductPrices($product['id']);
			foreach($product_prices as $pp){
				if($pp->country_id == '3')
					$product_price = $pp->price;
			}
			//get only available sizes
			foreach($sizes as $size){
				if($size->quantity > 0){
					$data['attributes'][0]['options'][] = $size->name;
				}
			}
			//default data, no idea why it does exists
			$data['default_attributes'][] = [
							'name' => 'Size',
							'option' => $data['attributes'][0]['options'][0]
						];
			//cicle through all available sizes and create product variations
			$i=0;
			foreach($data['attributes'][0]['options'] as $size){
				$data['variations'][$i]['regular_price'] = $product_price;
				$data['variations'][$i]['image']['src'] = ''; //cicle through all images?!?!
				$data['variations'][$i]['image']['position'] = 0; //cicle through all images?!?!
				$data['variations'][$i]['attributes']['name'] = 'Size';
				$data['variations'][$i]['attributes']['option'] = $size;
				$i++;
			}
			$woocommerce->post('products', $data);
			//print_r($woocommerce->post('products', $data));
			redirect('admin/woo/addproduct');
		}else{
			$this->render('add_product', array(
				'title' => 'Add Product',
				'object' => $object
			));
		}
	}
}
