<?php
/*
 * by Stefan
 * Relationships with Paradise Bot and www.paradiseshop.eu WooCommerce
 */
class API extends CI_Controller{
	
    public function __construct(){
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('vouchers_model');
        $this->load->model('woo_model');
        $this->load->model('user_model');
		$this->load->helper('url');
		$this->load->library('image');
    }
	
	public function coupon_update(){
		$data = json_decode(file_get_contents("php://input"), true);
		//ob_flush();
		ob_start();
		var_dump($data);
		file_put_contents('file.txt', ob_get_flush());
		//file_put_contents('file.txt', $data);
	}
	
	public function testjson(){
		$data = json_decode(file_get_contents("php://input"), true);
		ob_start();
		var_dump($data);
		file_put_contents('json_test.txt', ob_get_flush());
	}
	
	public function webhookOrderCreate(){
		$data = json_decode(file_get_contents("php://input"), true);
		
		if($data['coupon_lines']){
			foreach($data['coupon_lines'] as $coupon){
				$code = $coupon['code'];
				$voucher = $this->vouchers_model->getVoucherByCode($code);
				$this->vouchers_model->updateUseVoucherByCode($voucher['code']);
				if($voucher['type_id'] == '7'){
					$referee_name = $voucher['ref_name'];
					$referee_email = $voucher['ref_email'];
					$data = array('referee_name' => $referee_name, 'referee_email' => $referee_email);                                                                    
					$data_string = json_encode($data);
					
					$ch = curl_init('https://cms.flameflame.eu/cli/API/testjson');
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',                                                                                
						'Content-Length: ' . strlen($data_string))
					);
					$result = curl_exec($ch);
				}
			}
			
			ob_start();
			var_dump($data);
			//var_dump($data['coupon_lines'][0]['code']);
			file_put_contents('order_create.txt', ob_get_flush());
		}
	}

	public function getVoucherLink($customer_name, $email, $voucher_type_id, $referee_name = '', $referee_email = ''){
		$customer_name = str_replace('%20', ' ', $customer_name);
		$referee_name = str_replace('%20', ' ', $referee_name);

		$vouchers_types = $this->vouchers_model->getVouchersTypes();

		$rnd_code = $this->vouchers_model->getRandomCode();
		$code = sprintf('%05d', $rnd_code->rnd_code);

		$voucher_image = APPPATH.'../uploads/vouchers/'.$vouchers_types[$voucher_type_id - 1]['file']; //with full path
		$new_voucher_image = APPPATH.'../uploads/vouchers/'.$code.'.png';
		copy($voucher_image, $new_voucher_image);

		$ins['code'] = $code;
		$ins['name'] = $customer_name;
		$ins['email'] = $email;
		$ins['fb_page_id'] = '43';
		$ins['expiry_date'] = date('Y-m-d', strtotime("+7 days"));
		$ins['type_id'] = $voucher_type_id;
		$ins['link'] = base_url().'uploads/vouchers/'.$code.'.png';
		$expiry_date = date('d M Y', strtotime("+7 days"));
		
		if($voucher_type_id == '7'){
			$ins['ref_name'] = $referee_name;
			$ins['ref_email'] = $referee_email;
			$this->image->makeGiftCard($new_voucher_image, $expiry_date, $code, $customer_name, $referee_name);
		}else{
			$this->image->makeVoucher($new_voucher_image, $expiry_date, $code);
		}


		$this->vouchers_model->insertRow($ins);
		$ret=array();
		if($ins['link']){
			$ret['link'] = $ins['link'];
			$ret['code'] = $ins['code'];
			$ret['expiry_date'] = $ins['expiry_date'];
			$result = json_encode($ret);
			echo $result;
			return $result;
		}else{
			return false;
		}
	}
	
	public function importCoupon($data){
		//get data from customer
			//name, email, voucher_type
		
		//create voucher in the system
			
		//send voucher to coustomer
			//email to them
		//upload voucher in woocommerce
			//print_r($this->woo_model->updateCoupon($voucher['id'], $voucher['woo_id'], $data));
	}
	
	public function syncCoupons(){
		$woocommerce = $this->woo_model->wooInit();
		$vouchers = $this->vouchers_model->getAllVouchers();
		//grab all coupons from woocommerce
		//compare if that code exists in our DB
		
		foreach($vouchers as $voucher){
			$data['code'] = $voucher['code'];
			switch($voucher['discount_type']){
				case '1':{
					$data['discount_type'] = 'percent';
					break;
				}
				case '2':{
					$data['discount_type'] = 'percent';
					$data['free_shipping'] = true;
					break;
				}
				case '3':{
					$data['discount_type'] = 'fixed_cart';
					break;
				}
			}
			$data['amount'] = $voucher['discount'];
			$data['date_expires'] = $voucher['expiry_date'];
			$data['individual_use'] = true;
			$data['exclude_sale_items'] = true;
			$data['minimum_amount'] = $voucher['min_amount'];
			$data['email_restrictions'] = $voucher['email'];
			$data['usage_limit_per_user'] = 1;
			$data['usage_limit'] = 1;
			
			if($voucher['woo_id']){
				//print_r($this->woo_model->updateCoupon($voucher['id'], $voucher['woo_id'], $data));
				//print_r($this->woo_model->deleteCoupon($voucher['woo_id'], $voucher['id']));
			}else{
				$this->woo_model->createCoupon($voucher['id'], $data);
			}
		}
	}

	public function allproducts(){
		$products = $this->product_model->getAllProductsCodes();
		$ps = array();
		$i=0;
		foreach($products as $product){
			//if($product->code == 'SD110'){ //if you want to send an array with just 1 product
				$ps[$i] = $this->product($product->code);
				$i++;
			//}
		}

		$url = 'http://02d94a9d.ngrok.io/product';

		$data_string = json_encode($ps);

		$ch = curl_init($url);   // where to post
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);

		$result = curl_exec($ch);
	}

	public function product($code){
		$dbproduct = $this->product_model->findByCode($code);

		$product['code'] = str_replace("/", '-', $dbproduct['code']);
		$product['date_added'] = $dbproduct['date_added'];
		$product['tags'] = $dbproduct['tags'];
		$product['colors'] = $dbproduct['colors'];
		$product['category'] = $dbproduct['category'];
		$product['onsale'] = $dbproduct['onsale'];
		$product['fit'] = '';
		$product['fabric_quality'] = '';
		$product['manufacturing_quality'] = '';
		$product['season'] = '';
		$product['restockable'] = $dbproduct['is_stock_available'];
		$product['new_delivery_expected'] = $dbproduct['is_preorder'];
		$product['related_products'] = '';
		$product['product_speed'] = '';
		$product['weight'] = $dbproduct['weight'];
		$product['sizes'] = array();
		$sizes = $this->product_model->getProductSizes($dbproduct['id']);
		$i = 0;
		foreach($sizes as $size){
			if($size->quantity > 0){
				$product['sizes'][$i]['title'] = $size->name;
				$product['sizes'][$i]['quantity'] = $size->quantity;
				$product['sizes'][$i]['dimensions'] = $size->description;
				$i++;
			}
		}
		$product['prices'] = array();
		$prices = $this->product_model->getProductPrices($dbproduct['id']);
		$i = 0;
		foreach($prices as $price){
			$product['prices'][$i]['price'] = $price->price;
			$product['prices'][$i]['country'] = $price->name;
			$product['prices'][$i]['currency'] = $price->currency;
			$i++;
		}
		$product['images'] = array();
		$images = $this->product_model->getImages($dbproduct['id']);
		$i = 0;
		foreach($images as $image){
			$url1 = base_url('uploads').'/'.$dbproduct['id'].'/'.$image->filename.'_resized'.$image->ext; //get the address with https
			$url = str_replace("https:","http:",$url1); //change https with http
			$product['images'][$i]['url'] = $url;
			$i++;
		}
		return $product;
	}
}
?>
