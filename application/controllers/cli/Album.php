<?php

class Album extends CI_Controller {
  protected
  $load_models = array('album_model', 'facebook_model', 'product_model', 'images_model'),
  $load_libraries = array('facebooklib', 'facebookspider');

  public function __construct() {
    parent::__construct();
    $this->load->model($this->load_models);
    $this->load->library($this->load_libraries);
  }

  public function refreshAllPhotos() {
    $logFile = __DIR__.'/../../logs/refresh_photos.log';
    ini_set('memory_limit', '-1');
    set_time_limit(60*60*60*60);
    try {
      $pages = $this->facebook_model->getAllPages();
      $albums = $this->album_model->getAllAlbums();
      foreach ($pages as $page) {
        foreach ($albums as $album) {
          // check if album exists, if not remove it from database
          $albumInfo = $this->facebooklib->getAlbumInformation($album, $page);
          if (empty($albumInfo)) {
            $albumPhotos1 = $this->album_model->getAlbumPhotos($album);
            foreach ($albumPhotos1 as $albumPhoto) {
              $this->images_model->deleteFacebookPhoto($albumPhoto['id']);
            }
            $this->album_model->deleteRow($album['id']);
            continue;
          }
          // check each photo from the album, remove currently non-existed photos from database
          $photos = $this->album_model->getAlbumPhotos($album);
          foreach ($photos as $photo) {
            $photoInfo = $this->facebooklib->getPhotoInformation($photo, $page);
            if (empty($photoInfo)) {
              $this->images_model->deleteFacebookPhoto($photo['id']);
            }
          }
          // now download the photos
          $photos = $this->facebooklib->getPhotos($album, $page);
          $series = $this->product_model->getProductCodeSeries();
          $photos = $this->facebooklib->addProductCodes($photos, $series);
          $uploadsDir = __DIR__.'/../../../uploads/';
          $i = 0;
          foreach ($photos as $photo) {
            $fbPhotoId = $photo['id'];
            $facebookImages = $this->images_model->searchFacebookPhotosByAll(['fb_id' => $fbPhotoId]);
            if (count($facebookImages) == 0) {
              $product = null;
              if(!empty($photo['product_code']))
              {
                $product = $this->product_model->findByCode($photo['product_code']);
                if(!is_dir($uploadsDir.$product['code']))
                {
                  mkdir($uploadsDir.$product['code']);
                }
                $this->facebooklib->copyImageToLocal($photo['src'], $uploadsDir.$product['code'].'/');
                $photoUrl = $this->config->item('base_url').'/uploads/'.$product['code'].'/'.$this->facebooklib->getFileName($photo['src']);
              }
              else
              {
                if(!is_dir($uploadsDir.'unspecified'))
                {
                  mkdir($uploadsDir.'unspecified');
                }
                $this->facebooklib->copyImageToLocal($photo['src'], $uploadsDir.'unspecified'.'/');
                $photoUrl = $this->config->item('base_url').'/uploads/unspecified/'.$this->facebooklib->getFileName($photo['src']);
              }
              $imageData = [
                'date_added' => date('Y-m-d H:i:s'),
                'url' => $photoUrl,
                'file_name' => $this->facebooklib->getFileName($photo['src']),
                'file_ext' => pathinfo($photo['src'], PATHINFO_EXTENSION)
              ];
              if(!empty($product))
              {
                $imageData['code'] = $product['code'];
              }
              $imageId = $this->images_model->insertImageRow($imageData);
              $facebookImageData = [
                'product_id' => $product['id'],
                'image_id' => $imageId,
                'fb_id' => $photo['id'],
                'fb_album_id' => $album['id'],
                'photo_order' => ++$i
              ];
              if(!empty($photo['name']))
              {
                $facebookImageData['additional_notes'] = $photo['name'];
              }
              if(!empty($product))
              {
                $facebookImageData['product_id'] = $product['id'];
              }
              $facebookImageId = $this->images_model->insertFacebookPhotoRow($facebookImageData);
            }
            else {
              $facebookImage = $facebookImages[0];
              $facebookImageId = $facebookImage['id'];
              $facebookImageData = [
                'photo_order' => ++$i
              ];
              $this->images_model->updateFacebookPhotoRow($facebookImageId, $facebookImageData);
            }
          }
        }
      }
    } catch (\Exception $e) {
      file_put_contents($logFile, "Album refresh crashed at " . date('Y-m-d H:i:s') . "\n", FILE_APPEND);
    }
    file_put_contents($logFile, "Album refresh processed at " . date('Y-m-d H:i:s') . "\n", FILE_APPEND);
  }
}
