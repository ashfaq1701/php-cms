<?php

class Facebook extends CI_Controller {
  protected
  $load_models = array('album_model', 'facebook_model', 'images_model', 'commonoption_model'),
  $load_libraries = array('facebooklib', 'facebookspider');

  public function __construct() {
    parent::__construct();
    $this->load->model($this->load_models);
    $this->load->library($this->load_libraries);
  }

  public function processPhotos() {
    $logFile = __DIR__.'/../../logs/process_photos.log';
    try {
      $unprocessedPhotos = $this->images_model->getUnprocessedPhotos();
      foreach ($unprocessedPhotos as $pageId => $albumPhotos) {
        foreach ($albumPhotos as $albumId => $batchPhotos) {
          foreach ($batchPhotos as $batch => $photos) {
            $pageId = intval(trim($pageId));
            $albumId = intval(trim($albumId));
            $page = $this->facebook_model->findById($pageId);
            $album = $this->album_model->findAlbumById($albumId);
            $photoObjs = array();
            foreach ($photos as $photo) {
              $descStr = $photo['additional_notes'];
              $imageId = $photo['image_id'];
              $image = $this->images_model->findById($imageId);
              $fileUrl = $image['url'];
              $photoObjs[] = array(
                'message' => $descStr,
                'url' => $fileUrl
              );
            }
            $returnIds = $this->facebooklib->addPhotos($album, $page, $photoObjs);
            for ($i = 0; $i < count($photos); $i++) {
              $fbPostId = $returnIds[$i]['post_id'];
              $fbPost = $this->facebook_model->findFacebookPostByFbId($fbPostId);
              if (!empty($fbPost)) {
                $postId = $fbPost['id'];
              } else {
                $this->facebook_model->insertFacebookPostsRow(array(
                  'product_id' => $photos[$i]['product_id'],
                  'facebook_page_id' => $pageId,
                  'description' => $photos[$i]['description_format'],
                  'is_uploaded' => 1,
                  'fb_post_id' => $fbPostId
                ));
                $fbPost = $this->facebook_model->findFacebookPostByFbId($fbPostId);
                $postId = $fbPost['id'];
              }
              $photo = $photos[$i];
              $photo['fb_id'] = $returnIds[$i]['id'];
              $photo['fb_post_id'] = $postId;
              $photo['processed_at'] = date('Y-m-d H:i:s');
              $photoID = $photo['id'];
              unset($photo['id']);
              unset($photo['page_id']);
              $this->images_model->updateFacebookPhotoRow($photoID, $photo);
            }
            file_get_contents('http://flame-cms-ash.tk/cli/facebook/reorder?album_fb_id='.$album['fb_id']);
            $photoIds = $this->facebooklib->getAlbumPhotoIds($album, $page);
            $this->images_model->updatePhotoOrderings($photoIds);
          }
        }
      }
    } catch (\Exception $e) {
      file_put_contents($logFile, "Photo uploads crashed at " . date('Y-m-d H:i:s') . "\n", FILE_APPEND);
    }
    file_put_contents($logFile, "Photo uploads processed at " . date('Y-m-d H:i:s') . "\n", FILE_APPEND);
  }

  public function reorderPhotos() {
    file_put_contents('/var/www/reorder.log', "Reordering request came at " . date('Y-m-d H:i:s') . "\n", FILE_APPEND);
    $fbCookies = $this->commonoption_model->getFacebookCookies();
    $this->facebookspider->setCookies(json_decode($fbCookies, true));
    if($this->facebookspider->ifCookiesValid() == false)
    {
      $fbEmail = $this->commonoption_model->getFacebookEmail();
      $fbPassword = $this->commonoption_model->getFacebookPassword();
      $this->facebookspider->setEmail($fbEmail);
      $this->facebookspider->setPassword($fbPassword);
      $fbCookies = $this->facebookspider->login();
      $this->commonoption_model->setFacebookCookies(json_decode($fbCookies, true));
    }
    $albumId = $_GET['album_fb_id'];
    $this->facebookspider->uploadAlbumOrdering($albumId);
  }
}
