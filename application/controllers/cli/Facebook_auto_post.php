<?php

/**
 * Created by PhpStorm.
 * User: mrgreen
 * Date: 12.12.16
 * Time: 01:27
 */
class Facebook_auto_post extends CI_Controller
{

    private $post;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('facebook_model');
        $this->load->model('product_model');
        $this->load->model('country_model');
        $this->load->model('images_model');

        $this->load->library('image_lib');
        $this->load->library('image');
        $this->load->library('money');
    }

    public function init(){
        echo "------START------";
		
        $posts = $this->facebook_model->AP_getAllfacebookPosts(); // posts to upload
        foreach($posts as $post){
            $this->post = $post;
				/*echo "<pre>";
				var_dump($post);
				echo "</pre>";*/
            $response = $this->postToFacebook2($post);
            if($response){
				echo "<pre>------------------------------<br />";
				var_dump($response);
				echo "</pre>-----------------------------<br />";
                $this->facebook_model->updateFacebookPost($post->id, array('is_uploaded'=>1));
            }
            //sleep(rand(7,20));
        }
        echo "<br />------END------<br />";
    }
	
	public function updateTestPosts(){
		//get all posts for upload
		$sql1 = "select *,
						facebook_tests_posts.id as post_id
					from facebook_tests_posts
					left join facebook_pages as fb_pages on fb_pages.id = facebook_tests_posts.page_id
					where (status = 0 or status is NULL)
					order by facebook_tests_posts.id asc
				";
		$posts = $this->db->query($sql1)->result_array();
		
		foreach($posts as $post){
            $response = $this->postTestToFacebook($post);
			echo "<pre>";
			var_dump($response);
			echo "</pre>";
            sleep(rand(1,5));
        }
	}
	
	public function deleteTestPosts(){
		//get all posts for upload
		$sql1 = "select *,
						facebook_tests_posts.id as post_id
					from facebook_tests_posts
					left join facebook_pages as fb_pages on fb_pages.id = facebook_tests_posts.page_id
					where status = 2
					order by facebook_tests_posts.id asc
				";
		$posts = $this->db->query($sql1)->result_array();
		
		foreach($posts as $post){
            $this->deleteFBImage($post['page_id'], $post['post_id']);
            sleep(rand(1,5));
        }
	}
	
	public function markBadTestPosts(){
		//get all posts for upload
		$sql1 = "update facebook_tests_posts
					set status = 2
					where (reactions_initial + comments_initial) < 1
						and (reactions_1 + comments_1) < 1
						and status = 1
				";
		$this->db->query($sql1);
	}
	
	public function deleteFBImage($page_id, $post_id){
		
		$page = $this->facebook_model->findById($page_id);
		
		$config = array();
		$config['app_id'] = $page['app_id'];
		$config['app_secret'] = $page['secret_id'];
		$config['access_token'] = $page['access_token'];
		$config['default_graph_version'] = 'v2.9';
		$fb = new Facebook\Facebook($config);
		$fbApp = $fb->getApp();
		
		$sql1 = "select *
					from facebook_tests_images
					where post_id = '$post_id'
				";
				
		$images = $this->db->query($sql1)->result_array();
		
		$sql2 = "select *
					from facebook_tests_posts as ftp
					left join facebook_pages as fbp on ftp.page_id = fbp.id
					where ftp.id = '$post_id'
				";
		$posts = $this->db->query($sql2)->result_array();
		$post = $posts[0];
		
		$params[0]['method'] = "GET";
		$params[0]['relative_url']  = $post['fb_post_id'].'/insights/post_impressions';
		$params[1]['method'] = "GET";
		$params[1]['relative_url']  = $post['fb_post_id'].'/insights/post_reactions_by_type_total';
		$params[2]['method'] = "GET";
		$params[2]['relative_url']  = $post['fb_post_id'].'/comments?limit=50';
		
		$request = new Facebook\FacebookRequest($fbApp, $post['access_token'], 'POST', '?batch='.urlencode(json_encode($params)));
		try{
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			echo 'Graph returned an error: '.$e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			echo 'Facebook SDK returned an error: '.$e->getMessage();
			exit;
		}
		$fb_data = $response->getBody();
		$fb_data_array = json_decode($fb_data);
			
		$impressions = json_decode($fb_data_array[0]->body)->data[0]->values[0]->value;
		
		$reactions = json_decode($fb_data_array[1]->body)->data[0]->values[0]->value;
		$total_reactions = 0;
		foreach($reactions as $react){
			$total_reactions += $react;
		}
		
		$comments = json_decode($fb_data_array[2]->body)->data;
		$total_comments = count($comments);
		
		if(($total_comments + $total_reactions) > 0){
			$sql3 = "update facebook_tests_posts
						set status = 1
						where id = '$post_id'
					";
			$this->db->query($sql3);
		}else{
			
			$i = 0;
			foreach($images as $image){
				$params[$i]['method'] = "DELETE";
				$params[$i]['relative_url']  = '/'.$image['fb_image_id'];
				$i++;
			}
			
			$request = new Facebook\FacebookRequest($fbApp, $post['access_token'], 'POST', '?batch='.urlencode(json_encode($params)));
			try{
				$response = $fb->getClient()->sendRequest($request);
			}catch(Facebook\Exceptions\FacebookResponseException $e){
				echo 'Graph returned an error: '.$e->getMessage();
				exit;
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				echo 'Facebook SDK returned an error: '.$e->getMessage();
				exit;
			}
			$fb_data = $response->getBody();
			
			$sql3 = "update facebook_tests_posts
						set impressions_deleted = '$impressions',
							reactions_deleted = '$total_reactions',
							comments_deleted = '$total_comments',
							status = 3,
							date_deleted = NOW()
						where id = '$post_id'
					";
			$this->db->query($sql3);
		}
	}
	
	public function postTestToFacebook($post){
		
		$page_id = $post['fb_id'];
		$page_id_db = $post['page_id'];
		$sql5 = "select *
					from facebook_posts
					where facebook_page_id = '$page_id_db'
						and DATE_ADD(date_schedule, INTERVAL 2 HOUR) < NOW()
						and DATE_SUB(date_schedule, INTERVAL 2 HOUR) > NOW()
					LIMIT 1
				";
		$faps = $this->db->query($sql5)->result();
		
		$sql6 = "select *
					from facebook_tests_posts
					where page_id = '$page_id_db'
						and DATE_ADD(date_uploaded, INTERVAL 2 HOUR) > NOW()
					LIMIT 1
				";
		$tests_now = $this->db->query($sql6)->result();
			
			//make a call to FB to check if there is a post in last 1-2 hours
			
		if(empty($faps) && empty($tests_now)){
			
			$description = $post['description'];
			$post_id = $post['post_id'];
			$sql2 = "select *
						from facebook_tests_images
						where post_id = '$post_id'
					";
			$images = $this->db->query($sql2)->result_array();
			$price_eur = $images[0]['price_eur'];
			$sizes = '';
			if(isset($images[0]['sizes']))
				$sizes = $images[0]['sizes'];
			//$description = $this->parseTestsDescription($description, $sizes, $price_eur, $post['country']);
			$params2 = array(
						"access_token" => $post['access_token'],
						"message" => $description,
						"name" => $description
					);
			
			$i = 0;
			foreach($images as $image){
				
				$data['file_path'] = APPPATH.'../uploads/tests/';
				$data['file_name'] = $image['file_name'];
				$data['file_ext'] = $image['file_ext'];
				$file_to = $data['file_path'].$data['file_name'].$data['file_ext'];
				$code = '';
				$brand = $this->images_model->findBrandIDByShortName($post['brand_name']);
				$this->image->brandImage2($data, $code, $brand);
				
				$config = array();
				$config['app_id'] = $post['app_id'];
				$config['app_secret'] = $post['secret_id'];
				$config['default_graph_version'] = 'v2.9';
				$fb = new Facebook\Facebook($config);
				$description = $post['description'];
				$params = array(
					"access_token" => $post['access_token'],
					"message" => $description,
					"name" => $description,
					"published" => "false",
					"source" => $fb->fileToUpload($file_to),
					"description" => $description
				);
				
				try{
					$ret = $fb->post("/$page_id/photos", $params); //EDIT THIS TO BE ABLE TO POST TO TIMELINE
					//move to model
					$fb_post = $ret->getBody();
					$fb_post_array = json_decode($fb_post);
					$fb_post_id = $fb_post_array->id;
					echo $fb_post_id.'<br />';
					$params2['attached_media['.$i.']'] = '{"media_fbid":"'.$fb_post_id.'"}';
				}catch(Exception $e){
					echo $e->getMessage();
				}
				$i++;
				
				$image_id = $image['id'];
				$sqlui = "update facebook_tests_images
							set fb_image_id = '$fb_post_id'
							where id = '$image_id'
						";
				
				$this->db->query($sqlui);
			}
			
			$place = '/'.$page_id.'/feed';

			try{
				$response = $fb->post($place, $params2, $post['access_token']);
			}catch(Facebook\Exceptions\FacebookResponseException $e){
				echo 'Graph returned an error: '.$e->getMessage();
				exit;
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				echo 'Facebook SDK returned an error: '.$e->getMessage();
				exit;
			}
			
			$fb_post = $response->getBody();
			$fb_post_array = json_decode($fb_post);
			$full_fb_post_id = $fb_post_array->id;
			echo $full_fb_post_id;
			//update post id in db
			$sql7 = "update facebook_tests_posts
						set status = 1,
							fb_post_id = '$full_fb_post_id',
							date_uploaded = NOW(),
							date_initial = date_add(NOW(), interval 10 minute),
							date_1 = date_add(NOW(), interval 1 hour),
							date_2 = date_add(NOW(), interval 6 hour),
							date_3 = date_add(NOW(), interval 24 hour)
						where id = '$post_id'";
			$this->db->query($sql7);
		}else{
			return false;
		}
	}
	
	public function getTestsPostData($post){
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		
		$fb = new Facebook\Facebook(array('app_id'=>$post['app_id'], 'app_secret'=>$post['secret_id'], 'default_graph_version'=>'v2.9'));
		$fbApp = $fb->getApp();
		$place1 = $post['fb_post_id'].'/insights/post_impressions';
		$place2 = $post['fb_post_id'].'/insights/post_reactions_by_type_total';
		$place3 = $post['fb_post_id'].'/comments?limit=50';
		$params = [
					[
					  "method"  => "GET",
					  "relative_url"  => $place1
					],
					[
					  "method"  => "GET",
					  "relative_url"  => $place2
					],
					[
					  "method"  => "GET",
					  "relative_url"  => $place3
					]
				  ];		
		//what this function requests from facebook
		$request = new Facebook\FacebookRequest($fbApp, $post['access_token'], 'POST', '?batch='.urlencode(json_encode($params)));
		try{
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			echo 'Graph returned an error: '.$e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			echo 'Facebook SDK returned an error: '.$e->getMessage();
			exit;
		}
		$fb_data = $response->getBody();
		$fb_data_array = json_decode($fb_data);
		return $fb_data_array;
	}
	
	public function getTestPostsData(){
		
		//5-10 minutes
		$sql1 = "select *,
						facebook_tests_posts.id as post_id
					from facebook_tests_posts
					left join facebook_pages as fbp on fbp.id = facebook_tests_posts.page_id
					where status = 1
						and fb_post_id is not NULL
						and (now() between date_sub(date_initial, interval 7 minute) and date_add(date_initial, interval 7 minute))
				";
		$posts = $this->db->query($sql1)->result_array();
		
		foreach($posts as $post){
			$fb_data_array = $this->getTestsPostData($post);
			
			$impressions = json_decode($fb_data_array[0]->body)->data[0]->values[0]->value;
			
			$reactions = json_decode($fb_data_array[1]->body)->data[0]->values[0]->value;
			$total_reactions = 0;
			foreach($reactions as $react){
				$total_reactions += $react;
			}
			
			$comments = json_decode($fb_data_array[2]->body)->data;
			$total_comments = count($comments);
			
			$post_id = $post['post_id'];
			$sqlu1 = "update facebook_tests_posts
						set impressions_initial = '$impressions',
							reactions_initial = '$total_reactions',
							comments_initial = '$total_comments'
						where id = '$post_id'
					";
			$this->db->query($sqlu1);
		}
		
		//1 hour
		$sql1 = "select *,
						facebook_tests_posts.id as post_id
					from facebook_tests_posts
					left join facebook_pages as fbp on fbp.id = facebook_tests_posts.page_id
					where status = 1
						and fb_post_id is not NULL
						and (now() between date_sub(date_1, interval 7 minute) and date_add(date_1, interval 7 minute))
				";
		$posts = $this->db->query($sql1)->result_array();
		
		foreach($posts as $post){
			$fb_data_array = $this->getTestsPostData($post);
			
			$impressions = json_decode($fb_data_array[0]->body)->data[0]->values[0]->value;
			
			$reactions = json_decode($fb_data_array[1]->body)->data[0]->values[0]->value;
			$total_reactions = 0;
			foreach($reactions as $react){
				$total_reactions += $react;
			}
			
			$comments = json_decode($fb_data_array[2]->body)->data;
			$total_comments = count($comments);
			
			$post_id = $post['post_id'];
			$sqlu1 = "update facebook_tests_posts
						set impressions_1 = '$impressions',
							reactions_1 = '$total_reactions',
							comments_1 = '$total_comments'
						where id = '$post_id'
					";
			$this->db->query($sqlu1);
		}
		
		
		//6 hours
		$sql2 = "select *,
						facebook_tests_posts.id as post_id
					from facebook_tests_posts
					left join facebook_pages as fbp on fbp.id = facebook_tests_posts.page_id
					where status = 1
						and fb_post_id is not NULL
						and (now() between date_sub(date_2, interval 7 minute) and date_add(date_2, interval 7 minute))
				";
		$posts = $this->db->query($sql2)->result_array();
		
		foreach($posts as $post){
			$fb_data_array = $this->getTestsPostData($post);
			
			$impressions = json_decode($fb_data_array[0]->body)->data[0]->values[0]->value;
			
			$reactions = json_decode($fb_data_array[1]->body)->data[0]->values[0]->value;
			$total_reactions = 0;
			foreach($reactions as $react){
				$total_reactions += $react;
			}
			
			$comments = json_decode($fb_data_array[2]->body)->data;
			$total_comments = count($comments);
			
			$post_id = $post['post_id'];
			$sqlu2 = "update facebook_tests_posts
						set impressions_2 = '$impressions',
							reactions_2 = '$total_reactions',
							comments_2 = '$total_comments'
						where id = '$post_id'
					";
			$this->db->query($sqlu2);
		}
		
		
		//24 hours
		$sql3 = "select *,
						facebook_tests_posts.id as post_id
					from facebook_tests_posts
					left join facebook_pages as fbp on fbp.id = facebook_tests_posts.page_id
					where status = 1
						and fb_post_id is not NULL
						and (now() between date_sub(date_3, interval 7 minute) and date_add(date_3, interval 7 minute))
				";
		$posts = $this->db->query($sql3)->result_array();
		
		foreach($posts as $post){
			$fb_data_array = $this->getTestsPostData($post);
			
			$impressions = json_decode($fb_data_array[0]->body)->data[0]->values[0]->value;
			
			$reactions = json_decode($fb_data_array[1]->body)->data[0]->values[0]->value;
			$total_reactions = 0;
			foreach($reactions as $react){
				$total_reactions += $react;
			}
			
			$comments = json_decode($fb_data_array[2]->body)->data;
			$total_comments = count($comments);
			
			$post_id = $post['post_id'];
			$sqlu3 = "update facebook_tests_posts
						set impressions_3 = '$impressions',
							reactions_3 = '$total_reactions',
							comments_3 = '$total_comments'
						where id = '$post_id'
					";
			$this->db->query($sqlu3);
		}
		
		//cicle through all posts that have
		//date schedule < 20 minutes before NOW
		//date schedule 1h < 20 minutes before NOW
		//date schedule 6h < 20 minutes before NOW
		//date schedule 24h < 20 minutes before NOW
		//grab reach, comments, likes/reactions
		//make link to post to read comments
	}
	
	public function cc(){
		$page_ids = $this->facebook_model->getAllPagesIDs();
		foreach($page_ids as $page_id){
			$this->facebook_model->changeCover($page_id->id);
			sleep(rand(5,10));
		}
	}
	
	public function flame_cc(){
		$page_ids = $this->facebook_model->getAllPagesIDsByBrand('boutiqueflame');
		foreach($page_ids as $page_id){
			$this->facebook_model->changeCover($page_id->id);
			sleep(rand(5,10));
		}
	}
	
	public function paradise_cc(){
		$page_ids = $this->facebook_model->getAllPagesIDsByBrand('boutiqueparadise');
		foreach($page_ids as $page_id){
			$this->facebook_model->changeCover($page_id->id);
			sleep(rand(5,10));
		}
	}
	
	public function maximiliano_cc(){
		$page_ids = $this->facebook_model->getAllPagesIDsByBrand('maximiliano');
		foreach($page_ids as $page_id){
			$this->facebook_model->changeCover($page_id->id);
			sleep(rand(5,10));
		}
	}
	
	/*
	 * by Stefan
	 * Changes profile pictures
	 */
	public function flame_pp(){
		$page_ids = $this->facebook_model->getAllPagesIDsByBrand('boutiqueflame');
		foreach($page_ids as $page_id){
			$this->facebook_model->changeProfilePic($page_id->id);
			sleep(rand(5,10));
		}
	}
	
	/*
	 * by Stefan
	 * Changes profile pictures
	 */
	public function paradise_pp(){
		$page_ids = $this->facebook_model->getAllPagesIDsByBrand('boutiqueparadise');
		foreach($page_ids as $page_id){
			$this->facebook_model->changeProfilePic($page_id->id);
			sleep(rand(5,10));
		}
	}

    private function postToFacebook($post) {
		//rewrite this
		//get images from the new table
		$product = $this->product_model->findById($post->product_id);
		$pid = $product['id'];
		$brand = $post->brand_name;
		
		//search the new table here!!!
		
		$sql1 = "select *
					from fap_images
					where product_id = '$pid'
						and brand_name = '$brand'
					order by RAND()
					limit 1";
		
		$res1 = $this->db->query($sql1)->row();
		
		$path = APPPATH."../uploads/fap/";
		if($res1){
			$image = $path.$res1->filename.'_'.$res1->brand_name.$res1->fileext;
			if(!file_exists($image))
				return false;
			$post->image_id = $res1->id;
			
			$page = $this->facebook_model->findPageById($post->facebook_page_id);
			
			//$product = $this->getProduct($post);
			$config = array();
			$config['app_id'] = $post->app_id;
			$config['app_secret'] = $post->secret_id;
			$config['default_graph_version'] = 'v2.9';
			$fb = new Facebook\Facebook($config);
			$description = $post->description;
			$description = $this->parseDescription($description, $post->product_id, $post->facebook_page_id);
			
			$params = array(
				"access_token" => $post->access_token,
				"message" => $description,
				"name" => $description,
				"source" => $fb->fileToUpload($image),
				"description" => $description
			);
			
			try{
				$ret = $fb->post("/$post->origin_page_id/photos", $params);
				return $ret;
			}catch(Exception $e){
				echo $e->getMessage();
			}
			//proccess the response to get FB photo ID for future reference
		}
        return false;
    }
	
	/*
	 * by Stefan
	 * Posts to FB only front images
	 */
    private function postToFacebook2($post) {
		echo "<pre>";
		var_dump($post);
		echo "</pre>";
		$product = $this->product_model->findById($post->product_id);
		$pid = $product['id'];
		$code = $product['code'];
		$page = $this->facebook_model->findPageById($post->facebook_page_id);
		$brand = $this->images_model->findBrandIDByShortName($post->brand_name);
		if($post->facebook_page_id == '14'){
			//get only branded
			$sql1 = "select *
						from images
						where code = '$code'
							and (type = 'F' or
								type = 'C')
							and brand = 4
						order by times_posted";
			$res1 = $this->db->query($sql1)->result();
			if(empty($res1))
				return false;
				//2. pick an image
			$image = $res1[0]; //image with leasts posts
			//will be upgraded for using priority
			//copy image with new name
			$data['file_path'] = APPPATH.'../uploads/'.$code.'/';
			$data['file_name'] = $image->file_name;
			$data['file_ext'] = $image->file_ext;
			
			$file_from = $data['file_path'].$data['file_name'].'.'.$data['file_ext'];
			$file_to = $file_from;
		}else{
			if($post->facebook_page_id == '29'){
				//get only branded
				//move to model
				$sql1 = "select *
							from images
							where code = '$code'
								and (type = 'F' or
									type = 'C')
								and brand = 6
							order by times_posted";
				$res1 = $this->db->query($sql1)->result();
				if(empty($res1))
					return false;
				//2. pick an image
				$image = $res1[0]; //image with leasts posts
				//will be upgraded for using priority
				//copy image with new name
				$data['file_path'] = APPPATH.'../uploads/'.$code.'/';
				$data['file_name'] = $image->file_name;
				$data['file_ext'] = $image->file_ext;
				
				$file_from = $data['file_path'].$data['file_name'].'.'.$data['file_ext'];
				$file_to = $file_from;
			}else{
				//move query to model
				$brand_id = $brand->id;
				$sql1 = "select *
							from images
							where code = '$code'
								and (type = 'F' or
									type = 'C')
								and (brand = 11 or
									brand = '$brand_id')
							order by times_posted";
				$res1 = $this->db->query($sql1)->result();
				if(empty($res1)){
					return false;
				}else{
					$image = $res1[0]; //image with leasts posts
					//will be upgraded for using priority
					//copy image with new name
					$data['file_path'] = APPPATH.'../uploads/'.$code.'/';
					$data['file_name'] = $image->file_name;
					$data['file_ext'] = $image->file_ext;
					
					$file_from = $data['file_path'].$data['file_name'].'.'.$data['file_ext'];
					//brand it
					if($image->brand == 11){
						$file_to = $data['file_path'].$data['file_name'].'_temp.'.$data['file_ext'];
						copy($file_from, $file_to);
						$this->image->brandImage($data, $code, $brand);
					}else{
						$file_to = $file_from;
					}
					
				}
			}
		}
		
		//1. list all front images for this product
		
		
		//---it picks only originals atm---3. check if image is with correct brand
			//if not brand it and post it
			//else just post it
			
		//4. post it and get FB response
		//5. update times_posted in images
		//6. update pic_posted and fb_id in facebook_posts
		
		//$product = $this->getProduct($post);
		$config = array();
		$config['app_id'] = $post->app_id;
		$config['app_secret'] = $post->secret_id;
		$config['default_graph_version'] = 'v2.9';
		$fb = new Facebook\Facebook($config);
		$description = $post->description;
		$description = $this->parseDescription($description, $post->product_id, $post->facebook_page_id);
		
		$params = array(
			"access_token" => $post->access_token,
			"message" => $description,
			"name" => $description,
			"source" => $fb->fileToUpload($file_to),
			"description" => $description
		);
		echo "<pre>";
		var_dump($params);
		echo "</pre>";
		try{
			$ret = $fb->post("/$post->origin_page_id/photos", $params); //EDIT THIS TO BE ABLE TO POST TO TIMELINE
			//move to model
			$fb_post = $ret->getBody();
			$fb_post_array = json_decode($fb_post);
			$fb_post_id = $fb_post_array->id;
			$sql2 = "UPDATE images SET times_posted = times_posted + 1 WHERE id = '$image->id'";
			$this->db->query($sql2);
			$sql3 = "UPDATE facebook_posts SET posted_pic = '$image->id' WHERE id = '$post->id'";
			$this->db->query($sql3);
			$sql4 = "UPDATE facebook_posts SET fb_post_id = '$fb_post_id' WHERE id = '$post->id'";
			$this->db->query($sql4);
			return $ret;
		}catch(Exception $e){
			echo $e->getMessage();
		}
		//proccess the response to get FB photo ID for future reference
		
        return false;
    }
	
	//not used atm
    private function getProduct($post) {
        // TODO REWRITE IMAGE LIBRARY AND PROCESS IMAGES ON THE FLY HERE
        $productData = array();
        $product = (object)$this->product_model->findById($post->product_id);

        $preparedImage = $this->prepareImage($product);
        $productData['image_path'] = $preparedImage;
        $productData['product'] = $product;
        return $productData;
    }

    /**
	 * by Stefan
	 * Changes {$vars} in description with real values
	 * Returns the new description
     */
	public function parseDescription($description, $product_id, $page_id){
		//gets the product object
		$product = $this->product_model->findById($product_id);
		//gets the available sizes
		$product_sizes = $this->product_model->getProductSizes($product_id);
		$available_sizes = '';
		foreach($product_sizes as $product_size){
			if($product_size->quantity>0)
				$available_sizes .= $product_size->name.', ';
		}
		$available_sizes = rtrim($available_sizes,', ');
		//gets product price
		$page = $this->facebook_model->findPageById($page_id);
		$country = $this->country_model->findById($page['country']);
		$currency = $country['currency'];
		if($currency == 'BGN')
			$currency = 'лв';
		$prices = $this->product_model->getProductPrices($product_id);
		foreach($prices as $price){
			if($price->country_id == $page['country']){
				$real_price = $price->price;
				break;
			}
		}
		if(!isset($real_price)){
			foreach($prices as $price){
				if($price->country_id == '3'){
					$real_price = $price->price;
					break;
				}
			}
			$currency = 'EUR';
		}
		//make this a little bit better
		$emoticons = ['🎀','🌹','💕','💘','💓','♥','💙','💛','😘','💋','😍','💝','💞','❤','💖','💅','🎁','💎','🎈','🎉','🎊','🎇','🎆','🎗','👠','💄','👗','💐','🌸','🏵','🌻','🌺','🌼','🌷','🌈','🌟','💗'];
		$emo = $emoticons[rand(0, 36)];
		//words to be replaced
		$vars = array(
			'{$code}' => strtoupper($product['code']),
			'{$currency}' => $currency,
			'{$price}' => $real_price,
			'{$sizes}' => $available_sizes,
			'{$emo}' => $emo
		);
		//result description
		$descriptionWithValues = strtr($description, $vars);
		return $descriptionWithValues;
	}

    private function prepareImage($product) {
		//REWRITE TO GET DATA FROM NEW TABLE
				
        $preparedData = array();
		//get image from the
        $productImage = $this->product_model->getMainImage($product->id);

        $preparedData['source_image'] = "uploads/{$product->id}/{$productImage->filename}{$productImage->ext}";
        //$preparedData['watermark_image'] = $this->post->watermark_image;
        $preparedData['new_image'] = "temp/images/{$product->id}/{$productImage->filename}{$productImage->ext}";

        $this->createDir($product->id);

        $this->overlayWatermark($preparedData);
        $this->textWatermark($preparedData, $product->code);

        return $preparedData['new_image'];
    }

    public function overlayWatermark($data) {
        $source_image = $data['source_image'];

        $config['image_library'] = 'gd2';
        $config['source_image'] = $source_image;
        $config['new_image'] = $data['new_image']; // relative or absolute path on server
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = $data['watermark_image'];
        //the overlay image
        $config['wm_opacity'] = 100;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    public function textWatermark($data, $prodcode) {
        $config['source_image'] = $data['new_image'];
        $config['new_image'] = $data['new_image'];
        $config['wm_text'] = $prodcode;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/texb.ttf';
        $config['wm_font_size'] = '36';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'right';
        $config['wm_padding'] = '0';
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
            $this->image_lib->clear();
        }
        $this->image_lib->clear();
    }

    public function textCenterWatermark($data) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $data['new_image'];
        $config['new_image'] = $data['new_image'];
        $config['wm_text'] = 'Boutique Flame';
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/texb.ttf';
        $config['wm_font_size'] = '36';
        $config['wm_opacity'] = 20;
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $config['wm_padding'] = '0';
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
            $this->image_lib->clear();
            exit();
        }
        $this->image_lib->clear();
    }

    private function createDir($product_id) {
        if (!is_dir('./temp/images/' . $product_id)) {
            $old = umask(0);
            mkdir('./temp/images/' . $product_id, 0777, TRUE);
            umask($old);
        }
    }
}