<?php

class General extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('order_model');
    }

    public function recalculate_orders() {
        echo "Recalculating orders START\n";
        $orders = $this->getOrders();
        if(count($orders)) {
            foreach($orders as $order) {
                $this->recalculateTotal($order->id);
            }
        }
        echo "\nRecalculating orders FINISH\n";
    }
    
    public function change_orders_discount_to_percent() {
        echo "Change orders discount field to percentage |START \n";
        $orders = $this->getOrders();
        if(count($orders)) {
            foreach($orders as $order) {
                $this->changeDiscountToPercent($order->id);
            }
        }
        
        echo "\nChange orders discount field to percentage |FINISH \n";
    }

    private function getOrders() {
        $query = $this->db
            ->select('*')
            ->from('orders');
        $orders = $query->get()->result();
        return $orders;
    }

    private function recalculateTotal($id) {
        $order = $this->order_model->findById($id);
        $order_products = $this->order_model->getOrderProducts($id, $order->country_id);

        $new_total = 0;
        foreach ($order_products as $o_product) {
            $new_total = $new_total + $o_product['price'] * $o_product['quantity'];
        }

        $new_total = ($new_total + $order->delivery_cost) - $order->discount;
        $data = array(
            'total' => $new_total
        );
        $this->order_model->updateRow($id, $data);
    }
    
//    private function changeDiscountToPercent($id) {
//        $order = $this->order_model->findById($id);
//        $order_products = $this->order_model->getOrderProducts($id, $order->country_id);
//
//        $new_total = 0;
//        foreach ($order_products as $o_product) {
//            $new_total = $new_total + $o_product['price'] * $o_product['quantity'];
//        }
//        
//        $old_total = $new_total;
//        
//        $new_total = $old_total - $order->discount; // discount is a digit
//        $new_total = $new_total + $order->delivery_cost;
//        var_dump($order->discount);
//        $percentage_decrease = ($old_total - $order->discount) / $old_total;
//        $percentage = ($percentage_decrease == 1) ? 0 : $percentage_decrease;
//        
//        var_dump($percentage);
//        exit();
//        
//        $data = array(
//            'total' => $new_total
//        );
////        $this->order_model->updateRow($id, $data);
//    }

}
