<?php

/**
 * Created by PhpStorm.
 * User: mrgreen
 * Date: 12.12.16
 * Time: 01:27
 */
class Schedule_cron extends CI_Controller{

    private $post;

    public function __construct(){
        parent::__construct();
        $this->load->model('schedule_model');
    }

    /*
     * by Stefan
     * Used to reset weekly schedule confirm for everyone
     * Runed every monday by cronjob 
     */
    public function resetConfirm(){
        $this->schedule_model->resetConfirm();
    }
}