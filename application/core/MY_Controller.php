<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of MY_Controller
 *
 * @author chavdar
 */
class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
    }

}

abstract class Admin_Controller extends MY_Controller {

    const VIEW_DIRECTORY = 'admin';

    protected
        $load_models = array(),
        $load_helpers = array(),
        $load_libraries = array(),
        $has_filter = false;

    public function __construct() {
        parent::__construct();

        $this->load->library('userhelper');
        $this->load->helper(array('url', 'permission', 'navigation', 'crud', 'language'));
        $this->load->language('permission');

        if ($maintenance_mode = $this->config->item('maintenance_mode')) {
            redirect('maintenance');
        }

        $this->redirectUnlessLogged();
        $this->denyAccessUnlessGranted();

        $this->load->library('twig', array(
            'functions_safe' => array(
                'get_navigation',
                'is_navigation_item_active',
                'crud_action',
                'crud_is_active',
                'crud_get_status_for_order',
                'crud_get_status_for_todo',
                'lang',
                'form_input',
                'form_label',
                'form_checkbox',
                'form_dropdown',
                'get_controller_label',
                'get_action_label',
                'session',
                'session_has',
            )
        ));

        count($this->load_models) && $this->load->model($this->load_models);
        count($this->load_helpers) && $this->load->helper($this->load_helpers);
        count($this->load_libraries) && $this->load->library($this->load_libraries);

        $this->twig->addGlobal('current_controller', $this->router->fetch_class());
        $this->twig->addGlobal('current_action', $this->router->fetch_method());
        $this->twig->addGlobal('has_filter', $this->has_filter);
        $this->twig->addGlobal('_GET', $_GET);
        $this->twig->addGlobal('_POST', $_POST);
        $this->twig->addGlobal('_SERVER', $_SERVER);
        
        if($this->input->get('ep') == 'yesbaby') {
            $this->output->enable_profiler(TRUE);
        }
    }

    /**
     * Twig render method shortcut
     * 
     * @param string $template
     * @param array $params
     * @param boolean $guess
     */
    public function render($template, array $params = array(), $guess = true) {
        if ($guess) {
            $template = sprintf(
                '%s/%s/%s', self::VIEW_DIRECTORY, strtolower(get_class($this)), $template
            );
        }

        return $this->twig->display($template, $params);
    }

    /**
     * @return boolean
     */
    public function isPost() {
        return (strtoupper($this->input->method()) === 'POST');
    }

    /**
     * @return void
     */
    final public function redirectUnlessLogged() {
        if (strtolower(get_class($this)) === 'login') {
            return;
        }

        if (!$this->session->userdata('admin_user') || !$this->session->userdata('admin_user')) {
            redirect('admin/login');
        }
    }

    /**
     * @return void
     */
    final public function denyAccessUnlessGranted() {
        $core = & get_instance();
        $current_controller = strtolower($core->router->fetch_class());
        $current_action = strtolower($core->router->fetch_method());
        $user_data = $this->session->userdata('admin_user');

//		var_dump($user_data);
//		exit();
        if (!UserHelper::canAccess($current_controller, $current_action, $user_data['group_permissions'])) {
            show_error('You do not have access to this page.', 403, '403 Forbidden');
        }
    }

    /**
     * @return array
     */
    public static function getActions() {
        return array(
            'index',
            'create',
            'edit',
            'delete'
        );
    }

}

class Frontend_Controller extends MY_Controller {

    //put your code here

    const VIEW_DIRECTORY = '';

    public function __construct() {
        parent::__construct();

        $this->load->library('twig', array(
            'functions_safe' => array(
                'get_navigation',
                'is_navigation_item_active',
                'crud_action',
                'crud_is_active',
                'crud_get_status_for_order',
                'crud_get_status_for_todo',
                'lang',
                'form_input',
                'form_label',
                'form_checkbox',
                'form_dropdown',
                'get_controller_label',
                'get_action_label'
            )
        ));

        $this->twig->addGlobal('current_controller', $this->router->fetch_class());
        $this->twig->addGlobal('current_action', $this->router->fetch_method());
//        $this->twig->addGlobal('has_filter', $this->has_filter);
        $this->twig->addGlobal('_GET', $_GET);
        $this->twig->addGlobal('_POST', $_POST);
        $this->twig->addGlobal('_SERVER', $_SERVER);

        if (!$maintenance_mode = $this->config->item('maintenance_mode')) {
            $this->load->helper('url');
//            redirect('admin');
        }
    }

    /**
     * Twig render method shortcut
     * 
     * @param string $template
     * @param array $params
     * @param boolean $guess
     */
    public function render($template, array $params = array(), $guess = true) {
        if ($guess) {
            $template = sprintf(
                '%s/%s/%s', self::VIEW_DIRECTORY, strtolower(get_class($this)), $template
            );
        }

        return $this->twig->display($template, $params);
    }

}
