<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Model
 *
 * @author chavdar
 */
class MY_Model extends CI_Model {
	
	protected $table_name = null;
	protected $filter_columns = array();

	public function __construct() {
		parent::__construct();
	}

	/**
	 * 
	 * @return string|null
	 */
	public function getKeywordForSimpleFilter() {
		if (($keyword = $this->input->get('_filter')) && !empty($keyword)) {
			return filter_var($keyword, FILTER_SANITIZE_STRING);
		}
		
		return null;
	}
	
	/**
	 * @return array
	 */
	public function getResultWithSimpleFilter() {
		if (($columns = $this->filter_columns) && ($keyword = $this->getKeywordForSimpleFilter())) {
			foreach ($columns as $column) {
				$this->db->or_like($column, $keyword, 'both', false);
			}
		}
		
		return $this->db->get($this->table_name)->result();
	}

}
