<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * @param string $path
 * @param int $id
 * @param string $label
 * @return string
 */
function crud_action($path, $id, $label, $class = null) {
	$location = site_url(
		sprintf('%s/%d', rtrim($path, '/'), $id)
	);
	
	return anchor($location, $label, $class ? 'class="'. $class .'"' : '');
}

/**
 * 
 * @param int $value
 * @return string
 */
function crud_is_active($value) {
	return sprintf(
		'<span class="label label-%s">%s</span>',
		(boolval($value) === true) ? 'success' : 'danger',
		(boolval($value) === true) ? 'Yes' : 'No'
	);
}

/**
 * 
 * @param int $value
 * @return string
 */
function crud_get_status_for_order($value) {
	$available_statuses = array(
		1 => array('label' => 'Pending', 'class' => 'info'),
		2 => array('label' => 'Completed', 'class' => 'success'),
		3 => array('label' => 'Rejected', 'class' => 'warning'),
		4 => array('label' => 'Cancelled', 'class' => 'danger'),
		5 => array('label' => 'Missing Product', 'class' => 'warning')
	);
	
	if (isset($available_statuses[$value])) {
		return sprintf(
			'<span class="label label-%s">%s</span>',
			$available_statuses[$value]['class'],
			$available_statuses[$value]['label']
		);
	}
	
	return $value;
}

/**
 * 
 * @param int $value
 * @return string
 */
function crud_get_status_for_todo($value) {
	$available_statuses = array(
		1 => array('label' => 'Pending', 'class' => 'info'),
		2 => array('label' => 'Completed', 'class' => 'success'),
		3 => array('label' => 'Rejected', 'class' => 'warning'),
		4 => array('label' => 'Cancelled', 'class' => 'danger')
	);
	
	if (isset($available_statuses[$value])) {
		return sprintf(
			'<span class="label label-%s">%s</span>',
			$available_statuses[$value]['class'],
			$available_statuses[$value]['label']
		);
	}
	
	return $value;
}