<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function isCommandLineInterface()
{
    return (php_sapi_name() === 'cli');
}

function session($key) {
    return $_SESSION[$key] ? $_SESSION[$key] : null;
}

function session_has($key) {
    return $_SESSION[$key] ? true : false;
}
