<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function get_navigation() {
	$menu = array();
	
	$menu_config_path = APPPATH . 'config/menu/config.xml';
	$config = simplexml_load_file($menu_config_path);
	$restricted_modules = UserHelper::getCurrentUserAttribute('group_permissions');
	
	if (isset($config->main)) {
		$index = 0;
		
		/* @var $parent SimpleXMLElement */
		foreach ($config->main as $parent) {
			/* @var $attributes SimpleXMLElement */
			$attributes = $parent->attributes();
			if (! isset($attributes->label)) { continue; }
			
			$current_item = array();
			
			$current_item['label'] = attr($attributes, 'label');
			if (isset($attributes->path)) {
				$current_item['path'] = site_url(attr($attributes, 'path'));
			}
			if (isset($attributes->controller) && isset($attributes->action)) {
				$controller = attr($attributes, 'controller');
				$action = attr($attributes, 'action');
				$current_permission = $controller . '-' . $action;
				
				if (!UserHelper::canAccess($controller, $action, $restricted_modules)) {
					continue;
				}
				
				$current_item['is_active'] = is_navigation_item_active($controller, $action);
			}
			
			if ($parent->children()->count()) {
				$current_item['is_active'] = false;
				$current_item['children'] = array();
				
				foreach ($parent->children() as $child) {
					$child_attributes = $child->attributes();
					$child_item = array();
					
					if (isset($child_attributes->separator)) {
						$child_item['is_separator'] = true;
					}
					else {
						$controller = attr($child_attributes, 'controller');
						$action = attr($child_attributes, 'action');
						$current_permission = $controller . '-' . $action;
				
						if (!UserHelper::canAccess($controller, $action, $restricted_modules)) {
							continue;
						}
						
						$child_item['label'] = attr($child_attributes, 'label');
						$child_item['path'] = site_url(attr($child_attributes, 'path'));
						$child_item['is_active'] = is_navigation_item_active($controller, $action);

						if (is_navigation_item_active(attr($child_attributes, 'controller'))) {
							$current_item['is_active'] = true;
						}
						
						if ($child_item['is_active']) {
							$current_item['is_active'] = true;
						}
					}
					
					$current_item['children'][] = $child_item;
				}
			}
			
			if (isset($current_item['children']) && count($current_item['children']) === 0) {
				continue;
			}
			
			$menu[$index] = $current_item;
			$index++;
		}
	}
	
	return $menu;
}

/**
 * 
 * @param SimpleXMLElement $attributes
 * @param string $name
 * @return string
 */
function attr(SimpleXMLElement $attributes, $name) {
	return $attributes->{$name}->__toString();
}

/**
 * 
 * @param string $controller
 * @param string|null $action
 * @return boolean
 */
function is_navigation_item_active($controller, $action = null) {
	$core =& get_instance();
	$current_controller = $core->router->fetch_class();
	$current_action = $core->router->fetch_method();
	
	return $action ? (($current_controller == $controller) && ($current_action == $action)) : $current_controller == $controller;
}