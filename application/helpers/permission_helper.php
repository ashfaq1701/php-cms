<?php

/**
 * 
 * @param string $controller
 * @return string
 */
function get_controller_label($controller) {
	if ($controller_label = lang('controller_' . $controller)) {
		return $controller_label;
	}
	
	return $controller;
}

/**
 * 
 * @param string $action
 * @param string|null $controller
 * @return string
 */
function get_action_label($action, $controller = null) {
	if (!empty($controller) && $action_label = lang($controller . '_' . $action)) {
		return $action_label;
	}
	else if ($action_label = lang('action_' . $action)) {
		return $action_label;
	}
	
	return $action;
}