<?php

$lang['controller_country'] = 'Countries';
$lang['controller_group'] = 'Groups';
$lang['controller_order'] = 'Orders';
$lang['controller_product'] = 'Products';
$lang['controller_size'] = 'Sizes';
$lang['controller_supplier'] = 'Suppliers';
$lang['controller_user'] = 'Users';
$lang['controller_tag'] = 'Tags';
$lang['controller_home'] = 'Dashboard';
$lang['controller_todo'] = 'Todo List';

$lang['action_index'] = 'List items';
$lang['action_create'] = 'Create new item';
$lang['action_edit'] = 'Edit item';
$lang['action_delete'] = 'Delete item';
$lang['action_fap_index'] = 'List Autopost items';
$lang['action_fap_create'] = 'Create Autopost item';
$lang['action_fap_edit'] = 'Edit Autopost item';
$lang['action_fap_delete'] = 'Delete Autopost item';

$lang['home_index'] = 'View dashboard';