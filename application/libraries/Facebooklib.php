<?php

require_once __DIR__.'/../vendor/autoload.php';

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookException;

class Facebooklib
{
  private $ci;
  private $fb;
  private $redirectUrl;
  private $dummyAccessToken;

  public function __construct()
  {
    $this->ci =& get_instance();
    $this->ci->config->load('facebook');

    $this->fb = new \Facebook\Facebook([
      'app_id' => $this->ci->config->item('fb_app_id'),
      'app_secret' => $this->ci->config->item('fb_app_secret'),
      'default_graph_version' => 'v2.2',
      'http_client_handler' => 'stream'
    ]);
    $this->redirectUrl = $this->ci->config->item('base_url').'/admin/facebook/redirect';
    $this->dummyAccessToken = 'EAAbgtZBhsVuwBAOCXVosABznW4LdUo7zmQtJadGn7luaHTVlUHdHzh8iDztSP3QsLKItG0g1JJEyPruRhdUOMmuFzdGikVLsDWra7aLyaTnYCQQGLT2ktpHFxWUwhiZAc7owgWSrHFaKWluttKDteWbiKRDub0iX1RrJjMXgZDZD';
  }


  public function authUrl()
  {
    $helper = $this->fb->getRedirectLoginHelper();
    $permissions = ['manage_pages', 'publish_pages', 'pages_show_list', 'ads_management', 'read_page_mailboxes'];
		$loginUrl = $helper->getLoginUrl($this->redirectUrl, $permissions);
    return $loginUrl;
  }

  public function getAccessToken()
  {
    $helper = $this->fb->getRedirectLoginHelper();
		$accessToken = null;
		try {
			$accessToken = $helper->getAccessToken();
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		if (! isset($accessToken)) {
			return false;
			exit;
		}
		$token = $accessToken->getValue();
    return $token;
  }

  public function getLongLivedToken($shortLivedToken)
  {
    $cilent = $this->fb->getOAuth2Client();
    $accessToken = null;
    try {
      $accessToken = $cilent->getLongLivedAccessToken($shortLivedToken);
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo $e->getMessage();
      exit;
    }
    if (! isset($accessToken)) {
			return false;
			exit;
		}
		$token = $accessToken->getValue();
    return $token;
  }

  public function pageAccessToken($llToken)
  {
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', 'https://graph.facebook.com/v2.2/me/accounts?access_token='.$llToken);
    $content = $res->getBody();
    $content = json_decode($content, true);
    return $content['data'];
  }

  public function getPageInfo($url)
  {
    $endArr = explode('/', $url);
    $endEl = end($endArr);
    if(empty($endEl) && (count($endArr) - 2 >= 0))
    {
      $endEl = $endArr[count($endArr) - 2];
    }
    if(!empty($endEl))
    {
      $parts = explode('-', $endEl);
      $id = $parts[count($parts) - 1];
      if(!empty($id))
      {
        $jsonContent = file_get_contents('https://graph.facebook.com/'.$id.'?access_token='.$this->dummyAccessToken);
        $obj = json_decode($jsonContent, true);
        if(empty($obj['id']))
        {
          return null;
        }
        else
        {
          return $obj;
        }
      }
      else
      {
        return null;
      }
    }
    return null;
  }

  public function getAlbums($pageObj)
  {
    $fbApp = new \Facebook\FacebookApp($pageObj['app_id'], $pageObj['secret_id']);
    $request = new \Facebook\FacebookRequest($fbApp, $pageObj['access_token'], 'GET', '/'.$pageObj['fb_id'].'/albums?fields=name,description');
    $response = $this->fb->getClient()->sendRequest($request);
    $graphEdge = $response->getGraphList();
    $albums = [];
    while(true)
    {
      foreach ($graphEdge as $graphNode)
      {
        $albums[] = $graphNode->asArray();
      }
      if(empty($this->fb->next($graphEdge)))
      {
        break;
      }
      else
      {
        $graphEdge = $this->fb->next($graphEdge);
      }
    }
    return $albums;
  }

  public function getPhotos($albumObj, $pageObj)
  {
    $fbApp = new \Facebook\FacebookApp($pageObj['app_id'], $pageObj['secret_id']);
    $request = new \Facebook\FacebookRequest($fbApp, $pageObj['access_token'], 'GET', '/'.$albumObj['fb_id'].'/photos?fields=images,name');
    $response = $this->fb->getClient()->sendRequest($request);
    $graphEdge = $response->getGraphEdge();
    $photos = [];
    while(true)
    {
      foreach ($graphEdge as $graphNode)
      {
        $photos[] = $graphNode->asArray();
      }
      if(empty($this->fb->next($graphEdge)))
      {
        break;
      }
      else
      {
        $graphEdge = $this->fb->next($graphEdge);
      }
    }
    return $photos;
  }

  public function getShiftedPhotoIds($albumObj, $pageObj, $count)
  {
    $photoIds = [];
    $photos = $this->getPhotos($albumObj, $pageObj);
    foreach($photos as $photo)
    {
      $photoIds[] = $photo['id'];
    }
    for ($k = 0; $k < $count; $k++) {
      if(count($photoIds) > 0)
      {
        $lastPhotoId = $photoIds[count($photoIds) - 1];
      }
      for($i = count($photoIds) - 1; $i > 0; $i--)
      {
        $photoIds[$i] = $photoIds[$i - 1];
      }
      if(count($photoIds) > 0)
      {
        $photoIds[0] = $lastPhotoId;
      }
    }
    return $photoIds;
  }

  public function getAlbumPhotoIds($albumObj, $pageObj) {
    $photoIds = [];
    $photos = $this->getPhotos($albumObj, $pageObj);
    foreach($photos as $photo)
    {
      $photoIds[] = $photo['id'];
    }
    return $photoIds;
  }

  public function getBusinessPageInfo($pageObj)
  {
    $fbApp = new \Facebook\FacebookApp($pageObj['app_id'], $pageObj['secret_id']);
    $request = new \Facebook\FacebookRequest($fbApp, $pageObj['access_token'], 'GET', '/'.$pageObj['fb_id'].'?fields=link');
    $response = $this->fb->getClient()->sendRequest($request);
    $graphObject = $response->getGraphNode();
    $arr = $graphObject->asArray();

  }

  public function createAlbum($pageObj, $albumTitle, $albumSubtitle)
  {
    $data = [
      'name' => $albumTitle,
      'message' => $albumSubtitle
    ];
    $fbApp = new \Facebook\FacebookApp($pageObj['app_id'], $pageObj['secret_id']);
    $request = new \Facebook\FacebookRequest($fbApp, $pageObj['access_token'], 'POST', '/'.$pageObj['fb_id'].'/albums', $data);
    $response = $this->fb->getClient()->sendRequest($request);
    $graphObject = $response->getGraphNode();
    return $graphObject->asArray();
  }

  public function addPhoto($albumObj, $pageObj, $photoUrl, $photoDescription)
  {
    $data = [
      'url' => $photoUrl,
      'message' => $photoDescription
    ];
    $fbApp = new \Facebook\FacebookApp($pageObj['app_id'], $pageObj['secret_id']);
    $request = new \Facebook\FacebookRequest($fbApp, $pageObj['access_token'], 'POST', '/'.$albumObj['fb_id'].'/photos', $data);
    $response = $this->fb->getClient()->sendRequest($request);
    $graphObject = $response->getGraphNode();
    return $graphObject->asArray();
  }

  public function addPhotos($album, $page, $photoObjs) {
    $this->fb->setDefaultAccessToken($page['access_token']);

    $batches = array();
    $ret = array();
    for ($i = 1; $i <= count($photoObjs); $i++) {
      $batches['photo-' . $i] = $this->fb->request('POST', '/'.$album['fb_id'].'/photos', [
        'message' => $photoObjs[$i - 1]['message'],
        'url' => $photoObjs[$i - 1]['url']
      ]);
    }

    try {
      $responses = $this->fb->sendBatchRequest($batches);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      file_put_contents(__DIR__.'/../logs/batch-response', date('Y-m-d H:i:s') . ' : Graph returned an error: ' . $e->getMessage() . "\n", FILE_APPEND);
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      file_put_contents(__DIR__.'/../logs/batch-response', date('Y-m-d H:i:s') . ' : Facebook SDK returned an error: ' . $e->getMessage() . "\n", FILE_APPEND);
      exit;
    }

    foreach ($responses as $key => $response) {
      if ($response->isError()) {
        $e = $response->getThrownException();
        file_put_contents(__DIR__.'/../logs/batch-response', date('Y-m-d H:i:s') . ' : Error! Facebook SDK Said: ' . $e->getMessage() . "\n", FILE_APPEND);
        file_put_contents(__DIR__.'/../logs/batch-response', date('Y-m-d H:i:s') . ' : Graph Said: ' . "\n", FILE_APPEND);
        file_put_contents(__DIR__.'/../logs/batch-response', date('Y-m-d H:i:s') . ' : ' . json_encode($e->getResponse()) . "\n", FILE_APPEND);
      } else {

        $body = $response->getBody();
        $bodyObj = json_decode($body, true);
        $ret[] = $bodyObj;
      }
    }
    return $ret;
  }

  public function getPhoto($pageObj, $photoId)
  {
    $fbApp = new \Facebook\FacebookApp($pageObj['app_id'], $pageObj['secret_id']);
    $request = new \Facebook\FacebookRequest($fbApp, $pageObj['access_token'], 'GET', '/'.$photoId.'?fields=images');
    $response = $this->fb->getClient()->sendRequest($request);
    $graphObject = $response->getGraphObject();
    return $graphObject->asArray();
  }

  public function addProductCodes($photos, $series)
  {
    $combined = implode('|', $series);
    $exp = '/('.$combined.')\d{3}(\s|\.)/';
    for($i = 0; $i < count($photos); $i++)
    {
      $photo = $photos[$i];
      if(!empty($photo['name']))
      {
        $name = $photo['name'];
        preg_match_all($exp, $name, $matches, PREG_SET_ORDER, 0);
        if(count($matches) > 0)
        {
          $productCode = $matches[0][0];
          $productCode = preg_replace('/(\s|\.)/', '', $productCode);
          $photos[$i]['product_code'] = $productCode;
        }
      }
      if(!empty($photo['images']))
      {
        $imgs = $photo['images'];
        if(count($imgs) > 0)
        {
          $photos[$i]['src'] = $imgs[0]['source'];
          unset($photos[$i]['images']);
        }
      }
    }
    return $photos;
  }

  public function getFileName($url)
  {
    $name = basename($url);
    $parts = explode('?', $name);
    return $parts[0];
  }

  public function copyImageToLocal($url, $folder)
  {
    $contents=file_get_contents($url);
    $cpPath = $folder.$this->getFileName($url);
    file_put_contents($cpPath, $contents);
  }

  public function getPageInformation($page) {
    try {
      $fbApp = new \Facebook\FacebookApp($page['app_id'], $page['secret_id']);
      $request = new \Facebook\FacebookRequest($fbApp, $page['access_token'], 'GET', '/'.$page['fb_id']);
      $response = $this->fb->getClient()->sendRequest($request);
      $graphObject = $response->getGraphObject();
      return $graphObject->asArray();
    } catch (FacebookException $e) {
      return 'dummy';
    } catch (FacebookResponseException $e) {
      return 'dummy';
    } catch (\Exception $e) {
      return 'dummy';
    }
  }

  public function getAlbumInformation($album, $page) {
    try {
      $fbApp = new \Facebook\FacebookApp($page['app_id'], $page['secret_id']);
      $request = new \Facebook\FacebookRequest($fbApp, $page['access_token'], 'GET', '/'.$album['fb_id']);
      $response = $this->fb->getClient()->sendRequest($request);
      $graphObject = $response->getGraphObject();
      return $graphObject->asArray();
    } catch (FacebookException $e) {
      $code = $e->getCode();
      if (intval($code) == 120) {
        return null;
      }
      return 'dummy';
    } catch (FacebookResponseException $e) {
      $code = $e->getCode();
      if (intval($code) == 120) {
        return null;
      }
      return 'dummy';
    } catch (\Exception $e) {
      return 'dummy';
    }
  }

  public function getPhotoInformation($photo, $page) {
    try {
      $fbApp = new \Facebook\FacebookApp($page['app_id'], $page['secret_id']);
      $request = new \Facebook\FacebookRequest($fbApp, $page['access_token'], 'GET', '/'.$photo['fb_id']);
      $response = $this->fb->getClient()->sendRequest($request);
      $graphObject = $response->getGraphObject();
      return $graphObject->asArray();
    } catch (FacebookException $e) {
      $code = $e->getCode();
      if (intval($code) == 121) {
        return null;
      }
      return 'dummy';
    } catch (FacebookResponseException $e) {
      $code = $e->getCode();
      if (intval($code) == 121) {
        return null;
      }
      return 'dummy';
    } catch (\Exception $e) {
      return 'dummy';
    }
  }

  public function deletePhoto($photo, $page) {
    try {
      $fbApp = new \Facebook\FacebookApp($page['app_id'], $page['secret_id']);
      $request = new \Facebook\FacebookRequest($fbApp, $page['access_token'], 'DELETE', '/'.$photo['fb_id']);
      $response = $this->fb->getClient()->sendRequest($request);
      $graphObject = $response->getGraphObject();
      return $graphObject->asArray();
    } catch (\Exception $e) {
      return null;
    }
  }
}

?>
