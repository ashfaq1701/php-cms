<?php

use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\BrowserKit\Cookie;
use GuzzleHttp\Cookie\CookieJar;

class Facebookspider
{

  public $email;
  public $password;

  public $client;
  public $cookies;
  public $usefulCookies;
  public $guzzleClient;

  public function __construct()
  {
    $this->client = new Client();
    $this->guzzleClient = new GuzzleClient(array(
      'allow_redirects' => true,
      'cookies' => true
    ));
    $this->usefulCookies = ['datr', 'lu', 'c_user', 'fr', 'xs', 's', 'act'];

    $this->client->setClient($this->guzzleClient);
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }

  public function setPassword($password)
  {
    $this->password = $password;
  }

  public function setCookies($cookies)
  {
    $this->cookies = $cookies;
  }

  public function getCookies()
  {
    return $this->cookies;
  }

  public function getGuzzleCookieJar()
  {
    $this->getCookies();
    $cookiesArray = [];
    $cookieDomain = null;
    foreach($this->cookies as $cookie)
    {
      $cookiesArray[$cookie['name']] = $cookie['value'];
      $cookieDomain = $cookie['domain'];
    }
    $cookieJar = CookieJar::fromArray($cookiesArray, $cookieDomain);
    return $cookieJar;
  }

  public function getUserId()
  {
    foreach($this->cookies as $cookie) {
      if($cookie['name'] == 'c_user')
      {
        return $cookie['value'];
      }
    }
    return null;
  }

  public function setCookieJar()
  {
    $cookieJar = $this->client->getCookieJar();
    foreach($this->cookies as $cookie)
    {
      if (array_key_exists('expirationDate', $cookie)) {
        $expirationDate = $cookie['expirationDate'];
      } else {
        $expirationDate = time() + (10 * 365 * 24 * 60 * 60);
      }
      $cookieJar->set(new Cookie($cookie['name'], $cookie['value'], intval($expirationDate), $cookie['path'], $cookie['domain']));
    }
  }

  public function collectFbDtsg($url=null)
  {
    $this->setCookieJar();
    if(empty($url))
    {
      $url = 'https://www.facebook.com';
    }
    $crawler = $this->client->request('GET', $url);
    $html = $crawler->html();
    $startingPos = strpos($html, '<input type="hidden" name="fb_dtsg"');
    $firstCut = substr($html, $startingPos);
    $endingPos = strpos($firstCut, '>');
    $nodeStr = substr($firstCut, 0, $endingPos+1);
    $fbDtsg = null;
    if(!empty($nodeStr))
    {
      $dom = new \DOMDocument();
      $dom->loadHTML($nodeStr);
      $fbDtsg = $dom->getElementsByTagName('input')->item(0)->getAttribute('value');
    }
    return $fbDtsg;
  }

  public function ifAuthenticated()
  {
    $this->getCookies();
    $this->setCookies();
    $crawler = $this->client->request('GET', 'https://www.facebook.com/login.php');
    $loginForm = $crawler->filter('#login_form');
    if($loginForm->count() == 1)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public function ifCookiesValid()
  {
    $isValid = true;
    if(!empty($this->cookies))
    {
      if(count($this->cookies) > 0)
      {
        foreach ($this->cookies as $cookie) {
          if (array_key_exists('expirationDate', $cookie)) {
            $expirationDate = $cookie['expirationDate'];
          } else {
            $expirationDate = time() + (10 * 365 * 24 * 60 * 60);
          }
          $cookieObj = new Cookie($cookie['name'], $cookie['value'], intval($expirationDate), $cookie['path'], $cookie['domain']);
          $isValid = $isValid && (!$cookieObj->isExpired());
        }
      }
    }
    return $isValid;
  }

  public function login()
  {
    $crawler = $this->client->request('GET', 'https://www.facebook.com/login.php');
    $loginForm = $crawler->filter('#login_form')->form();

    $crawler = $this->client->submit($loginForm, array('email' => $this->email, 'pass' => $this->password));

    $cookieJar = $this->client->getCookieJar();
    $cookies = $cookieJar->all();
    $cookiesArr = [];
    foreach($cookies as $cookie)
    {
      $cookieName = $cookie->getName();
      if(in_array($cookieName, $this->usefulCookies))
      {
        $cookiesArr[] = [
          'name' => $cookie->getName(),
          'value' => $cookie->getValue(),
          'expirationDate' => $cookie->getExpiresTime(),
          'path' => $cookie->getPath(),
          'domain' => $cookie->getDomain()
        ];
      }
    }
    return $cookiesArr;
  }

  public function uploadAlbumOrdering($id) {
    $dtsg = $this->collectFbDtsg();
    $cookieJar = $this->getGuzzleCookieJar();
    $url = "https://business.facebook.com/photos/photo_albums/confirm_order_type/?album_fbid=".$id."&dpr=1";
    $response = $this->guzzleClient->post($url, array(
      'form_params' => array(
        'fb_dtsg' => $dtsg,
        'order_type' => 1,
        '__user' => $this->getUserId(),
        '__a' => 1
      ),
      'cookies' => $cookieJar
    ));
    $contents = (string) $response->getBody();
    echo $contents;
  }

  public function updatePhotoCaption($id, $caption)
  {
    $dtsg = $this->collectFbDtsg();
    $cookieJar = $this->getGuzzleCookieJar();
    $url = "https://www.facebook.com/photos/photo/edit/save/?fbid=$id&version=6&media_type=photo&editor_id=fbPhotoSnowliftInlineEditor";
    $response = $this->guzzleClient->post($url, array(
      'form_params' => array(
        'fb_dtsg' => $dtsg,
        'caption' => $caption,
        'caption_text' => $caption
      ),
      'cookies' => $cookieJar
    ));
    $contents = (string) $response->getBody();
    echo $contents;
  }

  public function updatePhotoOrderings($pageId, $albumId, $photos)
  {
    $dtsg = $this->collectFbDtsg();
    $cookieJar = $this->getGuzzleCookieJar();
    $url = 'https://www.facebook.com/ajax/photos/reorder.php?dpr=1';
    $reorderPostData = [
      'fb_dtsg' => $dtsg,
      'album_fbid' => $albumId,
      'owner' => $pageId,
      'order' => $photos,
      '__user' => $this->getUserId()
    ];
    $response = $this->guzzleClient->post($url, array(
      'form_params' => $reorderPostData,
      'cookies' => $cookieJar
    ));
    $contents = (string) $response->getBody();
    echo $contents;
  }

  public function updateBusinessPagePhotoOrderings($pageId, $albumId, $photos)
  {
    $dtsg = $this->collectFbDtsg();
    $cookieJar = $this->getGuzzleCookieJar();
    $url = 'https://business.facebook.com/ajax/photos/reorder.php?dpr=1';
    $reorderPostData = [
      'fb_dtsg' => $dtsg,
      'album_fbid' => $albumId,
      'owner' => $pageId,
      'order' => $photos,
      '__user' => $this->getUserId()
    ];
    $response = $this->guzzleClient->post($url, array(
      'form_params' => $reorderPostData,
      'cookies' => $cookieJar
    ));
    $contents = (string) $response->getBody();
    echo $contents;
  }
}

?>
