<?php

class Filter {
	
	public function hasActiveFilter($module) {
		$CI = &get_instance();
		
		if ($config = $CI->config->item($module, 'filter')) {
			$fields = array_keys($config);
			$globals = array_keys($_GET);
			
			return $fields;
			return (count(array_diff($fields, $globals)) < count($fields) && $this->hasValues($globals));
		}
		
		return false;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getConditions($module) {
		$conditions = array();
		$CI = &get_instance();
		
		if ($config = $CI->config->item($module, 'filter')) {
			foreach ($config as $field_name => $properties) {
				if (isset($_GET[$field_name]) && ($current_value = trim($_GET[$field_name])) && (!empty($current_value))) {
					$wildcards = 'both';
					
					if ($properties['type'] == 'select') {
						$wildcards = 'none';
					}
					
					$conditions[$field_name] = array(
						'value' => htmlspecialchars($current_value),
						'wildcards' => $wildcards
					);
				}
			}
		}
		
		return $conditions;
	}
	
	/**
	 * 
	 * @param array $data
	 * @return boolean
	 */
	private function hasValues($data) {
		$flag = false;
		
		foreach ($data as $key) {
			if (isset($_GET[$key]) && ($value = trim($_GET[$key])) && (!empty($value))) {
				$flag = true;
			}
		}
		
		return $flag;
	}
	
}