<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UploadImage
 * @author chavdar
 * @updates stefan
 */
class Image {

    /**
     * @param type $product_id
     * @param type $prodcode
     * @return type
     */
    public function upload($product_id, $prodcode = '') {
		ini_set('memory_limit', '-1');
        $core = &get_instance();

        if (!is_dir('uploads/' . $product_id)) {
            $old = umask(0);
            mkdir('./uploads/' . $product_id, 0777, TRUE);
            umask($old);
        }

        $config['upload_path'] = "./uploads/$product_id";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 20000;
        $new_name = time() . $_FILES["userfile"]['name'];
        $config['file_name'] = $new_name;

        $core->load->library('upload', $config);
        $core->load->library('image_lib');

        if (!$core->upload->do_upload('userfile')) {
            $error = array('error' => $core->upload->display_errors());
            return $error;
        } else {
            $data = array('upload_data' => $core->upload->data());

			//there is a php 5.6 error returning warning with exif orientation
			//@ - ignores the warning
            $exif = @exif_read_data($data['upload_data']['full_path']);
            if ($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                switch ($orientation) {
                    case 3:
                        $deg = 180;
                        break;
                    case 6:
                        $deg = 270;
                        break;
                    case 8:
                        $deg = 90;
                        break;
					default:
						$deg = 0;
						break;
                }
                $config_rotate = array();
                $config_rotate['image_library'] = 'gd2';
                $config_rotate['source_image'] = $data['upload_data']['full_path'];
                $config_rotate['rotation_angle'] = $deg;

                $core->image_lib->initialize($config_rotate);

                if (!$core->image_lib->rotate()) {
                    echo $core->image_lib->display_errors();
                }
                $core->image_lib->clear();
            }

            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['upload_data']['full_path'];
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 120;
            $config['height'] = 120;

            $core->image_lib->initialize($config);
            $core->image_lib->resize();
            $core->image_lib->clear();

            $config = array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['upload_data']['full_path'];
            $config['maintain_ratio'] = TRUE;
            $config['new_image'] = $data['upload_data']['raw_name'] . '_resized' . $data['upload_data']['file_ext'];
            $config['width'] = 900;
            $core->image_lib->initialize($config);
            $core->image_lib->resize();
            $core->image_lib->clear();

            $this->overlayWatermark($data);
            $core->image_lib->clear();
            $this->textWatermark($data, $prodcode);
            $core->image_lib->clear();
            return $data;
        }
    }

    public function uploadTestImage(){
		ini_set('memory_limit', '-1');
        $core = &get_instance();

        if(!is_dir('uploads/tests/')){
            $old = umask(0);
            mkdir('./uploads/tests/', 0777, TRUE);
            umask($old);
        }

        $config['upload_path'] = './uploads/tests';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 20000;
        $new_name = strtolower(time().$_FILES['userfile']['name']);
        $config['file_name'] = $new_name;

        $core->load->library('upload', $config);
		$core->upload->initialize($config);

        if(!$core->upload->do_upload('userfile')){
            $error = array('error' => $core->upload->display_errors());
            return $error;
        }else{
            $data = array('upload_data' => $core->upload->data());
            return $data;
        }
    }

	public function testupload(){
		ini_set('memory_limit', '-1');
        $core = &get_instance();

        if (!is_dir('uploads/' . $product_id)) {
            $old = umask(0);
            mkdir('./uploads/' . $product_id, 0777, TRUE);
            umask($old);
        }

        $config['upload_path'] = "./uploads/test";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 20000;
        $new_name = time() . $_FILES["userfile"]['name'];
        $config['file_name'] = $new_name;

        $core->load->library('upload', $config);
        $core->load->library('image_lib');

        if (!$core->upload->do_upload('userfile')) {
            $error = array('error' => $core->upload->display_errors());
            return $error;
        } else {
            $data = array('upload_data' => $core->upload->data());

			//there is a php 5.6 error returning warning with exif orientation
			//@ - ignores the warning
            $exif = @exif_read_data($data['upload_data']['full_path']);
            if ($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                switch ($orientation) {
                    case 3:
                        $deg = 180;
                        break;
                    case 6:
                        $deg = 270;
                        break;
                    case 8:
                        $deg = 90;
                        break;
					default:
						$deg = 0;
						break;
                }
                $config_rotate = array();
                $config_rotate['image_library'] = 'gd2';
                $config_rotate['source_image'] = $data['upload_data']['full_path'];
                $config_rotate['rotation_angle'] = $deg;

                $core->image_lib->initialize($config_rotate);

                if (!$core->image_lib->rotate()) {
                    echo $core->image_lib->display_errors();
                }
                $core->image_lib->clear();
            }

            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['upload_data']['full_path'];
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 120;
            $config['height'] = 120;

            $core->image_lib->initialize($config);
            $core->image_lib->resize();
            $core->image_lib->clear();

            $config = array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['upload_data']['full_path'];
            $config['maintain_ratio'] = TRUE;
            $config['new_image'] = $data['upload_data']['raw_name'] . '_resized' . $data['upload_data']['file_ext'];
            $config['width'] = 900;
            $core->image_lib->initialize($config);
            $core->image_lib->resize();
            $core->image_lib->clear();

            $this->overlayWatermark($data);
            $core->image_lib->clear();
            $this->textWatermark($data, $prodcode);
            $core->image_lib->clear();
            return $data;
        }
	}

    public function overlaySpecificImageWithWatermark($imagePath, $brand) {
  		$core = &get_instance();
      $config['image_library'] = 'gd2';
      $config['source_image'] = $imagePath;
      $config['wm_type'] = 'overlay';
      $config['wm_overlay_path'] = '.'.$brand['image'];
      $config['wm_opacity'] = 20;
      $config['wm_vrt_alignment'] = 'middle';
      $config['wm_hor_alignment'] = 'center';
      $core->load->library('image_lib');
      $core->image_lib->initialize($config);
      if (!$core->image_lib->watermark()) {
        echo $core->image_lib->display_errors();
      }
    }

    public function overlaySpecificImageTextWatermark($imagePath, $productCode) {
      $core = &get_instance();
      $config['source_image'] = $imagePath;
      $config['wm_text'] = "  ".$productCode;
      $config['wm_type'] = 'text';
      $config['wm_font_path'] = './system/fonts/agencyr.ttf';
      $config['wm_font_size'] = '36';
      $config['wm_font_color'] = 'f5ecf6';
      $config['wm_vrt_alignment'] = 'bottom';
      $config['wm_hor_alignment'] = 'left';
      $config['wm_padding'] = '20';
      $config['wm_shadow_color'] = '000000';
      $config['wm_shadow_distance'] = '2';
      $core->load->library('image_lib');
      $core->image_lib->initialize($config);
      if (!$core->image_lib->watermark()) {
        echo $core->image_lib->display_errors();
        exit();
      }
    }

    public function overlayWatermark($data) {
        $source_image = $data['upload_data']['file_path'] . $data['upload_data']['raw_name'] . '_resized' . $data['upload_data']['file_ext'];
		//get brand from brands_list
		$brand = $this->getBrand(2); //2 - flame
		$core = &get_instance();
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source_image;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = '.'.$brand->image;
        $config['wm_opacity'] = 20;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()) {
            echo $core->image_lib->display_errors();
        }
    }

    public function textWatermark($data, $prodcode) {
        $source_image = $data['upload_data']['file_path'] . $data['upload_data']['raw_name'] . '_resized' . $data['upload_data']['file_ext'];
        $core = &get_instance();
		$config['source_image'] = $source_image;
        $config['wm_text'] = "  ".$prodcode;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/agencyr.ttf';
        $config['wm_font_size'] = '30';
        $config['wm_font_color'] = '000000';
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()) {
            echo $core->image_lib->display_errors();
            exit();
        }
    }

	public function getBrand($brand_id){
        $core = &get_instance();
		$sql1 = "select *
					from brands_list
					where id = $brand_id";
		$res1 = $core->db->query($sql1)->row();
		return $res1;
	}

	public function makeGiftCard($image, $expiry_date, $code, $referer, $refered){
        $core = &get_instance();
        $core->load->library('image_lib');

		$config['source_image'] = $image;
        $config['wm_text'] = $code;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/segoeui.ttf';
        $config['wm_font_size'] = '28';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'top';
        $config['wm_vrt_offset'] = '444';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_hor_offset'] = '1130';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
		$core->image_lib->clear();

		$config['source_image'] = $image;
        $config['wm_text'] = $expiry_date;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/segoeui.ttf';
        $config['wm_font_size'] = '23';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'top';
        $config['wm_vrt_offset'] = '394';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_hor_offset'] = '955';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
		$core->image_lib->clear();

		$config['source_image'] = $image;
        $config['wm_text'] = $referer;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/segoeui.ttf';
        $config['wm_font_size'] = '23';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'top';
        $config['wm_vrt_offset'] = '536';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_hor_offset'] = '900';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
		$core->image_lib->clear();

		$config['source_image'] = $image;
        $config['wm_text'] = $refered;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/segoeui.ttf';
        $config['wm_font_size'] = '23';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'top';
        $config['wm_vrt_offset'] = '658';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_hor_offset'] = '900';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
		$core->image_lib->clear();
	}

	public function makeVoucher($image, $expiry_date, $code){
        $core = &get_instance();
        $core->load->library('image_lib');

		$config['source_image'] = $image;
        $config['wm_text'] = $code;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/segoeui.ttf';
        $config['wm_font_size'] = '23';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'top';
        $config['wm_vrt_offset'] = '95';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_hor_offset'] = '239';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
		$core->image_lib->clear();

		$config['source_image'] = $image;
        $config['wm_text'] = $expiry_date;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/segoeui.ttf';
        $config['wm_font_size'] = '23';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'top';
        $config['wm_vrt_offset'] = '226';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_hor_offset'] = '490';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
		$core->image_lib->clear();
	}

	/*
	 * by Stefan
	 * Overlay an image with specific brand - currently only for Boutique Paradise
	 * It is speciffic to paradiseshop.eu posting
	 */
    public function prepareImage($data, $product_code){
		ini_set('memory_limit', '-1'); //because uploading images require some memmory :/
		//gets the image path
        $original_file = $data['file_path'].$data['file_name'].$data['file_ext'];
        $source_image = $data['file_path'].$data['file_name'].'_prepared'.$data['file_ext'];
		copy($original_file, $source_image);
		$core = & get_instance();
		$brand = $this->getBrand(3); //3 - paradise
        $core->load->library('image_lib');
		$config['image_library'] = 'gd2';
        $config['source_image'] = $source_image;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = '.'.$brand->image;
        $config['wm_opacity'] = 100;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $core->image_lib->initialize($config);
        if(!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
        }
		$core->image_lib->clear();

		$config['source_image'] = $source_image;
        $config['wm_text'] = "  ".$product_code;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/agencyr.ttf';
        $config['wm_font_size'] = '30';
        $config['wm_font_color'] = '000000';
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
    }

	/*
	 * by Stefan
	 * Overlay an image with specific brand
	 * (object)$brand - full info about the brand
	 */
    public function brandImage($data, $product_code, $brand){
		ini_set('memory_limit', '-1'); //because uploading images require some memmory :/
		//gets the image path
        $source_image = $data['file_path'].$data['file_name'].'_temp.'.$data['file_ext'];

        $core = &get_instance();
        $core->load->library('image_lib');

		if($brand->abbreviation == '420GR' || $brand->abbreviation == '420BG'){
			$config['image_library'] = 'gd2';
			$config['source_image'] = $source_image;
			$config['wm_type'] = 'overlay';
			$config['wm_overlay_path'] = './uploads/brands/brand_420_hand.png';
			$config['wm_opacity'] = 100;
			$config['wm_vrt_alignment'] = 'bottom';
			$config['wm_hor_alignment'] = 'right';
			$core->image_lib->initialize($config);
			if (!$core->image_lib->watermark()){
				echo $core->image_lib->display_errors();
			}
			$core->image_lib->clear();
		}

        $config['image_library'] = 'gd2';
        $config['source_image'] = $source_image;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = './'.$brand->image; //get the proper brand image
        $config['wm_opacity'] = 100;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $core->image_lib->initialize($config);
        if(!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
        }
		$core->image_lib->clear();

		$config['source_image'] = $source_image;
        $config['wm_text'] = "  ".$product_code;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/agencyr.ttf';
        $config['wm_font_size'] = '30';
        $config['wm_font_color'] = '000000';
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
    }

    public function putCode($source_image, $product_code){
		ini_set('memory_limit', '-1'); //because uploading images require some memmory :/
		//gets the image path

        $core = &get_instance();
        $core->load->library('image_lib');

		$config['source_image'] = $source_image;
        $config['wm_text'] = "  ".$product_code;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/agencyr.ttf';
        $config['wm_font_size'] = '30';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
		$core->image_lib->clear();
    }

    public function putBrandMaximiliano($source_image, $product_code, $brand){
		ini_set('memory_limit', '-1'); //because uploading images require some memmory :/
		//gets the image path

        $core = &get_instance();
        $core->load->library('image_lib');

		if($brand->abbreviation == '420GR' || $brand->abbreviation == '420BG'){
			$config['image_library'] = 'gd2';
			$config['source_image'] = $source_image;
			$config['wm_type'] = 'overlay';
			$config['wm_overlay_path'] = './uploads/brands/brand_420_hand.png';
			$config['wm_opacity'] = 100;
			$config['wm_vrt_alignment'] = 'bottom';
			$config['wm_hor_alignment'] = 'right';
			$core->image_lib->initialize($config);
			if (!$core->image_lib->watermark()){
				echo $core->image_lib->display_errors();
			}
			$core->image_lib->clear();
		}

        $config['image_library'] = 'gd2';
        $config['source_image'] = $source_image;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = './'.$brand->image; //get the proper brand image
        $config['wm_opacity'] = 100;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $core->image_lib->initialize($config);
        if(!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
		$core->image_lib->clear();
    }

	/*
	 * by Stefan
	 * Overlay an image with specific brand
	 * (object)$brand - full info about the brand
	 */
    public function brandImage2($data, $product_code = '', $brand){
		ini_set('memory_limit', '-1'); //because uploading images require some memmory :/
		//gets the image path
        $source_image = $data['file_path'].$data['file_name'].$data['file_ext'];
		echo "<pre>";
		var_dump($source_image);
		var_dump($brand);
		echo "</pre>";

        $core = &get_instance();
        $core->load->library('image_lib');

		if($brand->abbreviation == '420GR' || $brand->abbreviation == '420BG'){
			$config['image_library'] = 'gd2';
			$config['source_image'] = $source_image;
			$config['wm_type'] = 'overlay';
			$config['wm_overlay_path'] = './uploads/brands/brand_420_hand.png';
			$config['wm_opacity'] = 100;
			$config['wm_vrt_alignment'] = 'bottom';
			$config['wm_hor_alignment'] = 'right';
			$core->image_lib->initialize($config);
			if (!$core->image_lib->watermark()){
				echo $core->image_lib->display_errors();
			}
			$core->image_lib->clear();
		}

        $config['image_library'] = 'gd2';
        $config['source_image'] = $source_image;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = './'.$brand->image; //get the proper brand image
        $config['wm_opacity'] = 100;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $core->image_lib->initialize($config);
        if(!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
        }
		$core->image_lib->clear();

		$config['source_image'] = $source_image;
        $config['wm_text'] = "  ".$product_code;
        $config['wm_type'] = 'text';
        $config['wm_font_path'] = './system/fonts/agencyr.ttf';
        $config['wm_font_size'] = '30';
        $config['wm_font_color'] = '000000';
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'left';
        $config['wm_padding'] = '0';
        $core->image_lib->initialize($config);
        if (!$core->image_lib->watermark()){
            echo $core->image_lib->display_errors();
            exit();
        }
    }

    public function remove(){

    }

    public function setAsMainPicture(){

    }
}
