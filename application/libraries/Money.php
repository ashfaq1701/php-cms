<?php

class Money {

    // Function to open stream
    public function open() {

        // Try to open a XML stream
        
        if($this->checkXMLIsFromToday()) {
            // read and return the xml;
//            echo 'XML IS FROM TODAY';
            $stream = simplexml_load_file($this->getCurrencyFilePath());
            return $stream;
        } else {
//            echo 'XML IS NOT FROM TODAY';
            // download the xml from bnb for today
            $this->saveCurrencyFile();
            $stream = simplexml_load_file($this->getCurrencyFilePath());
            if ($stream) {
            // If true, then return resource
            return($stream);
            } else {
                // Else return false
                return(false);
            }
        }
        
    }
    
    /**
     * Check if the xml file for the currency exists
     * and is up to date 
     */
    public function checkXMLIsFromToday() {
        $file = $this->getCurrencyFilePath();
        // lets check if the file exists
        if(file_exists($file)) {
            $tempString = explode('bnb-curs-', $file); // get the date of the file needed for comparsion

            $fileDate = str_replace('.xml', '', $tempString[1]); //remove .xml from the end of the string
            $fileDate = $fileDate;
            $currentDate =  date('Y-m-d');
            
            if($fileDate == $currentDate) {
                // if file is from today we will not update it 
                // and will read the xml
                return true;
            }
            return false;
        }
        return false;
    }
    
    public function saveCurrencyFile() {
        $xml = file_get_contents($this->getCurrencyXMLLink()); // your file is in the string "$xml" now.
        file_put_contents($this->getCurrencyFilePath(), $xml); // xml file is saved.

    }
    
    public function getCurrencyFilePath() {
        $mainDirectory = getcwd();
        $date = date('Y-m-d');
        $filePath = "$mainDirectory/temp/bnb-curs-$date.xml";
        return $filePath;
    }
    
    public function getCurrencyXMLLink() {
        return 'http://www.bnb.bg/Statistics/StExternalSector/StExchangeRates/StERForeignCurrencies/index.htm?download=xml&search=&lang=BG';
    }

    // Function to get current rate for money code
    public function get_rate_for($stream, $code) {
        $ver = $stream->ROW;
        $stop = false;
        foreach ($ver as $m) {
            if ($stop) {
                
            } elseif ($m->CODE != $code) {
                $return = false;
                $stop = false;
            } else {
                $return = bcdiv($m->RATE, $m->RATIO, 5);
                $stop = true;
                return($return);
            }
        }
		
		if($code == 'EUR') {
            return bcdiv('1.95583', '1', 5);
        }
		
        if (!$return) {
            return(false);
        }
    }

    public function convert($stream, $from, $to, $total, $after = 2) {
        if (!$stream) {
            return FALSE;
        } else {
            $value_one = str_replace(',', '#', $this->get_rate_for($stream, $from));
            if ($to == 'BGN') {
                $value_two = 1;
            } else {
                $value_two = str_replace(',', '#', $this->get_rate_for($stream, $to));
            }

            $value_one = str_replace('#', '.', $value_one);
            $value_two = str_replace('#', '.', $value_two);
			
			
            // Do some mathematical calculations
            $return = (($total * $value_one) / $value_two);

            // Return extra integers after a . 
            $return = number_format($return, $after, '.', '');
            return $return;
        }
    }
	
	public function convertAmount($amount, $decimalSymbols, $currency_from, $currency_to) {
        $stream = $this->open();
        if (!empty($stream) && $currency_from !== '' && $currency_to !== '' && !empty($amount)) {
           $convertedPrice = $this->convert($stream, $currency_from, $currency_to, $amount, $decimalSymbols);
        } 
        else {
            $convertedPrice = '';
        }
        
        return $convertedPrice;
    }
    
//    private function ch_price($price) {
//        if (strpos($price, ',') && strpos($price, '.')) {
//            return preg_replace('/,/', '', $price);
//        } else {
//            return $price;
//        }
//    }

}


//$instance = new Money();
//var_dump($instance->convertAmount('1',2 , 'EUR', 'TRY'));