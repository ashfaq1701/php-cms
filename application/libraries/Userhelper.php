<?php

final class UserHelper {
	
	/**
	 * 
	 * @param string $controller
	 * @param string $action
	 * @param string $current_permissions
	 * @return boolean
	 */
	public static function canAccess($controller, $action, $current_permissions) {
		return (strpos($current_permissions, sprintf('%s-%s', $controller, $action)) === false);
	}
	
	/**
	 * 
	 * @return array|false
	 */
	public static function getCurrentUserData() { 
		$core =& get_instance();
		$session = $core->session;

		if (!empty($session->userdata('admin_user')) && is_array($session->userdata('admin_user'))) {
			$user_data = $session->userdata('admin_user');

			return $user_data;
		}

		return false;
	}
	
	/**
	 * 
	 * @param string $attribute
	 * @return mixed
	 */
	public static function getCurrentUserAttribute($attribute) {
		if (($user_data = UserHelper::getCurrentUserData()) && isset($user_data[$attribute])) {
			return $user_data[$attribute];
		}

		return null;
	}
	
}