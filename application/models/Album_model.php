<?php

class Album_model extends MY_Model {
  protected $table_name = 'facebook_albums';

  public function getPageAlbums($pageId)
  {
    return $this->db->select('facebook_albums.*, album_categories.name as category')
                    ->from('facebook_albums')
                    ->join('album_categories', 'album_categories.id = facebook_albums.category_id', 'left')
                    ->where('fb_page_id', intval($pageId))
                    ->get()
                    ->result();
  }

  public function getAllAlbums() {
    return $this->db->select('facebook_albums.*', 'album_categories.name as category')
                    ->from('facebook_albums')
                    ->join('album_categories', 'album_categories.id = facebook_albums.category_id', 'left')
                    ->get()
                    ->result_array();
  }

  public function getAlbumPhotos($album) {
    $sql = 'SELECT * FROM facebook_photos WHERE fb_album_id = ' . $album['id'];
    return $this->db->query($sql)->result_array();
  }

  public function findAlbumById($id)
  {
    return $this->db->select('facebook_albums.*, album_categories.name as category')
                    ->from('facebook_albums')
                    ->join('album_categories', 'album_categories.id = facebook_albums.category_id', 'left')
                    ->where('facebook_albums.id', intval($id))
                    ->limit(1)
                    ->get()
                    ->row_array();
  }

  public function findAlbumByFBId($id){
		return $this->db->get_where('facebook_albums', array('fb_id' => intval($id)), $limit = 1)->row_array();
	}

  public function findAlbumsByPageAndCategory($pageId, $categoryId)
	{
    $this->db->select('*');
		$this->db->from('facebook_albums');
    $this->db->where('fb_page_id', intval($pageId));
    $this->db->where('category_id', intval($categoryId));
    return $this->db->get()->result_array();
	}

  public function syncAlbums($albums, $page)
  {
    foreach($albums as $album)
    {
      $foundAlbum = $this->findAlbumByFBId($album['id']);
      if(empty($foundAlbum))
      {
        $data = [
          'fb_url' => 'https://business/facebook.com/'.$album['id'].'/photos',
          'fb_id' => $album['id'],
          'fb_page_id' => $page['id']
        ];
        if(!empty($album['name']))
        {
          $data['title'] = $album['name'];
        }

        if(!empty($album['description']))
        {
          $data['subtitle'] = $album['description'];
        }
        $this->insertRow($data);
      }
    }
  }

  public function insertRow(array $data) {
		$this->db->trans_start();
		$this->db->insert('facebook_albums', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

  public function updateRow($id, array $data) {
    unset($data['category']);
    if(empty($data['category_id']))
    {
      $data['category_id'] = null;
    }
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('facebook_albums', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

  public function deleteRow($id)
  {
    $sqlDisable = 'SET foreign_key_checks = 0';
		$this->db->query($sqlDisable);
    $this->db->trans_start();
    $this->db->where('id', intval($id));
    $this->db->delete('facebook_albums');
    $this->db->trans_complete();
    $sqlEnable = 'SET foreign_key_checks = 1';
		$this->db->query($sqlEnable);
    return $this->db->trans_status();
  }
}

?>
