<?php

class Albumcategory_model extends MY_Model {
  protected $table_name = 'album_categories';

  public function getAlbumcategories()
  {
    return $this->db->select('*')
                    ->from('album_categories')
                    ->get()
                    ->result();
  }

  public function findAlbumcategoryById($id)
  {
    return $this->db->get_where('album_categories', array('id' => intval($id)), $limit = 1)->row_array();
  }

  public function findAlbumCategoryByName($name)
  {
    return $this->db->get_where('album_categories', array('name' => $name), $limit = 1)->row_array();
  }
}
?>
