<?php

class Commonoption_model extends MY_Model {
  protected $table_name = 'common_options';

  public function setFacebookEmail($email)
  {
    $existingEmail = $this->getFacebookEmail();
    if(empty($existingEmail))
    {
      $this->db->trans_start();
  		$this->db->insert('common_options', [
        'name' => 'facebook_email',
        'val' => $email
      ]);
  		$this->db->trans_complete();
    }
    else
    {
      $this->db->trans_start();
  		$this->db->where('name', 'facebook_email');
      $this->db->update('common_options', [
        'val' => $email
      ]);
  		$this->db->trans_complete();
    }
  }

  public function getFacebookEmail()
  {
    $fbEmailRows = $this->db->select('*')
		         ->from('common_options')
             ->where('name', 'facebook_email')
             ->get()
             ->result_array();
    if(count($fbEmailRows) > 0)
    {
      $fbEmailRow = $fbEmailRows[0];
      $email = $fbEmailRow['val'];
      return $email;
    }
    return null;
  }

  function simpleCrypt($string, $action = 'e') {
    $secret_key = 'my_simple_secret_key';
    $secret_iv = 'my_simple_secret_iv';
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
    if( $action == 'e' ) {
      $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
      $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
    return $output;
  }

  public function setFacebookPassword($password)
  {
    $existingPassword = $this->getFacebookPassword();
    if(empty($existingPassword))
    {
      $this->db->trans_start();
  		$this->db->insert('common_options', [
        'name' => 'facebook_password',
        'val' => $this->simpleCrypt($password)
      ]);
  		$this->db->trans_complete();
    }
    else
    {
      $this->db->trans_start();
  		$this->db->where('name', 'facebook_password');
      $this->db->update('common_options', [
        'val' => $this->simpleCrypt($password)
      ]);
  		$this->db->trans_complete();
    }
  }

  public function getFacebookPassword()
  {
    $fbPasswordRows = $this->db->select('*')
		         ->from('common_options')
             ->where('name', 'facebook_password')
             ->get()
             ->result_array();
    if(count($fbPasswordRows) > 0)
    {
      $fbPasswordRow = $fbPasswordRows[0];
      $password = $fbPasswordRow['val'];
      return $this->simpleCrypt($password, 'd');
    }
    return null;
  }

  public function setFacebookCookies($cookies)
  {
    $existingCookies = $this->getFacebookCookies();
    if(empty($existingCookies))
    {
      $this->db->trans_start();
  		$this->db->insert('common_options', [
        'name' => 'facebook_cookies',
        'val' => $cookies
      ]);
  		$this->db->trans_complete();
    }
    else
    {
      $this->db->trans_start();
  		$this->db->where('name', 'facebook_cookies');
  		$this->db->update('common_options', [
        'val' => $cookies
      ]);
  		$this->db->trans_complete();
    }
  }

  public function getFacebookCookies()
  {
    $fbCookiesRows = $this->db->select('*')
		         ->from('common_options')
             ->where('name', 'facebook_cookies')
             ->get()
             ->result_array();
    if(count($fbCookiesRows) > 0)
    {
      $fbCookiesRow = $fbCookiesRows[0];
      $cookies = $fbCookiesRow['val'];
      return $cookies;
    }
    return null;
  }
}

?>
