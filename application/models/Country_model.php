<?php


class Country_model extends MY_Model {
	
	protected $table_name = 'countries';
	protected $filter_columns = array(
		'name', 'currency', 'iso_code'
	);
	
	private static $countries_list = array();
	
	public static function getCountriesCollection() {
		if (! self::$countries_list) {
			self::$countries_list = (new Country_model())->getCollection();
		}
		
		return self::$countries_list;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getCollection() {
		return $this->getResultWithSimpleFilter();
	}
	
	public function getCollectionOrdered() {
		$this->db->select('*');
		$this->db->from('countries');
		$this->db->where('is_active', 1);
		$this->db->order_by('order', 'ASC');
		return $this->db->get()->result();
	}
	
	/**
	 * 
	 * @param int $id
	 */
	public function findById($id) {
		return $this->db->get_where($this->table_name, array('id' => intval($id)), $limit = 1)->row_array();
	}
	
	/**
	 * 
	 * @param array $data
	 */
	public function insertRow(array $data) {
		$this->db->trans_start();
		$this->db->insert($this->table_name, $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	/**
	 * 
	 * @param int $id
	 * @param array $data
	 */
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update($this->table_name, $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	public function getSelectCollection() {
		$data = $this->db
			->select('id, name')
            ->where('is_active', 1)
			->get($this->table_name)
			->result();
		$response = array();
		foreach($data as $country)  {
			$response[$country->id] = $country->name;
		}
		return  $response;
	}
	
}
