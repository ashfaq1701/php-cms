<?php

function sortArrayByUnreadMessageCount($a, $b){
	$a = $a['unseen_message_count'];
	$b = $b['unseen_message_count'];

	if ($a == $b){
		return 0;
	}

	return ($a < $b) ? -1 : 1;
}

function sortArrayByUnseenMessageCount($a, $b){
	$a = $a['unseen_message_count'];
	$b = $b['unseen_message_count'];

	if ($a == $b)
	{
		return 0;
	}

	return ($a < $b) ? -1 : 1;
}

class Facebook_model extends MY_Model{

	const ITEMS_PER_PAGE = 10;

	public function getCollection(){
		return $this->db
            ->select('facebook_pages.*, countries.name as country_name')
            ->from('facebook_pages')
            ->join('countries', 'facebook_pages.country = countries.id', 'left')
			->where('facebook_pages.active', '1')
            ->order_by('facebook_pages.id', 'ASC')
			->get()
			->result();
	}

	public function getGroupedCollection()
	{
		$sql = 'SELECT facebook_pages.*, countries.name as country_name FROM facebook_pages LEFT JOIN countries ON facebook_pages.country = countries.id';
		$res = $this->db->query($sql)->result_array();
		$firstGrp = [];
		foreach ($res as $el)
		{
			if (!array_key_exists($el['gender'], $firstGrp))
			{
				$firstGrp[$el['gender']] = [];
			}
			$firstGrp[$el['gender']][] = $el;
		}
		$secondGrp = [];
		foreach($firstGrp as $k => $grp)
		{
			$grpArr = [];
			foreach($grp as $el)
			{
				if (!array_key_exists($el['country_name'], $grpArr))
				{
					$grpArr[$el['country_name']] = [];
				}
				$grpArr[$el['country_name']][] = $el;
			}
			$secondGrp[$k] = $grpArr;
		}
		return $secondGrp;
	}

	public function getFacebookPhotos($productId)
	{
		$this->db->select('*');
		$this->db->from('facebook_photos');
    $this->db->where('product_id', intval($productId));
    return $this->db->get()->result_array();
	}

	public function getBatchNos($page, $album) {
		$sql1 = "SELECT DISTINCT ph.batch_no FROM facebook_photos ph
		    LEFT JOIN facebook_albums a ON ph.fb_album_id = a.id
				LEFT JOIN facebook_pages pg ON a.fb_page_id = pg.id";

		$isWhere = false;
		if((!empty($page) && $page != 0) || (!empty($album) && $album != 0)) {
			$isWhere = true;
		}

		$whereClause = array();
		if(!empty($page) && $page != 0) {
			$whereClause[] = "pg.id = $page";
		}
		if(!empty($album) && $album != 0) {
			$whereClause[] = "a.id = $album";
		}

		$whereClauseStr = implode(' AND ', $whereClause);
		if(!empty($whereClauseStr)) {
			$sql1 .= (' WHERE '.$whereClauseStr);
		}
		$results = $this->db->query($sql1)->result_array();
		$batchNos = array();
		foreach ($results as $res) {
			if (!empty($res['batch_no'])) {
				$batchNos[] = $res['batch_no'];
			}
		}
		return $batchNos;
	}

	public function getAlbumPhotos($page, $album, $batch, $start, $limit)
	{
		$userId = session('admin_user')['id'];
		$userNameSql = 'SELECT * FROM user WHERE id=' . $userId;
		$resUser = $this->db->query($userNameSql)->row_array();
		$username = $resUser['username'];
		$sql1 = "SELECT SQL_CALC_FOUND_ROWS
			ph.id AS facebook_photo_id,
			ph.fb_id AS facebook_photo_fb_id,
			ph.batch_no AS facebook_photo_batch_no,
			ph.uploaded_at AS facebook_photo_uploaded_at,
			ph.additional_notes AS facebook_photo_description,
			a.id AS facebook_album_id,
			a.title AS facebook_album_title,
			a.fb_id AS facebook_album_fb_id,
			a.fb_url AS facebook_album_fb_url,
			pg.id AS facebook_page_id,
			pg.title AS facebook_page_title,
			pg.fb_id AS facebook_page_fb_id,
			pg.url AS facebook_page_url,
			im.id AS image_id,
			im.url AS image_url,
			pr.id AS product_id,
			pr.name AS product_name,
			pr.code AS product_code,
			u.id AS user_id,
			u.name AS user_name
			FROM facebook_photos ph
			LEFT JOIN facebook_albums a ON ph.fb_album_id = a.id
			LEFT JOIN facebook_pages pg ON a.fb_page_id = pg.id
			LEFT JOIN images im ON ph.image_id = im.id
			LEFT JOIN products pr ON ph.product_id = pr.id
			LEFT JOIN user u ON ph.uploaded_by = u.id";
		$isWhere = false;
		if((!empty($page) && $page != 0) || (!empty($album) && $album != 0) || (!empty($batch) && $batch != 0)) {
			$isWhere = true;
		}
		$whereClause = array();
		if(!empty($page) && $page != 0) {
			$whereClause[] = "pg.id = $page";
		}
		if(!empty($album) && $album != 0) {
			$whereClause[] = "a.id = $album";
		}
		if(!empty($batch) && $batch != 0) {
			$whereClause[] = "ph.batch_no = $batch";
		}
		$whereClauseStr = implode(' AND ', $whereClause);
		if(!empty($whereClauseStr)) {
			$sql1 .= (' WHERE '.$whereClauseStr);
		}
		$sql1 .= (" LIMIT $limit OFFSET $start");
		$results = $this->db->query($sql1)->result_array();
		$transformedResults = array();
		foreach ($results as $result) {
			$transformedResult = array();
			$transformedResult['page'] = '<a href="'.$result['facebook_page_url'].'">'.$result['facebook_page_title'].'</a>';
			$transformedResult['photo_description'] = $result['facebook_photo_description'];
			$transformedResult['product'] = '<a href="/admin/products/edit/'.$result['product_id'].'">'.$result['product_code'].'</a>';
			$transformedResult['thumbnail'] = '<a href="https://business.facebook.com/'.$result['facebook_photo_fb_id'].'"><img style="height: 100px; width: 130px" src="'.$result['image_url'].'"/></a>';
			$transformedResult['upload_date'] = $username.'-'.$result['facebook_photo_uploaded_at'];
			$transformedResult['uploaded_by'] = '<a href="/admin/user/edit/'.$result['user_id'].'">'.$result['user_name'].'</a>';
			$transformedResult['batch_no'] = $result['facebook_photo_batch_no'];
			$transformedResult['photo_id'] = $result['facebook_photo_id'];
			$transformedResults[] = $transformedResult;
		}
		$sql2 = 'SELECT FOUND_ROWS() AS foundRows';
		$countRes = $this->db->query($sql2)->result();
		$countObj = $countRes[0];
		$count = $countObj->foundRows;
		return array(
			'results' => $transformedResults,
			'count' => $count
		);
	}

	public function getBatchNo() {
		$userId = session('admin_user')['id'];
		$userNameSql = 'SELECT * FROM user WHERE id=' . $userId;
		$resUser = $this->db->query($userNameSql)->row_array();
		$username = $resUser['username'];

		$sql = 'SELECT * FROM facebook_photos WHERE batch_no LIKE "'.$username.'%"';
		$res = $this->db->query($sql)->result_array();

		$batches = array();
		foreach ($res as $item) {
			$batchNo = $item['batch_no'];
			$batch = intval(str_replace($username, '', $batchNo));
			$batches[] = $batch;
		}
		if (count($batches) == 0) {
			$maxBatch = 0;
		} else {
			$maxBatch = max($batches);
		}
		++$maxBatch;
		$currentBatch = $username . $maxBatch;
		return $currentBatch;
	}

	public function addTokenToPages($appId, $appSecret, $pageTokens)
	{
		foreach($pageTokens as $pageToken)
		{
			$this->db->trans_start();
			$this->db->where('fb_id',$pageToken['id']);
			$this->db->update('facebook_pages',[
				'app_id' => $appId,
				'secret_id' => $appSecret,
				'access_token' => $pageToken['access_token']
			]);
			$this->db->trans_complete();
		}
	}

	public function getUnseenMessages(){
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		$app_id = '1848049545471939';
		$app_secret = '60a7c3fbf130abd33ada6479dfe48822';
		$page_id = '239682463042533';
		$mytoken = 'EAAaQypCvZA8MBAFIsyaqGk4pJ4MhhNc5hSYFFCv1LYhtLQHdZBt3QZBhmcei0alkpZBhzc3lC4fEdDaQZC8MKlZAwIZAVuluF7KcmZA2ze79WEmAu4uB6A1WxzjV8PCczpqVOPnY48A5ESwEk4tO9HWxaX01QrLOOhMZD';
		$fb = new Facebook\Facebook(array('app_id'=>$app_id, 'app_secret'=>$app_secret,'http_client_handler' => 'stream','default_graph_version'=>'v2.8'));
		$fbApp = $fb->getApp();
		$request = new Facebook\FacebookRequest($fbApp, $mytoken, 'GET', '/me/accounts?fields=id,unseen_message_count,unread_message_count&limit=50');
		try{
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			echo 'Graph returned an error: '.$e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			echo 'Facebook SDK returned an error: '.$e->getMessage();
			exit;
		}
		$wtf = $response->getGraphEdge();
		$i=0;
		$fb_pages = array();
		foreach($wtf as $page){
			$fb_pages[$i]['id'] = $page['id'];
			$fb_pages[$i]['unread_message_count'] = $page['unread_message_count'];
			$fb_pages[$i]['unseen_message_count'] = $page['unseen_message_count'];
			$i++;
		}
		$sql1 = "select id from facebook_pages where active = '1' and fb_id is not null";
		$res1 = $this->db->query($sql1)->result();
		$pages = array();
		$i=0;
		foreach($res1 as $row){
			$pid = $row->id;
			$page = $this->findPageById($pid);
			foreach($fb_pages as $fb_page){
				if($fb_page['id'] == $page['fb_id']){
					if($fb_page['unseen_message_count'] > 0){
						$pages[$i]['fb_title'] = $page['fb_title'];
						$pages[$i]['fb_id'] = $page['fb_id'];
						$pages[$i]['url'] = $page['url'];
						$pages[$i]['unread_message_count'] = $fb_page['unread_message_count'];
						$pages[$i]['unseen_message_count'] = $fb_page['unseen_message_count'];
						$fb_page_id = $page['id'];
						$i++;
					}
				}
			}
		}
		usort($pages,'sortArrayByUnreadMessageCount');
		$pages = array_reverse($pages);
		return $pages;
	}

	public function getTestPosts($current_page = 1, $count = false){
		$limit = self::ITEMS_PER_PAGE;

		$current_page = ($current_page < 1) ? 0 : $current_page;

		$offset = $current_page ? ($current_page - 1) * $limit : $current_page;
		$this->db
            ->select('*')
            ->from('facebook_tests_posts')
            ->order_by('test_id', 'DESC')
			->group_by('test_id');
		if($count){
			return $this->db->get()->num_rows();
		}else{
			$this->db->limit($limit, $offset);
			return $this->db->get()->result_array();
		}
	}

	public function getConfigForPagination(){
		$limit = self::ITEMS_PER_PAGE;
		$url = site_url('admin/facebook/listtest');
		$segment = 4;

		return array(
			'base_url' => $url,
			'total_rows' => $this->countTotalRows(),
			'per_page' => $limit,
			'uri_segment' => $segment,
			'reuse_query_string' => TRUE,
			'use_page_numbers' => true,
			'full_tag_open' => '<div data-role="paging"><ul class="pagination">',
			'full_tag_close' => '</ul></div>',
			'first_tag_open' => '<li class="prev page">',
			'first_tag_close' => '</li>',
			'last_tag_open' => '<li class="next page">',
			'last_tag_close' => '</li>',
			'next_tag_open' => '<li class="next page">',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li class="prev page">',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li class="active"><a href="">',
			'cur_tag_close' => '</a></li>',
			'num_tag_open' => '<li class="page">',
			'num_tag_close' => '</li>'
		);
	}

	public function countTotalRows(){
		return $this->getTestPosts($this->uri->segment(3), true);
	}

	/*
	 * by Stefan
	 * Analyze Test Image filename
	 */
	public function testImageFile($file1){
		$image = array();
		$error = array();
		//trim excess symbols
		if(substr($file1, 0, 1) == '_')
			$file1 = ltrim($file1, '_');
		$file1 = strtolower($file1); //everything goes to lowercase
		$file1 = str_replace(' ', '', $file1); //trim spaces
		$file1 = str_replace('-', '', $file1); //trim -
		$file1 = str_replace('(', '_(', $file1); //in case there is no underscore before priority
		$file1 = preg_replace('/_+/', '_', $file1); //in case there are multiple underscores

		$fileext = pathinfo($file1, PATHINFO_EXTENSION);
		$image['file_ext'] = $fileext;
		if($image['file_ext'] == 'gif' || $image['file_ext'] == 'png' || $image['file_ext'] == 'jpg' || $image['file_ext'] == 'jpeg'){
			//proper format
		}else{
			//wrong format
			$error[] = 'wrong extension';
		}

		//file_name
		$filename = pathinfo($file1, PATHINFO_BASENAME);
		$filename = rtrim($filename, $fileext);
		$filename = rtrim($filename, '.');
		$image['file_name'] = $filename;

		//explode on '_'
		$file_name_array = explode('_', $filename);
		foreach($file_name_array as $cmd){
			//check what text you have in the filename array
			switch($cmd){
				case $this->images_model->codeCheck($cmd):{
					if(isset($image['code'])){
						$error[] = 'multiple codes';
					}else{
						$image['code'] = $cmd;
					}
					break;
				}
				case substr($cmd, -2) === 'eu':{
					if(isset($image['price_eur'])){
						$error[] = 'multiple prices';
					}else{
						$image['price_eur'] = substr($cmd, 0, -2);
					}
					break;
				}
				case substr($cmd, -3) === 'eur':{
					if(isset($image['price_eur'])){
						$error[] = 'multiple prices';
					}else{
						$image['price_eur'] = substr($cmd, 0, -3);
					}
					break;
				}
				case substr($cmd, 0, 1) === '(':{
					//not using it, just break
					break;
				}
				case $this->fbtestsSizesCheck($cmd):{
					//sizes command
					if(isset($image['sizes'])){
						$error[] = 'multiple sizes';
					}else{
						$image['sizes'] = $cmd;
					}
					break;
				}
				case $this->fbtestsSupplierCheck($cmd):{
					//supplier command
					if(isset($image['supplier'])){
						$error[] = 'multiple suppliers';
					}else{
						$image['supplier'] = $cmd;
					}
					break;
				}
				default :{
					$error[] = 'unknown symbols<br />';
					break;
				}
			}
		}
		if(!isset($image['price_eur'])){
			$error[] = 'no price tag';
		}

		return $image;
	}

	public function insertTestRow(array $data){
		$this->db->insert('facebook_tests_posts', $data);
		return $this->db->insert_id();
	}

	public function insertTestImageRow(array $data){
		$this->db->insert('facebook_tests_images', $data);
		return $this->db->insert_id();
	}

	public function fbtestsSizesCheck($cmd){
		$commands = [
						'xsl',
						'sxl',
						'sm',
						'sml',
						'mxl',
						'mxxl',
						'xsxl',
						'sxxl',
						'xsxxl',
						'xss',
						'lxl',
						'lxxl',
						'mxl',
						's,m,xl',
						's,l',
						'xsm',
						'xlxxl',
						's,xl,xxl',
						'xs,l',
					];
		foreach($commands as $command){
			if($cmd == $command){
				return true;
			}
		}

		return false;
	}

	public function fbtestsSupplierCheck($cmd){
		$commands = [
						'alper',
						'durmu',
						'shaki',
						'hari',
						'arslan'
					];
		foreach($commands as $command){
			if($cmd == $command){
				return true;
			}
		}

		return false;
	}

	public function getAllPages(){
		$sql = "select *
					from facebook_pages
					where active = 1
						and fb_id is not null
				";
		$res = $this->db->query($sql)->result_array();
		return $res;
	}

	public function parseTestsDescription($description, $sizes = '', $price_eur = '', $country_id = '3', $code = ''){

		switch($sizes){
			case 'xsl':{
				$available_sizes = 'XS, S, M, L';
				break;
			}
			case 'sm':{
				$available_sizes = 'S, M';
				break;
			}
			case 'sml':{
				$available_sizes = 'S, M, L';
				break;
			}
			case 'sxl':{
				$available_sizes = 'S, M, L, XL';
				break;
			}
			case 'sxxl':{
				$available_sizes = 'S, M, L, XL, XXL';
				break;
			}
			case 'mxxl':{
				$available_sizes = 'M, L, XL, XXL';
				break;
			}
			case 'mxl':{
				$available_sizes = 'M, L, XL';
				break;
			}
			case 'xsxl':{
				$available_sizes = 'XS, S, M, L, XL';
				break;
			}
			case 'xsxxl':{
				$available_sizes = 'XS, S, M, L, XL, XXL';
				break;
			}
			case 'xss':{
				$available_sizes = 'XS, S';
				break;
			}
			case 'lxl':{
				$available_sizes = 'L, XL';
				break;
			}
			case 'lxxl':{
				$available_sizes = 'L, XL, XXL';
				break;
			}
			case 'mxl':{
				$available_sizes = 'M, L, XL';
				break;
			}
			case 's,m,xl':{
				$available_sizes = 'S, M, XL';
				break;
			}
			case 's,l':{
				$available_sizes = 'S, L';
				break;
			}
			case 'xsm':{
				$available_sizes = 'XS, S, M,';
				break;
			}
			case 'xlxxl':{
				$available_sizes = 'XL, XXL';
				break;
			}
			case 's,xl,xxl':{
				$available_sizes = 'S, XL, XXL';
				break;
			}
			case 'xs,l':{
				$available_sizes = 'XS, L';
				break;
			}
			default:{
				$available_sizes = '';
				break;
			}
		}

		switch($country_id){
			case '1':{
				//Bulgaria - convert to BGN
				$real_price = ceil($this->money->convertAmount($price_eur, 2 , 'EUR', 'BGN'));
				$available_next_week = 'В наличност от следващата седмица';
				$product_test = 'Тест на продукта! - ако тази публикация достигне 20 харесвания, ще имаме продукта следващата седмица';
				$restock = 'Имаме го отново :)';
				$currency = 'лв';
				break;
			}
			case '2':{
				//Romania - convert to LEI
				$real_price = ceil($this->money->convertAmount($price_eur, 2 , 'EUR', 'RON'));
				$available_next_week = 'Disponibilă săptămâna viitoare';
				$product_test = 'Test de produs! - dacă acest post ajunge la 20 de persoane, avem produsul săptămâna viitoare';
				$restock = 'reaprovizionat :)';
				$currency = 'LEI';
				break;
			}
			case '3':{
				//Greece
				$real_price = $price_eur;
				$available_next_week = 'Διατίθεται την επόμενη εβδομάδα';
				$product_test = 'Δοκιμές προϊόντος! - αν η θέση αυτή φτάσει τα 20 άτομα, θα έχουμε το προϊόν την επόμενη εβδομάδα';
				$restock = 'επανεγκαθίστανται :)';
				$currency = '€';
				break;
			}
			case '5':{
				//Croatia - Convert price to Kuna(kn)HRK
				$real_price = ceil($this->money->convertAmount($price_eur, 2 , 'EUR', 'HRK'));
				$available_next_week = 'Sljedeći tjedan dostupan';
				$product_test = 'Ispitivanje proizvoda! - ako ovaj post dostigne 20 osoba, proizvod ćemo sljedećeg tjedna';
				$restock = 'nadopunjavaju :)';
				$currency = 'kn';
				break;
			}
			case '6':{
				//Slovania
				$real_price = $price_eur;
				$available_next_week = 'Na voljo naslednji teden';
				$product_test = 'Testiranje izdelkov! - če ta objava doseže 20 ljudi, bomo naslednji teden imeli izdelek';
				$restock = 'obnovljeno :)';
				$currency = '€';
				break;
			}
			case '7':{
				//Slovakia
				$real_price = $price_eur;
				$available_next_week = 'K dispozícii budúci týždeň';
				$product_test = 'Testovanie produktov! - ak je tento príspevok dosiahne 20 ľudí, budeme mať produkt budúci týždeň';
				$restock = 'zásobiť sa :)';
				$currency = '€';
				break;
			}
			case '8':{
				// The Czech
				$real_price = $price_eur;
				$available_next_week = 'K dispozici bude příští týden';
				$product_test = 'Testování produktu! - pokud tento příspěvek dosáhne 20 lidí, budeme mít produkt příští týden';
				$restock = 'obnoven :)';
				$currency = '€';
				break;
			}
			case '9':{
				//Hungary
				$real_price = $price_eur;
				$available_next_week = 'Elérhető a jövő héten';
				$product_test = 'Termék tesztelés! - ha ez a bejegyzés eléri a 20 személyt, jövő héten lesz a termékünk';
				$restock = 'újra népesít :)';
				$currency = '€';
				break;
			}
			default:{
				$real_price = $price_eur;
				$currency = '€';
			}
		}

		//words to be replaced
		$vars = array(
			'{$code}' => $code,
			'{$currency}' => $currency,
			'{$price}' => $real_price,
			'{$sizes}' => $available_sizes,
			'{$Available next week}' => $available_next_week,
			'{$Product testing}' => $product_test,
			'{Restock}' => $restock
		);
		//result description
		$descriptionWithValues = strtr($description, $vars);
		return $descriptionWithValues;
	}

	public function findById($id){
		return $this->db->get_where('facebook_pages', array('id' => intval($id)), $limit = 1)->row_array();
	}

	public function findByFBId($id){
		return $this->db->get_where('facebook_pages', array('fb_id' => intval($id)), $limit = 1)->row_array();
	}

	/*
	 * by Stefan
	 * Picks Image for autoposting
	 * $code - product code
	 * $brand_id - ID of the brand from DB brands_list
	 */
	public function getPictureForAutopost($code, $brand_id){
		$sql = "select *
					from images
					where code = '$code'
						and (type = 'F' or
							type = 'C')
						and brand = '$brand_id'
					order by times_posted";
		$res = $this->db->query($sql1)->result();
		return $res;
	}

    /**
	 * by Stefan
	 * Changes Profile Picture
     */
	public function changeProfilePic($page_id){
		//facebook required
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');

		$page = $this->findPageById($page_id);
		$place = '/'.$page['fb_id'].'/picture';
		echo "<pre>";
		var_dump($page);
		$fb = new Facebook\Facebook([
			'app_id' => $page['app_id'],
			'app_secret' => $page['secret_id'],
			'default_graph_version' => 'v2.9'
			]);


		$brand = $this->images_model->findBrandByPageID($page_id);
		$br = $brand->id; //check if MS or SD, may be
		if($br){
			var_dump($brand);
			$sql1 = "select *
						from images
						where type = 'pp'
							and brand = '$br'
						order by times_posted
						limit 1";

			$res1 = $this->db->query($sql1)->result();
			if($res1){
				var_dump($res1);
				$filename = $res1[0]->file_name;
				$fileext = $res1[0]->file_ext;

				$path = APPPATH.'../uploads/'.$res1[0]->code.'/';
				$image = $path.$filename.$fileext;
				$image = $res1[0]->url;

				$data = [
					'message' => 'Featured Profile Picture',
					'source' => $fb->fileToUpload($image)
				];

				try{
					//Returns a `Facebook\FacebookResponse` object
					$response = $fb->post($place, $data, $page['access_token']);
				}catch(Facebook\Exceptions\FacebookResponseException $e){
					echo 'Graph returned an error: ' . $e->getMessage();
					exit;
				}catch(Facebook\Exceptions\FacebookSDKException $e){
					echo 'Facebook SDK returned an error: ' . $e->getMessage();
					exit;
				}

				$graphNode = $response->getGraphNode();
				$image_id = $res1[0]->id;

				$sql2 = "UPDATE images SET times_posted = times_posted + 1 WHERE id = '$image_id'";
				$this->db->query($sql2);

				//update images table with the id and times posted
				var_dump($graphNode);
			}
		}
		echo "</pre>";
	}

    /**
	 * by Stefan
	 * Changes Covers
     * The whole controller needs to be a lot more automated
     */
	public function changeCover($page_id){
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');

		$page = $this->facebook_model->findPageById($page_id);
		$fb = new Facebook\Facebook([
			'app_id' => $page['app_id'],
			'app_secret' => $page['secret_id'],
			'default_graph_version' => 'v2.9'
			]);
		$brand = $this->images_model->findBrandByPageID($page_id);
		$br = $brand->id; //check if MS or SD, may be
		if($br){
			$sql1 = "select *
						from images
						where type = 'cov'
							and brand = '$br'
						order by times_posted
						limit 1";

			$res1 = $this->db->query($sql1)->result();
			if($res1){
				echo "<pre>";
				echo "-----------------------------------------------<pre>";
				var_dump($res1);
				echo "</pre>";
				echo "<br /><br />";
				$data['file_path'] = APPPATH.'../uploads/'.$res1[0]->code.'/';
				$data['file_name'] = $res1[0]->file_name;
				$data['file_ext'] = $res1[0]->file_ext;

				$image = APPPATH.'../uploads/'.$res1[0]->code.'/'.$res1[0]->file_name.'.'.$res1[0]->file_ext;

				$data = [
					'message' => 'Featured Cover',
					'source' => $fb->fileToUpload($image)
				];

				try{
					$response = $fb->post('/me/photos', $data, $page['access_token']);
				}catch(Facebook\Exceptions\FacebookResponseException $e){
					echo 'Graph returned an error: ' . $e->getMessage();
					exit;
				}catch(Facebook\Exceptions\FacebookSDKException $e){
					echo 'Facebook SDK returned an error: ' . $e->getMessage();
					exit;
				}

				$graphNode = $response->getGraphNode();

				$fb_photo_id = $graphNode['id'];

				$args = array('cover' => $fb_photo_id, 'offset_y' =>0);

				try{
					$response = $fb->post('/me/', $args, $page['access_token']);
				}catch(Facebook\Exceptions\FacebookResponseException $e){
					echo 'Graph returned an error: ' . $e->getMessage();
					exit;
				}catch(Facebook\Exceptions\FacebookSDKException $e){
					echo 'Facebook SDK returned an error: ' . $e->getMessage();
					exit;
				}
				$graphNode = $response->getGraphNode();

				$img_id = $res1[0]->id;
				$sql2 = "UPDATE images SET times_posted = times_posted + 1 WHERE id = '$img_id'";
				$this->db->query($sql2);
				$sql3 = "UPDATE facebook_pages SET cover_changed = NOW() WHERE id = '$page_id'";
				$this->db->query($sql3);

				echo "<pre>";
				var_dump($graphNode);
				echo "</pre>";
			}
		}
	}

	/*
	 * by Stefan
	 * Returns brand object
	 * @params int, product_id from DB table products
	 * @params int, page_id from DB table facebook_pages
	 */
	public function chooseImageForPost($product_id, $page_id){
		//get images from the new table
		//put some checks
		$product = $this->product_model->findById($product_id);
		$pid = $product['id'];
		$code = $product['code'];
		$page = $this->facebook_model->findPageById($page_id);
		$brand = $this->images_model->findBrandIDByShortName($page['brand_name']);
		$brand_id = $brand->id;

		//types F, C, P
		//brand - either choosed or original(id=11)
		//ordered by last posted
		if($page_id == '14' || $page_id == '29'){
			//BG pages exclude original images
			$sql1 = "select *
				from images
				left join test_posted_pics as p on p.images_id = images.id
				where (type = 'F'
					   OR type = 'C'
					   OR type = 'P')
					   AND code = '$code'
					   AND brand = '$brand_id'
				ORDER BY p.posted_on ASC
				";
		}else{
			$sql1 = "select *
				from images
				left join test_posted_pics as p on p.images_id = images.id
				where (type = 'F'
					   OR type = 'C'
					   OR type = 'P')
					   AND code = '$code'
					   AND (brand = '$brand_id'
							OR brand = '11')
				ORDER BY p.posted_on ASC
				";
		}
		$res1 = $this->db->query($sql1)->row();
		if(empty($res1))
			return false;
		$image = $res1;

		$data['file_path'] = APPPATH.'../uploads/'.$code.'/';
		$data['file_name'] = $image->file_name;
		$data['file_ext'] = $image->file_ext;

		$file_from = $data['file_path'].$data['file_name'].'.'.$data['file_ext'];
		$file_to = $data['file_path'].$data['file_name'].'_TEMP.'.$data['file_ext'];

		copy($file_from, $file_to);

		if($image->brand == 11){
			$this->image->brandImage($data, $code, $brand);
		}
		echo "<pre>";
		var_dump($image);
		echo "</pre>";
		$url = base_url().'uploads/'.$image->code.'/'.$image->file_name.'_TEMP.'.$image->file_ext;
		return $url;
	}

	public function getunreadmessagesbyuser($user_id){
		//$user_id = 99; //adelina
		require_once 'application/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '64M');
		$app_id = '1848049545471939';
		$app_secret = '60a7c3fbf130abd33ada6479dfe48822';
		$page_id = '239682463042533';
		$mytoken = 'EAAaQypCvZA8MBAFIsyaqGk4pJ4MhhNc5hSYFFCv1LYhtLQHdZBt3QZBhmcei0alkpZBhzc3lC4fEdDaQZC8MKlZAwIZAVuluF7KcmZA2ze79WEmAu4uB6A1WxzjV8PCczpqVOPnY48A5ESwEk4tO9HWxaX01QrLOOhMZD';
		$fb = new Facebook\Facebook(array('app_id'=>$app_id, 'app_secret'=>$app_secret, 'default_graph_version'=>'v2.8'));
		$fbApp = $fb->getApp();
		$request = new Facebook\FacebookRequest($fbApp, $mytoken, 'GET', '/me/accounts?fields=id,unseen_message_count&limit=50');
		try{
			$response = $fb->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			echo 'Graph returned an error: '.$e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			echo 'Facebook SDK returned an error: '.$e->getMessage();
			exit;
		}
		$wtf = $response->getGraphEdge();
		$i=0;
		$fb_pages = array();
		foreach($wtf as $page){
			$fb_pages[$i]['id'] = $page['id'];
			$fb_pages[$i]['unseen_message_count'] = $page['unseen_message_count'];
			$i++;
		}
		//change to query users first, then query pages
		$sql1 = "select id from facebook_pages where active = '1' and fb_id is not null";
		$res1 = $this->db->query($sql1)->result();
		$pages = array();
		$i=0;
		foreach($res1 as $row){
			$pid = $row->id;
			$page = $this->facebook_model->findPageById($pid);
			foreach($fb_pages as $fb_page){
				if($fb_page['id'] == $page['fb_id']){
					$pages[$i]['fb_title'] = $page['fb_title'];
					$pages[$i]['fb_id'] = $page['fb_id'];
					$pages[$i]['url'] = $page['url'];
					$pages[$i]['unseen_message_count'] = $fb_page['unseen_message_count'];
					$fb_page_id = $page['id'];
					$sql2 = "select *
								from fb_pages_users
								where page_id = '$fb_page_id'
									and role = 'Primary'
							";
					$res2 = $this->db->query($sql2)->row_array();
					if($res2['user_id'] == $user_id){
						$pages[$i]['myrole'] = 'Primary';
						$i++;
					}
				}
			}
		}
		unset($pages[$i]);
		usort($pages,'sortArrayByUnreadMessageCount');
		$pages = array_reverse($pages);
		return $pages;
	}

	public function getusernamebyid($uid){
		$sql = "select name
					from user
					where id = '$uid'
				";
		$res = $this->db->query($sql)->row_array();
		return $res['name'];
	}

    /**
	 * by Stefan
     * Populate a facebook page row
     * And call extendtoken, extending the facebook page token
     */
	public function populatePageRow(array $data){
		$page_id = $data['fb_id'];
		$page_array = $this->findPageByFBId($page_id);
		$id = $page_array['id'];
		if($id){
			//update
			$this->db->trans_start();
			$this->db->where('id',$id);
			$this->db->update('facebook_pages',$data);
			$this->db->trans_complete();
			echo '<br />Successfully Updated: <br />'.$data['fb_title'].'<br /><a href="'.$data['url'].'">'.$data['url'].'</a><br />'; //control text
		}else{
			//insert
			$this->db->trans_start();
			$this->db->insert('facebook_pages', $data);
			$this->db->trans_complete();
			echo '<br />Successfully Added: <br />'.$data['fb_title'].'<br /><a href="'.$data['url'].'">'.$data['url'].'</a><br />'; //control text
		}
		$this->extendToken($id);
	}

    /**
	 * by Stefan
     * Extends a facebook page token, by given DB id
     */
	public function extendToken($id){
		$page = $this->findPageById($id);
		if($page){
			$pid = $page['id'];
			$app_id = $page['app_id'];
			$app_secret = $page['secret_id'];
			$access_token = $page['access_token'];
			//generate link
			$special_link = 'https://graph.facebook.com/oauth/access_token?client_id='.$app_id.'&client_secret='.$app_secret.'&grant_type=fb_exchange_token&fb_exchange_token='.$access_token;
			//catch the new token
			$access_token = file_get_contents($special_link);
			$access_token = json_decode($access_token);
			$page['access_token'] = $access_token->access_token;
			//update DB
			$this->db->trans_start();
			$this->db->where('id',$id);
			$this->db->update('facebook_pages',$page);
			$this->db->trans_complete();
		}
		echo "<br />Token Successfully Extended!<br />"; //control text
	}

    /**
	 * by Stefan
     * Finds and returns Page object by given DB ID
     */
	public function findPageById($id){
		return $this->db->get_where('facebook_pages', array('id' => intval($id)), $limit = 1)->row_array();
	}

	public function deleteRow($id)
	{
		$sqlDisable = 'SET foreign_key_checks = 0';
		$this->db->query($sqlDisable);
		$this->db->trans_start();
    $this->db->where('id', intval($id));
    $this->db->delete('facebook_pages');
    $this->db->trans_complete();
		$sqlEnable = 'SET foreign_key_checks = 1';
		$this->db->query($sqlEnable);
    return $this->db->trans_status();
	}

	/*
	 * by Stefan
	 * Finds and returns Page object by given Facebook Page ID
	 */
	public function findPageByFBId($fb_id){
		return $this->db->get_where('facebook_pages', array('fb_id' => intval($fb_id)), $limit = 1)->row_array();
	}

	/*
	 * by Stefan
	 * Returns arrays with all pages IDs
	 * Used for changing covers
	 */
	public function getAllPagesIDs(){
		$sql1 = "select id
					from facebook_pages
					where active = 1
						and fb_id is not null";
		return $this->db->query($sql1)->result();
	}

	/*
	 * by Stefan
	 * Returns arrays with all pages IDs by given brand
	 * Used for changing covers
	 */
	public function getAllPagesIDsByBrand($brand){
		$sql1 = "select id
					from facebook_pages
					where brand_name = '$brand'
						and active = 1
						and fb_id is not null";
		return $this->db->query($sql1)->result();
	}

	/*
	 * by Stefan
	 * Returns arrays with all flame pages IDs
	 * Used for changing covers
	 */
	public function getAllFlamePagesIDs(){
		$sql1 = "select id
					from facebook_pages
					where active = '1'
						and fb_id is not null
						and brand";
		return $this->db->query($sql1)->result();
	}

	/*
	 * by Chavdar
	 * Manually Inserts Facebook Page Row, by givven $data array
	 */
	public function insertRow(array $data) {
		$this->db->trans_start();
		$this->db->insert('facebook_pages', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	/**
	 * by Chavdar
	 * Updates Facebook page row in DB by givven ID and $data
	 */
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('facebook_pages', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	/*
	 * Deprecated
	 */
	public function populateFBPagesUsersRow(array $data){
		//insert only
		$this->db->trans_start();
		$this->db->insert('fb_pages_users', $data);
		$this->db->trans_complete();
	}

	/*
	 * by Chavdar
	 * Gets Data for a select in <create facebook post>, used for FAP
	 */
    public function getSelectCollectionFAP() {
		$data = $this->db
			->select('id, title, country')
			->from('facebook_pages')
            ->where('origin_page_id !=', '')
            ->where('active', 1)
            ->get()
			->result();

		return  $data;
	}

	/*
	 * by Chavdar
	 * What is this exactly used for?!?!
	 */
	public function getSelectCollection() {
		$data = $this->db
			->select('id, title')
            ->where('active', 1)
			->get('facebook_pages')
			->result();
		$response = array();
		foreach($data as $country)  {
			$response[$country->id] = $country->title;
		}

		return  $response;
	}

	/*
	 * by Chavdar
	 * What is this exactly used for?!?!
	 */
	public function getUsersAccess($id) {
		$data = $this->db
			->select('id_user')
			->from('facebook_pages_users')
			->where('id_page', $id)
			->get()
			->result();
		$response = array();
		foreach($data as $user)  {
			$response[] = $user->id_user;
		}
		return $response;
	}

	/*
	 * by Chavdar
	 * Going to be depricated soon, being replaced by automated functions
	 */
	public function insertManageRow(array $data) {
		$this->db->trans_start();
		$this->db->insert('facebook_pages_users', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	/*
	 * by Chavdar
	 * Going to be depricated soon, being replaced by automated functions
	 */
	public function updateManageRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('facebook_pages_users', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	/*
	 * by Chavdar
	 * Going to be depricated soon, being replaced by automated functions
	 */
	public function findManageById($id_user, $id_page) {
		return $this->db->get_where('facebook_pages_users',
			array(
				'id_user' => intval($id_user),
				'id_page' => intval($id_page)
				), $limit = 1)
			->row_array();
	}

	/**
	 *
	 * by Chavdar
	 * Going to be depricated soon, being replaced by automated functions
	 * @param int $id_user
	 * @param int $id_page
	 */
	public function deleteManageRow($id_user, $id_page) {
			$this->db->where('id_user', $id_user);
			$this->db->where('id_page', $id_page);
			$this->db->delete('facebook_pages_users');
	}

	public function insertFacebookPostsRow(array $data) {
        $this->db->trans_start();
        $this->db->insert('facebook_posts', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

	/**
	 * Get all facebook posts for listing
	 *edited by Georgi on 12042017 to add filter by fb_page
	 * @return mixed
	 */
	public function getAllFacebookPosts( $fb_page = null)
	{
		$SQL = "SELECT
							facebook_posts.*,
							products.id as products_table_id,
							products.code,
							facebook_pages.title as page_title,
							(SELECT `file_name`
								 FROM `images`
								 WHERE images.code = products.code
									AND images.type = 'F'
								 ORDER BY times_posted ASC
								 LIMIT 1
							) as filename,
							brands_list.short_name as brand_name
						FROM
							facebook_posts
							LEFT JOIN facebook_pages
								ON facebook_pages.id = facebook_posts.facebook_page_id
							LEFT JOIN products
								ON products.id = facebook_posts.product_id
							LEFT JOIN brands_list
								ON facebook_pages.brand_name = brands_list.short_name ";
		if (isset($fb_page)) {

			$SQL .= ' WHERE facebook_page_id = ' . $fb_page . ' ';
		};
		$SQL .= ' ORDER BY date_schedule DESC
							LIMIT 100 ';
		$products_query = $this->db->query($SQL);
		//$index_brand_product = $products_query->result_array();
		$result = $products_query->result();

			return $result;
	}

	public function getAllFacebookPosts_backup(){
		$result = $this->db
					->select('facebook_posts.*, products.id as products_table_id, products.code, facebook_pages.title as page_title')
					->from('facebook_posts')
					->join('facebook_pages', 'facebook_pages.id = facebook_posts.facebook_page_id', 'left')
					->join('products', 'products.id = facebook_posts.product_id', 'left')
					->order_by('facebook_posts.id', 'DESC')
					->get()
					->result();

			$result = array_map(function($post){
					return $post;
			},$result);

			return $result;
	}

    public function findFacebookPost($post_id){
        return $this->db
            ->select('facebook_posts.*, facebook_pages.country AS country')
            ->from('facebook_posts')
            ->join('facebook_pages', 'facebook_pages.id = facebook_posts.facebook_page_id', 'left')
            ->where('facebook_posts.id', $post_id)
            ->limit(1)
            ->get()
            ->row();
    }

		public function findFacebookPostByFbId($fbId) {
			$sql = "SELECT * FROM facebook_posts WHERE fb_post_id LIKE '" . $fbId . "' LIMIT 1";
			return $this->db->query($sql)->row_array();
		}

    public function updateFacebookPost($post_id , $data) {
        $this->db->trans_start();
        $this->db->where('id', intval($post_id));
        $this->db->update('facebook_posts', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function deleteFacebookPost($post_id){
        $this->db->trans_start();
        $this->db->where('id', $post_id);
        $this->db->delete('facebook_posts');
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function getFacebookPagesDescriptions($page_id) {
        return $this->db
            ->select('facebook_pages_descriptions.*, facebook_pages.title as page_name')
            ->from('facebook_pages_descriptions')
            ->join('facebook_pages', 'facebook_pages.id = facebook_pages_descriptions.page_id', 'left')
            ->order_by('facebook_pages_descriptions.id', 'ASC')
            ->where('facebook_pages_descriptions.page_id', $page_id)
            ->get()
            ->result();
    }

    /**
     * Insert Facebook page description
     *
     * @param $data
     */
    public function insertFacebookPageDescription($data){
        $this->db->trans_start();
        $this->db->insert('facebook_pages_descriptions', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function findFacebookPageDescription($facebook_page_description_id){
        $row =  $this->db->get_where('facebook_pages_descriptions',
            array(
                'id' => intval($facebook_page_description_id),
            ), $limit = 1)
            ->row();

        //$row->description = utf8_decode($row->description);

        return $row;
    }

    public function findFacebookPageDescriptions($facebook_page_id){
        $data = $this->db->get_where('facebook_pages_descriptions',
            array(
                'page_id' => intval($facebook_page_id),
            ))
            ->result();

        return array_map(function($row) {
            $row->description = $row->description;
            return $row;
        }, $data);
    }

    public function deleteFacebookPageDescription($facebook_page_description_id) {
        $this->db->trans_start();
        $this->db->where('id', $facebook_page_description_id);
        $this->db->delete('facebook_pages_descriptions');
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function updateFacebookPageDescription($facebook_page_description_id, $data){
        $this->db->trans_start();
        $this->db->where('id', intval($facebook_page_description_id));
        $this->db->update('facebook_pages_descriptions', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

	/*
	 * by Chavdar
	 * Used in direct posting to facebook
	 */
    public function AP_getAllfacebookPosts(){
        return $this->db
            ->select('facebook_posts.*,
             facebook_pages.app_id,
             facebook_pages.secret_id,
             facebook_pages.access_token,
             facebook_pages.origin_page_id,
             facebook_pages.watermark_image,
             facebook_pages.brand_name
             ')
            ->from('facebook_posts')
            ->join('facebook_pages', 'facebook_pages.id = facebook_posts.facebook_page_id', 'left')
            ->where('is_uploaded', 0)
            ->where('DATE_ADD(date_schedule, INTERVAL 5 MINUTE) < NOW()')
            ->get()
            ->result();
    }

	/*
	by Georgi for automating FB post
	*/
	public function product_fap($products, $fb_pages, $description, $startdatetime, $timeinterval){
		/*INPUTS
		$products = array (3,34,123);
		$fb_pages = array (1,3,8);
		$description = array ('Description one', 'description seven', 'description bla', 'description sadga');
		$startdatetime = new DateTime('tomorrow');
		$timeinterval = 5;
		*/

		//$post_date = strtotime($startdatetime);
		$post_date = date_create_from_format('Y-m-d H:i:s', $startdatetime);

		foreach ($fb_pages as $fb_page){
			foreach ($products as $product){
				$data = array(
				'product_id' => $product,
				'facebook_page_id' => $fb_page,
				'description' => $description[mt_rand(0, count($description) - 1 )],
				//'description' => utf8_encode($description[mt_rand(0, count($description) - 1 )]),
				'date_schedule' => $post_date->format('Y-m-d H:i:s'),
				'is_uploaded' => 0
				);
				//var_dump($data);
				$this->facebook_model->insertFacebookPostsRow($data);
				//Randomizes the time interval between posts from 1min to the selected time interval
				//$post_date->add(new DateInterval('PT'. mt_rand(1,$timeinterval) .'M'));
				$post_date->add(new DateInterval('PT'. $timeinterval .'M'));
			}
		}
	}
	/*
	by Georgi for easy DB queries
	*/
	public function from_db($table, $fields, $where){
		$data = $this->db
			->select('id, title, country')
			->from($table)
            ->where($where)
            //->where('active', 1)
            ->get()
			->result();

		return  $data;

	}

	/*
	by Georgi for automating FB post
	Makes queries for the multiselect form in the auto fap view
	*/
	public function get_fap_lists( $sel_product_serie = '%')
	{
		//Creates Array with all active Facebook pages and their IDs
		$fb_pages_query = $this->db->query('SELECT id, title
																				FROM facebook_pages
																				WHERE facebook_pages.active = 1
																				ORDER BY 2 ASC');
		foreach ($fb_pages_query->result() as $row){
			//This is an array with all active Facebook pages and their IDs
			$fb_pages_list[$row->id] = $row->title;
		}
		//var_dump($fb_pages_list);

		//Creates an Array with all descriptions and their IDs
		$description_query = $this->db->query('SELECT id, description FROM facebook_pages_descriptions');
		foreach ($description_query->result() as $row){
			//This is an array with all descriptions and their IDs
			$description_list[$row->id] = $row->description;
		}
		//var_dump($description_list);

		//Creates an Array with all available products and their IDs
		//var_dump($sel_product_serie);
		$test = 'SELECT id, code FROM products WHERE	code LIKE \'' . $sel_product_serie . '%' . '\' ORDER BY code';
		//var_dump($test);
		$products_query = $this->db->query('SELECT id, code FROM products WHERE	code LIKE \'' . $sel_product_serie . '%' . '\' ORDER BY code');
		foreach ($products_query->result() as $row){
			//This is an array with all active Facebook pages and their IDs
			$products_list[$row->id] = $row->code;
		}
		//var_dump($products_list);

		$time_interval_list = array(5,10,15,20,25,30,45,60,90,120,180,240,300);

		//Creates an Array with all available product series
		$series = $this->facebook_model->unique_product_series();

		//var_dump($products_list);
		$results['fb_pages_list'] = $fb_pages_list;
		$results['description_list'] = $description_list;
		$results['products_list'] = $products_list;
		$results['products_series'] = $series;
		$results['time_interval_list'] = $time_interval_list;

		return($results);
	}

	/*
	by Georgi for getting the unique product series in the DB
	Creates list for the dropdown filter in auto_fap
	*/
	public function unique_product_series( )
	{
		$products_query = $this->db->query('SELECT id, code FROM products ORDER BY code');
		foreach ($products_query->result() as $row){
			//This is an array with all active Facebook pages and their IDs
			$product_series[$row->id] = strtoupper(substr($row->code, 0, 2));
		}
		//var_dump($product_series);

		$series = array_unique($product_series);
		$results = array_values($series);

		return($results);
	}

	/*
	by Georgi for getting the brands list from db that
	Creates list for the dropdown filter in indexByBrand
	*/
	public function index_brand_product($brand_id=NULL)
	{

		$SQL = 'SELECT
							br_rel.brands_list_id,
					    br_rel.products_id,
					    brand.full_name as brand_name,
					    products.code,
					    products.name as product_name,
					    SUM(CASE When sz.size_id=1 Then sz.quantity Else 0 End ) as xs,
					    SUM(CASE When sz.size_id=2 Then sz.quantity Else 0 End ) as s,
					    SUM(CASE When sz.size_id=3 Then sz.quantity Else 0 End ) as m,
					    SUM(CASE When sz.size_id=4 Then sz.quantity Else 0 End ) as l,
					    SUM(CASE When sz.size_id=5 Then sz.quantity Else 0 End ) as xl,
					    SUM(CASE When sz.size_id=6 Then sz.quantity Else 0 End ) as xxl,
					    SUM(CASE When sz.size_id=7 Then sz.quantity Else 0 End ) as xxxl,
					    SUM(sz.quantity) as total,
					    max(br_rel.added_at) as last_added,
							now() as today,
							DATEDIFF(now(),max(br_rel.added_at)) as daysActive
						FROM
							`brand_products_relations` AS br_rel
						LEFT JOIN
							brands_list as brand
							on br_rel.brands_list_id=brand.id
						LEFT JOIN
							products
							on  br_rel.products_id=products.id
						LEFT JOIN
							product_size AS sz
							on  br_rel.products_id=sz.product_id
						WHERE
							removed_at IS NULL ';
		if ($brand_id !== NULL) {
			$SQL .= ' AND br_rel.brands_list_id = ' . $brand_id . ' ';
	 	};
		$SQL .=' GROUP BY br_rel.products_id
						ORDER BY total desc';

		$products_query = $this->db->query($SQL);

		$index_brand_product = $products_query->result_array();

		//var_dump($index_brand_product);

		return($index_brand_product);
	}

	/*
	by Georgi to filter products by series
	for jquery requests
	*/
	public function get_stocked_products($pr_serie=NULL)
	{
		$SQL = 'SELECT
							pr.id,
					    pr.code,
							SUM(CASE When sz.size_id=1 Then sz.quantity Else 0 End ) as xs,
					    SUM(CASE When sz.size_id=2 Then sz.quantity Else 0 End ) as s,
					    SUM(CASE When sz.size_id=3 Then sz.quantity Else 0 End ) as m,
					    SUM(CASE When sz.size_id=4 Then sz.quantity Else 0 End ) as l,
					    SUM(CASE When sz.size_id=5 Then sz.quantity Else 0 End ) as xl,
					    SUM(CASE When sz.size_id=6 Then sz.quantity Else 0 End ) as xxl,
					    SUM(CASE When sz.size_id=7 Then sz.quantity Else 0 End ) as xxxl,
					    SUM(sz.quantity) as total,
					    pr.name as product_name
						FROM
							products as pr
						LEFT JOIN
							product_size as sz
						    ON pr.id = sz.product_id
						WHERE sz.quantity > 0';
		if ($pr_serie !== NULL) {
			$SQL .= ' AND pr.code LIKE \'' . $pr_serie .'%\'';
	 	};
		$SQL .=' GROUP BY pr.code
						ORDER BY total desc';

		$products_query = $this->db->query($SQL);
		$index_brand_product = $products_query->result_array();

		return($index_brand_product);
	}

	/*
	by Georgi to filter products by series
	for jquery requests
	*/
	public function get_stocked_products_st($pr_serie=NULL)
	{
		$SQL = 'SELECT
							pr.id,
							pr.code,
							SUM(CASE When sz.size_id=1 Then sz.quantity Else 0 End ) as xs,
							SUM(CASE When sz.size_id=2 Then sz.quantity Else 0 End ) as s,
							SUM(CASE When sz.size_id=3 Then sz.quantity Else 0 End ) as m,
							SUM(CASE When sz.size_id=4 Then sz.quantity Else 0 End ) as l,
							SUM(CASE When sz.size_id=5 Then sz.quantity Else 0 End ) as xl,
							SUM(CASE When sz.size_id=6 Then sz.quantity Else 0 End ) as xxl,
							SUM(CASE When sz.size_id=7 Then sz.quantity Else 0 End ) as xxxl,
							SUM(sz.quantity) as total,
							pr.name as product_name
						FROM
							products as pr
						LEFT JOIN
							product_size as sz
								ON pr.id = sz.product_id
						WHERE sz.quantity > 0';
		if ($pr_serie !== NULL) {
			$SQL .= ' AND pr.code LIKE \'' . $pr_serie .'%\'';
		};
		$SQL .=' GROUP BY pr.code
						ORDER BY pr.code desc';

		$products_query = $this->db->query($SQL);
		$index_brand_product = $products_query->result_array();

		return($index_brand_product);
	}

}
