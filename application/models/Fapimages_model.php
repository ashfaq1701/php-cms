<?php

class Fapimages_model extends MY_Model{
	
	public function insertImageRow(array $data){
		$this->db->trans_start();
		$this->db->insert('fap_images', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}
}