<?php

class Fbc_model extends MY_Model
{
	
	public function populateConversationRow(array $data){
		$conversation_id = $data['conversation_id'];
		$conversation_array = $this->findConversationById($conversation_id);
		
		$id = $conversation_array['id'];
		
		if($id){
			//update
			$this->db->trans_start();
			$this->db->where('id',$id);
			$this->db->update('fb_conversations',$data);
			$this->db->trans_complete();
			echo '<br />Successfully Updated: <br />'.$data['conversation_id'].'<br /><a href="https://business.facebook.com'.$data['conversation_link'].'">'.$data['conversation_link'].'</a><br />';
		}else{
			//insert
			$this->db->trans_start();
			$this->db->insert('fb_conversations', $data);
			$this->db->trans_complete();
			echo '<br />Successfully Added: <br />'.$data['conversation_id'].'<br /><a href="https://business.facebook.com'.$data['conversation_link'].'">'.$data['conversation_link'].'</a><br />';
		}
	}
	
	public function populateMessageRow(array $data){
		$message_id = $data['message_id'];
		$message_array = $this->findMessageById($message_id);
				
		$id = $message_array['id'];
		
		if($id){
			//update
			$this->db->trans_start();
			$this->db->where('id',$id);
			$this->db->update('fb_messages',$data);
			$this->db->trans_complete();
			echo '<br />Successfully Updated: <br />'.$data['message_id'].'<br />';
		}else{
			//insert
			$this->db->trans_start();
			$this->db->insert('fb_messages', $data);
			$this->db->trans_complete();
			echo '<br />Successfully Added: <br />'.$data['message_id'].'<br />';
		}
	}
	
	public function populateFBPagesUsersRow(array $data){
		//insert only
		$this->db->trans_start();
		$this->db->insert('fb_pages_users', $data);
		$this->db->trans_complete();
	}
	
	public function findConversationById($conversation_id){
		//searches for conversation id in our DB and returns
		return $this->db->get_where('fb_conversations', array('conversation_id' => $conversation_id), $limit = 1)->row_array();
	}
	
	public function findConversationByDBId($id){
		//searches for conversation id in our DB and returns
		return $this->db->get_where('fb_conversations', array('id' => $id), $limit = 1)->row_array();
	}
	
	public function findMessageById($message_id){
		//searches for message id in our DB and returns
		return $this->db->get_where('fb_messages', array('message_id' => $message_id), $limit = 1)->row_array();
	}
	
	public function getAllActivePages(){
		//query DB for all pages having active attribute
		$sql1 = "SELECT *
					FROM fb_pages
					WHERE active = 1";
		
		return $this->db->query($sql1)->result();
	}
	
	public function getAllActiveAgents(){
		//query DB for all users having FB ID
		//(meaning they are actually selling atm)
		//would most liekly require a future change
		$sql1 = "SELECT id, fb_id, username, name
					FROM user
					WHERE fb_id IS NOT NULL";
		
		return $this->db->query($sql1)->result();
	}
}