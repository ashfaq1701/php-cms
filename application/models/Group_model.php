<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends MY_Model {
	
	const TYPE_ROOT = 'root';
	const TYPE_ADMINISTRATOR = 'admin';
	const TYPE_STAFF = 'staff';
	
	/**
	 * 
	 * @return array
	 */
	public function getCollection() {
		return $this->db->get_where('user_groups', array('is_hidden' => false))->result();
	}
	
	/**
	 * 
	 * @param int $id
	 */
	public function findById($id) {
		return $this->db->get_where('user_groups', array('id' => intval($id)), $limit = 1)->row_array();
	}
	
	/**
	 * 
	 * @param int $id
	 * @param array $data
	 * @return boolean
	 */
	public function updateRow($id, $data) {
		$permissions = $data['permissions'];

		$new_permissions = null;
		if ($permissions) {
			$new_permissions = '|' . implode('|', $permissions) . '|';
		}

		$data['permissions'] = $new_permissions ?: null;
		
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('user_groups', $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public static function isRoot() {
		return self::getCurrentUserGroupIdentifier() === self::TYPE_ROOT;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public static function isAdmin() {
		return self::getCurrentUserGroupIdentifier() === self::TYPE_ADMINISTRATOR;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public static function isStaff() {
		return self::getCurrentUserGroupIdentifier() === self::TYPE_STAFF;
	}
	
	/**
	 * 
	 * @return string|null
	 */
	private static function getCurrentUserGroupIdentifier() {
		$core =& get_instance();
		
		if (isset($core->session->userdata) && isset($core->session->userdata['admin_user'])) {
			return $core->session->userdata['admin_user']['group_identifier'];
		}
		
		return null;
	}
	
}