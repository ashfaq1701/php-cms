<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Images_model extends MY_Model {

	/*
	 * by Stefan
	 * Returns brand ID
	 * @params string
	 */
	public function findBrandIDByName($brand){
		$sql1 = "select id
					from brands_list
					where abbreviation = '$brand'
					limit 1";

		return $this->db->query($sql1)->row();
	}

	public function getUnprocessedPhotos() {
		$sql = 'SELECT ph.*,
					pg.id AS page_id
		    	FROM facebook_photos ph
					LEFT JOIN facebook_albums a ON ph.fb_album_id = a.id
					LEFT JOIN facebook_pages pg ON a.fb_page_id = pg.id
					WHERE ph.fb_id IS NULL';
		$rows = $this->db->query($sql)->result_array();
		$groupedPhotos = array();
		foreach ($rows as $row) {
			if (!array_key_exists(' ' . $row['page_id'], $groupedPhotos)) {
				$groupedPhotos[' ' . $row['page_id']] = array();
			}
			if (!array_key_exists(' ' . $row['fb_album_id'], $groupedPhotos[' ' . $row['page_id']])) {
				$groupedPhotos[' ' . $row['page_id']][' ' . $row['fb_album_id']] = array();
			}
			if (!array_key_exists(' ' . $row['batch_no'], $groupedPhotos[' ' . $row['page_id']][' ' . $row['fb_album_id']])) {
				$groupedPhotos[' ' . $row['page_id']][' ' . $row['fb_album_id']][' ' . $row['batch_no']] = array();
			}
			$groupedPhotos[' ' . $row['page_id']][' ' . $row['fb_album_id']][' ' . $row['batch_no']][] = $row;
		}
		return $groupedPhotos;
	}

	public function findById($photoId) {
		$sql = 'SELECT * FROM images WHERE id = ' . $photoId;
		$row = $this->db->query($sql)->row_array();
		return $row;
	}

	public function deleteFacebookPhoto($photoId) {
		$sqlDisable = 'SET foreign_key_checks = 0';
		$this->db->query($sqlDisable);
		$sql1 = 'SELECT * FROM facebook_photos WHERE id = '.$photoId;
		$row1 = $this->db->query($sql1)->row_array();
		$sql2 = 'SELECT * FROM images WHERE id = '.$row1['image_id'];
		$row2 = $this->db->query($sql2)->row_array();
		$uploadFileUrl = $row2['url'];
		$parseUrl = parse_url($uploadFileUrl);
		$relativePath = $parseUrl['path'];
		$uploadedFilePath = __DIR__.'/../..'.$relativePath;
		if (file_exists($uploadedFilePath)) {
			unlink($uploadedFilePath);
		}
		$sql3 = 'DELETE FROM images WHERE id = '.$row1['image_id'];
		$this->db->query($sql3);
		$sql4 = 'DELETE FROM facebook_photos WHERE id = '.$photoId;
		$this->db->query($sql4);
		$sqlEnable = 'SET foreign_key_checks = 1';
		$this->db->query($sqlEnable);
	}

	public function searchByAll($data)
	{
		$this->db->select('*');
		$this->db->from('images');
		foreach($data as $k => $v)
		{
			$this->db->where($k, $v);
		}
		return $this->db->get()->result_array();
	}

	/*
	 */
	public function findBrandByID($brand_id){
		$sql1 = "select *
					from brands_list
					where id = '$brand_id'
					limit 1";

		return $this->db->query($sql1)->row();
	}

	/*
	 * by Stefan
	 * Returns brand object
	 * @params string
	 */
	public function findBrandByName($brand){
		$sql1 = "select *
					from brands_list
					where abbreviation = '$brand'
					limit 1";

		return $this->db->query($sql1)->row();
	}
	/*
	 * by Stefan
	 * Returns brand object
	 * @params int, product_id from DB table products
	 * @params int, page_id from DB table facebook_pages
	 */
	public function chooseImageForPost($product_id, $page_id){
		//get images from the new table
		//put some checks
		$product = $this->product_model->findById($product_id);
		$pid = $product['id'];
		$code = $product['code'];
		$page = $this->facebook_model->findPageById($page_id);
		$brand = $this->findBrandIDByShortName($page['brand_name']);
		$brand_id = $brand->id;

		//types F, C, P
		//brand - either choosed or original(id=11)
		//ordered by last posted
		if($page_id == '14' || $page_id == '29'){
			//BG pages exclude original images
			$sql1 = "SELECT *
				from images
				left join test_posted_pics as p on p.images_id = images.id
				where (type = 'F'
					   OR type = 'C'
					   OR type = 'P')
					   AND code = '$code'
					   AND brand = '$brand_id'
				ORDER BY p.posted_on ASC
				";
		}else{
			$sql1 = "SELECT *
				from images
				left join test_posted_pics as p on p.images_id = images.id
				where (type = 'F'
					   OR type = 'C'
					   OR type = 'P')
					   AND code = '$code'
					   AND (brand = '$brand_id'
							OR brand = '11')
				ORDER BY p.posted_on ASC
				";
		}
		$res1 = $this->db->query($sql1)->row();
		if(empty($res1))
			return false;
		$image = $res1;

		$data['file_path'] = APPPATH.'../uploads/'.$code.'/';
		$data['file_name'] = $image->file_name;
		$data['file_ext'] = $image->file_ext;

		$file_from = $data['file_path'].$data['file_name'].'.'.$data['file_ext'];
		$file_to = $data['file_path'].$data['file_name'].'_TEMP.'.$data['file_ext'];

		copy($file_from, $file_to);

		if($image->brand == 11){
			$this->image->brandImage($data, $code, $brand);
		}
		echo "<pre>";
		var_dump($image);
		echo "</pre>";
		$url = base_url().'uploads/'.$image->code.'/'.$image->file_name.'_TEMP.'.$image->file_ext;
		return $url;
	}

	/*
	 * by Stefan
	 * Returns brand object
	 * @params string
	 */
	public function findBrandIDByShortName($shortname){
		$sql1 = "select *
					from brands_list
					where short_name = '$shortname'
					limit 1";

		return $this->db->query($sql1)->row();
	}

	/*
	 * by Stefan
	 * Returns brand object
	 * @params string
	 */
	public function findBrandByPageID($page_id){
		$sql1 = "select brands_list.*
					from brands_list
					left join facebook_pages as fbp
						on fbp.brand_name = brands_list.short_name
					where fbp.id = '$page_id'
					limit 1";

		return $this->db->query($sql1)->row();
	}

	public function updatePhotoOrderings($photosArr)
	{
		for($i = 0; $i < count($photosArr); $i++)
		{
			$this->db->trans_start();
			$this->db->where('fb_id', $photosArr[$i]);
			$this->db->update('facebook_photos', array('photo_order' => $i + 1));
			$this->db->trans_complete();
		}
	}

	/*
	 * by Stefan
	 * Inserts Image into DB
	 * @params array
	 */
	public function insertImageRow(array $data){
		//check if it has to insert or update (may be)
		$this->db->insert('images', $data);
		return $this->db->insert_id();
	}

	public function searchFacebookPhotosByAll($data)
	{
		$this->db->select('*');
		$this->db->from('facebook_photos');
		foreach($data as $k => $v)
		{
			$this->db->where($k, $v);
		}
		return $this->db->get()->result_array();
	}

	public function findBrandByIdAsArr($brandId) {
		$sql = 'SELECT * FROM brands_list WHERE id = ' . $brandId . ' LIMIT 1';
		return $this->db->query($sql)->row_array();
	}

	public function insertBrand($data) {
		$this->db->insert('brands_list', $data);
		return $this->db->insert_id();
	}

	public function getBrandList() {
		$sql = "SELECT * FROM brands_list";
		return $this->db->query($sql)->result_array();
	}

	public function searchBrandsByAll($data) {
		$this->db->select('*');
		$this->db->from('brands_list');
		foreach($data as $k => $v)
		{
			$this->db->where($k, $v);
		}
		return $this->db->get()->result_array();
	}

	public function insertFacebookPhotoRow(array $data)
	{
		$this->db->insert('facebook_photos', $data);
		return $this->db->insert_id();
	}

	public function updateFacebookPhotoRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('facebook_photos', $data);
		$this->db->trans_complete();
  }

	/*
	 * by Stefan
	 * Returns count of ids for speciffic code
	 * @params string
	 */
	public function lastImageID(){
		$sql1 = "select max(id) as cn
					from images";
		return $this->db->query($sql1)->row();
	}

	/*
	 * by Stefan
	 * Returns count of ids for speciffic code
	 * @params string
	 */
	public function lastTestImageID(){
		$sql1 = "SELECT max(facebook_tests_images.id) as cn,
					max(facebook_tests_posts.test_id) as tn
					from facebook_tests_images, facebook_tests_posts";
		$res1 = $this->db->query($sql1)->row();
		$test['image_id'] = $res1->cn;
		$test['test_id'] = $res1->tn;
		return $res1;
	}

	/*
	 * by Stefan
	 * Check if Image command exists
	 * @params string
	 */
	public function cmdCheck($cmd){
		$cmd = strtolower($cmd);
		$commands = [
						'cov',
						'pp',
						'f',
						's',
						'b',
						'd',
						'c',
						'p',
						'te'
					];
		foreach($commands as $command){
			if($cmd == $command){
				return true;
			}
		}

		return false;
	}

	/*
	 * by Stefan
	 * Check if Image side content command exists
	 * @params string
	 */
	public function sideContentCheck($cmd){
		$cmd = strtolower($cmd);
		$commands = [
						'ccoo',
						'ccgr',
						'ccro',
						'ccbg'
					];
		foreach($commands as $command){
			if($cmd == $command){
				return true;
			}
		}

		return false;
	}

	/*
	 * by Stefan
	 * Check if code is properly defined
	 * @params string
	 */
	public function codeCheck($code){
		$code = strtolower($code);
		if(preg_match('/^[a-z]{2}[0-9]{3}$/', $code))
			return true;
		return false;
	}

	public function sizecheck($cmd){
		$commands = [
						'xs', 'xss', 'xsm', 'xsl', 'xsxl', 'xsxxl', 'xsxxxl',
						's', 'sm', 'sl', 'sxl', 'sxxl', 'sxxxl',
						'm', 'ml', 'mxl', 'mxxl', 'mxxxl',
						'l', 'lxl', 'lxxl', 'lxxxl',
						'xl', 'xlxxl', 'xlxxxl',
						'xxl', 'xxlxxxl',
						'xxxl'
					];
		foreach($commands as $command){
			if($cmd == $command){
				return true;
			}
		}

		return false;
	}

	public function suppliercheck($cmd){
		$commands = [
						'alper',
						'durmu',
						'shaki',
						'hari',
						'arslan'
					];
		foreach($commands as $command){
			if($cmd == $command){
				return true;
			}
		}

		return false;
	}
}
