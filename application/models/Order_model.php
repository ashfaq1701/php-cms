<?php

class Order_model extends MY_Model {

	const ITEMS_PER_PAGE = 50;

	/**
	 *
	 * @param int $id
	 */
	public function findById($id){
		$this->db->where('id', intval($id));
		$this->db->limit(1);

                if(isCommandLineInterface() === false){
                    if (! UserHelper::getCurrentUserAttribute('super_admin')){
			$this->db->where('created_by_id', UserHelper::getCurrentUserAttribute('id'));
                    }
                }

		return $this->db->get('orders')->row();
	}

	/**
	 *
	 * @param array $data
	 */
	public function insertRow(array $data) {
//		$this->db->trans_start();
		$this->db->insert('orders', $data);
//		$this->db->trans_complete();

		return $this->db->insert_id();
	}

	/**
	 *
	 * @param array $data
	 */
	public function insertProductRow(array $data) {
//		$this->db->trans_start();
		$this->db->insert('order_products', $data);
//		$this->db->trans_complete();

		return $this->db->insert_id();
	}

	/**
	 *
	 * @param int $id
	 * @param array $data
	 */
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('orders', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function findProductsForAutocomplete($term, $country) {
		$sql = "
			SELECT
			  products.*, sizes.id as size_id, sizes.name as size_name,product_size.quantity,product_prices.price,
				product_images.filename AS imagefile,
				product_images.ext AS imagefile_ext,
                countries.currency      as currency
			FROM
			  products
			  LEFT JOIN product_size
				ON products.id = product_size.product_id

			  LEFT JOIN sizes
				ON product_size.size_id = sizes.id

			  LEFT JOIN product_prices
				ON product_prices.product_id = products.id

			  LEFT JOIN product_images
				ON product_images.product_id = products.id AND product_images.is_main = 1

			  LEFT JOIN countries
                ON countries.id = product_prices.country_id
			WHERE products.name LIKE '%$term%' OR products.code LIKE '%$term%' AND product_prices.`country_id` =$country";
		return $this->db->query($sql)->result_array();
	}

	public function getOrderProducts($id, $country_id = 1) {
		$sql = "
			SELECT order_products.*, products.name, products.code, sizes.`name` as size_name,
				product_prices.price as price,
				product_images.filename AS imagefile,
				product_images.ext AS imagefile_ext
			FROM order_products
			LEFT JOIN products ON order_products.product_id = products.id
			LEFT JOIN sizes ON order_products.size_id = sizes.id
			LEFT JOIN product_prices ON order_products.product_id = product_prices.product_id
			LEFT JOIN product_images
				ON product_images.product_id = products.id AND product_images.is_main = 1
			WHERE  order_products.order_id = $id
				AND product_prices.`country_id` = $country_id
			";

		return $this->db->query($sql)->result_array();
	}

	/**
	 *
	 * @param int $order_id
	 * @return array
	 */
	public function getComments($order_id) {
		return $this->db
			->select('orders_comments.*, user.username AS by_user')
			->from('orders_comments')
			->join('user', 'orders_comments.user_id = user.id', 'left')
			->where('orders_comments.order_id', intval($order_id))
			->order_by('id', 'DESC')
			->get()
			->result();
	}

	/**
	 *
	 * @param int $order_id
	 * @param array $data
	 */
	public function saveComment(array $data) {
		$this->db->insert('orders_comments', $data);

		return $this->db->insert_id();
	}

	/**
	 *
	 * @param int $offset
	 * @param int $limit
	 * @return array
	 */
	public function getCollection($current_page = 1, $count = false, $limit = self::ITEMS_PER_PAGE){
		//if ajax request
		if(!empty($this->input->post('term'))) {
			$term = $this->input->post('term');
		} else {
			$term = '';
		}

		//gets who is order creator
		if(!empty($this->input->post('assigned_to_id'))) {
			$assigned_to_id = $this->input->post('assigned_to_id');
		} else {
			$assigned_to_id = UserHelper::getCurrentUserAttribute('id');
		}

		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$result = array();

		$current_page = ($current_page < 1) ? 0 : $current_page;
		$offset = $current_page ? ($current_page - 1) * $limit : $current_page;

		$this->db->select('orders.*, countries.name as country_name, countries.currency');
		$this->db->select('user.name as agent_name');
		$this->db->select('facebook_pages.title as facebook_page');
		$this->db->select('facebook_pages.fb_id as facebook_page_fb_id');
		$this->db->select('count(DISTINCT orders_comments.id) as comment_count');
		$this->db->from('orders');
		$this->db->join('countries', 'orders.country_id = countries.id' ,'left');
		$this->db->join('facebook_pages', 'orders.facebook_page_id = facebook_pages.id' ,'left');

		$this->db->join('order_products','orders.id = order_products.order_id', 'union');
		$this->db->join('orders_comments', 'orders_comments.order_id = orders.id', 'left');
		$this->db->join('products','order_products.product_id = products.id', 'union');
		$this->db->join('user', 'user.id = orders.created_by_id', 'left');
/*
		if(! UserHelper::getCurrentUserAttribute('super_admin')){
			$this->db->where('created_by_id', UserHelper::getCurrentUserAttribute('id'));
		}else*/
		if(!empty($this->input->post('assigned_to_id'))){
			if($assigned_to_id != 'all_users') {
				$this->db->where('created_by_id', $assigned_to_id);
			}
		}

		$date_to = date_format(date_create($date_to),"Y-m-d 23:59:59");
		$this->db->where('orders.date_added <=', $date_to);

		if(empty($date_from)) {
			$date_from = date("Y-m-d H:i:s", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 week" ) );
		}

		$this->db->where('orders.date_added > ', $date_from);

		$this->db->group_start();
		$this->db->or_like('products.code', $term, 'both', true);
		$this->db->or_like('orders.phone', $term, 'both', true);
		$this->db->or_like('orders.customer_name', $term, 'both', true);
		$this->db->or_like('orders.city', $term, 'both', true);
		$this->db->or_like('orders.address', $term, 'both', true);
		$this->db->group_end();

		$this->db->group_by('orders.id');

		$this->db->order_by('orders.date_added', 'DESC');

		if($count){
			return $this->db->get()->num_rows();
		} else {
			$this->db->limit($limit, $offset);
			$collection = $this->db->get()->result();
//			print $this->db->last_query();
		}
		if($collection){
			$products = $this->db
				->select('order_products.*, products.name AS product, products.code AS code, sizes.name AS size')
				->from('orders')
				->join('order_products', 'order_products.order_id = orders.id', 'inner')
				->join('products', 'order_products.product_id = products.id', 'left')
				->join('sizes', 'order_products.size_id = sizes.id', 'left')
				->where_in('orders.id', array_map(create_function('$o', 'return $o->id;'), $collection))
				->get()
				->result();

			if($products){
				$order_products = array();
				foreach($products as $item){
					if(!isset($order_products[$item->order_id])){
						$order_products[$item->order_id] = array();
					}

					$order_products[$item->order_id][] = $item;
				}
			}

			foreach($collection as $row){
				$row->products = array();
				if(isset($order_products[$row->id])){
					$row->products = $order_products[$row->id];
				}
				//$row->phone = str_replace(' ', '', $row->phone);
				$row->phone_clean = preg_replace("/[^0-9]/","",$row->phone);
				$row->post_code_clean = preg_replace("/[^0-9]/","",$row->post_code);

				$result[] = $row;
			}

			unset($products);
		}

		return $result;
	}

	/**
	 *
	 * @return array
	 */
	public function getConfigForPagination($limit = self::ITEMS_PER_PAGE) {
		return array(
			'base_url' => site_url('admin/orders'),
			'total_rows' => $this->countTotalRows(),
			'per_page' => $limit,
			'uri_segment' => 3,
			'use_page_numbers' => true,
			'full_tag_open' => '<div data-role="paging"><ul class="pagination">',
			'full_tag_close' => '</ul></div>',
			'first_tag_open' => '<li class="prev page">',
			'first_tag_close' => '</li>',
			'last_tag_open' => '<li class="next page">',
			'last_tag_close' => '</li>',
			'next_tag_open' => '<li class="next page">',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li class="prev page">',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li class="active"><a href="">',
			'cur_tag_close' => '</a></li>',
			'num_tag_open' => '<li class="page">',
			'num_tag_close' => '</li>'
		);
	}

	/**
	 *
	 * @return int
	 */
	public function countTotalRows() {
		$current_page = $this->input->post('current_page'); // get from ajax call
		if($current_page === null){
			$current_page = $this->uri->segment(3);
		}

		if($current_page === null){
			$current_page = 1;
		}


		return $this->getCollection($current_page, $count = true);
	}

	public function getOrderProduct($order_id, $product_id, $size_id){
		$this->db->select();
		$this->db->from('order_products');
		$this->db->where('order_id', intval($order_id));
		$this->db->where('product_id', intval($product_id));
		$this->db->where('size_id', intval($size_id));

		return $this->db->get()->row();
	}

	public function updateOrderProductRow($order_id, $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($order_id));
		$this->db->update('order_products', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function removeOrderProduct($order_id, $product_id, $size_id) {
		$this->db->trans_start();
		$this->db->where('order_id', intval($order_id));
		$this->db->where('product_id', intval($product_id));
		$this->db->where('size_id', intval($size_id));
		$this->db->delete('order_products');
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function getProductReservedQuantity($product_id, $size_id, $is_shipped = 0){
		$this->db->select('sum(quantity) as reserved_quantity');
		$this->db->from('order_products');
		$this->db->where('product_id', $product_id);
		$this->db->where('size_id', $size_id);
		$this->db->where('is_shipped', $is_shipped);
		$sql = "select sum(quantity) as reserved_quantity
					from order_products
					left join orders as or1
						on or1.id = order_products.order_id
					where product_id = '$product_id'
						and size_id = '$size_id'
						and is_shipped = '$is_shipped'
						and or1.status != '4'";

		return $this->db->query($sql)->row();

		//return $this->db->get()->row();
	}

	public function findCustomerNamesForAutocomplete($term){
		$this->db->select('DISTINCT(orders.customer_name) as customer_name');
		$this->db->from('orders');
		$this->db->like('orders.customer_name', $term);

		return $this->db->get()->result();
	}
}
