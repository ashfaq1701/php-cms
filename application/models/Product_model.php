<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product_model
 *
 * @author chavdar
 */
class Product_model extends MY_Model {

	const ITEMS_PER_PAGE = 50;

	protected $table_name = 'products';
	protected $filter_columns = array(
		'products.name', 'products.code',
		'suppliers.name',
	);

	public function getProductCodeSeries()
	{
		$sql = "SELECT distinct left(code, 2) as series
					FROM products
					order by series
				";
		$res = $this->db->query($sql)->result_array();
		$series = [];
		foreach ($res as $ele)
		{
			$series[] = $ele['series'];
		}
		return $series;
	}

	/**
	 *
	 * @return array
	 */
	public function getCollection($current_page = 1, $expected = false, $count = false) {
		$limit = self::ITEMS_PER_PAGE;

		$current_page = ($current_page < 1) ? 0 : $current_page;

		if (strpos($_SERVER['QUERY_STRING'], '&submit=1') !== false) {
			$current_page = 1;
			// reset the pagination if search is submited now
		}

		$offset = $current_page ? ($current_page - 1) * $limit : $current_page;

		if($expected){
			$sort_column = $this->uri->segment(4);
			$order_res = $this->uri->segment(5);
		}else{
			$sort_column = $this->uri->segment(3);
			$order_res = $this->uri->segment(4);
		}

		$this->db->select('products.*, suppliers.name AS supplier');
		$this->db->select('product_images.filename AS imagefile');
		$this->db->select('product_images.ext AS imagefile_ext');


//       		$this->db->select('created_by_id as saleagent')

		$this->db->select('pp1.price AS price_bgn');
		$this->db->select('pp2.price AS price_lei');
		$this->db->select('pp3.price AS price_eur');
//		$this->db->select('pp4.price AS price_try');

		$this->db->select('ps1.quantity as quantity_xs');
		$this->db->select('ps2.quantity as quantity_s');
		$this->db->select('ps3.quantity as quantity_m');
		$this->db->select('ps4.quantity as quantity_l');
		$this->db->select('ps5.quantity as quantity_xl');
		$this->db->select('ps6.quantity as quantity_xxl');
		$this->db->select('ps7.quantity as quantity_xxxl');

		//cheap hack on combining nam and sizes description
		//so agents will be able to see it
		$this->db->select('psd1.description as desc_xs');
		$this->db->select('psd2.description as desc_s');
		$this->db->select('psd3.description as desc_m');
		$this->db->select('psd4.description as desc_l');
		$this->db->select('psd5.description as desc_xl');
		$this->db->select('psd6.description as desc_xxl');
		$this->db->select('psd7.description as desc_xxxl');


                $this->db->select('(
                    ifnull(ps1.quantity, 0) +
                    ifnull(ps2.quantity, 0) +
                    ifnull(ps3.quantity, 0) +
                    ifnull(ps4.quantity, 0) +
                    ifnull(ps5.quantity, 0) +
                    ifnull(ps6.quantity, 0) +
                    ifnull(ps7.quantity, 0)
                    ) as total_quantity');


		//$this->db->select('products.total_quantity AS total_quantity');

		$this->db->select('count(products_comments.id) as comment_count');

		$this->db->from('products');

		if($expected){
			$this->db->where('products.is_preorder', 1);
		} else {
			$this->db->where('products.is_preorder', 0);
		}

		$this->db->join('suppliers', 'products.supplier_id = suppliers.id', 'left');
		$this->db->join('product_images', 'product_images.product_id = products.id AND product_images.is_main = 1', 'left');

		$this->db->join('product_prices as pp1', 'pp1.product_id = products.id AND pp1.country_id = 1', 'left');
		$this->db->join('product_prices as pp2', 'pp2.product_id = products.id AND pp2.country_id = 2', 'left');
		$this->db->join('product_prices as pp3', 'pp3.product_id = products.id AND pp3.country_id = 3', 'left');
//		$this->db->join('product_prices as pp4', 'pp4.product_id = products.id AND pp4.country_id = 4', 'left');

		$this->db->join('product_size as ps1', 'ps1.product_id = products.id AND ps1.size_id = 1', 'left');
		$this->db->join('product_size as ps2', 'ps2.product_id = products.id AND ps2.size_id = 2', 'left');
		$this->db->join('product_size as ps3', 'ps3.product_id = products.id AND ps3.size_id = 3', 'left');
		$this->db->join('product_size as ps4', 'ps4.product_id = products.id AND ps4.size_id = 4', 'left');
		$this->db->join('product_size as ps5', 'ps5.product_id = products.id AND ps5.size_id = 5', 'left');
		$this->db->join('product_size as ps6', 'ps6.product_id = products.id AND ps6.size_id = 6', 'left');
		$this->db->join('product_size as ps7', 'ps7.product_id = products.id AND ps7.size_id = 7', 'left');

		//fragment for sizes description hack
		$this->db->join('product_size_description as psd1', 'psd1.product_id = products.id AND psd1.size_id = 1', 'left');
		$this->db->join('product_size_description as psd2', 'psd2.product_id = products.id AND psd2.size_id = 2', 'left');
		$this->db->join('product_size_description as psd3', 'psd3.product_id = products.id AND psd3.size_id = 3', 'left');
		$this->db->join('product_size_description as psd4', 'psd4.product_id = products.id AND psd4.size_id = 4', 'left');
		$this->db->join('product_size_description as psd5', 'psd5.product_id = products.id AND psd5.size_id = 5', 'left');
		$this->db->join('product_size_description as psd6', 'psd6.product_id = products.id AND psd6.size_id = 6', 'left');
		$this->db->join('product_size_description as psd7', 'psd7.product_id = products.id AND psd7.size_id = 7', 'left');
		//sizes description hack

		$this->db->join('products_comments', 'products_comments.product_id = products.id', 'left');

		if ($keyword = $this->getKeywordForSimpleFilter()) {
			$this->db->group_start();

			foreach ($this->filter_columns as $column) {
				$this->db->or_like($column, $keyword, 'both', false);
			}

			$this->db->group_end();
		}

		//sort by db queries
		$sizes_array = array("xs", "s", "m", "l", "xl", "xxl", "xxxl");

		if (in_array($sort_column, $sizes_array)){
			$this->db->order_by("quantity_".$sort_column, $order_res);
		}elseif($sort_column == 'total_quantity'){
			$this->db->order_by("total_quantity", $order_res);
		}elseif($sort_column == 'EUR'){
			$this->db->order_by("price_eur", $order_res);
		}elseif($sort_column == 'Supplier'){
			$this->db->order_by("products.supplier_id", $order_res);
		}else{
			$this->db->order_by("products.".$sort_column, $order_res);
		}
		//end of sort

		$this->db->group_by('products.id');
		if($count) {
//			var_dump($this->db->get()->num_rows());
			return $this->db->get()->num_rows();
		} else {
			$this->db->limit($limit, $offset);
//			$this->db->get()->result();
//			print_r($this->db->last_query()); exit();


			return $this->db->get()->result();
		}
	}

	public function getAllSeries(){
		$sql1 = "SELECT distinct left(code, 2) as series
					FROM products
					order by series
				";
		$res1 = $this->db->query($sql1)->result_array();
		foreach($res1 as &$r1){
			$ser = $r1['series'];
			$sql2 = "SELECT max(right(code, 3)) as cd
						from products
						where left(code, 2) = '$ser'
					";
			$r1['next'] = $this->db->query($sql2)->row()->cd + 1;
		}
		return $res1;
	}

	public function getAllImagesByCode($code){
		$sql1 = "select *
					from images
					where code = '$code'
				";
		return $this->db->query($sql1)->result();
	}

	/**
	 *
	 * @param int $id
	 */
	public function findById($id) {
		return $this->db->get_where('products', array('id' => intval($id)), $limit = 1)->row_array();
	}

	/*
	 * by Stefan
	 * returns product info, by product code
	 */
	public function findByCode($code) {
		$sql = "SELECT * FROM products WHERE code LIKE '" . $code . "' LIMIT 1";
		return $this->db->query($sql)->row_array();
	}

	/*
	 * by Stefan
	 * returns all categories
	 */
	public function getCategories(){
		$sql1 = "select *
					from product_categories";

		return $this->db->query($sql1)->result();
	}

	/**
	 * by Stefan
	 * returns total quantity of a product
	 * @param int $product_id
	 */
	public function getTotalQuantity($product_id){
		$sql1 = "select sum(quantity)
					from product_sizes
					where product_id = '$product_id'";

		return $this->db->query($sql1)->result();
	}

	/*
	 * by Stefan
	 * returns product info, by product code
	 */
	public function getAllProductsCodes(){
		$sql1 = "SELECT distinct code
					FROM products";
		return $this->db->query($sql1)->result();
	}

	/**
	 *
	 * @param array $data
	 */
	public function insertRow(array $data) {
//		$this->db->trans_start();
		$this->db->insert('products', $data);
//		$this->db->trans_complete();

//		return $this->db->trans_status();
		return $this->db->insert_id();
	}

	/**
	 *
	 * @param int $id
	 * @param array $data
	 */
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('products', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function calcTotal($product_id)
	{
		//calulate total quantity of all products

		$sql="UPDATE products
				SET total_quantity = ( SELECT SUM( product_size.quantity )
										FROM product_size
										WHERE product_size.product_id = '$product_id' )
				WHERE products.id = '$product_id'";

		return $this->db->query($sql);

	}

	/**
	 * @return array
	 */
	public function findSuppliersForSelect() {
		$result = array();
		$query = $this->db->select('id, name')->get('suppliers');

		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$result[$row['id']] = $row['name'];
			}
		}

		return $result;
	}

	public function findSupplierByName($supplier_name)
	{
		return $this->db->get_where('suppliers', array('name' => $supplier_name), $limit = 1)->row_array();
	}

	public function findSupplierById($id)
	{
		return $this->db->get_where('suppliers', array('id' => intval($id)), $limit = 1)->row_array();
	}

	public function getSizesDescription($id) {
		$this->db->select('product_size_description.description, sizes.name');
		$this->db->from('product_size_description');
		$this->db->join('sizes', 'product_size_description.size_id = sizes.id', 'left');
		$this->db->where('product_id', intval($id));

		return $this->db->get()->result();
	}

	public function insertSizeDescriptionRow($data) {
		$this->db->trans_start();
		$this->db->insert('product_size_description', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function updateSizeDescriptionRow($product_id, $size_id, $data) {
		$this->db->trans_start();
		$this->db->where('product_id', intval($product_id));
		$this->db->where('size_id', intval($size_id));
		$this->db->update('product_size_description', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}


	/**
	 *
	 * @param array $data
	 */
	public function insertSizeRow(array $data) {
		$this->db->trans_start();
		$this->db->insert('product_size', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	/**
	 *
	 * @param int $id
	 * @param array $data
	 */
	public function updateSizeRow($id, $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('product_size', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	/**
	 * @param array $data
	 */
	public function insertPriceRow(array $data) {
		$this->db->trans_start();
		$this->db->insert('product_prices', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	/**
	 *
	 * @param int $id
	 * @param array $data
	 */
	public function updatePriceRow($product_id, $country_id, array $data) {
		$this->db->trans_start();
		$this->db->where('product_id', intval($product_id));
		$this->db->where('country_id', intval($country_id));
		$this->db->update('product_prices', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function getProductSizes($id) {
		$this->db->select('product_size.*, sizes.name, product_size_description.description');
		$this->db->from('product_size');
		$this->db->join('sizes', 'product_size.size_id = sizes.id', 'left');
		$this->db->join('product_size_description',
			'product_size.size_id = product_size_description.size_id AND product_size.product_id = product_size_description.product_id', 'left');
		$this->db->where('product_size.product_id', $id);

		return $this->db->get()->result();
	}

	public function getProductPrices($id) {
		$this->db->select('product_prices.*, countries.name, countries.currency, countries.id as country_id');
		$this->db->from('product_prices');
		$this->db->join('countries', 'product_prices.country_id = countries.id', 'left');
		$this->db->where('product_id', $id);
		$this->db->order_by('countries.order', 'ASC');

		return $this->db->get()->result();
	}

	/**
	 *
	 * @param array $data
	 */
	public function insertImageRow(array $data) {
		$this->db->insert('product_images', $data);
		return $this->db->insert_id();
	}

	public function getImages($id) {
		$this->db->select('*');
		$this->db->from('product_images');
		$this->db->where('product_id', $id);
		$this->db->order_by('is_main','desc');

		return $this->db->get()->result();
	}

	/**
	 * Find Image by Image ID
	 *
	 * @param int $id
	 */
	public function findImageById($id) {
		$this->db->select('*');
		$this->db->from('product_images');
		$this->db->where('id', $id);
//		$this->db->limit(1);

		return $this->db->get()->row();
	}

	public function deleteImageById($id) {
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->delete('product_images');
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function findAllMainImagesForProduct($product_id) {
		$this->db->select('*');
		$this->db->from('product_images');
		$this->db->where('product_id', $product_id);
		$this->db->where('is_main', 1);
		return $this->db->get()->result();
	}

	public function updateMainImage($image_id, $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($image_id));
		$this->db->update('product_images', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function getMainImage($product_id) {
		$this->db->select('filename, ext');
		$this->db->from('product_images');
		$this->db->where('product_id', $product_id);
		$this->db->where('is_main', 1);
		return $this->db->get()->row();
	}

	public function updateProductSizeRow($id, $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('product_size', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}
//
//	public function updateProductSizeRowByPS($product_id, $size_id, $data) {
//		$this->db->trans_start();
////		$this->db->where('product_id', intval($product_id));
//		$this->db->where('id', intval($size_id));
//		$this->db->update('product_size', $data);
//		$this->db->trans_complete();
//
//		return $this->db->trans_status();
//	}

	public function getProductSizeRowByPS($product_id, $size_id) {
		$this->db->select('*');
		$this->db->from('product_size');
		$this->db->where('product_id', intval($product_id));
		$this->db->where('size_id', intval($size_id));
		return $this->db->get()->row();
	}

	public function getProductSizeRow($product_id, $size_id) {
		$this->db->select('*');
		$this->db->from('product_size');
		$this->db->where('product_id', intval($product_id));
		$this->db->where('size_id', intval($size_id));

		return $this->db->get()->row();
	}

	/**
	 *
	 * @return array
	 */
	public function getConfigForPagination($expected = false, $limit = self::ITEMS_PER_PAGE) {
		if($expected){
			$column = $this->uri->segment(4);
			$order = $this->uri->segment(5);
			$url = site_url('admin/products/expected'). "/$column" . "/$order";
		}else{
			$column = $this->uri->segment(3);
			$order = $this->uri->segment(4);
			$url = site_url('admin/products'). "/$column" . "/$order";
		}
		if($expected){
			$segment = 6;
		} else {
			$segment = 5;
		}

		return array(
			'base_url' => $url,
			'total_rows' => $this->countTotalRows($expected),
			'per_page' => $limit,
			'uri_segment' => $segment,
			'reuse_query_string' => TRUE,
			'use_page_numbers' => true,
			'full_tag_open' => '<div data-role="paging"><ul class="pagination">',
			'full_tag_close' => '</ul></div>',
			'first_tag_open' => '<li class="prev page">',
			'first_tag_close' => '</li>',
			'last_tag_open' => '<li class="next page">',
			'last_tag_close' => '</li>',
			'next_tag_open' => '<li class="next page">',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li class="prev page">',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li class="active"><a href="">',
			'cur_tag_close' => '</a></li>',
			'num_tag_open' => '<li class="page">',
			'num_tag_close' => '</li>'
		);
	}

	/**
	 *
	 * @return int
	 */
	public function countTotalRows($expected) {
//		var_dump($this->uri->segment(5));
		return $this->getCollection($this->uri->segment(6), $expected, true);
	}

	/**
	 *
	 * @param int $order_id
	 * @param array $data
	 */
	public function saveComment(array $data) {
		$this->db->insert('products_comments', $data);

		return $this->db->insert_id();
	}


	/**
	 *
	 * @param int $product_id
	 * @return array
	 */
	public function getComments($product_id) {
		return $this->db
			->select('products_comments.*, user.username AS by_user')
			->from('products_comments')
			->join('user', 'products_comments.user_id = user.id', 'left')
			->where('products_comments.product_id', intval($product_id))
			->order_by('id', 'DESC')
			->get()
			->result();
	}

	/**
	 * Check if code exists in database
	 *
	 * @param string $code
	 * return array
	 */
	public function checkCode($code) {
		$this->db->select();
		$this->db->from('products');
		$this->db->like('code', $code);
		return $this->db->get()->row();
	}
}
