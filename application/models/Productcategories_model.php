<?php

/**
 * Description of Productcategories_model
 * Serves ProductCategories Controller
 * Keep all DB queries here
 *
 * @author Stefan
 */
class Productcategories_model extends MY_Model {
	
	/**
	 * 
	 * @return array
	 */
	public function getCollection() {
		
		$this->db->select('*');
		$this->db->from('product_categories');
		
		return $this->db->get()->result();
	}
	
	/**
	 * 
	 * @param int $id
	 */
	public function findById($id) {
		return $this->db->get_where('product_categories', array('id' => intval($id)), $limit = 1)->row_array();
	}
	
	public function findBySlug($slug) {
		return $this->db->get_where('product_categories', array('slug' => $slug), $limit = 1)->row_array();
	}
	
	/**
	 * 
	 * @param array $data
	 */
	public function insertRow(array $data) {
		$this->db->trans_start();
		$this->db->insert('product_categories', $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	/**
	 * 
	 * @param int $id
	 * @param array $data
	 */
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('product_categories', $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	/**
	 * Returns all slugs for categories
	 * @return array
	 */
	public function getAllSlugs(){
		$sql1 = "select slug
					from product_categories";
					
		return $this->db->query($sql1)->result;
	}
}