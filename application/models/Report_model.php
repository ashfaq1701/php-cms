<?php

class Report_model extends MY_Model {
	
	public function getStoreValue(){
		$sql = "SELECT sum(ps.quantity * pp.price_stock) as store_value,
						sum(ps.quantity) as total_items_count
					from products
					left join product_prices as pp on pp.product_id = products.id
					left join product_size as ps on ps.product_id = products.id
					where pp.country_id = 1
				";
				
		return $this->db->query($sql)->result_array();
	}

	public function getRevenueBG($date_from, $date_to){
		
		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");

		$sql = "SELECT sum(total) as sum_total
					FROM orders
					left join countries as cn on orders.country_id = cn.id
					WHERE country_id = 1
						AND status = 2
						AND (orders.date_added BETWEEN '$date_from' and '$date_to')
				";
		return $this->db->query($sql)->row();
	}
	
	public function salesByCountry($date_from, $date_to){
		
		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");
		
		$sql = "SELECT cn.name as country_name,
						cn.currency as country_currency,
						count(DISTINCT orders.id) as orders_total,
						count(DISTINCT or1.id) as products_total,
						round(sum(orders.total)) as income_total
					FROM orders
					left join countries as cn on orders.country_id = cn.id
					left join order_products as or1 on orders.id = or1.order_id
					where (orders.date_added BETWEEN '$date_from' and '$date_to')
						and orders.status = '2'
					group by orders.country_id
				";
		return $this->db->query($sql)->result_array();
	}
	
	public function getSalesOrders($date_from, $date_to){
		
		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");
		
        $sql = "SELECT user.username,
					COUNT(op2.order_id ) AS orders_sold,
					COUNT(op3.order_id ) AS orders_returned,
					COUNT(op4.order_id ) AS orders_problem,
					SUM( op2.quantity ) AS products_sold,
					SUM( op2.quantity ) / COUNT(DISTINCT op2.order_id ) as products_per_order,
					SUM( op3.quantity ) AS products_returned
					FROM user, orders
					LEFT JOIN order_products AS op4 ON orders.status ='4'
						AND op4.order_id = orders.id
					LEFT JOIN order_products AS op3 ON orders.status ='3'
						AND op3.order_id = orders.id
					LEFT JOIN order_products AS op2 ON orders.status ='2'
						AND op2.order_id = orders.id
						AND orders.total > 15
					WHERE user.id = orders.created_by_id
						AND user.isactive = 1
						AND orders.date_added <= '$date_to'
						AND orders.date_added > '$date_from'
					GROUP BY user.id";
					
		return $this->db->query($sql)->result_array();
    }
	
    public function getSoldProducts($date_from, $date_to){
		
		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");
				
        $sql = "SELECT user.username,
					SUM( op2.quantity ) AS products_sold,
					SUM((SELECT COUNT(order_products.id) FROM order_products WHERE order_id = op2.order_id))  AS products_sold_wrong
					FROM user, orders
					LEFT JOIN order_products AS op2 ON orders.status ='2'
						AND op2.order_id = orders.id
						AND orders.total > 16
					WHERE user.id = orders.created_by_id
						AND user.isactive =1
						AND orders.date_added <= '$date_to'
						AND orders.date_added > '$date_from'
					GROUP BY user.id";
		
		return $this->db->query($sql)->result_array();
    }
	
    public function getSoldProductsByPage($date_from, $date_to){
		
		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");
		
		$sql = "SELECT facebook_pages.title AS title,
						SUM(order_products.quantity) AS in_order,
						SUM(CASE WHEN orders.status = '2' THEN order_products.quantity ELSE 0 END) AS sold,
						SUM(CASE WHEN orders.status = '3' THEN order_products.quantity ELSE 0 END) AS returned,
						SUM(CASE WHEN orders.status = '4' THEN order_products.quantity ELSE 0 END) AS missing
					FROM order_products
						LEFT JOIN orders on orders.id = order_products.order_id
						LEFT JOIN facebook_pages on facebook_pages.id = orders.facebook_page_id
					WHERE order_products.order_id = orders.id
						AND orders.date_added <= '$date_to'
						AND orders.date_added > '$date_from'
					GROUP BY facebook_pages.title";
		
		return $this->db->query($sql)->result_array();
    }
	
    public function getUserProfit($date_from, $date_to){
		
		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");
		
		//it doesn't work yet!!!
        $sql = "SELECT user.username,
					SUM( op2.quantity ) AS products_sold
					FROM user, orders
					LEFT JOIN order_products AS op2 ON orders.status ='2'
						AND op2.order_id = orders.id
						AND orders.total > 15
					WHERE user.id = orders.created_by_id
						AND user.isactive =1
						AND orders.date_added <= '$date_to'
						AND orders.date_added > '$date_from'
					GROUP BY user.id";
		
		return $this->db->query($sql)->result_array();
    }
	
	public function getTopProducts($date_from, $date_to){
		
		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");
		
		$sql = "SELECT products.code AS code,
						SUM(order_products.quantity) AS in_order,
						SUM(CASE WHEN orders.status = '2' THEN order_products.quantity ELSE 0 END) AS sold,
						(SUM(CASE WHEN orders.status = '2' THEN order_products.quantity ELSE 0 END))/(DATEDIFF('$date_to', '$date_from')) AS speed,
						SUM(CASE WHEN orders.status = '3' THEN order_products.quantity ELSE 0 END) AS returned,
						SUM(CASE WHEN orders.status = '4' THEN order_products.quantity ELSE 0 END) AS missing
					FROM order_products
						LEFT JOIN products on order_products.product_id = products.id
						LEFT JOIN orders on orders.id = order_products.order_id
					WHERE products.id = order_products.product_id
						AND orders.date_added <= '$date_to'
						AND orders.date_added > '$date_from'
					GROUP BY code
					ORDER BY sold DESC";
		
		return $this->db->query($sql)->result_array();
	}
}
