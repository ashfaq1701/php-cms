<?php

class Schedule_model extends MY_Model{
	public function getCollection($date_from, $date_to){
		$date_from = date("Y-m-d", strtotime($date_from));
		$date_to = date("Y-m-d", strtotime($date_to));

		$intervals = [ '8-10', '10-12', '12-14', '14-16', '16-18', '18-20', '20-22', '22-24' ];

		$dates = [
					(date('Y-m-d', strtotime($date_from))),
					(date('Y-m-d', strtotime($date_from .' +1 day'))),
					(date('Y-m-d', strtotime($date_from .' +2 day'))),
					(date('Y-m-d', strtotime($date_from .' +3 day'))),
					(date('Y-m-d', strtotime($date_from .' +4 day'))),
					(date('Y-m-d', strtotime($date_from .' +5 day'))),
					(date('Y-m-d', strtotime($date_from .' +6 day')))
				];
		
		$sql1 = "select *
					from schedule
					where (date between '$date_from' and '$date_to')
					";
		$res1 = $this->db->query($sql1)->result_array();

		$schedule = array();
		
		$schedule[0][0] = 'Hours';
		$i = 1;
		foreach($intervals as $interval)
			$schedule[$i++][0] = $interval;
		$i = 1;
		foreach($dates as $date)
			$schedule[0][$i++] = $date;
		foreach($res1 as $row){
			$i = 1;
			foreach($dates as $date){
				if($date == $row['date']){
					$schedule[$row['hours']][$i][] = $row['user_id'];
				}
				$i ++;
			}
		}
		for($i = 1; $i <= 8; $i ++)
			for($j = 0; $j < 8; $j++)
				if(empty($schedule[$i][$j]))
					$schedule[$i][$j] = 'empty';
		foreach($schedule as &$sch){
			ksort($sch);
		}
		/*echo "<pre>";
		var_dump($dates);
		echo "</pre>";*/

		return $schedule;
	}

	public function getUserSchedule($user_id){
		if($this->user_model->findbyid($user_id)){
			$sql1 = "SELECT date,
							(select case
								when schedule.hours = '1' then '8-10'
								when schedule.hours = '2' then '10-12'
								when schedule.hours = '3' then '12-14'
								when schedule.hours = '4' then '14-16'
								when schedule.hours = '5' then '16-18'
								when schedule.hours = '6' then '18-20'
								when schedule.hours = '7' then '20-22'
								when schedule.hours = '8' then '22-24'
							end) as hours
						FROM schedule
						where user_id = '$user_id'
							and date > NOW()
						order by date, hours";
			return $this->db->query($sql1)->result_array();
		}
	}

	public function resetConfirm(){
		$sql = "update user
					set schedule_confirmed = 0
				";
		$this->db->query($sql);
	}
}