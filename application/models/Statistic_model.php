<?php

class Statistic_model extends MY_Model {

	/*
	 * by Stefan
	 * gets all users sales data for all time
	 * returns onject/array
	 */
    public function getUserProfitTotal($date_from, $date_to){

		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");

		$sql = "SELECT
					us1.id as user_id,
					status,
					us1.username as username,
					count(distinct orders.id) as orders_total,
					sum(op1.quantity) as products_total
				FROM orders
				left join order_products as op1
					on orders.id = op1.order_id
				left join user as us1
					on us1.id = orders.created_by_id
				WHERE date_added between '$date_from' and '$date_to'
				group by  created_by_id, status
				";
		//$sql1 by Danny
		$sql1 ="SELECT us1.id as user_id,
						status,
						us1.username as username,
						count(distinct orders.id) as orders_total,
						round(sum(
                            CASE 	when op1.product_id in (select id from products
											where category = 'men-t-shirts'
												or category = 'women-t-shirts'
												or (orders.total/(orders.total + orders.discount)) < 0.7)
									then op1.quantity/2

                            		when op1.product_id in
                                        (select id from products where code REGEXP 'PR...')
                                    then op1.quantity = 0 else op1.quantity end)) as products_total
                FROM orders
				left join order_products as op1
					on orders.id = op1.order_id
				left join user as us1
					on us1.id = orders.created_by_id
				WHERE date_added between '$date_from' and '$date_to'
				group by created_by_id, status
				";

		return $this->db->query($sql1)->result_array();
    }

    public function getWages($date_from, $date_to){

		$date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $date_to = date_format(date_create($date_to), "Y-m-d 23:59:59");

        $date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
        $sql = "SELECT user.username,
					COUNT(DISTINCT orders.id) AS orders,
					SUM( op2.quantity ) AS products_sold,
					SUM((SELECT COUNT(order_products.id) FROM order_products WHERE order_id = op2.order_id))  AS products_sold_wrong
					FROM user, orders
					LEFT JOIN order_products AS op2 ON orders.status ='2'
						AND op2.order_id = orders.id
						AND orders.total > 16
					WHERE user.id = orders.created_by_id
						AND user.isactive =1
						AND orders.date_added <= '$date_to'
						AND orders.date_added > '$date_from'
					GROUP BY user.id";

        return $this->db->query($sql)->result_array();
    }

    public function getStatsBGN() {
        $date_from = $this->input->post('from_date');
        $date_to = $this->input->post('to_date');
        $date_to = date_format(date_create($date_to),"Y-m-d 23:59:59");
        if($date_from == '') {
			$date_from = '0000-00-00 00:00:00';
        }

        $date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
//        var_dump();
        $sql = "
			SELECT id,
				SUM(total) as incomebg
				FROM `orders`
				WHERE status = '2' AND country_id = '1'
                AND `orders`.`date_added` <= '$date_to'
                AND `orders`.`date_added` > '$date_from'
				GROUP BY id";

			/*SELECT * FROM orders
				SUM total
				where status = 2
            SELECT  orders.id AS order_id
                SUM((SELECT COUNT(*) FROM orders WHERE orders.id = order_products.order_id))  AS products_saled

                FROM `user`
                LEFT JOIN orders ON user.id = orders.`created_by_id`
                LEFT JOIN order_products ON orders.id = order_products.`order_id`
                WHERE isActive = 1 AND orders.`status` = 2
                AND `orders`.`date_added` <= '$date_to'
                AND `orders`.`date_added` > '$date_from'
                GROUP BY user.id";
        */
		return $this->db->query($sql)->result_array();
    }

    public function getStatsEUR() {
        $date_from = $this->input->post('from_date');
        $date_to = $this->input->post('to_date');
        $date_to = date_format(date_create($date_to),"Y-m-d 23:59:59");
        if($date_from == '') {
			$date_from = '0000-00-00 00:00:00';
        }

        $date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
//        var_dump();
        $sql = "
			SELECT id,
				SUM(total) as incomeeu
				FROM `orders`
				WHERE status = '2' AND country_id = '3'
                AND `orders`.`date_added` <= '$date_to'
                AND `orders`.`date_added` > '$date_from'
				GROUP BY id";

			/*SELECT * FROM orders
				SUM total
				where status = 2
            SELECT  orders.id AS order_id
                SUM((SELECT COUNT(*) FROM orders WHERE orders.id = order_products.order_id))  AS products_saled

                FROM `user`
                LEFT JOIN orders ON user.id = orders.`created_by_id`
                LEFT JOIN order_products ON orders.id = order_products.`order_id`
                WHERE isActive = 1 AND orders.`status` = 2
                AND `orders`.`date_added` <= '$date_to'
                AND `orders`.`date_added` > '$date_from'
                GROUP BY user.id";
        */
		return $this->db->query($sql)->result_array();
    }

    public function getStatsLEI() {
        $date_from = $this->input->post('from_date');
        $date_to = $this->input->post('to_date');
        $date_to = date_format(date_create($date_to),"Y-m-d 23:59:59");
        if($date_from == '') {
			$date_from = '0000-00-00 00:00:00';
        }

        $date_from = date_format(date_create($date_from), "Y-m-d 00:00:00");
//        var_dump();
        $sql = "
			SELECT id,
				SUM(total) as incomero
				FROM `orders`
				WHERE status = '2' AND country_id = '2'
                AND `orders`.`date_added` <= '$date_to'
                AND `orders`.`date_added` > '$date_from'
				GROUP BY id";

			/*SELECT * FROM orders
				SUM total
				where status = 2
            SELECT  orders.id AS order_id
                SUM((SELECT COUNT(*) FROM orders WHERE orders.id = order_products.order_id))  AS products_saled

                FROM `user`
                LEFT JOIN orders ON user.id = orders.`created_by_id`
                LEFT JOIN order_products ON orders.id = order_products.`order_id`
                WHERE isActive = 1 AND orders.`status` = 2
                AND `orders`.`date_added` <= '$date_to'
                AND `orders`.`date_added` > '$date_from'
                GROUP BY user.id";
        */
		return $this->db->query($sql)->result_array();
    }

}
