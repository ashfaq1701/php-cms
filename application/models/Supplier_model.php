<?php

class Supplier_model extends MY_Model {
	
	protected $table_name = 'suppliers';
	protected $filter_columns = array(
		'name', 'email', 'phone'
	);

	public function get_all() {
		return $this->getResultWithSimpleFilter();
	}
	
	/**
	 * 
	 * @param int $id
	 */
	public function findById($id) {
		return $this->db->get_where($this->table_name, array('id' => intval($id)), $limit = 1)->row_array();
	}
	
	/**
	 * 
	 * @param array $data
	 */
	public function insertRow(array $data) {
		$this->db->trans_start();
		$this->db->insert($this->table_name, $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	/**
	 * 
	 * @param int $id
	 * @param array $data
	 */
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update($this->table_name, $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

}
