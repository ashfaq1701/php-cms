<?php

class Todo_model extends MY_Model {
	
	protected $table_name = 'todo_list';
	protected $filter_columns = array('title', 'username');
	
	/**
	 * 
	 * @return array
	 */
	public function getCollection() {
		$this->db->select('todo_list.*, user.username AS assigned_to');
		$this->db->from($this->table_name);
		$this->db->join('user', 'todo_list.assigned_to_id = user.id', 'left');
		
		if ($keyword = $this->getKeywordForSimpleFilter()) {
			foreach ($this->filter_columns as $column) {
				$this->db->or_like($column, $keyword, 'both', false);
			}
		}
		
		return $this->db->get()->result();
	}
	
	/**
	 * 
	 * @param int $id
	 */
	public function findById($id) {
		return $this->db->get_where($this->table_name, array('id' => intval($id)), $limit = 1)->row_array();
	}
	
	/**
	 * 
	 * @param array $data
	 */
	public function insertRow(array $data) {
		$this->db->trans_start();
		$this->db->insert($this->table_name, $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	/**
	 * 
	 * @param int $id
	 * @param array $data
	 */
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update($this->table_name, $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	public function deleteRow($id_todo)
	{
        $this->db->trans_start();
        $this->db->where('id', $id_todo);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
		
        return $this->db->trans_status();
	}
}
