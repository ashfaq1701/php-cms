<?php

class User_model extends MY_Model {
	
	protected $table_name = 'user';
	protected $filter_columns = array('username', 'email');
	
	/**
	 * 
	 * @return array
	 */
	public function getCollection() {
		$this->db->select('user.*, user_groups.title AS group_title');
		$this->db->from($this->table_name);
		$this->db->join('user_groups', 'user.group_id = user_groups.id', 'left');
		$this->db->where('user_groups.is_hidden = 0', null, false);
		
		if ($keyword = $this->getKeywordForSimpleFilter()) {
			$this->db->group_start();
			
			foreach ($this->filter_columns as $column) {
				$this->db->or_like($column, $keyword, 'both', false);
			}
			
			$this->db->group_end();
		}
		
		return $this->db->get()->result();
	}

	public function getAllUsersFBIDs(){
		$sql = "select *
					from user
					where fb_id is not NULL
				";
		return $this->db->query($sql)->result_array();
	}
	
	/**
	 * 
	 * @param int $id
	 */
	public function findById($id) {
		return $this->db->get_where($this->table_name, array('id' => intval($id)), $limit = 1)->row();
	}
	
	/**
	 * 
	 * @param array $data
	 */
	public function insertRow(array $data) {
		$this->db->trans_start();
		$this->db->insert($this->table_name, $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	/**
	 * 
	 * @param int $id
	 * @param array $data
	 */
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update($this->table_name, $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	/**
	 * 
	 * @param string $username
	 * @param string $password
	 * @return 
	 */
	public function findUserForLogin($username, $password) {
		$user = $this->db->select('user.*, user_groups.identifier, user_groups.permissions')
			->from($this->table_name)
			->join('user_groups', 'user.group_id = user_groups.id', 'left')
			->where('username', filter_var($username, FILTER_SANITIZE_STRING))
			->where('isActive', 1)
			->limit(1)
			->get()
			->row();
		
		$return = array();
		
		if ($user && password_verify($password, $user->password)) {
			$return = array(
				'id' => $user->id,
				'username' => $user->username,
                                'name' => $user->name,
				'super_admin' => boolval($user->is_super),
				'group_id' => $user->group_id,
				'group_identifier' => $user->identifier,
				'group_permissions' => $user->permissions
			);
		}
		
		return $return;
	}
	
	public function findUsersForSelect() {
		$result = array();
		
		$query = $this->db->select('user.id, user.username')
			->from($this->table_name)
			->join('user_groups', 'user.group_id = user_groups.id', 'left')
			->order_by("user.username", "asc")
			->where('user_groups.is_hidden', 0)
			->get()
			->result();
		
		if ($query) {
			foreach ($query as $row) {
				$result[$row->id] = $row->username;
			}
		}
		
		return $result;
	}
	
	/**
	 * @return array
	 */
	public function findGroupsForSelect() {
		$result = array();
		$query = $this->db->select('id, title')
			->where('is_hidden', 0)
			->get('user_groups');
		
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$result[$row['id']] = $row['title'];
			}
		}
		
		return $result;
	}
	
	/**
	 * @param int $user_id
	 * @return array
	 */
	public function getCurrentUserOrders($user_id) {
		return $this->db->select('orders.*, countries.name AS country_name')
			->from('orders')
			->join('countries', 'orders.country_id = countries.id', 'left')
			->where('created_by_id', filter_var($user_id, FILTER_SANITIZE_NUMBER_INT))
			->order_by('id', 'DESC')
			->limit(15)
			->get()
			->result();
	}
	
	/**
	 * @param int $user_id
	 * @return array
	 */
	public function getCurrentUserTodoList($user_id) {
		return $this->db->select('todo_list.*')
			->from('todo_list')
			->where('assigned_to_id', filter_var($user_id, FILTER_SANITIZE_NUMBER_INT))
			->order_by('id', 'DESC')
			->limit(15)
			->get()
			->result();
	}
	
	public function getStaffGroupUsers() {
		$data = $this->db->select('user.id, user.username')
			->from('user')
			->join('user_groups', 'user.group_id = user_groups.id', 'left')
			->where('isActive', 1)
			->order_by('id', 'DESC')
			->get()
			->result();
		
		$response = array();
		foreach($data as $user)  {
			$response[$user->id] = $user->username;
		}
		return  $data;
	}
	
	public function deleteUser($user_id)
	{
        $this->db->trans_start();
        $this->db->where('id', $user_id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
		
        return $this->db->trans_status();
	}
}
