<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vouchers_model extends MY_Model{
	
	public function updateUseVoucherByCode($code){
		$voucher = $this->getVoucherByCode($code);
		$sql = "update vouchers
					set used = '1'
					where code = '$code'";
		$this->db->query($sql);
	}
	
	public function getAllVouchers(){
		$sql = "select 	vouchers.*,
						vt.discount as discount,
						vt.name as type,
						vt.min_amount as min_amount,
						vt.discount_type as discount_type
					from vouchers
					left join vouchers_types as vt on vouchers.type_id = vt.id
					order by vouchers.id DESC
				";
		$vouchers = $this->db->query($sql)->result_array();
		return $vouchers;
	}
	
	public function getVoucherByCode($code){
		$sql = "select vouchers.*,
						vt.*
					from vouchers
					left join vouchers_types as vt on vouchers.type_id = vt.id
					where vouchers.code = '$code'
					";
		$vouchers = $this->db->query($sql)->row_array();
		return $vouchers;
	}
	
	public function insertRow(array $data){
		$this->db->trans_start();
		$this->db->insert('vouchers', $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	public function getActiveFacebookPages(){
		$sql = "select *
					from facebook_pages
					where active = '1'
						and fb_id is not null
					order by title
				";
		$facebook_pages = $this->db->query($sql)->result_array();
		return $facebook_pages;
	}
	
	public function getVouchersTypes(){
		$sql = "select *
					from vouchers_types
					order by id ASC
				";
		$vouchers_types = $this->db->query($sql)->result_array();
		return $vouchers_types;
	}
	
	public function getRandomCode(){
		$sql = "SELECT FLOOR(RAND() * 99999) AS rnd_code
					FROM vouchers 
					WHERE 'rnd_code' NOT IN (SELECT code FROM vouchers)
					LIMIT 1
				";
		$code = $this->db->query($sql)->row();
		if(!$code->rnd_code){
			$code->rnd_code = '12345';
		}
		return $code;
	}
	
	public function updateRow($id, array $data) {
		$this->db->trans_start();
		$this->db->where('id', intval($id));
		$this->db->update('vouchers', $data);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
}