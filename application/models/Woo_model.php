<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	require_once(APPPATH.'vendor/WooCommerce/Client.php');
	require_once(APPPATH.'vendor/WooCommerce/HttpClient/BasicAuth.php');
	require_once(APPPATH.'vendor/WooCommerce/HttpClient/HttpClient.php');
	require_once(APPPATH.'vendor/WooCommerce/HttpClient/HttpClientException.php');
	require_once(APPPATH.'vendor/WooCommerce/HttpClient/OAuth.php');
	require_once(APPPATH.'vendor/WooCommerce/HttpClient/Options.php');
	require_once(APPPATH.'vendor/WooCommerce/HttpClient/Request.php');
	require_once(APPPATH.'vendor/WooCommerce/HttpClient/Response.php');

	use Automattic\WooCommerce\Client;
	
class Woo_model extends MY_Model{
	
	/*
	 * by Stefan
	 * Returns ready to use $woocommerce client
	 */
	public function wooInit(){
		$consumer_key = 'ck_91d74f64055d69c05d1998c2a66d48be65c76dab';
		$consumer_secret = 'cs_2718db527ead5baeebc1fef07f89320f720df39e';
		$store_url = 'https://www.paradiseshop.eu/';

		$woocommerce = new Client(
			$store_url, // Your store URL
			$consumer_key, // Your consumer key
			$consumer_secret, // Your consumer secret
			[
				'wp_api' => true, // Enable the WP REST API integration
				'version' => 'wc/v2', // WooCommerce WP REST API version
				'query_string_auth' => true // Force Basic Authentication as query string true and using under HTTPS
			]
		);
		
		return $woocommerce;
	}
	
	/*
	 * by Stefan
	 * Inserts Coupon in Woocommerce
	 * And Inserts Woo ID in CMS DB
	 */
	public function createCoupon($db_id, array $data){
		$woocommerce = $this->wooInit();
		//insert the coupon in Woocommerce
		$result = $woocommerce->post('coupons', $data);
		//insert to DB
		$woo_id = $result['id'];
		$sql = "UPDATE vouchers
					SET woo_id = '$woo_id'
					where id = '$db_id'";
		$this->db->query($sql);
		//return $result;
	}
	
	/*
	 * by Stefan
	 * Updates Coupon in Woocommerce
	 * And Updates Woo ID in CMS DB
	 */
	public function updateCoupon($db_id, $woo_id, array $data){
		$woocommerce = $this->wooInit();
		//insert the coupon in Woocommerce
		$result = $woocommerce->put('coupons/'.$woo_id, $data);
		$woo_id = $result['id'];
		$sql = "UPDATE vouchers
					SET woo_id = '$woo_id'
					where id = '$db_id'";
		$this->db->query($sql);
		return $result;
	}
	
	/*
	 * by Stefan
	 * Deletes Coupon from Woocommerce
	 */
	public function deleteCoupon($woo_id, $db_id){
		$woocommerce = $this->wooInit();
		//insert the coupon in Woocommerce
		$result = $woocommerce->delete('coupons/'.$woo_id, ['force' => true]);
		$sql = "UPDATE vouchers
					SET woo_id = NULL
					where id = '$db_id'";
		$this->db->query($sql);
		return $result;
	}
	
	/*
	 * by Stefan
	 * Finds if we have uploaded that code to the website
	 * and returns woocommerce id
	 * it will be changed with a handle in our DB
	 */
	public function findWooProductByWooID($code){
		$woocommerce = $this->wooInit();
		$product = $this->product_model->findByCode($code);
		if($product == null)
			return null;
		//cicle through all pages of returned products
		//search woo list for chosen product code
		$i = 1;
		$n = 0;
		$products = array();
		$woo_id = null;
		do{
			$wooproducts = $woocommerce->get('products', array('page' => $i));
			if($wooproducts == null)
				break;
			$i++;
			foreach($wooproducts as $wooproduct){
				$products[$n]['code'] = $wooproduct['sku'];
				$products[$n]['woo_id'] = $wooproduct['id'];
				$categories = $wooproduct['categories'];
				$products[$n]['categories'] = '';
				foreach($categories as $category){
					$products[$n]['categories'] .= $category['id'].',';
				}
				$products[$n]['categories'] = rtrim($products[$n]['categories'], ',');
				$n++;
			}
		}while(!($wooproducts == null));
		foreach($products as $wcp){
			if(strtoupper($wcp['code']) == strtoupper($product['code'])){
				$woo_id = $wcp['woo_id'];
			}
		}
		return $woo_id;
	}
}
