<!DOCTYPE html>
<html>
<head>
<!-- Bootstrap theme link -->
<link rel="stylesheet" href="https://bootswatch.com/flatly/bootstrap.min.css"></link>
<!-- DateTime picker links part 1  (part 2 after the body) -->
<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"/> -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/plugins/jquery/autofap/jquery-ui.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('assets/admin/plugins/jquery-datetimepicker/build/jquery.datetimepicker.min.css'); ?>">


<!-- <script src="http://code.jquery.com/jquery-1.9.1.js"></script> -->
<script src="<?php echo base_url('assets/admin/plugins/jquery/autofap/jquery-1.9.1.js'); ?>"></script>
<!-- <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> -->
<script src="<?php echo base_url('assets/admin/plugins/jquery/autofap/jquery-ui.js'); ?>"></script>
</head>

<body>
<?php echo validation_errors(); 
//echo "<br/>" . base_url('assets/admin/plugins/jquery-datetimepicker/build/jquery.datetimepicker.min.css') . "<br/>";
//echo "<br/>" . base_url('assets/admin/plugins/jquery-ui/jquery-ui.min.js');
//echo "<br/>" . base_url('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
//echo "<br/>" . base_url('assets/admin/plugins/jquery-datetimepicker/build/jquery.datetimepicker.full.js');
	
?>

<div style="max-width: 1600px; margin: 0 auto; padding: 10px;">
	<div class="container-fluid">
		<div class="row" style="height: 350px">	
			<div class="col-sm-3">
			<?php 
				$attributes = array(); // ne e testwano!
				echo form_open('admin/facebook/auto_fap', $attributes); 
			//form helper must be loaded  to open the form with the CI function!
			//(in autoload.php find $autoload['helper'] = array(); and add 'form')
			?>
				<?php
					//Products multiselect
					echo form_label('Products:');
					$products_attributes = array(
												'class'    => "form-control",
												'style'    => "height: 100%",
												//'name'     => "sel_products[]",
												//'multiple' => "multiple"
												);
					echo form_multiselect('sel_products[]', $products_list, '', $products_attributes);
				?>
			</div>
			<div class="col-sm-3">
				<?php
					//Facebook pages multiselect
					echo form_label('Facebook pages:');
					$fb_pages_attributes = array(
												'class' =>  "form-control",
												'style' => 	"height: 100%"
												);
					echo form_multiselect('sel_fb_pages[]', $fb_pages_list, '', $fb_pages_attributes);
				?>
			</div>
			<div class="col-sm-3">
				<?php
					//Descriptions multiselect
					echo form_label('Descriptions:');
					$description_attributes = array(
												'class' =>  "form-control",
												'style' => 	"height: 100%",
												);
					echo form_multiselect('sel_descriptions[]', $description_list, '', $description_attributes);
				?> 
			</div>
			<div class="col-sm-3">
				<?php
					//Datetime input
					echo form_label('Autoposting Start:');
					$start_time_attributes = array(
												'type'  =>  "time",
												'name'  =>  "startdatetime",
												'value' =>  set_value('startdatetime'),
												'id' 	=>  "datetimepicker",											
												'class' =>  "form-control datetimepicker",
												'style' => 	"height: 20%",												
												);
					echo form_input($start_time_attributes);
				?>  
			</div>
			<div class="col-sm-3">
				<?php
					//Time interval select					
					//$time_interval_list = array(5,10,15,20,25,30,45,60,90,120,180);
					echo form_label('Time interval between posts:');
					$description_attributes = array(
												'class' =>  "form-control",
												'style' => 	"height: 70%",
												);
					echo form_multiselect('timeinterval', $time_interval_list, '', $description_attributes);
				?> 
			</div>
		</div>
		<div class="row">	
			<div class="col-sm-12">
				<?php
				//Submit button
				$data = array(
						//'name'          => 'mysubmit',
						//'id'            => 'button',
						//'value'         => 'true',
						'type'          => 'submit',
						//'content'       => 'Reset'
						'class' 		=>'btn btn-primary form-control',
				);
				echo form_button($data, 'Generate posts');
				?>
			</div>	
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

</body>

<!--  Datetimepicker includes  -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/plugins/jquery-datetimepicker/build/jquery.datetimepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/jquery-datetimepicker/build/jquery.datetimepicker.full.js'); ?>"></script>

<script type="text/javascript">
	$(function() {
		jQuery.datetimepicker.setLocale('en');
		jQuery('.datetimepicker').datetimepicker({
			format:'Y-m-d H:i:s'
		});
	});
</script>

</html>