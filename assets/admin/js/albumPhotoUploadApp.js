Dropzone.autoDiscover = false;
var app = angular.module("albumPhotoUploadApp", ["checklist-model", "thatisuday.dropzone", 'ui.bootstrap', 'ngRoute', 'angularFileUpload'], function($interpolateProvider){
	$interpolateProvider.startSymbol('[[').endSymbol(']]');
});
