app.controller('albumPhotoUploadCtrl', function($scope, AlbumPhotoUploadService, $uibModal, AlbumPhotoUploadFactory, FileUploader) {
  $scope.pages = [];
  $scope.countries = [];
  $scope.brands = [
    {
      id: 0,
      name: 'Select'
    }
  ];
  $scope.genders = [
    { name: 'Female', value: 'F' },
    { name: 'Male', value: 'M' },
    { name: 'Neutral', value: 'N' }
  ];
  $scope.uploadFactory = AlbumPhotoUploadFactory;
  $scope.description = '😇{$price}{$currency}\n'+
'👉{$sizes}\n'+
'⭐{$Product testing}';
  $scope.albumCategories = [];
  $scope.selectedAlbumCategories = [];
  $scope.filters = {
    genders: [],
    countries: []
  };
  $scope.selectedPages = [];
  $scope.allCountries = false;
  $scope.files = [];
  $scope.isBranded = false;
  $scope.overlayProductCode = false;
  $scope.isNewBrand = 'no';

  $scope.brandObj = {
    fullName: '',
    shortName: '',
    abbreviation: '',
    brand: '',
    brandImage: ''
  };

	$scope.dzOptions = {
		url : '/admin/facebook/image/upload',
		paramName : 'photo',
		maxFilesize : '10',
		acceptedFiles : 'image/jpeg, images/jpg, image/png',
		addRemoveLinks : true,
	};

  $scope.brandImageUploader = new FileUploader ({
    url: '/admin/images/brand/upload'
  });

  $scope.brandImageUploader.onAfterAddingFile = function() {
    $scope.brandImageUploader.uploadAll();
  };

  $scope.brandImageUploader.onSuccessItem = function(item, response) {
    $scope.brandObj.brandImage = response.filename;
  };

	$scope.dzCallbacks = {
		'addedfile' : function(file){
			//console.log(file);
			$scope.newFile = file;
		},
		'success' : function(file, xhr){
      //console.log(file, xhr);
		}
	};

	$scope.dzMethods = {};

  var openModal = function() {
    $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'processing.html',
      controller: 'ProcessingModalInstanceCtrl',
      controllerAs: '$ctrl'
    });
  }

  var refreshBrands = function(brandId) {
    brandId = brandId || 0;
    AlbumPhotoUploadService.getBrandList().then(function(resp) {
      $scope.brands.splice(0, $scope.brands.length);
      $scope.brands.push({
        id: 0,
        name: 'Select'
      });
      for(var i = 0; i < resp.data.length; i++)
      {
        $scope.brands.push({
          id: resp.data[i].id,
          name: resp.data[i].full_name
        });
      }
      if (brandId != 0) {
        $scope.brandObj.brand = brandId;
      }
    });
  }

  $scope.saveBrand = function() {
    AlbumPhotoUploadService.saveBrand($scope.brandObj.fullName, $scope.brandObj.shortName, $scope.brandObj.abbreviation, $scope.brandObj.brandImage).then(function(resp) {
      refreshBrands(resp.data.brand_id);
    });
  }

  $scope.submitPhotos = function()
  {
    var fileNames = [];
    var fileObjs = $scope.dzMethods.getAllFiles();
    for(var i = 0; i < fileObjs.length; i++)
    {
      var fileObj = fileObjs[i];
      fileNames.push(fileObj.name);
    }
    $scope.uploadFactory.uploadProcessing = true;
    openModal();
    AlbumPhotoUploadService.postToFacebookPages($scope.selectedPages, $scope.selectedAlbumCategories, $scope.description, fileNames, $scope.isBranded, $scope.overlayProductCode, $scope.brandObj.brand).then(function(resp)
    {
      $scope.uploadFactory.uploadProcessing = false;
    });
  }

  $scope.selectAllCountries = function()
  {
    $scope.filters.countries.splice(0, $scope.filters.countries.length);
    if($scope.allCountries == true)
    {
      for(var i = 0; i < $scope.countries.length; i++)
      {
        $scope.filters.countries.push($scope.countries[i]);
      }
    }
  }

  $scope.updateFilters = function()
  {
    var newSelectedPages = [];
    for(var i = 0; i < $scope.filters.genders.length; i++)
    {
      var g = $scope.filters.genders[i];
      for(var j = 0; j < $scope.filters.countries.length; j++)
      {
        var c = $scope.filters.countries[j];
        var currentPages = $scope.pages[g][c];
        if(currentPages != null)
        {
          for(var k = 0; k < currentPages.length; k++)
          {
            var currentPage = currentPages[k];
            newSelectedPages.push(currentPage.id);
          }
        }
      }
    }
    $scope.selectedPages.splice(0, $scope.selectedPages.length);
    for(var m = 0; m < newSelectedPages.length; m++)
    {
      $scope.selectedPages.push(newSelectedPages[m]);
    }
    console.log($scope.selectedPages);
  }

  var initialize = function()
  {
    AlbumPhotoUploadService.getPages().then(function(resp)
    {
      $scope.pages = resp.data;
      for (var g in $scope.pages)
      {
        if ($scope.pages.hasOwnProperty(g)) {
          var items = $scope.pages[g];
          for(var c in items)
          {
            if(items.hasOwnProperty(c))
            {
              if(c == '')
              {
                var entries = $scope.pages[g][c];
                delete $scope.pages[g][c];
                c = 'Unspecified';
                $scope.pages[g][c] = entries;
              }
              if($scope.countries.indexOf(c) == -1)
              {
                $scope.countries.push(c);
              }
            }
          }
        }
      }
      refreshBrands();
      $scope.countries.sort();
    });
    AlbumPhotoUploadService.getAlbumCategories().then(function(resp)
    {
      $scope.albumCategories = resp.data;
    });
  }
  initialize();
});
