app.controller('managePhotosCtrl', function($scope, $compile, ManagePhotosService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder) {
  $scope.page = 0;
  $scope.album = 0;
  $scope.batch = 0;
  $scope.pageOptions = [
    {
      id: 0,
      title: 'Select'
    }
  ];
  $scope.albumOptions = [
    {
      id: 0,
      title: 'Select'
    }
  ];
  $scope.batchOptions = [
    {
      id: 0,
      title: 'Select'
    }
  ];
  $scope.dtColumnDefs = [
    DTColumnBuilder.newColumn('page').withTitle('Page'),
    DTColumnBuilder.newColumn('photo_description').withTitle('Description'),
    DTColumnBuilder.newColumn('product').withTitle('product'),
    DTColumnBuilder.newColumn('thumbnail').withTitle('Thumbnail'),
    DTColumnBuilder.newColumn('upload_date').withTitle('Upload Date'),
    DTColumnBuilder.newColumn('uploaded_by').withTitle('Uploaded By'),
    DTColumnBuilder.newColumn('batch_no').withTitle('Batch No'),
    DTColumnBuilder.newColumn('photo_id').withTitle('Actions').renderWith(function(data, type, full, meta) {
      return '<button class="btn btn-danger btn-xs" ng-click="deletePhoto('+data+')">Delete</button>';
    })
  ];
  $scope.dtInstance = {};
  $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
    url: '/admin/facebook/photos/search',
    type: 'GET',
    dataSrc: 'data',
  })
  .withOption('fnServerParams', function (aoData) {
    aoData.page = $scope.page;
    aoData.album = $scope.album;
    aoData.batch = $scope.batch;
  })
  .withOption('createdRow', function(row) {
    $compile(angular.element(row).contents())($scope);
  })
  .withOption('processing', true)
  .withOption('serverSide', true)
  .withPaginationType('full_numbers');

  $scope.getAlbums = function()
  {
    $scope.album = 0;
    if($scope.page != 0)
    {
      ManagePhotosService.getAlbums($scope.page).then(function(resp)
      {
        $scope.albumOptions.splice(0, $scope.albumOptions.length);
        $scope.albumOptions.push({
          id: 0,
          title: 'Select'
        });
        for(var i = 0; i < resp.data.length; i++)
        {
          $scope.albumOptions.push(resp.data[i]);
        }
      });
    }
    else
    {
      $scope.albumOptions.splice(0, $scope.albumOptions.length);
      $scope.albumOptions.push({
        id: 0,
        title: 'Select'
      });
    }
    ManagePhotosService.getBatchNo($scope.page, $scope.album).then(function(resp)
    {
      $scope.batchOptions.splice(0, $scope.batchOptions.length);
      $scope.batchOptions.push({
        id: 0,
        title: 'Select'
      });
      for(var i = 0; i < resp.data.length; i++) {
        $scope.batchOptions.push({
          id: resp.data[i],
          title: resp.data[i]
        });
      }
      $scope.dtInstance.rerender();
    });
    $scope.dtInstance.rerender();
  };

  $scope.albumSelected = function()
  {
    ManagePhotosService.getBatchNo($scope.page, $scope.album).then(function(resp)
    {
      $scope.batchOptions.splice(0, $scope.batchOptions.length);
      $scope.batchOptions.push({
        id: 0,
        title: 'Select'
      });
      for(var i = 0; i < resp.data.length; i++) {
        $scope.batchOptions.push({
          id: resp.data[i],
          title: resp.data[i]
        });
      }
      $scope.dtInstance.rerender();
    });
  };

  $scope.batchSelected = function()
  {
    $scope.dtInstance.rerender();
  }

  $scope.deletePhoto = function(photoId)
  {
    ManagePhotosService.deletePhoto(photoId).then(function(){
      $scope.dtInstance.rerender();
    });
  }

  var initialize = function()
  {
    ManagePhotosService.getPages().then(function(resp)
    {
      $scope.pageOptions.splice(0, $scope.pageOptions.length);
      $scope.pageOptions.push({
        id: 0,
        title: 'Select'
      });
      for(var i = 0; i < resp.data.length; i++)
      {
        $scope.pageOptions.push(resp.data[i]);
      }
    });
    ManagePhotosService.getBatchNo($scope.page, $scope.album).then(function(resp)
    {
      $scope.batchOptions.splice(0, $scope.batchOptions.length);
      $scope.batchOptions.push({
        id: 0,
        title: 'Select'
      });
      for(var i = 0; i < resp.data.length; i++) {
        $scope.batchOptions.push({
          id: resp.data[i],
          title: resp.data[i]
        });
      }
    });
  };
  initialize();
});
