app.controller('ProcessingModalInstanceCtrl', function ($uibModalInstance, $window, $timeout, AlbumPhotoUploadFactory) {
  var $ctrl = this;
  $ctrl.uploadFactory = AlbumPhotoUploadFactory;

  $ctrl.completed = function () {
    $uibModalInstance.close();
    //$window.location.reload();
  };
});
