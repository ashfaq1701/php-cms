var FAPHelper = (function() {

    var settings = {
        autocomplete_target: '',
        autocomplete_url: '',
        country: '',
    }

    var _init = function() {
        _handleAutocomplete();
    }

    var _handleAutocomplete = function() {
        // on change
        var selectCountry = $('select[name="facebook_page"]');
        // $(selectCountry).change(function(event) {
        //         // $(settings.autocomplete_target).autocomplete("search");
        //     }
        // );

        $(settings.autocomplete_target).autocomplete({

            minLength: 2,
            source: function(request, response) {
                settings.country = $(selectCountry).find(":selected").data('country');

                var call = $.ajax({
                    url: settings.autocomplete_url, dataType: 'json', type: 'POST', data: {
                        term: request.term,
                        country: settings.country
                    }
                });

                call.success(function(callback_response) {
                    response($.map(callback_response, function (item, key) {
                        return {
                            label: item.name + " [ " + item.code + " ]",
                            value: item.name + " [ " + item.code + " ]",
                            object: item
                        }
                    }));
                });
            },
            select: function(event, ui) {
				// console.log(ui.item.object);
				$('#product_id').val(ui.item.object.id);
                $(settings.autocomplete_target).val(ui.item.object.code); // good idea to put it in orders too
                // console.log(window.location.domain);
                // console.log(window.location.host);
                // console.log(document.location.origin);
                var imagePath = document.location.origin+'/uploads/'+ui.item.object.id+'/'+ui.item.object.imagefile+'_thumb'+ui.item.object.imagefile_ext;
                console.log(imagePath);
                $('#product-info').find('img').attr('src', imagePath);
                $('#product-info').show('500');
//				console.log();
//                 var tableRows = $('#order_products tbody tr').length;
//                 $.ajax({
//                     url: settings.autocomplete_addrowurl,
//                     dataType: 'html',
//                     data: {
//                         table_rows: tableRows,
//                         product: ui.item.object,
// //						format: 'json'
//                     },
//                     error: function () {
//                         alert('ss');
//                     },
//                     success: function (data) {
//                         $('#order_products tbody').append(data);
//                         // _calcme();
//                     },
//                     done: function(data) {
//                         alert('doneee');
//                     },
//                     type: 'POST',
//                     async: true
//                 });
                return false;
//
            }
        });
    };





    return {
        init: _init,
        setAutocompleteTarget: function(target) {
            settings.autocomplete_target = target;
        },
        setUrlForAutocomplete: function(url) {
            settings.autocomplete_url = url;
        },

    }

})();