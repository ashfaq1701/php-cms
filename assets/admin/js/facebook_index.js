var pageTitleInput = '<input type="text" class="form-control page-title-ip" onfocusout="pageTitleChanged(this)" placeholder="Page Title"/>';
var pageCountryInput = null;
var countriesObj = null;
var albumTitleInput = '<input type="text" class="form-control album-title-ip" onfocusout="albumTitleChanged(this)" placeholder="Album Title"/>';
var albumSubtitleInput = '<input type="text" class="form-control album-subtitle-ip" onfocusout="albumSubtitleChanged(this)" placeholder="Album Subtitle"/>';
var albumCategoryInput = null;
var albumCategoriesObj = null;

var pageTitleSpan = '<span class="page-title" onclick="pageTitleIp(this)"></span>';
var pageCountrySpan = '<span class="page-country" onclick="pageCountryIp(this)"></span>';
var albumTitleSpan = '<span class="album-title" onclick="albumTitleIp(this)"></span>';
var albumSubtitleSpan = '<span class="album-subtitle" onclick="albumSubtitleIp(this)"></span>';
var albumCategorySpan = '<span class="album-category" onclick="albumCategoryIp(this)"></span>';

function makeCountrySelect()
{
  jQuery.ajax(
    {
      type: 'GET',
      url: '/admin/country',
      success: function(data) {
        countriesObj = data;
        var localCountryIp = '<select class="form-control page-country-ip" onchange="pageCountryChanged(this)">';
        for(var i = 0; i < data.length; i++)
        {
          var country = data[i];
          localCountryIp = localCountryIp + '<option value="'+country.id+'">'+country.name+'</option>';
        }
        localCountryIp = localCountryIp + '</select>';
        pageCountryInput = localCountryIp;
        console.log(pageCountryInput);
      }
    }
  );
}

function makeAlbumCategorySelect()
{
  jQuery.ajax(
    {
      type: 'GET',
      url: '/admin/album-category',
      success: function(data)
      {
        data.unshift({id: 0, name: 'No Category'});
        albumCategoriesObj = data;
        var localAlbumCategoryIp = '<select class="form-control album-category-ip" onchange="albumCategoryChanged(this)">';
        for(var i = 0; i < data.length; i++)
        {
          var albumCategory = data[i];
          localAlbumCategoryIp = localAlbumCategoryIp + '<option value="'+albumCategory.id+'">'+albumCategory.name+'</option>';
        }
        localAlbumCategoryIp = localAlbumCategoryIp + '</select>';
        albumCategoryInput = localAlbumCategoryIp;
        console.log(albumCategoryInput);
      }
    }
  );
}

makeCountrySelect();
makeAlbumCategorySelect();

function getCountryId(name)
{
  for(var i = 0; i < countriesObj.length; i++)
  {
    var country = countriesObj[i];
    if(country.name == name)
    {
      return country.id;
    }
  }
  return null;
}

function getAlbumCategoryId(name)
{
  for(var i = 0; i < albumCategoriesObj.length; i++)
  {
    var albumCategory = albumCategoriesObj[i];
    if(albumCategory.name == name)
    {
      return albumCategory.id;
    }
  }
  return null;
}

function pageTitleIp(element)
{
  var parent = jQuery(element).parent();
  var oldVal = jQuery(element).text();
  jQuery(element).remove();
  var newNode = jQuery.parseHTML(pageTitleInput);
  jQuery(newNode).val(oldVal);
  jQuery(parent).prepend(newNode);
}

function pageCountryIp(element)
{
  var parent = jQuery(element).parent();
  var oldTxt = jQuery(element).text();
  var oldVal = getCountryId(oldTxt);
  jQuery(element).remove();
  var newNode = jQuery.parseHTML(pageCountryInput);
  jQuery(newNode).val(oldVal);
  jQuery(parent).prepend(newNode);
}

function albumTitleIp(element)
{
  var parent = jQuery(element).parent();
  var oldVal = jQuery(element).text();
  jQuery(element).remove();
  var newNode = jQuery.parseHTML(albumTitleInput);
  jQuery(newNode).val(oldVal);
  jQuery(parent).prepend(newNode);
}

function albumSubtitleIp(element)
{
  var parent = jQuery(element).parent();
  var oldVal = jQuery(element).text();
  jQuery(element).remove();
  var newNode = jQuery.parseHTML(albumSubtitleInput);
  jQuery(newNode).val(oldVal);
  jQuery(parent).prepend(newNode);
}

function albumCategoryIp(element)
{
  var parent = jQuery(element).parent();
  var oldTxt = jQuery(element).text();
  var oldVal = getAlbumCategoryId(oldTxt);
  jQuery(element).remove();
  var newNode = jQuery.parseHTML(albumCategoryInput);
  jQuery(newNode).val(oldVal);
  jQuery(parent).prepend(newNode);
}

function pageTitleChanged(element)
{
  var newVal = jQuery(element).val();
  var parent = jQuery(element).parent();
  var pageId = jQuery(parent).attr('data-id');
  jQuery.ajax({
    type: 'POST',
    url: '/admin/facebook/edit/'+pageId,
    data: {
      title: newVal
    },
    success: function(data)
    {
      var newNode = jQuery.parseHTML(pageTitleSpan);
      jQuery(newNode).text(newVal);
      jQuery(element).remove();
      jQuery(parent).prepend(newNode);
    }
  });
}

function pageCountryChanged(element)
{
  var newVal = jQuery(element).val();
  var newTxt = jQuery(element).find('option:selected').text();
  var parent = jQuery(element).parent();
  var pageId = jQuery(parent).attr('data-id');
  jQuery.ajax({
    type: 'POST',
    url: '/admin/facebook/edit/'+pageId,
    data: {
      country: newVal
    },
    success: function(data)
    {
      var newNode = jQuery.parseHTML(pageCountrySpan);
      jQuery(newNode).text(newTxt);
      jQuery(element).remove();
      jQuery(parent).prepend(newNode);
    }
  });
}

function albumTitleChanged(element)
{
  var newVal = jQuery(element).val();
  var parent = jQuery(element).parent();
  var albumId = jQuery(parent).attr('data-id');
  jQuery.ajax({
    type: 'POST',
    url: '/admin/album/edit/'+albumId,
    data: {
      title: newVal
    },
    success: function(data)
    {
      var newNode = jQuery.parseHTML(albumTitleSpan);
      jQuery(newNode).text(newVal);
      jQuery(element).remove();
      jQuery(parent).prepend(newNode);
    }
  });
}

function albumSubtitleChanged(element)
{
  var newVal = jQuery(element).val();
  var parent = jQuery(element).parent();
  var albumId = jQuery(parent).attr('data-id');
  jQuery.ajax({
    type: 'POST',
    url: '/admin/album/edit/'+albumId,
    data: {
      subtitle: newVal
    },
    success: function(data)
    {
      var newNode = jQuery.parseHTML(albumSubtitleSpan);
      jQuery(newNode).text(newVal);
      jQuery(element).remove();
      jQuery(parent).prepend(newNode);
    }
  });
}

function albumCategoryChanged(element)
{
  var newVal = jQuery(element).val();
  if(newVal == 0)
  {
    newVal = null;
  }
  var newTxt = jQuery(element).find('option:selected').text();
  var parent = jQuery(element).parent();
  var albumId = jQuery(parent).attr('data-id');
  jQuery.ajax({
    type: 'POST',
    url: '/admin/album/edit/'+albumId,
    data: {
      category_id: newVal
    },
    success: function(data)
    {
      var newNode = jQuery.parseHTML(albumCategorySpan);
      jQuery(newNode).text(newTxt);
      jQuery(element).remove();
      jQuery(parent).prepend(newNode);
    }
  });
}

function deletePage(element, pageId)
{
  jQuery.ajax({
    type: 'GET',
    url: '/admin/facebook/delete/'+pageId,
    success: function(data)
    {
      var parent = jQuery(element).parent().parent();
      jQuery(parent).remove();
      $('#alert-success span').text(data.message);
      $('#alert-success').css('display', 'block');
      $('#alert-error').css('display', 'none');
    },
    error: function()
    {
      $('#alert-error span').text('Could not delete facebook page');
      $('#alert-error').css('display', 'block');
      $('#alert-success').css('display', 'none');
    }
  });
}

function deleteAlbum(element, albumId)
{
  jQuery.ajax({
    type: 'GET',
    url: '/admin/album/delete/'+albumId,
    success: function(data)
    {
      var parent = jQuery(element).parent().parent();
      jQuery(parent).remove();
      $('#alert-success span').text(data.message);
      $('#alert-success').css('display', 'block');
      $('#alert-error').css('display', 'none');
    },
    error: function()
    {
      $('#alert-error span').text('Could not delete album');
      $('#alert-error').css('display', 'block');
      $('#alert-success').css('display', 'none');
    }
  });
}

function refreshPage(pageId)
{
  jQuery.ajax({
    type: 'GET',
    url: '/admin/facebook/refresh/'+pageId,
    success: function(data)
    {
      $('#alert-success span').text('Page albums refreshed');
      $('#alert-success').css('display', 'block');
      $('#alert-error').css('display', 'none');
    },
    error: function()
    {
      $('#alert-error span').text('Could not refresh page');
      $('#alert-error').css('display', 'block');
      $('#alert-success').css('display', 'none');
    }
  });
}

function refreshAlbum(albumId)
{
  jQuery.ajax({
    type: 'GET',
    url: '/admin/album/refresh/'+albumId,
    success: function(data)
    {
      $('#alert-success span').text('Album photos refreshed');
      $('#alert-success').css('display', 'block');
      $('#alert-error').css('display', 'none');
    },
    error: function()
    {
      $('#alert-error span').text('Could not refresh album');
      $('#alert-error').css('display', 'block');
      $('#alert-success').css('display', 'none');
    }
  });
}

function parseAlbums(albums)
{
  var html = '<table class="table" style="display: block; padding-left:50px;">'
              +'<thead>'
              +'<tr>'
              +'<th>ID</th>'
              +'<th>Title</th>'
              +'<th>Subtitle</th>'
              +'<th>Album Category</th>'
              +'<th>Photos</th>'
              +'<th></th>'
              +'</tr>'
              +'</thead>'
              +'<tbody>';
  for(var i = 0; i < albums.length; i++)
  {
    var album = albums[i];
    var albumCat = null;
    if(album.category == null)
    {
      albumCat = 'No Category';
    }
    else
    {
      albumCat = album.category;
    }
    html = html + '<tr>' +
            '<td>'+album.id+'</td>'+
            '<td data-id="'+album.id+'">'+
            '<span class="album-title">'+album.title+'</span>&nbsp;'+
            '<a href="'+album.fb_url+'"><i class="fa fa-external-link" aria-hidden="true"></i></a>'+
            '</td>'+
            '<td data-id="'+album.id+'"><span class="album-subtitle">'+album.subtitle+'</span></td>'+
            '<td data-id="'+album.id+'"><span class="album-category" onclick="albumCategoryIp(this)">'+albumCat+'</span></td>'+
            '<td><button onclick="showPhotos('+album.id+')" class="btn btn-success btn-xs show-photos" data-id="'+album.id+'">Photos</button></td>'+
            '<td>'+
            '<a class="refresh-link" href="javascript:;" onclick="refreshAlbum('+album.id+')"><i class="fa fa-refresh" aria-hidden="true"></i></a>'+
            '<button onclick="deleteAlbum(this, '+album.id+')" class="btn btn-danger btn-xs delete-album" data-id="'+album.id+'">Delete Album</button>'
            '</td>'+
            '</tr>';
  }
  html = html + '</tbody></table>';
  return html;
}

function showAddAlbum(pageId)
{
  jQuery.ajax({
    type: 'POST',
    url: '/admin/facebook/page/'+pageId,
    success: function(response)
    {
      $('#page-name').attr('href', response.url);
      $('#page-name').text(response.title);
      $('#page-id').val(response.id);
      $('#albumCreate').modal('show');
    }
  });
}

jQuery(document).ready(function($)
{
  var table = $('.datatables').DataTable({
    "bPaginate": false
  });

  $('#albumCreate').modal({ show: false});

  $('.datatables tbody').on('click', 'button.show-albums', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    var pageId = $(this).attr('data-id');
    if (row.child.isShown()) {
      row.child.hide();
    }
    else {
      jQuery.ajax({
        type: 'POST',
        url: '/admin/album/page/'+pageId,
        success: function(res)
        {
          row.child(parseAlbums(res)).show();
        }
      });
    }
  });

  $('#create-album-submit').click(function()
  {
    var pageId = $('#page-id').val();
    var title = $('#album-title').val();
    var subtitle = $('#album-subtitle').val();
    jQuery.ajax({
      type: 'POST',
      url: '/admin/album/page/'+pageId+'/create',
      data:
      {
        fb_page_id: pageId,
        title: title,
        subtitle: subtitle
      },
      success: function(res)
      {
        $('#albumCreate').modal('hide');
        $('#alert-success span').text(res.message);
        $('#alert-success').css('display', 'block');
      }
    });
  });
});
