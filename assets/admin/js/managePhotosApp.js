var app = angular.module("managePhotosApp", ['datatables', 'datatables.bootstrap'], function($interpolateProvider, $httpProvider){
	$interpolateProvider.startSymbol('[[').endSymbol(']]');
	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
});
