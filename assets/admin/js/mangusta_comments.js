$(document).on('click', 'a[data-role="toggle-comments"]', function(event) {
	event.preventDefault(); // just in case
	
	var $this = $(this);
	var current_row = $this.closest('tr');
	
	if ($this.attr('data-has-comments')) {
		current_row.next('tr').remove();
		$this.removeAttr('data-has-comments');
		
		return false;
	}
	
	$this.attr('data-has-comments', true);
	
	var location_url = $this.attr('href');
	var request = $.ajax({
		url: location_url, type: 'POST', dataType: 'json'
	});
	
	
	request.success(function(response) {
		if (response && typeof response.comments !== 'undefined') {
			var cells_count = current_row.find('td').length;
			$('<tr>')
					.css({'background-color': '#DFDFDF'})
					.append($('<td>').attr('colspan', cells_count).html(response.comments))
					.insertAfter(current_row);
		}
	});
	
});
