var OrdersHelper = (function() {
	
	var settings = {
		autocomplete_target: '',
		autocomplete_url: '',
		autocomplete_addrowurl: '',
		comments_url: '',
		deliverycost_url: '',
		autocomplete_customer_target: '',
		autocomplete_customer_url:'',
                order_status: '',
	}
	
	var _init = function() {
		_handleAutocomplete();
		_removeButtonForPrototype();
		_handleCommentsForm();
		_handleDeliveryCostByCountry();
		_handleRemoveProduct();
		_handleRemoveProductLegacy();
		_calculateTotalPrice();
		_handleAutocompleteCustomer();
                _handleOrderStatusComplete();
	}
	
	var _handleAutocomplete = function() {
		$(settings.autocomplete_target).autocomplete({
			
			minLength: 2,
			source: function(request, response) {
				var country = $('select[name="country"]').find(":selected").val();
				var call = $.ajax({
					url: settings.autocomplete_url, dataType: 'json', type: 'POST', data: {
						term: request.term,
						country: country
					}
				});
				
				call.success(function(callback_response) {
				response($.map(callback_response, function (item, key) {
						return {
							label: item.name + " [ " + item.code + " ]",
							value: item.name + " [ " + item.code + " ]",
							object: item
						}
					}));
				});
			},
			select: function(event, ui) {
//				console.log(ui.item.object);
//				console.log();
				var tableRows = $('#order_products tbody tr').length;
				$.ajax({
					url: settings.autocomplete_addrowurl,
					dataType: 'html',
					data: {
						table_rows: tableRows,
						product: ui.item.object,
//						format: 'json'
					},
					error: function () {
						alert('ss');
					},
					success: function (data) { 
						$('#order_products tbody').append(data);
						_calcme();
					},
					done: function(data) {
						alert('doneee');
					},
					type: 'POST',
					async: true
				});
				return false;
//				
			}
		});
	};
	
	var _removeButtonForPrototype = function() {
		if ($('div[data-role="collection"]').length) {
			$('div[data-role="collection"] a[data-role="add-element"]').remove();
		}
	};
	
	var _handleDeliveryCostByCountry = function() {
		var select = $('select[name="country"]');
		
		// on load
		var country_id = select.val(),
			target = $('input[name="delivery_cost"]'),
			request = $.ajax({
				url: settings.deliverycost_url, dataType: 'json', type: 'POST', data: {
					country_id: parseInt(country_id)
				}
			});

		request.success(function(response){
			if(typeof response.success !== 'undefined' && typeof response.content !== 'undefine') {
				target.val(response.content);
			}

		});
		
		// on change
		select.change(function(event) {
			event.preventDefault();
			
			var country_id = select.val(),
				target = $('input[name="delivery_cost"]'),
				request = $.ajax({
					url: settings.deliverycost_url, dataType: 'json', type: 'POST', data: {
						country_id: parseInt(country_id)
					}
				});
				
			request.success(function(response){
				if(typeof response.success !== 'undefined' && typeof response.content !== 'undefine') {
					target.val(response.content);
				}
				
			});
		});
	};
	
	var _handleCommentsForm = function() {
		var button = $('button[data-role="add-comment"]');
	
		button.click(function(event) {
			event.preventDefault();
			
			var target = $('div[data-role="comments-list"]'),
				order_id = $('input[name="order_id"]').val(),
				comment = $('textarea#compose-textarea').val(),
				request = $.ajax({
				url: settings.comments_url, dataType: 'json', type: 'POST', data: {
					id: parseInt(order_id, 10),
					comment: comment
				}
			});
			
			request.success(function(response) {
				if (typeof response.success !== 'undefined' && typeof response.content !== 'undefined') {
					target.replaceWith(response.content);
				}
			});
		});
	};
	
	var _handleRemoveProduct = function() {
//		return confirm('Are you sure?');
		var button = $(document).on('click','#remove-order-product', function(event){
			
			if(confirm('Are you sure ?')){
				$(this).closest('tr').remove();
				_calcme();
			}
			
		});
	};
	
	var _handleRemoveProductLegacy = function() {
		var button = $(document).on('click','#remove-order-product-legacy', function(event) {
//			confirm('Are you sure ?');
			if(confirm('Are you sure ?')){
				
			} else {
			event.preventDefault();
			}
		});
	};
	
	var _calcme = function() {
		var products = $('table#order_products tbody > tr');
		var total = 0;
		$.each(products, function(index, value) {
			var pr_price = $(value).find('td.price').html();
			var pr_quantity = $(value).find('td.quantity input').val();
			if(typeof pr_quantity === 'undefined') {
				pr_quantity = parseInt($(value).find('td.quantity').html());
			}
			console.log(pr_quantity);
			var pr_total = pr_price * pr_quantity;
			
			total = total + pr_total;
		});
		var delivery_cost = parseFloat($('input[name="delivery_cost"]').val());
		
		var discount = parseFloat($('input[name="discount"]').val());
                
                var discount_percentage = parseFloat($('input[name="discount_percentage"]').val());
//		console.log(discount);
//		total = (total + delivery_cost) - discount;
                total = (total - (total * (discount_percentage / 100))) - discount;
                total = total + delivery_cost;
		$('.total_price').html(total.toFixed(2));
//		console.log(total);
	};
	
	var _calculateTotalPrice = function() {
		var delivery_cost = 0.00;
		$(document).on('change', 'input[name="delivery_cost"]', function(event) {
			_calcme();
		});
		
		$(document).on('change', 'input[name="discount"]', function(event) {
			_calcme();
		});
                
		$(document).on('change', 'input[name="discount_percentage"]', function(event) {
			_calcme();
		});
		
		$(document).on('change', 'table#order_products', function(event) {
			_calcme();
		});
		
		$(document).on('change', 'td.quantity input', function(event) {
			_calcme();
		});
	};
	
	var _handleAutocompleteCustomer = function() { 
		$(settings.autocomplete_customer_target).autocomplete({
			minLength: 1,
			source: function(request, response) {
//				var country = $('select[name="country"]').find(":selected").val();
				var call = $.ajax({
					url: settings.autocomplete_customer_url, dataType: 'json', type: 'POST', data: {
						term: request.term,
//						country: country
					}
				});
				
				call.success(function(callback_response) {
				response($.map(callback_response, function (item, key) {
						return {
							label: item.customer_name,
							value: item.customer_name,
							object: item
						}
					}));
				});
			},
		});
		
	};
        
        var _handleOrderStatusComplete = function() {
//                console.log(settings.order_status);
                // if order status is completed disable editing in order
                if(settings.order_status == 2) {
                    $('select[name=country]').prop('disabled', true);
                    $('select[name=facebook_page]').prop('disabled', true);
                    $('input[name=email]').prop('disabled', true);
                    $('input[name=phone]').prop('disabled', true);
                    $('input[name=customer_name]').prop('disabled', true);
                    $('input[name=customer_facebook_url]').prop('disabled', true);
                    $('input[name=address]').prop('disabled', true);
                    $('input[name=city]').prop('disabled', true);
                    $('input[name=post_code]').prop('disabled', true);
                    $('#order_save').prop('disabled', true);
                    $('input#product_autocomplete').prop('disabled', true);
                    $('input[name=delivery_cost]').prop('disabled', true);
                    $('input[name=discount]').prop('disabled', true);
                    $('a#change-order-product-legacy').addClass('disabled');
                    $('a#remove-order-product-legacy').addClass('disabled');
                }
        };
	

	
	return {
		init: _init,
		setAutocompleteTarget: function(target) {
			settings.autocomplete_target = target;
		}, 
		setUrlForAutocomplete: function(url) {
			settings.autocomplete_url = url;
		},
		setUrlForAddRow: function(url) {
			settings.autocomplete_addrowurl = url;
		},
		setUrlForComments: function(url) {
			settings.comments_url = url;
		},
		setUrlForCountryDeliveryCost: function(url) {
			settings.deliverycost_url = url;
		},
		setAutocompleteCustomerTarget: function (target) {
			settings.autocomplete_customer_target = target;
		},
		setUrlForAutocompleteCustomer: function (url) {
			settings.autocomplete_customer_url = url;
		},
                setOrderStatus: function(status) {
                        settings.order_status = status;
                }
 	}
	
})();