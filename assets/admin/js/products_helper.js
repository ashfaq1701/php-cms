var ProductHelper = (function() {
	
	var settings = {
		comments_url: '',
                action: '',
	}
	
	var _init = function() {
		
		_handleCommentsForm();
		_disableInput();
		$("#imgInp").change(function(){
			readURL(this);
		});
		
		$('.del_photo').click(function(){
			return confirm('Are you sure you want to delete image?');
		});
		
		$('.set_main_photo').click(function() {
			return confirm('Set Image as Main ?');
		});
		
	};
	
	var readURL = function (input) { console.log('asd');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#previewImage').attr('src', e.target.result);
            };
            
            reader.readAsDataURL(input.files[0]);
        }
		
		
    };
	
	var _handleCommentsForm = function() {
		var button = $('button[data-role="add-comment"]');
	
		button.click(function(event) {
			event.preventDefault();
			
			var target = $('div[data-role="comments-list"]'),
				order_id = $('input[name="product_id"]').val(),
				comment = $('textarea#compose-textarea').val(),
				request = $.ajax({
				url: settings.comments_url, dataType: 'json', type: 'POST', data: {
					id: parseInt(order_id, 10),
					comment: comment
				}
			});
			
			request.success(function(response) {
				if (typeof response.success !== 'undefined' && typeof response.content !== 'undefined') {
					target.replaceWith(response.content);
				}
			});
		});
	};
        
        var _disableInput = function() {
                if(settings.action == 'edit'){
                    $('input[name=code]').prop('readonly', true);
                }
                
        }
    
    return {
		init: _init,
		setUrlForComments: function(url) {
			settings.comments_url = url;
		},
                setAction: function(action) {
                        settings.action = action;
                },
 	};
	
})();