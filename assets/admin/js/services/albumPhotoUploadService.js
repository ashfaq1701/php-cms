app.factory('AlbumPhotoUploadService', ['$http', function($http)
{
  return {
    getPages: function()
    {
      return $http.get('/admin/facebook/grouped');
    },
    getAlbumCategories: function()
    {
      return $http.get('/admin/album-category');
    },
    postToFacebookPages: function(pages, albumCategories, description, files, isBranded, overlayProductCode, brandId)
    {
      return $http.post('/admin/facebook/uploadPhotos/post', {
        pages: pages,
        albumCategories: albumCategories,
        description: description,
        files: files,
        isBranded: isBranded,
        overlayProductCode: overlayProductCode,
        brandId: brandId
      });
    },
    getBrandList: function () {
      return $http.get('/admin/images/brand/list');
    },
    saveBrand: function(fullName, shortName, abbreviation, brandImage) {
      return $http.post('/admin/images/brand/save', {
        full_name: fullName,
        short_name: shortName,
        abbreviation: abbreviation,
        brand_image: brandImage
      });
    }
  };
}]);
