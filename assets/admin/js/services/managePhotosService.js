app.factory('ManagePhotosService', ['$http', function($http)
{
  return {
    getPages: function()
    {
      return $http.get('/admin/facebook/list');
    },
    getAlbums: function(page)
    {
      return $http.get('/admin/album/page/'+page);
    },
    getBatchNo: function(page, album)
    {
      return $http.get('/admin/facebook/batch-no?page='+page+"&album="+album);
    },
    deletePhoto: function(photo)
    {
      return $http.get('/admin/facebook/photo/delete/'+photo);
    }
  };
}]);
