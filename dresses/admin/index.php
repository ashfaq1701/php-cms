<?php
include("../config.php");
include("../functions.php");
include("../Template.inc");
require_once("paginator.class.php");
//include("ckeditor/ckeditor.php") ;
//error_reporting(E_ALL ^ E_NOTICE);
header('Content-Type: text/html; charset=utf-8');
header("Cache-Control: no-cache, must-revalidate"); 

session_start();

if (isset($_SESSION['token'])) {
	$token = $_SESSION['token'];
} else {
	$token = GenToken();
	$_SESSION['token'] = $token;
}

$DB = new MySQL();
$DB->Connect2DB($host, $name, $pass, $db_name);

IF($_GET['act'] == 'testing') {
exit;
}
if(isset($_GET['act'])) {
	$act = $_GET['act'];
} else {
	$act = "orders";
}
if(isset($_GET['id'])>0) {
	$id = intval($_GET['id']);
} else {
	$id = 0;
}

$content = "";
$lang = "en";

$t = new Template("inc/template.html");

if (isset($_SESSION['message'])) {
	$t->setVar($_SESSION['message']['type'], $_SESSION['message']['msg']);
	unset($_SESSION['message']);
}

if(isset($_GET['scroll'])) {
	$scroll = intval($_GET['scroll']);
} else {
	$scroll = 0;
}

// Check Login
if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
//	session_regenerate_id(true);
	if($_POST['login']) {
		$username = isset($_POST['username']) ? mb_substr($_POST['username'], 0, 16) : '';
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		$token = isset($_POST['token']) ? $_POST['token'] : '';

		if (!isset($_SESSION['token']) || ($token != $_SESSION['token'])) {
			$t->setVar('error', $MSG['en']['MSG_ERROR_LOGIN']);
		} elseif ($username != '' && $password != '' ) {
			$username = SlashString($username);
			$query = "
				SELECT
					`users`.`id`,
					`users`.`password`,
					`users`.`salt`,
					`users`.`group`,
					`users`.`country`,
					`users`.`username`,
					`users`.`name`
				FROM
					`users`
				WHERE
					`users`.`username` = '$username'
				LIMIT 1";
			$result = mysql_query($query);
			$row = mysql_fetch_assoc($result);
			if (mysql_num_rows($result) == 1) {
				if ($row['password'] == SaltPassword($password, $row['salt'])) {
					$_SESSION['timeout'] = time();
					$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
					$_SESSION['login'] = $login_phrase;
					$_SESSION['user_id'] = $row['id'];
					$_SESSION['user'] = $row['username'];
					$_SESSION['name'] = $row['name'];
					$_SESSION['group'] = $row['group'];
					$_SESSION['country'] = $row['country'];
					$_SESSION['useragent'] = $_SERVER['HTTP_USER_AGENT'];
				} else {
					$t->setVar('error', $MSG['en']['MSG_ERROR_LOGIN']);
				}

				if (isset($_SESSION['login']) && ($_SESSION['login'] == $login_phrase))
					$success = 1;
				else
					$success = 0;

				if ($success == 1) {
					header("Location: ".$_SERVER['HTTP_REFERER']);
					exit;
				} else {
					$t->setVar('error', $MSG['en']['MSG_ERROR_LOGIN']);
				}
			} else {
				$t->setVar('error', $MSG['en']['MSG_ERROR_LOGIN']);
			}
		} else {
			$t->setVar('error', $MSG['en']['MSG_ERROR_LOGIN_REQUIRE']);
		}
	}
	$t->setVar("where", "Log in");
	$t2 = new Template("inc/login.html");
	$t2->setVar("token", $token);
	$content .= $t2->toString();
	$t->setVar("content", $content);
} else {
	// Check for authorized login
	$isLoginOk =
		($_SESSION['login'] == $login_phrase) &&
		(time() - $_SESSION['timeout'] < $login_admin_time);
	if ($isLoginOk) {
		UpdateLogin();

		// Get User Privilegies
		$privileges = array();
		$query_privileges = "
			SELECT `users_roles`.`name`
			FROM `users_groups_privileges`
			LEFT JOIN `users_roles` ON
				`users_groups_privileges`.`role` = `users_roles`.`id`
			WHERE
				`users_groups_privileges`.`group` = '".$_SESSION['group']."'
		";
		$result_privileges = mysql_query($query_privileges);
		while($tmp = mysql_fetch_assoc($result_privileges)) {
			// Show privilegied menus, subpages or options
			$t->setVar("menu_".$tmp['name'], " ");
			$privileges[] = $tmp['name'];
		}

		$login = true;
	} else {
		Logout();
	}
}

if($login === true) {
	if($act=="changelog") {
		$t->setVar("where", "Changelog");
		$changelog_lines = file("changelog.txt", FILE_IGNORE_NEW_LINES);

		$content .= "<pre>";
		foreach($changelog_lines as $id=>$text) {
			$content .= $text."\n";
		}
		$content .= "</pre>";
		$t->setVar("content", $content);
	} elseif($act=="suppliers") {
		CheckPrivileges("suppliers");
		$t->setVar("where", "Suppliers");
		if(isset($_GET['delete']) && $id>0) {
			$query = "DELETE FROM `suppliers` WHERE `id` = '".$id."' LIMIT 1";
			if(!mysql_query($query)) {
				$t->setVar("error", $MSG[$lang]['MSG_ERROR_DELETE']);
			} else {
				Logger("Suppliers","Supplier","delete",array('Supplier №'=>$id));
				header("Location: index.php?act=".$act);
				exit;
			}
		} elseif(isset($_GET['edit']) && $id>0) {
			if(isset($_POST['submit'])) {
				$_POST = makepost($_POST);
				field_validator("Names", $_POST['name'], "string", 3, 255, 1);
				field_validator("Phone", $_POST['phone'], "string", 3, 255, 0);
				field_validator("E-Mail", $_POST['email'], "string", 3, 255, 0);

				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					$query = "UPDATE `suppliers`
							SET
								`name` = '".makepost($_POST['name'])."',
								`phone` = '".makepost($_POST['phone'])."',
								`email` = '".makepost($_POST['email'])."',
								email = '".mysql_real_escape_string($_POST['email'])."',
								email_period = '".intval($_POST['email_period'])."'
							WHERE `id` = '".$id."'";
					if(!mysql_query($query)) {
						$t->setVar("error", $MSG[$lang]['MSG_ERROR_EDIT']);
					} else {
						Logger("Suppliers","Supplier","edit",array('Supplier №'=>$id));
						header("Location: index.php?act=".$act);
						exit;
					}
				}
			}
			$t2 = new Template("inc/suppliers_edit.html");
			$query ="SELECT * FROM `suppliers` WHERE `id` = '".$id."' LIMIT 1";
			$result = mysql_query($query);
			$row = mysql_fetch_assoc($result);
			$t2->setVar("id", $row['id']);
			$t2->setVar("name", $row['name']);
			$t2->setVar("phone", $row['phone']);
			$t2->setVar("email", $row['email']);
			$t2->setVar("email_period", $row['email_period']);

			mysql_free_result($result);
			$content .= $t2->toString();
		} else {
			if(isset($_POST['submit'])) {
				$_POST = makepost($_POST);
				field_validator("Names", $_POST['name'], "string", 3, 255, 1);
				field_validator("Phone", $_POST['phone'], "string", 3, 255, 0);
				field_validator("E-Mail", $_POST['email'], "string", 3, 255, 0);

				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					$query = "INSERT INTO `suppliers` (name,phone,email) VALUES('".makepost($_POST['name'])."','".makepost($_POST['phone'])."','".makepost($_POST['email'])."')";
					if(!mysql_query($query)) {
						$t->setVar("error", $MSG[$lang]['MSG_ERROR_ADD']);
					} else {
						Logger("Suppliers","Supplier","add",array('Supplier name'=>makepost($_POST['name'])));
						header("Location: index.php?act=".$act);
						exit;
					}
				}
			}
			$t2 = new Template("inc/suppliers.html");
			$query ="SELECT * FROM `suppliers`";
			$result = mysql_query($query) or die(mysql_error());
			while ($row = mysql_fetch_assoc($result)) {
				$t2->gotoNext("suppliers");
				$t2->setVar("suppliers.id", $row['id']);
				$t2->setVar("suppliers.name", $row['name']);
				$t2->setVar("suppliers.phone", $row['phone']);
				$t2->setVar("suppliers.email", $row['email']);
			}
			mysql_free_result($result);

			$content .= $t2->toString();
		}
		$t->setVar("content", $content);
	} elseif($act=="orders") {
		CheckPrivileges("orders");
		$t->setVar("where", "Orders");
		if(isset($_GET['load_note']) && $id>0) {
			$query = "	SELECT
							notes
						FROM `orders`
						WHERE
							id = '".$id."'
						LIMIT 1
			";
			$result = mysql_query($query);
			$row = mysql_fetch_assoc($result);
			$notes = str_replace('\\r\\n', "\r\n", $row['notes']);
			echo $notes;
			exit;
		} elseif(isset($_GET['save_note'])) {
			$query = "UPDATE orders SET notes = '".mysql_real_escape_string($_POST['value'])."' WHERE id = '".intval($_POST['id'])."' LIMIT 1";
			$result = mysql_query($query);
			Logger("Orders","Notes","edit",array('Order №'=>intval($_POST['id']),'Note'=>mysql_real_escape_string($_POST['value'])));
			$notes = nl2br($_POST['value']);
			echo $notes;
			exit;
		} elseif(isset($_GET['delete']) && $id>0) {
			CheckPrivileges("orders_delete");
			$query = "DELETE FROM `orders` WHERE `id` = '".$id."' LIMIT 1";
			if(!mysql_query($query)) {
				$t->setVar("error", $MSG[$lang]['MSG_ERROR_DELETE']);
			} else {
				Logger("Orders","Order","delete",array('Order №'=>$id));
				$_SESSION['message']['type'] = "success";
				$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_ADD'];
				header("Location: index.php?act=".$act);
				exit;
			}
		} elseif(isset($_GET['edit_product']) && $id>0) {
			CheckPrivileges("orders_edit");
			if($_POST) {
				mysql_query("START TRANSACTION");
				$success = true;
				$order_detail_id = intval($_POST['id']);
				$order_id = intval($_POST['order_id']);
				$quantity = intval($_POST['quantity']);
				$size = intval($_POST['size']);

				if($quantity > 0) {
					$product_query = "UPDATE orders_details SET size = '".$size."', quantity = '".$quantity."' WHERE id = '".$order_detail_id."' LIMIT 1";
					if(!mysql_query($product_query)) {
						$success = false;
					}
				} else {
					$success = false;
				}

				if($success==true) {
					mysql_query("COMMIT");
					Logger("Orders","Order details","edit",array('Order №'=>$order_id,'Order detail №'=>$order_detail_id));
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_EDIT'];
					header("Location: index.php?act=orders&edit&id=".$order_id);
				} else {
					mysql_query("ROLLBACK");
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_EDIT'];
					header("Location: ".$_SERVER['HTTP_REFERER']);
				}
				exit;
			}
			$t2 = new Template("inc/orders_edit_product.html");

			$query = "	SELECT
							*
						FROM `orders_details`
						WHERE
							id = '".$id."'
						LIMIT 1
			";
			$result = mysql_query($query);
			$row = mysql_fetch_assoc($result);
			$t2->setVar("id", $row['id']);
			$t2->setVar("quantity", $row['quantity']);
			$t2->setVar("order_id", $row['order_id']);
			mysql_free_result($result);

			$sizes_query ="SELECT * FROM `sizes`";
			$sizes_result = mysql_query($sizes_query);
			while ($sizes_row = mysql_fetch_assoc($sizes_result)) {
				$t2->gotoNext("sizes");
				$t2->setVar("sizes.id", $sizes_row['id']);
				$t2->setVar("sizes.name", $sizes_row['name']);
				if($sizes_row['id'] == $row['size']) {
					$t2->setVar("sizes.selected", " ");
				}
			}
			mysql_free_result($countries_result);

			$content .= $t2->toString();
		} elseif(isset($_GET['delete_product']) && $id>0) {
			$query = "DELETE FROM `orders_details` WHERE `id` = '".$id."' LIMIT 1";
			if(!mysql_query($query)) {
				$t->setVar("error", 'Грешка при изтриване!');
			} else {
				Logger("Orders","Order details","delete",array('Order detail №'=>$id));
				$_SESSION['message']['type'] = "success";
				$_SESSION['message']['msg'] = "Артикулът беше успешно изтрит от поръчката.";
				header("Location: ".$_SERVER['HTTP_REFERER']);
				exit;
			}
		} elseif(isset($_GET['edit']) && $id>0) {
			CheckPrivileges("orders_edit");
			if($_POST['add_product']) {
				mysql_query("START TRANSACTION");
				$success = true;
				$order_id = intval($_POST['id']);
				$product = intval($_POST['product']);
				foreach($_POST['quantities'] as $size => $quantity) {
					if($quantity > 0) {
						$product_query = "INSERT INTO orders_details (order_id,product,size,quantity) VALUES('".$order_id."','".$product."','".intval($size)."','".intval($quantity)."')";
 						if(!mysql_query($product_query)) {
							$success = false;
						}
					}
				}
				if($success==true) {
					mysql_query("COMMIT");
					Logger("Orders","Order","add",array('Order №'=>$order_id,'Product №'=>$product));
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_ADD'];
				} else {
					mysql_query("ROLLBACK");
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_ADD'];
				}
				header("Location: ".$_SERVER['HTTP_REFERER']);
				exit;
			} elseif($_POST['submit']) {
				$_POST = makepost($_POST);
				field_validator("Names", $_POST['buyer_names'], "string", 3, 255, 1);
				field_validator("E-Mail", $_POST['buyer_email'], "string", 3, 255, 1);
				field_validator("Phone", $_POST['phone'], "string", 3, 255, 1);

				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					$query = "UPDATE `orders`
							SET
								`buyer_names` = '".makepost($_POST['buyer_names'])."',
								`buyer_email` = '".makepost($_POST['buyer_email'])."',
								`payment_method` = '".makepost($_POST['payment_method'])."',
								`country` = '".intval($_POST['country'])."',
								`post_code` = '".makepost($_POST['post_code'])."',
								`city` = '".makepost($_POST['city'])."',
								`address` = '".makepost($_POST['address'])."',
								`phone` = '".makepost($_POST['phone'])."',
								`delivery_costs` = '".makepost($_POST['delivery_costs'])."',
								`discount` = '".makepost($_POST['discount'])."',
								`notes` = '".makepost($_POST['notes'])."'
							WHERE `id` = '".$id."'";
					if(!mysql_query($query)) {
						$t->setVar("error", 'Грешка при редактиране!');
						echo mysql_error();
					} else {
						Logger("Orders","Order","edit",array('Order №'=>$id));
						$_SESSION['message']['type'] = "success";
						$_SESSION['message']['msg'] = "Поръчката беше обновена успешно.";
						//header("Location: ".$_SERVER['HTTP_REFERER']);
						header("Location: index.php?act=orders&search=".$_GET['search']);
						exit;
					}
				}
			}
			$t2 = new Template("inc/orders_edit.html");

			$query = "	SELECT
							*
						FROM `orders`
						WHERE
							id = '".$id."'
						LIMIT 1
			";
			$result = mysql_query($query);
			$row = mysql_fetch_assoc($result);
			$t2->setVar("id", $row['id']);
			$t2->setVar("buyer_names", $row['buyer_names']);
			$t2->setVar("buyer_email", $row['buyer_email']);
			$t2->setVar("payment_method", $row['payment_method']);
			$t2->setVar("country", $row['country']);
			$t2->setVar("post_code", $row['post_code']);
			$t2->setVar("city", $row['city']);
			$t2->setVar("address", $row['address']);
			$t2->setVar("phone", $row['phone']);
			$t2->setVar("delivery_costs", $row['delivery_costs']);
			$t2->setVar("discount", $row['discount']);
			$t2->setVar("total", GetTotal($row['id']));
			$notes = str_replace('\\r\\n', "\r\n", $row['notes']);
			$t2->setVar("notes", $notes);
			mysql_free_result($result);

			$countries_query ="SELECT * FROM `countries` WHERE enabled = '1' ORDER BY `order`";
			$countries_result = mysql_query($countries_query);
			while ($countries_row = mysql_fetch_assoc($countries_result)) {
				$t2->gotoNext("countries");
				$t2->setVar("countries.id", $countries_row['id']);
				$t2->setVar("countries.name", $countries_row['name']);
				if($countries_row['id'] == $row['country']) {
					$t2->setVar("countries.selected", " ");
				}
			}
			mysql_free_result($countries_result);

			$products_query = "	SELECT
									`products`.*,
									`products_pictures`.`file`
								FROM `products`
								LEFT JOIN `products_pictures` ON `products`.`id` = `products_pictures`.`product`
								WHERE `products_pictures`.`main` = '1'
								GROUP BY `products`.`id`
			";
			$products_result = mysql_query($products_query) or die(mysql_error());
			while ($products_row = mysql_fetch_assoc($products_result)) {
				$t2->gotoNext("products");
				$t2->setVar("products.id", $products_row['id']);
				$t2->setVar("products.code", $products_row['code']);
				$t2->setVar("products.name", $products_row['name']);
				$t2->setVar("products.file", $products_row['file']);
				$prices = array();
				$prices_query = "SELECT products_prices.*,countries.currency FROM products_prices LEFT JOIN countries ON products_prices.country = countries.id WHERE products_prices.product = '".$products_row['id']."'";
				$prices_result = mysql_query($prices_query) or die(mysql_error());
				while ($prices_row = mysql_fetch_assoc($prices_result)) {
					$prices[] = array('country'=>$prices_row['country'],'price'=>$prices_row['price'],'currency'=>$prices_row['currency']);
				}
				$t2->setVar("products.prices", json_encode($prices));
			}
			mysql_free_result($products_result);

			$sizes_query ="SELECT * FROM `sizes`";
			$sizes_result = mysql_query($sizes_query) or die(mysql_error());
			while ($sizes_row = mysql_fetch_assoc($sizes_result)) {
				$t2->gotoNext("sizes");
				$t2->setVar("sizes.id", $sizes_row['id']);
				$t2->setVar("sizes.name", $sizes_row['name']);
			}
			mysql_free_result($sizes_result);

			$details_query = "	SELECT
									*,
									products.code,
									products.name,
									products_pictures.file AS picture,
									sizes.name AS size,
									orders_details.id,
									products_prices.price,
									countries.currency
								FROM `orders_details`
								LEFT JOIN products ON orders_details.product = products.id
								LEFT JOIN products_pictures ON orders_details.product = products_pictures.product
								LEFT JOIN products_prices ON orders_details.product = products_prices.product
								LEFT JOIN sizes ON sizes.id = orders_details.size
								LEFT JOIN countries ON countries.id = '".$row['country']."'
								WHERE
									products_pictures.main = '1'
									AND orders_details.order_id = '".$row['id']."'
									AND products_prices.country = '".$row['country']."'
			";
			$details_result = mysql_query($details_query) or die(mysql_error());
			while ($details_row = mysql_fetch_assoc($details_result)) {
				$t2->gotoNext("orders_details");
				$t2->setVar("orders_details.id", $details_row['id']);
				$t2->setVar("orders_details.size", $details_row['size']);
				$t2->setVar("orders_details.quantity", $details_row['quantity']);
				$t2->setVar("orders_details.code", $details_row['code']);
				$t2->setVar("orders_details.name", $details_row['name']);
				$t2->setVar("orders_details.price", $details_row['price']);
				$t2->setVar("orders_details.currency", $details_row['currency']);
				$t2->setVar("orders_details.picture", $details_row['picture']);
			}
			mysql_free_result($countries_result);

			$content .= $t2->toString();
		} elseif(isset($_GET['color']) && $id>0) {
			CheckPrivileges("orders_color");
			$colors = array('green','red','gray','purple');
			if(in_array($_GET['color'],$colors)) {
				$color = $_GET['color'];

				$query ="SELECT color FROM `orders` WHERE `id` = '".$id."' LIMIT 1";
				$result = mysql_query($query);
				$row = mysql_fetch_assoc($result);
				if($row['color'] == $_GET['color']) {
					$color = 'NULL';
				} else {
					$color = "'".mysql_real_escape_string($_GET['color'])."'";
				}

				if($row['color']=="green" && $_GET['color']!="green") {
					// ako e bilo zeleno i natisnem nqkakav drug cvqt
					$order_details_query = "SELECT product,size,quantity FROM orders_details WHERE order_id = '".$id."'";
					$order_details_result = mysql_query($order_details_query);
					while($order_details_row = mysql_fetch_assoc($order_details_result)) {
						mysql_query("UPDATE products_quantities SET quantity = quantity + ".$order_details_row['quantity']." WHERE product = '".$order_details_row['product']."' AND size = '".$order_details_row['size']."' LIMIT 1;");
					}
				} elseif($row['color']!="green" && $_GET['color']=="green") {
					// ako ne e bilo zeleno i natisnem zelenoto
					$order_details_query = "SELECT product,size,quantity FROM orders_details WHERE order_id = '".$id."'";
					$order_details_result = mysql_query($order_details_query);
					while($order_details_row = mysql_fetch_assoc($order_details_result)) {
						mysql_query("UPDATE products_quantities SET quantity = quantity - ".$order_details_row['quantity']." WHERE product = '".$order_details_row['product']."' AND size = '".$order_details_row['size']."' LIMIT 1;");
					}
				}
				mysql_query("UPDATE orders SET color = ".$color." WHERE id = '".$id."' LIMIT 1");
				Logger("Orders","Color","edit",array('Order №'=>$id,'Color'=>$color));
			}
			$clean_link = parse_url($_SERVER['HTTP_REFERER']); // Parse URL
			parse_str($clean_link['query'], $args); // Parse Query String to an array
			$args['scroll'] = $scroll;
			$url = "?".http_build_query($args);
			header("Location: ".$url);
			//header("Location: ".$_SERVER['HTTP_REFERER']."&scroll=".$scroll);
			//header("Location: index.php?act=orders&scroll=".$scroll);
			exit;
		} elseif(isset($_GET['toggle']) && $id>0) {
			$query ="SELECT * FROM `orders_details` WHERE `id` = '".$id."' LIMIT 1";
			$result = mysql_query($query);
			$row = mysql_fetch_assoc($result);
			if($row['notified'] == 0) {
				$notified = '1';
			} else {
				$notified = '0';
			}
			if(!mysql_query("UPDATE orders_details SET notified = '".$notified."' WHERE id = '".$id."' LIMIT 1")) {
				$_SESSION['message']['type'] = "error";
				$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_EDIT'];
				$scroll = 0;
			} else {
				Logger("Orders","Toggle","edit",array('Order №'=>$id,'Notified'=>$notified));
			}
			$clean_link = parse_url($_SERVER['HTTP_REFERER']); // Parse URL
			parse_str($clean_link['query'], $args); // Parse Query String to an array
			$args['scroll'] = $scroll;
			$url = "?".http_build_query($args);
			header("Location: ".$url);
			//header("Location: index.php?act=orders&scroll=".$scroll);
			//header("Location: ".$_SERVER['HTTP_REFERER']."&scroll=".$scroll);
			exit;
		} else {
			CheckPrivileges("orders_list");
			$t2 = new Template("inc/orders.html");

			if(in_array("orders_list_current", $privileges)) {
				$privileges_where = " AND orders.added_user = '".$_SESSION['user_id']."'";
			} else {
				$privileges_where = "";
			}

			if(isset($_GET['search']) && mb_strlen($_GET['search'])>0) {
				$keywords = explode(" ", trim($_GET['search']));
				foreach($keywords as $key=>$word) {
					$word = makepost($word);
					$search .= ' AND concat(`users`.`name`,IFNULL(`color`, \'\'),`notes`,`orders`.`buyer_names`, `buyer_email`, `payment_method`, countries.name, `post_code`, `city`, `address`, `orders`.`phone`, (SELECT GROUP_CONCAT(products.code) FROM orders_details INNER JOIN products ON orders_details.product = products.id WHERE orders_details.order_id = orders.id)) LIKE "%' . $word . '%"';
				}
				$search_string = $_GET['search'];
			}
			if($_POST['csv']) {
				$t3 = new Template("inc/csv.html");
				foreach($_POST['orders'] as $order=>$status) {
					$order_query = "	SELECT
											t1.*,
											(
												SELECT GROUP_CONCAT(t3.code ORDER BY t2.id SEPARATOR ', ' ) 
												FROM orders_details t2
												LEFT JOIN products t3 ON t2.product = t3.id
												WHERE t2.order_id = t1.id
											) AS products,
											countries.iso_code
										FROM orders t1
										LEFT JOIN countries ON t1.country = countries.id
										LEFT JOIN users ON t1.added_user = users.id
										WHERE t1.id = '".intval($order)."'
										LIMIT 1
					";
					$order_result = mysql_query($order_query);
					$order_row = mysql_fetch_assoc($order_result);
					$t3->gotoNext("orders");
					$t3->setVar("orders.SndrTel", $SndrTel);
					$t3->setVar("orders.SndrName", $SndrName);
					$t3->setVar("orders.SndrSiteType", $SndrSiteType);
					$t3->setVar("orders.SndrSiteName", $SndrSiteName);
					$t3->setVar("orders.SndrStreetType", $SndrStreetType);
					$t3->setVar("orders.SndrStreetName", $SndrStreetName);
					$t3->setVar("orders.SndrStreetNo", $SndrStreetNo);
					$t3->setVar("orders.SndrEntrance", $SndrEntrance);
					$t3->setVar("orders.RcptTel", $order_row['phone']);
					$t3->setVar("orders.RcptName", $order_row['buyer_names']);
					$t3->setVar("orders.RcptCountryId_ISO", $order_row['iso_code']);
					$t3->setVar("orders.RcptSiteName", $order_row['city']);
					$t3->setVar("orders.RcptPostCode", $order_row['post_code']);
					// ako adresut e v chujbina
					if($order_row['iso_code']=="100") {
						$t3->setVar("orders.RcptStreetName", $order_row['address']);
					} else {
						$t3->setVar("orders.RcptFrnAddressLine1", $order_row['address']);
					}	
					$t3->setVar("orders.Contents", $order_row['products']);
					mysql_free_result($order_result);
				}
				header('Content-Type: text/csv; charset=utf-8');
				header('Content-Disposition: attachment; filename=data.csv');
				echo "\xEF\xBB\xBF";
				print $t3->toString();
				exit;
			} elseif($_POST['color']) {
				CheckPrivileges("orders_color");
				$colors = array('green','red','gray','purple');
				if(in_array($_POST['colorpick'],$colors)) {
					$color = mysql_real_escape_string($_POST['colorpick']);
					mysql_query("START TRANSACTION");
					$success = true;
					foreach($_POST['orders'] as $id=>$status) {
					
						$query ="SELECT color FROM `orders` WHERE `id` = '".$id."' LIMIT 1";
						$result = mysql_query($query);
						$row = mysql_fetch_assoc($result);
						if($row['color'] == $color) {
							$update_color = 'NULL';
						} else {
							$update_color = "'".$color."'";
						}

						if($row['color']=="green" && $color!="green") {
							// ako e bilo zeleno i natisnem nqkakav drug cvqt
							$order_details_query = "SELECT product,size,quantity FROM orders_details WHERE order_id = '".$id."'";
							$order_details_result = mysql_query($order_details_query);
							while($order_details_row = mysql_fetch_assoc($order_details_result)) {
								if(!mysql_query("UPDATE products_quantities SET quantity = quantity + ".$order_details_row['quantity']." WHERE product = '".$order_details_row['product']."' AND size = '".$order_details_row['size']."' LIMIT 1;")) {
									$success = false;
								}
							}
						} elseif($row['color']!="green" && $color=="green") {
							// ako ne e bilo zeleno i natisnem zelenoto
							$order_details_query = "SELECT product,size,quantity FROM orders_details WHERE order_id = '".$id."'";
							$order_details_result = mysql_query($order_details_query);
							while($order_details_row = mysql_fetch_assoc($order_details_result)) {
								if(!mysql_query("UPDATE products_quantities SET quantity = quantity - ".$order_details_row['quantity']." WHERE product = '".$order_details_row['product']."' AND size = '".$order_details_row['size']."' LIMIT 1;")) {
									$success = false;
								}
							}
						}
						if(!mysql_query("UPDATE orders SET color = ".$update_color." WHERE id = '".$id."' LIMIT 1")) {
							$success = false;
						}
						if($success==true) {
							mysql_query("COMMIT");
							Logger("Orders","Order color","edit",array('Order №'=>$id,'Color'=>$update_color));
							$_SESSION['message']['type'] = "success";
							$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_EDIT'];
						} else {
							$_SESSION['message']['type'] = "error";
							$_SESSION['message']['msg'] = mysql_error().$MSG[$lang]['MSG_ERROR_EDIT'];
						}
					}
				}
				header("Location: index.php?act=orders");
				exit;
			}

			$count_query = "	SELECT COUNT(*) AS `count`
								FROM `orders`
								LEFT JOIN `countries` ON `countries`.`id` = `orders`.`country`
								WHERE 1=1
								".$search."
			";
			$count_result = mysql_query($count_query);
			$count_row = mysql_fetch_assoc($count_result);

			$pages = new Paginator;
			$pages->items_total = $count_row['count'];
			$pages->items_per_page = $results_per_page;
			$pages->paginate();

			$color_codes = array(
				"green" => "#99E699",
				"red" => "#FF9980",
				"gray" => "#999999",
				"purple" => "#CC80FF",
				"darkred" => "#8B0000",
			);
			$query ="	SELECT
							`orders`.*,
							w1.buyer_names AS w_buyer_names,
							w2.phone AS w_phone,
							b1.buyer_names AS b_buyer_names,
							b2.phone AS b_phone,
							DATE_FORMAT(`orders`.`added`,'%d.%m') AS `added`,
							countries.name AS `country`,
							users.name AS `added_user`
						FROM `orders`
						LEFT JOIN `countries` ON `countries`.`id` = `orders`.`country`
						LEFT JOIN `users` ON `users`.`id` = `orders`.`added_user`
						LEFT JOIN WhiteList w1 ON `orders`.`buyer_names` = w1.`buyer_names`
						LEFT JOIN WhiteList w2 ON `orders`.`phone` = w2.`phone`
						LEFT JOIN BlackList b1 ON `orders`.`buyer_names` = b1.`buyer_names`
						LEFT JOIN BlackList b2 ON `orders`.`phone` = b2.`phone`
						WHERE 1=1
						".$search."
						".$privileges_where."
						GROUP BY `orders`.`id`
						ORDER BY `orders`.`id` DESC
			".$pages->limit;
			
			$result = mysql_query($query) or die(mysql_error());
			$tabindex = 1;
			
			while ($row = mysql_fetch_assoc($result)) {
			
				$t2->gotoNext("orders");
				$t2->setVar("orders.tabindex", $tabindex);
				$t2->setVar("orders.id", $row['id']);
				$t2->setVar("orders.added", $row['added']);
				if($row['color']!=NULL) {
					$t2->setVar("orders.color", " style='background-color: ".$color_codes[$row['color']]."'");
				}
				if($row['buyer_names']==$row['phone']) {
					$t2->setVar("orders.color", " style='background-color: ".$color_codes['darkred']."'");
				}
				$t2->setVar("orders.added_user", mb_substr($row['added_user'],0,1));
				$t2->setVar("orders.added_user_full", $row['added_user']);
				$t2->setVar("orders.buyer_names", $row['buyer_names']);
				$t2->setVar("orders.w_buyer_names", $row['w_buyer_names']);
				$t2->setVar("orders.b_buyer_names", $row['b_buyer_names']);
				$t2->setVar("orders.buyer_email", $row['buyer_email']);
				$t2->setVar("orders.phone", $row['phone']);
				$t2->setVar("orders.w_phone", $row['w_phone']);
				$t2->setVar("orders.b_phone", $row['b_phone']);
				$t2->setVar("orders.products", $row['products']);
				$t2->setVar("orders.payment_method", $row['payment_method']);
				$t2->setVar("orders.country", $row['country']);
				$t2->setVar("orders.post_code", $row['post_code']);
				$t2->setVar("orders.city", $row['city']);
				$t2->setVar("orders.address", $row['address']);
				$t2->setVar("orders.delivery_costs", $row['delivery_costs']);
				$t2->setVar("orders.discount", $row['discount']);
				$t2->setVar("orders.total", GetTotal($row['id']));
				$notes = nl2br(str_replace('\\r\\n', "\r\n", $row['notes']));
				$t2->setVar("orders.notes", $notes);
				$t2->setVar("orders.done", $row['done']);
				$t2->setVar("orders.search", $search_string);
				$products_query ="	SELECT
										`orders_details`.id AS order_detail_id,
										`products`.*,
										`sizes`.`name` AS `size`,
										`orders_details`.`quantity`,
										`orders_details`.`notified`
									FROM `orders_details`
									LEFT JOIN `products` ON `orders_details`.`product` = `products`.`id`
									LEFT JOIN `sizes` ON `orders_details`.`size` = `sizes`.`id`
									WHERE `orders_details`.`order_id` = '".$row['id']."'
				";
				$products_result = mysql_query($products_query) or die(mysql_error());
				while ($products_row = mysql_fetch_assoc($products_result)) {
					$t2->gotoNext("orders.products");
					$t2->setVar("orders.products.order_detail_id", $products_row['order_detail_id']);
					$t2->setVar("orders.products.id", $products_row['id']);
					$t2->setVar("orders.products.code", $products_row['code']);
					$t2->setVar("orders.products.name", $products_row['name']);
					$t2->setVar("orders.products.size", $products_row['size']);
					$t2->setVar("orders.products.notified", $products_row['notified']);
					$t2->setVar("orders.products.quantity", $products_row['quantity']);
				}
				$tabindex++;
				foreach($privileges as $key=>$privilege) {$t2->setVar("orders.menu_".$privilege, " ");}
			}
			
			mysql_free_result($result);
			$t2->setVar("search", $search_string);
			$t2->setVar("pagination", $pages->display_pages());
			$content .= $t2->toString();
		}
		$t->setVar("content", $content);
	} elseif($act=="orders_new") {
		CheckPrivileges("orders_new");
		$t->setVar("where", "New order");
		$t2 = new Template("inc/orders_new.html");

		if(isset($_POST['submit'])) {
			$_POST = makepost($_POST);
			if($_POST['delivery_costs']=="0") {
				$_POST['delivery_costs'] = "0.00";
			}
			field_validator("Names", $_POST['buyer_names'], "string", 3, 100, 1);
			field_validator("E-Mail", $_POST['buyer_email'], "string", 3, 100, 0);
			field_validator("Postal code", $_POST['post_code'], "string", 3, 100, 1);
			field_validator("Phone", $_POST['phone'], "string", 3, 100, 1);
			field_validator("City", $_POST['city'], "string", 3, 100, 1);
			field_validator("Address", $_POST['address'], "string", 3, 100, 1);
			field_validator("Delivery costs", $_POST['delivery_costs'], "string", 1, 10, 1);
			field_validator("Discount", $_POST['discount'], "string", 0, 2, 0);

			if(count($_POST['products'])==0) {
				$messages[] = "Choose a product.";
			} else {
				$quantities = 0;
				foreach($_POST['quantities'] as $size => $arr) {
					foreach($arr as $key1=>$quantity) {
						$quantities += $quantity;
					}
				}
				if($quantities == 0) {
					$messages[] = "Choose more than one quantity.";
				}
			}

			if(count($messages)>0) {
				$t->setVar("error", show_array($messages));

				$t2->setVar("buyer_names", $_POST['buyer_names']);
				$t2->setVar("buyer_email", $_POST['buyer_email']);
				$t2->setVar("payment_method", $_POST['payment_method']);
				$t2->setVar("country", $_POST['country']);
				$t2->setVar("post_code", $_POST['post_code']);
				$t2->setVar("city", $_POST['city']);
				$t2->setVar("address", $_POST['address']);
				$t2->setVar("delivery_costs", $_POST['delivery_costs']);
				$t2->setVar("phone", $_POST['phone']);
				$t2->setVar("discount", $_POST['discount']);
				$t2->setVar("notes", str_replace('\\r\\n', "\r\n", $_POST['notes']));

				$sizes_query ="SELECT * FROM `sizes`";
				$sizes_result = mysql_query($sizes_query) or die(mysql_error());
				$sizes = array();
				while ($sizes_row = mysql_fetch_assoc($sizes_result)) {
					$sizes[] = $sizes_row;
				}
				mysql_free_result($sizes_result);

				if(isset($_POST['products']) && count($_POST['products'])>0 && is_array($_POST['products'])) {
					$i=1;
					foreach($_POST['products'] as $key=>$product) {
						$product_query = "	SELECT
												products.*,
												products_pictures.file,
												products_prices.price,
												countries.currency
											FROM products
											LEFT JOIN products_pictures ON products.id = products_pictures.product
											LEFT JOIN products_prices ON products.id = products_prices.product
											LEFT JOIN countries ON countries.id = products_prices.country
											WHERE
												products.id = '".intval($product)."'
												AND products_pictures.main = '1'
												AND products_prices.country = '".intval($_POST['country'])."'
											LIMIT 1
						";
						$product_result = mysql_query($product_query) or die(mysql_error());
						$product_row = mysql_fetch_assoc($product_result);
						$t2->gotoNext("post_products");
						$t2->setVar("post_products.i", $i);
						$t2->setVar("post_products.id", $product_row['id']);
						$t2->setVar("post_products.code", $product_row['code']);
						$t2->setVar("post_products.name", $product_row['name']);
						$t2->setVar("post_products.price", $product_row['price']);
						$t2->setVar("post_products.currency", $product_row['currency']);
						$t2->setVar("post_products.file", $product_row['file']);
						foreach($sizes as $key2=>$size) {
							$t2->gotoNext("post_products.post_sizes");
							$t2->setVar("post_products.post_sizes.id", $size['id']);
							$t2->setVar("post_products.post_sizes.name", $size['name']);
							$t2->setVar("post_products.post_sizes.quantity", intval($_POST['quantities'][$size['id']][$key]));
						}
						$i++;
					}
				}
			} else {
				mysql_query("START TRANSACTION");
				$success = true;
				$query = "INSERT INTO `orders` (buyer_names,buyer_email,payment_method,country,post_code,city,address,delivery_costs,phone,notes,discount,added,added_user) VALUES(
							'".makepost($_POST['buyer_names'])."',
							'".makepost($_POST['buyer_email'])."',
							'".makepost($_POST['payment_method'])."',
							'".intval($_POST['country'])."',
							'".makepost($_POST['post_code'])."',
							'".makepost($_POST['city'])."',
							'".makepost($_POST['address'])."',
							'".makepost($_POST['delivery_costs'])."',
							'".makepost($_POST['phone'])."',
							'".makepost($_POST['notes'])."',
							'".intval($_POST['discount'])."',
							NOW(),
							'".intval($_SESSION['user_id'])."'
				)";
				if(!mysql_query($query)) {
					$success = false;
					$t->setVar("error", $MSG[$lang]['MSG_ERROR_ADD']);
				} else {
					$order_id = mysql_insert_id();
					foreach($_POST['products'] as $key=>$product) {
						foreach($_POST['quantities'] as $size => $arr) {
							if($_POST['quantities'][$size][$key] > 0) {
								$product_query = "INSERT INTO orders_details (order_id,product,size,quantity) VALUES('".$order_id."','".intval($product)."','".intval($size)."','".intval($_POST['quantities'][$size][$key])."')";
								if(!mysql_query($product_query)) {
									$success = false;
								}
							}
						}
					}
				}
				if($success==true) {
					mysql_query("COMMIT");
					Logger("Orders","Order","add",array('Buyer names'=>makepost($_POST['buyer_names']),'Buyer E-Mail'=>makepost($_POST['buyer_email'])));
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_ADD'];
				} else {
					mysql_query("ROLLBACK");
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_ADD'];
				}
				header("Location: index.php?act=".$act);
				exit;
			}
		}

		$countries_query ="SELECT * FROM `countries` WHERE `enabled` = '1' ORDER BY `order`";
		$countries_result = mysql_query($countries_query) or die(mysql_error());
		while ($countries_row = mysql_fetch_assoc($countries_result)) {
			$t2->gotoNext("countries");
			$t2->setVar("countries.id", $countries_row['id']);
			$t2->setVar("countries.name", $countries_row['name']);
			if((isset($_POST['country']) && intval($_POST['country'])==$countries_row['id']) || ($_SESSION['country']==$countries_row['id'] && !isset($_POST))) {
				$t2->setVar("countries.selected", " ");
			}
		}
		mysql_free_result($countries_result);

		$products_query = "	SELECT
								`products`.*,
								`products_pictures`.`file`
							FROM `products`
							LEFT JOIN `products_pictures` ON `products`.`id` = `products_pictures`.`product`
							WHERE `products_pictures`.`main` = '1'
							GROUP BY `products`.`id`
		";
		$products_result = mysql_query($products_query) or die(mysql_error());
		while ($products_row = mysql_fetch_assoc($products_result)) {
			$t2->gotoNext("products");
			$t2->setVar("products.id", $products_row['id']);
			$t2->setVar("products.code", $products_row['code']);
			$t2->setVar("products.name", $products_row['name']);
			$t2->setVar("products.file", $products_row['file']);
			$prices = array();
			$prices_query = "SELECT products_prices.*,countries.currency FROM products_prices LEFT JOIN countries ON products_prices.country = countries.id WHERE products_prices.product = '".$products_row['id']."'";
			$prices_result = mysql_query($prices_query) or die(mysql_error());
			while ($prices_row = mysql_fetch_assoc($prices_result)) {
				$prices[] = array('country'=>$prices_row['country'],'price'=>$prices_row['price'],'currency'=>$prices_row['currency']);
			}
			$t2->setVar("products.prices", json_encode($prices));
		}
		mysql_free_result($products_result);

		$sizes_query ="SELECT * FROM `sizes`";
		$sizes_result = mysql_query($sizes_query) or die(mysql_error());
		while ($sizes_row = mysql_fetch_assoc($sizes_result)) {
			$t2->gotoNext("sizes");
			$t2->setVar("sizes.id", $sizes_row['id']);
			$t2->setVar("sizes.name", $sizes_row['name']);
		}
		mysql_free_result($sizes_result);
		
		$content .= $t2->toString();
		$t->setVar("content", $content);
	} elseif($act=="products") {
		CheckPrivileges("products");
		$t->setVar("where", "Products");
		if(isset($_GET['ajax']) && isset($_GET['term']) && mb_strlen($_GET['term'])>=2) {
			$term = mysql_real_escape_string($_GET['term']);
			$query = "	SELECT
							products.id AS id,
							products.in_stock,
							code,
							code AS value,
							code AS label,
							name AS description,
							products_pictures.file AS picture
						FROM `products`
						LEFT JOIN products_pictures ON products_pictures.product = products.id
						WHERE
							products.code LIKE '%".$term."%'
						LIMIT 10
			";
			$result = mysql_query($query);
			$products = array();
			while($row = mysql_fetch_assoc($result)) {

				$prices = array();
				$prices_query = "SELECT products_prices.*,countries.currency FROM products_prices LEFT JOIN countries ON products_prices.country = countries.id WHERE products_prices.product = '".$row['id']."'";
				$prices_result = mysql_query($prices_query) or die(mysql_error());
				while ($prices_row = mysql_fetch_assoc($prices_result)) {
					$prices[] = array('country'=>$prices_row['country'],'price'=>$prices_row['price'],'currency'=>$prices_row['currency']);
				}
				$row['prices'] = json_encode($prices);

				$quantities = array();
				$quantities_query = "SELECT products_quantities.*,sizes.name FROM products_quantities LEFT JOIN sizes ON products_quantities.size = sizes.id WHERE products_quantities.product = '".$row['id']."' AND quantity > 0";
				$quantities_result = mysql_query($quantities_query) or die(mysql_error());
				while ($quantities_row = mysql_fetch_assoc($quantities_result)) {
					$quantities[] = array('size'=>$quantities_row['name'],'quantity'=>$quantities_row['quantity']);
				}
				$row['quantities'] = json_encode($quantities);

				if($row['in_stock']==1) {
					$row['in_stock'] = "<span style='color: green;'>Yes</span>";
				} else {
					$row['in_stock'] = "<span style='color: red;'>No</span>";
				}

				$products[] = $row;
			}
			echo json_encode($products);
			exit;
		} elseif(isset($_GET['load_quantity'])) {
			CheckPrivileges("products_edit");
			$str = explode("_",$_GET['id']);
			$id = intval($str[1]);
			if($id>0) {
				$query = "	SELECT
								quantity
							FROM `products_quantities`
							WHERE
								id = '".$id."'
							LIMIT 1
				";
				$result = mysql_query($query) or die(mysql_error());
				$row = mysql_fetch_assoc($result);
				echo $row['quantity'];
			}
			exit;
		} elseif(isset($_GET['save_quantity'])) {
			CheckPrivileges("products_edit");
			$str = explode("_",$_POST['id']);
			$id = intval($str[1]);
			$quantity = mysql_real_escape_string($_POST['value']);
			if($id>0) {
				$query = "UPDATE products_quantities SET quantity = '".$quantity."' WHERE id = '".$id."' LIMIT 1";
				$result = mysql_query($query);
				Logger("Products","Product quantity","edit",array('Product №'=>$id,'Quantity'=>$quantity));
				echo intval($_POST['value']);
			}
			exit;
		} elseif(isset($_GET['load_min_quantity'])) {
			CheckPrivileges("products_edit");
			$str = explode("_",$_GET['id']);
			$id = intval($str[1]);
			if($id>0) {
				$query = "	SELECT
								min_quantity
							FROM `products_quantities`
							WHERE
								id = '".$id."'
							LIMIT 1
				";
				$result = mysql_query($query) or die(mysql_error());
				$row = mysql_fetch_assoc($result);
				echo $row['min_quantity'];
			}
			exit;
		} elseif(isset($_GET['save_min_quantity'])) {
			CheckPrivileges("products_edit");
			$str = explode("_",$_POST['id']);
			$id = intval($str[1]);
			if($id>0) {
				$query = "UPDATE products_quantities SET min_quantity = '".mysql_real_escape_string($_POST['value'])."' WHERE id = '".$id."' LIMIT 1";
				$result = mysql_query($query);
				Logger("Products","Product minimum quantity","edit",array('Product №'=>$id,'Quantity'=>$quantity));
				echo intval($_POST['value']);
			}
			exit;
		} elseif(isset($_GET['download_pic']) && $id>0) {
			$query = "	SELECT
							`products_pictures`.`file` AS `picture`
						FROM `products`
						LEFT JOIN `suppliers` ON `products`.`supplier` = `suppliers`.`id`
						LEFT JOIN `products_pictures` ON `products_pictures`.`product` = `products`.`id` AND `products_pictures`.`main` = '1'
						WHERE
							`products`.`id` = '".$id."'
						LIMIT 1
			";
			$result = mysql_query($query) or die(mysql_error());
			$row = mysql_fetch_assoc($result);
			header("Content-Type: application/octet-stream");    //
			header("Content-Length: " . filesize($large.$row['picture']));    
			header('Content-Disposition: attachment; filename='.$row['picture']);
			readfile($large.$row['picture']);  
			exit;
		} elseif(isset($_GET['in_stock']) && $id>0) {
			$query ="SELECT * FROM `products` WHERE `id` = '".$id."' LIMIT 1";
			$result = mysql_query($query);
			$row = mysql_fetch_assoc($result);
			if($row['in_stock'] == 0) {
				$in_stock = '1';
			} else {
				$in_stock = '0';
			}
			if(!mysql_query("UPDATE products SET in_stock = '".$in_stock."' WHERE id = '".$id."' LIMIT 1")) {
				$_SESSION['message']['type'] = "error";
				$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_EDIT'];
				$scroll = 0;
			} else {
				Logger("Products","Product in stock","edit",array('Product №'=>$id,'In stock'=>$in_stock));
			}
			header("Location: index.php?act=products&scroll=".$scroll);
			exit;
		} elseif(isset($_GET['delete']) && $id>0) {
			CheckPrivileges("products_delete");

			//deleting pictures
			$query2 ="SELECT * FROM `products_pictures` WHERE `product` = '".$id."'";
			$result2 = mysql_query($query2);
			while ($row2 = mysql_fetch_assoc($result2)) {
				unlink("../upload/thumbs/".$row2['file']);
				unlink("../upload/large/".$row2['file']);
			}

			$query = "DELETE FROM `products` WHERE `id` = '".$id."' LIMIT 1";
			if(mysql_query($query)) {
				Logger("Products","Product","delete",array('Product №'=>$id));
				$_SESSION['message']['type'] = "success";
				$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_DELETE'];
			} else {
				$_SESSION['message']['type'] = "error";
				$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_DELETE'];
			}
			header("Location: ".$_SERVER['HTTP_REFERER']);
			exit;
		} elseif(isset($_GET['edit']) && $id>0) {
			CheckPrivileges("products_edit");
			if (isset($_GET['delpic']) && intval($_GET['delpic'])>0) {
				$picture = intval($_GET['delpic']);
				$query2 ="SELECT * FROM `products_pictures` WHERE `id` = '".$picture."'";
				$result2 = mysql_query($query2);
				while ($row2 = mysql_fetch_assoc($result2)) {
					unlink("../upload/thumbs/".$row2['file']);
					unlink("../upload/large/".$row2['file']);
				}
				if(mysql_query("DELETE FROM `products_pictures` WHERE `id` = '".$picture."'")) {
					Logger("Products","Product picture","delete",array('Product №'=>$id,'Picture №'=>$picture,'Picture filename'=>$row2['file']));
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_DELETE'];
				} else {
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_DELETE'];
				}
				header("Location:".$_SERVER['HTTP_REFERER']);
				exit;
			} elseif (isset($_GET['main']) && intval($_GET['main'])>0) {
				$picture = intval($_GET['main']);
				mysql_query("START TRANSACTION");
				$query1 = "UPDATE `products_pictures` SET `main` = NULL WHERE product = '".$id."'";
				$query2 = "UPDATE `products_pictures` SET `main` = 1 WHERE id = '".$picture."'";
				if(mysql_query($query1) && mysql_query($query2)) {
					mysql_query("COMMIT");
					Logger("Products","Product set main picture","edit",array('Product №'=>$id,'Picture №'=>$picture));
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = "Снимката беше успешно зададена като основна.";
				} else {
					mysql_query("ROLLBACK");
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = "Възникна грешка при задаването на снимката като основна.";
				}
				header("Location:".$_SERVER['HTTP_REFERER']);
				exit;
			}
			if(isset($_POST['submit'])) {
				$_POST = makepost($_POST);

				$success = true;
				mysql_query("START TRANSACTION");

				if($_FILES && $_FILES['error']!=="4") {
					$active_keys = array();
					foreach($_FILES[$fieldname]['name'] as $key => $filename){
						$array = explode(".", $filename); 
						$nr = count($array); 
						$ext = strtolower($array[$nr-1]);
						if(!empty($filename)){
							$active_keys[] = $key;
						}
					}

					foreach($active_keys as $key){
						if($_FILES[$fieldname]['error'][$key] != 0) {
							$errors[] = $_FILES[$fieldname]['tmp_name'][$key].': '.$errors[$_FILES[$fieldname]['error'][$key]];
						}
					}

					foreach($active_keys as $key){
						if(!is_uploaded_file($_FILES[$fieldname]['tmp_name'][$key])) {
							$errors[] = $_FILES[$fieldname]['tmp_name'][$key].' not an HTTP upload';
						}
					}

					foreach($active_keys as $key){
						if(!getimagesize($_FILES[$fieldname]['tmp_name'][$key])) {
							$errors[] = $_FILES[$fieldname]['tmp_name'][$key].' not an image';
						}
					}

					foreach($active_keys as $key){
						$now = time();
						while(file_exists($uploadFilename[$key] = $uploadsDirectory.$now.'-'.$_FILES[$fieldname]['name'][$key])){
							$now++;
						}
					}
					foreach($active_keys as $key) {
						if(!move_uploaded_file($_FILES[$fieldname]['tmp_name'][$key], $uploadFilename[$key])) {
							$errors[] = 'receiving directory insuffiecient permission';
						}
						$pic = $now."-".$_FILES[$fieldname]['name'][$key];

						$img = new imaging;
						$img->set_img($uploadsDirectory.$pic);
						$img->set_quality(80);

						if(!mysql_query("INSERT INTO products_pictures (product,file) VALUES ('".$id."','".$pic."')")) {
							$success = false;
							$errors[] = "Грешка в базата данни.";
							unlink($uploadsDirectory.$pic);
						} else {
							$img->set_size(160);
							$img->save_img($thumbs.$pic);


							$img->set_size(600);
							$img->save_img($large.$pic);

							// Finalize
							$img->clear_cache();

							unlink($uploadFilename[$key]);
						}
					}
				}

				$query = "UPDATE `products`
						SET
							`code` = '".makepost($_POST['code'])."',
							`name` = '".makepost($_POST['name'])."',
							`supplier` = '".intval($_POST['supplier'])."'
						WHERE `id` = '".$id."'";
				if(!mysql_query($query)) {
					$success = false;
				}
				foreach($_POST['quantity'] as $size => $quantity) {
					$update_query = "
										INSERT INTO products_quantities
											(product, size, quantity, min_quantity)
										VALUES
										('".intval($id)."', '".intval($size)."', '".intval($quantity)."', '".intval($_POST['min_quantity'][$size])."')
										ON DUPLICATE KEY UPDATE
											quantity     = '".intval($quantity)."',
											min_quantity     = '".intval($_POST['min_quantity'][$size])."'
					";
					if(!mysql_query($update_query)) {
						$success = false;
					}
				}
				foreach($_POST['prices'] as $country => $price) {
					if(mb_strlen($price) == 0) {
						$price = 0;
					}
					$update_query = "
										INSERT INTO products_prices
											(product, country, price)
										VALUES
										('".intval($id)."', '".intval($country)."', '".mysql_real_escape_string($price)."')
										ON DUPLICATE KEY UPDATE
											price     = '".mysql_real_escape_string($price)."'
					";
					if(!mysql_query($update_query)) {
						$success = false;
					}
				}
				if($success==true) {
					mysql_query("COMMIT");
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_EDIT'];
					Logger("Products","Product","edit",array('Product №'=>$id));
					header("Location: index.php?act=products");
					exit;
				} else {
					mysql_query("ROLLBACK");
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_EDIT'];
				}
			}
			$t2 = new Template("inc/products_edit.html");
			$query ="SELECT * FROM `products` WHERE `id` = '".$id."' LIMIT 1";
			$result = mysql_query($query);
			$row = mysql_fetch_assoc($result);
			$t2->setVar("id", $row['id']);
			$t2->setVar("code", $row['code']);
			$t2->setVar("name", $row['name']);
			$t2->setVar("supplier", $row['supplier']);
			$t2->setVar("price", $row['price']);

			$suppliers_query ="SELECT * FROM `suppliers`";
			$suppliers_result = mysql_query($suppliers_query);
			while($suppliers_row = mysql_fetch_assoc($suppliers_result)) {
				$t2->gotoNext("suppliers");
				$t2->setVar("suppliers.id", $suppliers_row['id']);
				$t2->setVar("suppliers.name", $suppliers_row['name']);
				if($suppliers_row['id']==$row['supplier']) {
					$t2->setVar("suppliers.selected", " ");
				}
			}
			mysql_free_result($result);

			// quantities
			$sizes_query ="SELECT * FROM `sizes`";
			$sizes_result = mysql_query($sizes_query) or die(mysql_error());
			while ($sizes_row = mysql_fetch_assoc($sizes_result)) {
				$sizes[$sizes_row['id']] = $sizes_row['name'];
			}
			mysql_free_result($sizes_result);

			$query ="SELECT * FROM `products_quantities` WHERE `product` = '".$id."'";
			$result = mysql_query($query);
			$quantities = array();
			$min_quantities = array();
			while($row = mysql_fetch_assoc($result)) {
				$quantities[$row['size']] = $row['quantity'];
				$min_quantities[$row['size']] = $row['min_quantity'];
			}
			mysql_free_result($result);

			foreach($sizes as $size_id => $size_name) {
				$t2->gotoNext("quantities");
				$t2->setVar("quantities.size_id", $size_id);
				$t2->setVar("quantities.size", $size_name);
				$t2->setVar("quantities.quantity", intval($quantities[$size_id]));
				$t2->setVar("quantities.min_quantity", intval($min_quantities[$size_id]));
			}

			// prices
			$countries_query ="SELECT * FROM `countries` WHERE enabled = '1'";
			$countries_result = mysql_query($countries_query) or die(mysql_error());
			while ($countries_row = mysql_fetch_assoc($countries_result)) {
				$countries[$countries_row['id']] = $countries_row['name'];
				$currencies[$countries_row['id']] = $countries_row['currency'];
			}
			mysql_free_result($countries_result);

			$query ="SELECT * FROM `products_prices` WHERE `product` = '".$id."'";
			$result = mysql_query($query);
			$prices = array();
			while($row = mysql_fetch_assoc($result)) {
				$prices[$row['country']] = $row['price'];
			}
			mysql_free_result($result);

			foreach($countries as $country_id => $country_name) {
				$t2->gotoNext("prices");
				$t2->setVar("prices.country_id", $country_id);
				$t2->setVar("prices.country", $country_name);
				$t2->setVar("prices.price", $prices[$country_id]);
				$t2->setVar("prices.currency", $currencies[$country_id]);
			}

			// pictures
			$query ="	SELECT
							*
						FROM `products_pictures`
						WHERE `product` = '".$id."'
						ORDER BY `main` DESC, `id`
			";
			$result = mysql_query($query) or die(mysql_error());
			while ($row = mysql_fetch_assoc($result)) {
				$t2->gotoNext("file");
				$t2->setVar("file.id", $id);
				$t2->setVar("file.picture", $row['id']);
				$t2->setVar("file.file", $row['file']);
				$t2->setVar("file.main", intval($row['main']));
			}
			mysql_free_result($result);

			$content .= $t2->toString();
		} else {
			CheckPrivileges("products_list");
			if(isset($_POST['submit'])) {
				$_POST = makepost($_POST);

				$success = true;
				mysql_query("START TRANSACTION");

				$query = "INSERT INTO `products` (supplier,code,name,added) VALUES('".intval($_POST['supplier'])."','".makepost($_POST['code'])."','".makepost($_POST['name'])."',NOW())";
				if(!mysql_query($query)) {
					$success = false;
				}
				$id = mysql_insert_id();

				if($_FILES && $_FILES['error']!=="4") {
					$active_keys = array();
					$first_picture = true;
					foreach($_FILES[$fieldname]['name'] as $key => $filename){
						$array = explode(".", $filename); 
						$nr = count($array); 
						$ext = strtolower($array[$nr-1]);
						if(!empty($filename)){
							$active_keys[] = $key;
						}
					}

					foreach($active_keys as $key){
						if($_FILES[$fieldname]['error'][$key] != 0) {
							$errors[] = $_FILES[$fieldname]['tmp_name'][$key].': '.$errors[$_FILES[$fieldname]['error'][$key]];
						}
					}

					foreach($active_keys as $key){
						if(!is_uploaded_file($_FILES[$fieldname]['tmp_name'][$key])) {
							$errors[] = $_FILES[$fieldname]['tmp_name'][$key].' not an HTTP upload';
						}
					}

					foreach($active_keys as $key){
						if(!getimagesize($_FILES[$fieldname]['tmp_name'][$key])) {
							$errors[] = $_FILES[$fieldname]['tmp_name'][$key].' not an image';
						}
					}

					foreach($active_keys as $key){
						$now = time();
						while(file_exists($uploadFilename[$key] = $uploadsDirectory.$now.'-'.$_FILES[$fieldname]['name'][$key])){
							$now++;
						}
					}
					foreach($active_keys as $key) {
						if(!move_uploaded_file($_FILES[$fieldname]['tmp_name'][$key], $uploadFilename[$key])) {
							$errors[] = 'receiving directory insuffiecient permission';
						}
						$pic = $now."-".$_FILES[$fieldname]['name'][$key];

						$img = new imaging;
						$img->set_img($uploadsDirectory.$pic);
						$img->set_quality(80);

						if($first_picture==true) {
							$main = '1';
						} else {
							$main = '0';
						}
						if(!mysql_query("INSERT INTO products_pictures (product,file,main) VALUES ('".$id."','".$pic."','".$main."')")) {
							$success = false;
							$errors[] = "Грешка в базата данни.";
							unlink($uploadsDirectory.$pic);
						} else {
							$img->set_size(160);
							$img->save_img($thumbs.$pic);


							$img->set_size(600);
							$img->save_img($large.$pic);

							// Finalize
							$img->clear_cache();

							unlink($uploadFilename[$key]);
							$first_picture = false;
						}
					}
				}

				foreach($_POST['quantity'] as $size => $quantity) {
					$update_query = "
										INSERT INTO products_quantities
											(product, size, quantity, min_quantity)
										VALUES
										('".intval($id)."', '".intval($size)."', '".intval($quantity)."', '".intval($_POST['min_quantity'][$size])."')
										ON DUPLICATE KEY UPDATE
											quantity     = '".intval($quantity)."',
											min_quantity     = '".intval($_POST['min_quantity'][$size])."'
					";
					if(!mysql_query($update_query)) {
						$success = false;
					}
				}
				foreach($_POST['prices'] as $country => $price) {
					if(mb_strlen($price) == 0) {
						$price = 0;
					}
					$update_query = "
										INSERT INTO products_prices
											(product, country, price)
										VALUES
										('".intval($id)."', '".intval($country)."', '".mysql_real_escape_string($price)."')
										ON DUPLICATE KEY UPDATE
											price     = '".mysql_real_escape_string($price)."'
					";
					if(!mysql_query($update_query)) {
						$success = false;
					}
				}
				if($success==true) {
					mysql_query("COMMIT");
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_ADD'];
					Logger("Products","Product","add",array('Product №'=>$id));
					header("Location: index.php?act=products");
					exit;
				} else {
					mysql_query("ROLLBACK");
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_ADD'];
				}
			}
			$t2 = new Template("inc/products.html");

			$countries_query = "SELECT * FROM `countries` WHERE enabled = '1'";
			$countries_result = mysql_query($countries_query);
			$countries_count = 0;
			$countries = array();
			while($countries_row = mysql_fetch_assoc($countries_result)) {
				$t2->gotoNext("countries");
				$t2->setVar("countries.name", $countries_row['name']);
				$countries_count++;
				$countries[$countries_row['id']] = $countries_row['currency'];
			}
			$t2->setVar("countries_count", $countries_count);

			$prices_query = "SELECT * FROM `products_prices`";
			$prices_result = mysql_query($prices_query);
			$prices = array();
			while($prices_row = mysql_fetch_assoc($prices_result)) {
				$prices[$prices_row['product']][$prices_row['country']] = $prices_row['price'];
			}

			$sizes_query = "SELECT * FROM `sizes`";
			$sizes_result = mysql_query($sizes_query);
			$sizes_count = 0;
			$sizes = array();
			while($sizes_row = mysql_fetch_assoc($sizes_result)) {
				$t2->gotoNext("sizes");
				$t2->setVar("sizes.name", $sizes_row['name']);
				$t2->gotoNext("sizes2");
				$t2->setVar("sizes2.name", $sizes_row['name']);
				$sizes_count++;
				$sizes[$sizes_row['id']] = $sizes_row['name'];
			}
			$t2->setVar("sizes_count", $sizes_count);

			$quantities_query = "SELECT * FROM `products_quantities`";
			$quantities_result = mysql_query($quantities_query);
			$quantities = array();
			while($quantities_row = mysql_fetch_assoc($quantities_result)) {
				$quantities[$quantities_row['product']][$quantities_row['size']] = array('id'=>$quantities_row['id'],'value'=>$quantities_row['quantity']);
				$min_quantities[$quantities_row['product']][$quantities_row['size']] = array('id'=>$quantities_row['id'],'value'=>$quantities_row['min_quantity']);
			}

			$query = "	SELECT
							`products`.*,
							 DATE_FORMAT(products.added,'%Y-%m-%d') AS added,
							`suppliers`.`name` AS `supplier`,
							`products_pictures`.`file` AS `picture`
						FROM `products`
						LEFT JOIN `suppliers` ON `products`.`supplier` = `suppliers`.`id`
						LEFT JOIN `products_pictures` ON `products_pictures`.`product` = `products`.`id` AND `products_pictures`.`main` = '1'
			";
			$result = mysql_query($query) or die(mysql_error());
			while ($row = mysql_fetch_assoc($result)) {
				$t2->gotoNext("products");
				$t2->setVar("products.id", $row['id']);
				$t2->setVar("products.added", $row['added']);
				$t2->setVar("products.picture", $row['picture']);
				$t2->setVar("products.code", $row['code']);
				$t2->setVar("products.supplier", $row['supplier']);
				$t2->setVar("products.name", $row['name']);
				$t2->setVar("products.in_stock", $row['in_stock']);
				if($row['in_stock']==0) {
					$t2->setVar("products.color", " style='background-color: #FF9980'");
				}
				$quantity = 0;
				foreach($sizes as $size_id=>$size_name) {
					$t2->gotoNext("products.products_sizes");
					$t2->setVar("products.products_sizes.id", $quantities[$row['id']][$size_id]['id']);
					$t2->setVar("products.products_sizes.quantity", $quantities[$row['id']][$size_id]['value']);
					$t2->gotoNext("products.products_sizes2");
					$t2->setVar("products.products_sizes2.id", $min_quantities[$row['id']][$size_id]['id']);
					$t2->setVar("products.products_sizes2.quantity", $min_quantities[$row['id']][$size_id]['value']);
					$quantity += $quantities[$row['id']][$size_id]['value'];
					if(in_array("products_edit",$privileges)) {
						$t2->setVar("products.products_sizes.products_edit", " ");
						$t2->setVar("products.products_sizes2.products_edit", " ");
					}
				}
				foreach($countries as $country_id=>$currency) {
					$t2->gotoNext("products.products_prices");
					$price = $prices[$row['id']][$country_id];
					if($price > 0) {
						$t2->setVar("products.products_prices.price", $price);
						$t2->setVar("products.products_prices.currency", $currency);
					}
				}
				$t2->setVar("products.quantity", $quantity);
				foreach($privileges as $key=>$privilege) {$t2->setVar("products.menu_".$privilege, " ");}
			}
			mysql_free_result($result);

			$suppliers_query ="SELECT * FROM `suppliers`";
			$suppliers_result = mysql_query($suppliers_query);
			while($suppliers_row = mysql_fetch_assoc($suppliers_result)) {
				$t2->gotoNext("suppliers");
				$t2->setVar("suppliers.id", $suppliers_row['id']);
				$t2->setVar("suppliers.name", $suppliers_row['name']);
			}

			// quantities
			$sizes_query ="SELECT * FROM `sizes`";
			$sizes_result = mysql_query($sizes_query) or die(mysql_error());
			while ($sizes_row = mysql_fetch_assoc($sizes_result)) {
				$sizes[$sizes_row['id']] = $sizes_row['name'];
			}
			mysql_free_result($sizes_result);

			$query ="SELECT * FROM `products_quantities` WHERE `product` = '".$id."'";
			$result = mysql_query($query);
			$quantities = array();
			$min_quantities = array();
			while($row = mysql_fetch_assoc($result)) {
				$quantities[$row['size']] = $row['quantity'];
				$min_quantities[$row['size']] = $row['min_quantity'];
			}
			mysql_free_result($result);

			foreach($sizes as $size_id => $size_name) {
				$t2->gotoNext("quantities");
				$t2->setVar("quantities.size_id", $size_id);
				$t2->setVar("quantities.size", $size_name);
				$t2->setVar("quantities.quantity", intval($quantities[$size_id]));
				$t2->setVar("quantities.min_quantity", intval($min_quantities[$size_id]));
			}

			// prices
			$countries_query ="SELECT * FROM `countries` WHERE enabled = '1'";
			$countries_result = mysql_query($countries_query) or die(mysql_error());
			while ($countries_row = mysql_fetch_assoc($countries_result)) {
				$countries[$countries_row['id']] = $countries_row['name'];
				$currencies[$countries_row['id']] = $countries_row['currency'];
			}
			mysql_free_result($countries_result);

			$query ="SELECT * FROM `products_prices` WHERE `product` = '".$id."'";
			$result = mysql_query($query);
			$prices = array();
			while($row = mysql_fetch_assoc($result)) {
				$prices[$row['country']] = $row['price'];
			}
			mysql_free_result($result);

			foreach($countries as $country_id => $country_name) {
				$t2->gotoNext("prices");
				$t2->setVar("prices.country_id", $country_id);
				$t2->setVar("prices.country", $country_name);
				$t2->setVar("prices.price", $prices[$country_id]);
				$t2->setVar("prices.currency", $currencies[$country_id]);
			}

			// pictures
			$query ="	SELECT
							*
						FROM `products_pictures`
						WHERE `product` = '".$id."'
						ORDER BY `main` DESC, `id`
			";
			$result = mysql_query($query) or die(mysql_error());
			while ($row = mysql_fetch_assoc($result)) {
				$t2->gotoNext("file");
				$t2->setVar("file.id", $id);
				$t2->setVar("file.picture", $row['id']);
				$t2->setVar("file.file", $row['file']);
				$t2->setVar("file.main", intval($row['main']));
			}
			mysql_free_result($result);

			$content .= $t2->toString();
		}
		$t->setVar("content", $content);
	} elseif ($act=="reports") {
		CheckPrivileges("reports");
		$t->setVar("where", "Reports");
		$t2 = new Template("inc/reports.html");

		if(isset($_POST['date_from']) || isset($_POST['date_to'])) {
			$_POST = makepost($_POST);
			$t2->setVar("date_from", $_POST['date_from']);
			$t2->setVar("date_to", $_POST['date_to']);

			$date_from = strtotime($_POST['date_from']);
			$date_to = strtotime($_POST['date_to']);
			$date_to += 60*60*24;
			if($date_from && $date_to && $date_to > 60*60*24) {
				if($date_from==$date_to) {
					$where = "AND UNIX_TIMESTAMP(orders.added) BETWEEN ".$date_from." AND ".$date_to;
				} else {
					$where = "AND UNIX_TIMESTAMP(orders.added) BETWEEN ".$date_from." AND ".$date_to;
				}
			} elseif($date_from) {
				$where = "AND UNIX_TIMESTAMP(orders.added) >= ".$date_from;
			} elseif($date_to > 60*60*24) {
				$where = "AND UNIX_TIMESTAMP(orders.added) <= ".$date_to;
			}
		}

		$sizes_query = "SELECT * FROM `sizes`";
		$sizes_result = mysql_query($sizes_query);
		$sizes_count = 0;
		$sizes = array();
		while($sizes_row = mysql_fetch_assoc($sizes_result)) {
			$t2->gotoNext("sizes");
			$t2->setVar("sizes.name", $sizes_row['name']);
			$t2->gotoNext("sizes2");
			$t2->setVar("sizes2.name", $sizes_row['name']);
			$sizes_count++;
			$sizes[$sizes_row['id']] = $sizes_row['name'];
		}
		$t2->setVar("sizes_count", $sizes_count);

		$quantities_query = "SELECT * FROM `products_quantities`";
		$quantities_result = mysql_query($quantities_query);
		$quantities = array();
		while($quantities_row = mysql_fetch_assoc($quantities_result)) {
			$quantities[$quantities_row['product']][$quantities_row['size']] = array('id'=>$quantities_row['id'],'value'=>$quantities_row['quantity']);
		}

		$products_query ="	SELECT
								`orders_details`.`id` AS `detail_id`,
								`products`.`id`,
								`products`.`code`,
								`products`.`name`,
								`orders_details`.`size`,
								`sizes`.`name` AS `size_name`,
								`products_pictures`.`file`,
								COUNT(`orders_details`.`product`) AS `ordered`,
								`suppliers`.`name` AS `supplier_name`,
								orders.color
							FROM `orders_details`
							LEFT JOIN `products` ON `orders_details`.`product` = `products`.`id`
							LEFT JOIN `products_pictures` ON `products_pictures`.`product` = `products`.`id`
							LEFT JOIN `products_quantities` ON `orders_details`.`product` = `products_quantities`.`product` AND `orders_details`.`size` = `products_quantities`.`size`
							LEFT JOIN `sizes` ON `orders_details`.`size` = `sizes`.`id`
							LEFT JOIN `orders` ON `orders_details`.`order_id` = `orders`.`id`
							LEFT JOIN `suppliers` ON `products`.`supplier` = `suppliers`.`id`
							WHERE
								`orders_details`.`notified` = '0'
								AND ((`orders`.`color` != 'gray' AND `orders`.`color` != 'green') OR `orders`.`color` IS NULL)
								".$where."
							GROUP BY
								`products`.`id`,
								`orders_details`.`size`
							ORDER BY
								`products`.`code`,
								`orders_details`.`size`
		";
		$products_result = mysql_query($products_query) or die(mysql_error());
		while($products_row = mysql_fetch_assoc($products_result)) {
			$quantity = $quantities[$products_row['id']][$products_row['size']]['value'];
			if($products_row['ordered'] > $quantity) {
				$t2->gotoNext("products");
				$t2->setVar("products.id", $products_row['id']);
				$t2->setVar("products.detail_id", $products_row['detail_id']);
				$t2->setVar("products.code", $products_row['code']);
				$t2->setVar("products.name", $products_row['name']);
				$t2->setVar("products.size_name", $products_row['size_name']);
				$t2->setVar("products.file", $products_row['file']);
				$t2->setVar("products.ordered", $products_row['ordered']);
				$t2->setVar("products.supplier_name", $products_row['supplier_name']);
				$t2->setVar("products.quantity", $quantity);
				$t2->setVar("products.diff", $quantity - $products_row['ordered']);
				$t2->setVar("products.absolute", $absolute);
			}
		}

		$content .= $t2->toString();
		$t->setVar("content", $content);
	} elseif ($act=="email") {
		CheckPrivileges("email");
		$t->setVar("where", "E-Mail");

		$content = "";

		if($_POST['send']) {
			// mail start
			require 'phpmailer/PHPMailerAutoload.php';
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 587;
			$mail->SMTPSecure = 'tls';
			$mail->SMTPAuth = true;
			$mail->Username = "alerts@ue-varna.bg";
			$mail->Password = "\$kak@uec";
			$mail->setFrom('alerts@ue-varna.bg', 'Petar Dimitrov');
			$mail->addReplyTo('alerts@ue-varna.bg', 'Petar Dimitrov');
			$mail->Subject = 'Dresses E-Mail';
			$mail->AltBody = 'This is a plain-text message body';

			$suppliers_query ="SELECT id,name,email FROM `suppliers`";
			$suppliers_result = mysql_query($suppliers_query);
			while($suppliers_row = mysql_fetch_assoc($suppliers_result)) {
				$t2 = new Template("inc/email.html");
				$products_query ="	SELECT
										`products`.`id`,
										`products`.`code`,
										`products`.`name`,
										`products_pictures`.`file`
									FROM `orders_details`
									LEFT JOIN `products` ON `orders_details`.`product` = `products`.`id`
									LEFT JOIN `products_pictures` ON `products_pictures`.`product` = `products`.`id`
									WHERE
										`products`.`supplier` = '".$suppliers_row['id']."'
										AND `products_pictures`.`main` = '1'
										AND `orders_details`.`notified` = '0'
									GROUP BY
										`products`.`id`
				";
				$products_result = mysql_query($products_query) or die(mysql_error());
				$products = 0;
				while($products_row = mysql_fetch_assoc($products_result)) {
					$t2->gotoNext("products");
					$t2->setVar("products.id", $products_row['id']);
					$t2->setVar("products.code", $products_row['code']);
					$t2->setVar("products.name", $products_row['name']);
					$t2->setVar("products.file", $products_row['file']);
					$t2->setVar("products.absolute", $absolute);
					$quantities_query = "	SELECT
												`orders_details`.`quantity`,
												`sizes`.`name` AS `size`
											FROM `orders_details`
											LEFT JOIN `sizes` ON `orders_details`.`size` = `sizes`.`id`
											WHERE
												`orders_details`.`product` = '".$products_row['id']."'
												AND `orders_details`.`notified` = '0'
					";
					$quantities_result = mysql_query($quantities_query) or die(mysql_error());
					while($quantities_row = mysql_fetch_assoc($quantities_result)) {
						$t2->gotoNext("products.quantities");
						$t2->setVar("products.quantities.size", $quantities_row['size']);
						$t2->setVar("products.quantities.quantity", $quantities_row['quantity']);
					}
					mysql_query("UPDATE `orders_details` SET `notified` = '1' WHERE `product` = '".$products_row['id']."';");
					$products++;
				}
				$email_content = $t2->toString();
				if($products>0) {
					$mail->addAddress($suppliers_row['email'], $suppliers_row['name']);
					$mail->msgHTML($email_content);
					if (!$mail->send()) {
						$_SESSION['message']['type'] = "error";
						$_SESSION['message']['msg'] = "Mailer Error: " . $mail->ErrorInfo;
					} else {
						$_SESSION['message']['type'] = "success";
						$_SESSION['message']['msg'] = "Mailer success.";
					}
					// mail end
				} else {
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = "No new orders.";
				}
			}
			header("Location: ".$_SERVER['HTTP_REFERER']);
			exit;
		} elseif($_POST['reset']) {
			$reset_query = "UPDATE `orders_details` SET `notified` = '0'";
			mysql_query($reset_query);
			Logger("Email","Email reset","edit");
			$_SESSION['message']['type'] = "success";
			$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_EDIT'];
			header("Location: ".$_SERVER['HTTP_REFERER']);
			exit;
		}


		$content .= "<form method='post'><div>
			<input type='submit' name='reset' class='button' value='Нулирай уведомленията' />
			<input type='submit' name='send' class='button' value='Изпрати имейл' />
		</div></form>";
		$t->setVar("content", $content);
	} elseif($act=="roles") {
		CheckPrivileges("roles");
		$t->setVar("where", "Roles");
		if(isset($_GET['id'])) { $id = intval($_GET['id']); } else { $id = 0; }
		if(isset($_GET['delete']) && $id>0) {
			CheckPrivileges("roles_delete");
			$token = isset($_GET['token']) ? $_GET['token'] : '';
			if (!isset($_SESSION['token']) || ($token !== $_SESSION['token'])) {
				$t->setVar("error", "Грешен идентификатор.");
			} else {
				$role_query = "
					SELECT
						`left`,
						`right`,
						`name`,
						`title`
					FROM `users_roles`
					WHERE `id` = '".$id."'
					LIMIT 1
				";
				$role_result = mysql_query($role_query);
				$row = mysql_fetch_assoc($role_result);

				$diff = $row['right'] - $row['left'];
				$minus = $diff + 1;

				mysql_query("DELETE FROM `users_roles` WHERE (`left` > '".$row['left']."' AND `right` < '".$row['right']."')");
				mysql_query("UPDATE `users_roles` SET `left` = `left` - ".$minus." WHERE `left` > ".$row['left']."");
				mysql_query("UPDATE `users_roles` SET `right` = `right` - ".$minus." WHERE `right` > ".$row['right']."");
				mysql_query("DELETE FROM `users_privileges` WHERE role = '".$id."'");
				if(!mysql_query("DELETE FROM `users_roles` WHERE id = '".$id."' LIMIT 1")) {
					$t->setVar("error", "Грешка при изтриване ролята.");
				} else {
					Logger("User roles","User role","delete", array('Role №'=>$id,'Role name'=>$row['name']));
					header("Location: index.php?act=roles");
					exit;
				}
			}
		} elseif(isset($_GET['edit']) && $id>0) {
			CheckPrivileges("roles_edit");
			if($_POST) {
				$_POST = makepost($_POST);
				field_validator("Name", $_POST['title'], "string", 3, 64, 1);
				field_validator("Alias", $_POST['name'], "string", 3, 64, 1);
				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					$update_query = "	UPDATE
											`users_roles`
										SET
											title = '".$_POST['title']."',
											name = '".$_POST['name']."'
										WHERE
											`id` = '".$id."'
										LIMIT 1
					";
					if(!mysql_query($update_query)) {
						$t->setVar("error", "Възникна грешка при редактиране.");
					} else {
						Logger("User roles","User role","edit", array('Role №'=>$id));
						header("Location: index.php?act=roles");
						exit;
					}
				}
			}
			$t2 = new Template("inc/roles_edit.html");
			$query ="SELECT * FROM `users_roles` WHERE `id` = '".$id."' LIMIT 1";
			$result = mysql_query($query);
			$row = mysql_fetch_array($result);
			$t2->setVar("id", $row['id']);
			$t2->setVar("title", $row['title']);
			$t2->setVar("name", $row['name']);
			mysql_free_result($result);
			$content .= $t2->toString();
		} else {
			CheckPrivileges("roles");
			if(isset($_POST['submit'])) {
				CheckPrivileges("roles_add");
				$_POST = makepost($_POST);
				field_validator("Name", $_POST['title'], "string", 3, 64, 1);
				field_validator("Alias", $_POST['name'], "string", 3, 64, 1);
				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					if ($_POST['parent'] > 0) {
						$rgt_query = "SELECT `left`, `right` FROM `users_roles` WHERE `id` = '".intval($_POST['parent'])."' LIMIT 1";
						$res = mysql_query($rgt_query) or die(mysql_error());
						$rgt_row = mysql_fetch_assoc($res);
						$left = $rgt_row['right'] + 1;
						$right = $rgt_row['right'] + 1;
						mysql_query("UPDATE `users_roles` SET `left` = `left` + 2 WHERE `left` >= ".$left."");
						mysql_query("UPDATE `users_roles` SET `right` = `right` + 2 WHERE `right` >= ".$rgt_row['right']."");
						if(!mysql_query("INSERT INTO `users_roles` (`name`, `title`,`left`, `right`) VALUES ('".$_POST['name']."','".$_POST['title']."','".$rgt_row['right']."','".$right."')")) {
							$t->setVar("error", "Възникна грешка при добавяне.");
						} else {
							Logger("User roles","User role","add", array('Role name'=>$_POST['name']));
							header("Location: index.php?act=roles");
							exit;
						}
					} else {
						if($_POST['nextto'] == 0) {
							$max_rgt_query = "SELECT MAX(`right`) FROM `users_roles`";
							$max_rgt_result = mysql_query($max_rgt_query);
							$max_rgt_row = mysql_fetch_assoc($max_rgt_result);
							$max_right = $max_rgt_row['MAX(`right`)'];
							$left = $max_right + 1;
							$right = $max_right + 2;
							if(!mysql_query("INSERT INTO `users_roles` (`name`, `title`, `left`, `right`) VALUES ('".$_POST['name']."', '".$_POST['title']."', '".$left."', '".$right."')")) {
								$t->setVar("error", "Възникна грешка при добавяне.");
							} else {
								Logger("User roles","User role","add", array('Role name'=>$_POST['name']));
								header("Location: index.php?act=roles");
								exit;
							}
						} else {
							$rgt_query = "SELECT `right` FROM `users_roles` WHERE id = '".intval($_POST['nextto'])."'";
							$rgt_result = mysql_query($rgt_query);
							$rgt_row = mysql_fetch_assoc($rgt_result);
							$right_current = $rgt_row['right'];
							$left = $right_current+1;
							$right = $right_current+2;
							mysql_query("UPDATE `users_roles` SET `left` = `left` + 2 WHERE `left` >= ".$left."");
							mysql_query("UPDATE `users_roles` SET `right` = `right` + 2 WHERE `right` > ".$rgt_row['right']."");
							if(!mysql_query("INSERT INTO `users_roles` (`name`, `title`, `left`, `right`) VALUES('".SlashString($_POST['name'])."','".SlashString($_POST['title'])."','".$left."','".$right."')")) {
								$t->setVar("error", "Възникна грешка при добавяне.");
							} else {
								Logger("User roles","User role","add", array('Role name'=>$_POST['name']));
								header("Location: index.php?act=roles");
								exit;
							}
						}
					}
				}
			}
			$t2 = new Template("inc/roles.html");
			$query = "
				SELECT
					`users_roles`.*,
					CONCAT( REPEAT('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', COUNT(parent.`title`) - 1), `users_roles`.`title`) AS title
				FROM
					`users_roles`,
					`users_roles` AS parent
				WHERE `users_roles`.`left` BETWEEN parent.`left` AND parent.`right`
				GROUP BY `users_roles`.`name`
				ORDER BY `users_roles`.`left`
			";
			$result = mysql_query($query);
			while ($row = mysql_fetch_array($result)) {
				$t2->gotoNext("roles");
				$t2->setVar("roles.id", $row['id']);
				$t2->setVar("roles.title", $row['title']);
				$t2->setVar("roles.name", $row['name']);
				$t2->setVar("roles.token", $token);

				$t2->gotoNext("roles2");
				$t2->setVar("roles2.id", $row['id']);
				$t2->setVar("roles2.title", $row['title']);
				$t2->setVar("roles2.name", $row['name']);

				$t2->gotoNext("roles3");
				$t2->setVar("roles3.id", $row['id']);
				$t2->setVar("roles3.title", $row['title']);
				$t2->setVar("roles3.name", $row['name']);
			}
			mysql_free_result($result);
			$content .= $t2->toString();
		}
		$t->setVar("content", $content);
	} elseif($act=="users") {
		$t->setVar("where", "Users");
		if(isset($_GET['id'])) { $id = intval($_GET['id']); } else { $id = 0; }
		if(isset($_GET['delete']) && $id>0) {
			CheckPrivileges("users_delete");
			$token = isset($_GET['token']) ? $_GET['token'] : '';
			if (!isset($_SESSION['token']) || ($token !== $_SESSION['token'])) {
				$t->setVar("error", "Грешен идентификатор.");
			} else {
				$delete_privileges_query = "DELETE FROM users_privileges WHERE user = '".$id."'";
				if(!mysql_query($delete_privileges_query)) {
					$t->setVar("error", "Грешка при изтриване на привилегиите на потребителя.");
				} else {
					$delete_query = "DELETE FROM users WHERE id = '".$id."' LIMIT 1";
					if(!mysql_query($delete_query)) {
						$t->setVar("error", "Грешка при изтриване на потребителя.");
					} else {
						Logger("Users","User","delete", array('User №'=>$id));
						header("Location: index.php?act=users");
						exit;
					}
				}
			}
		} elseif(isset($_GET['edit']) && $id>0) {
			CheckPrivileges("users_edit");
			if($_POST) {
				$_POST = makepost($_POST);
				field_validator("Username", $_POST['username'], "string", 0, 16, 1);
				field_validator("Names", $_POST['name'], "string", 0, 75, 1);
				field_validator("E-Mail", $_POST['email'], "email", 0, 50, 1);
				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					$contact = base64_encode(serialize($_POST['contact']));
					$update_query = "	UPDATE
											`users`
										SET
											`group` = '".intval($_POST['group'])."',
											`country` = '".intval($_POST['country'])."',
											username = '".mysql_real_escape_string($_POST['username'])."',
											name = '".mysql_real_escape_string($_POST['name'])."',
											last_modify_user = '".intval($_SESSION['user_id'])."',
											last_modify = NOW()
										WHERE
											`id` = '".$id."'
										LIMIT 1
					";
					if(!mysql_query($update_query)) {
						$t->setVar("error", $MSG[$lang]['MSG_ERROR_EDIT']);
					} else {
						Logger("Users","User","edit", array('User №'=>$id));
						$t->setVar("success", $MSG[$lang]['MSG_SUCCESS_EDIT']);
					}
				}
			}
			$t2 = new Template("inc/users_edit.html");
			$query ="SELECT * FROM `users` WHERE `id` = '".$id."' LIMIT 1";
			$result = mysql_query($query);
			$row = mysql_fetch_array($result);
			$t2->setVar("id", $row['id']);
			$t2->setVar("username", $row['username']);
			$t2->setVar("name", $row['name']);
			$t2->setVar("email", $row['email']);
			mysql_free_result($result);

			$users_groups_query ="SELECT * FROM `users_groups`";
			$users_groups_result = mysql_query($users_groups_query);
			while ($users_groups_row = mysql_fetch_array($users_groups_result)) {
				$t2->gotoNext("users_groups");
				$t2->setVar("users_groups.id", $users_groups_row['id']);
				$t2->setVar("users_groups.name", $users_groups_row['name']);
				if($row['group']==$users_groups_row['id']) {
					$t2->setVar("users_groups.selected", " ");
				}
			}
			mysql_free_result($result);

			$countries_query ="SELECT * FROM `countries` WHERE `enabled` = '1' ORDER BY `order`";
			$countries_result = mysql_query($countries_query);
			while ($countries_row = mysql_fetch_array($countries_result)) {
				$t2->gotoNext("countries");
				$t2->setVar("countries.id", $countries_row['id']);
				$t2->setVar("countries.name", $countries_row['name']);
				if($row['country']==$countries_row['id']) {
					$t2->setVar("countries.selected", " ");
				}
			}
			mysql_free_result($countries_result);

			$content .= $t2->toString();
		} elseif(isset($_GET['password']) && $id>0) {
			CheckPrivileges("users_password");
			if($_POST) {
				$_POST = makepost($_POST);
				field_validator("Password", $_POST['password'], "string", 0, 20, 1);
				field_validator("Repeated password", $_POST['password2'], "string", 0, 20, 1);
				if($_POST['password'] != $_POST['password2']) {
					$messages[] = $MSG[$lang]['MSG_ERROR_PASS_MISMATCH'];
				}
				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					$salt = GenSalt();
					$password = SaltPassword($_POST['password'], $salt);
					$update_query = "	UPDATE
											`users`
										SET
											password = '".$password."',
											salt = '".$salt."'
										WHERE
											`id` = '".$id."'
										LIMIT 1
					";
					if(!mysql_query($update_query)) {
						$t->setVar("error", $MSG[$lang]['MSG_ERROR_EDIT']);
					} else {
						Logger("Users","User password","edit", array('User №'=>$id));
						header("Location: index.php?act=users");
						exit;
					}
				}
			}
			$t2 = new Template("inc/users_password.html");
			$content .= $t2->toString();
		} else {
			CheckPrivileges("users");
			if($_POST['submit']) {
				CheckPrivileges("users_add");
				$_POST = makepost($_POST);
				field_validator("Username", $_POST['username'], "string", 0, 16, 1);
				field_validator("Names", $_POST['name'], "string", 0, 75, 1);
				field_validator("E-Mail", $_POST['email'], "email", 0, 50, 1);
				field_validator("Password", $_POST['password'], "string", 0, 20, 1);
				field_validator("Repeated password", $_POST['password2'], "string", 0, 20, 1);
				if($_POST['password'] != $_POST['password2']) {
					$messages[] = $MSG[$lang]['MSG_ERROR_PASS_MISMATCH'];
				}
				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					$salt = GenSalt();
					$password = SaltPassword($_POST['password'], $salt);
					$contact = base64_encode(serialize($_POST['contact']));
					$insert_query = "	INSERT INTO
											`users` (`group`,`country`,`username`,`name`,`email`,`password`,`salt`,`added_user`,`added`)
										VALUES (
											'".intval($_POST['group'])."',
											'".intval($_POST['country'])."',
											'".$_POST['username']."',
											'".$_POST['name']."',
											'".$_POST['email']."',
											'".$password."',
											'".$salt."',
											'".$_SESSION['user_id']."',
											NOW()
										)
					";
					if(!mysql_query($insert_query)) {
						$t->setVar("error", $MSG[$lang]['MSG_ERROR_ADD']);
					} else {
						Logger("Users","User","add", array('Username'=>$_POST['username']));
						header("Location: index.php?act=users");
						exit;
					}
				}
			}
			$t2 = new Template("inc/users.html");
			// there may be a POST request here, let's populate some fields...
			foreach($_POST as $field=>$string) {
				$t2->setVar($field, $string);
			}
			$query ="	SELECT
							`users`.*,
							`users_groups`.`name` AS `group`,
							`countries`.`name` AS `country`
						FROM `users`
						LEFT JOIN `users_groups` ON `users`.`group` = `users_groups`.`id`
						LEFT JOIN `countries` ON `users`.`country` = `countries`.`id`
			";
			$result = mysql_query($query);
			while ($row = mysql_fetch_array($result)) {
				$t2->gotoNext("users");
				$t2->setVar("users.id", $row['id']);
				$t2->setVar("users.group", $row['group']);
				$t2->setVar("users.country", $row['country']);
				$t2->setVar("users.username", $row['username']);
				$t2->setVar("users.name", $row['name']);
				$t2->setVar("users.email", $row['email']);
				$t2->setVar("users.token", $_SESSION['token']);
				// print allowed menues
				foreach($privileges as $key=>$privilege) {$t2->setVar("users.menu_".$privilege, " ");}
			}
			mysql_free_result($result);

			$users_groups_query ="SELECT * FROM `users_groups`";
			$users_groups_result = mysql_query($users_groups_query);
			while ($users_groups_row = mysql_fetch_array($users_groups_result)) {
				$t2->gotoNext("users_groups");
				$t2->setVar("users_groups.id", $users_groups_row['id']);
				$t2->setVar("users_groups.name", $users_groups_row['name']);
			}
			mysql_free_result($users_groups_result);

			$countries_query ="SELECT * FROM `countries` WHERE `enabled` = '1' ORDER BY `order`";
			$countries_result = mysql_query($countries_query);
			while ($countries_row = mysql_fetch_array($countries_result)) {
				$t2->gotoNext("countries");
				$t2->setVar("countries.id", $countries_row['id']);
				$t2->setVar("countries.name", $countries_row['name']);
			}
			mysql_free_result($countries_result);

			$content .= $t2->toString();
		}
		$t->setVar("content", $content);
	} elseif($act=="users_groups") {
		$t->setVar("where", "User groups");
		if(isset($_GET['id'])) { $id = intval($_GET['id']); } else { $id = 0; }
		if(isset($_GET['delete']) && $id>0) {
			CheckPrivileges("users_groups_delete");
			$token = isset($_GET['token']) ? $_GET['token'] : '';
			if (!isset($_SESSION['token']) || ($token !== $_SESSION['token'])) {
				$_SESSION['message']['type'] = "error";
				$_SESSION['message']['msg'] = $MSG[$lang]['wrong_token'];
			} else {
				$delete_query = "DELETE FROM users_groups WHERE id = '".$id."' LIMIT 1";
				if(mysql_query($delete_query)) {
					Logger("User groups","User group","delete", array('Group №'=>$id));
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_DELETE'];
				} else {
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_DELETE'];
				}
			}
			header("Location: ".$_SERVER['HTTP_REFERER']);
			exit;
		} elseif(isset($_GET['edit']) && $id>0) {
			CheckPrivileges("users_groups_edit");
			if($_POST) {
				$_POST = makepost($_POST);

				$success = true;
				mysql_query("START TRANSACTION");

				// Delete all old privileges before add changes
				$delete_privileges_query = "DELETE FROM `users_groups_privileges` WHERE `group` = '".$id."'";
				mysql_query($delete_privileges_query);

				$update_query = "	UPDATE
										`users_groups`
									SET
										name = '".mysql_real_escape_string($_POST['name'])."'
									WHERE
										`id` = '".$id."'
									LIMIT 1
				";
				if(!mysql_query($update_query)) {
					$success = false;
				}

				if(isset($_POST['privileges']) && count($_POST['privileges']) > 0) {
					$group_privileges = '';
					foreach($_POST['privileges'] as $privilege => $yes) {
						$group_privileges .= "('".$id."','".intval($privilege)."'), ";
					}
					$group_privileges = trim($group_privileges, ", ");

					$query = "INSERT INTO `users_groups_privileges` (`group`, `role`) VALUES ".$group_privileges;
					if(!mysql_query($query)) {
						$success = false;
					}
				}

				if($success==false) {
					mysql_query("ROLLBACK");
					$_SESSION['message']['type'] = "error";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_EDIT'];
				} else {
					mysql_query("COMMIT");
					$_SESSION['message']['type'] = "success";
					$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_EDIT'];
					Logger("User groups","User group","edit", array('Group №'=>$id));
					header("Location: ?act=users_groups");
					exit;
				}
			}
			$t2 = new Template("inc/users_groups_edit.html");
			$query ="SELECT * FROM `users_groups` WHERE `id` = '".$id."' LIMIT 1";
			$result = mysql_query($query);
			$row = mysql_fetch_array($result);
			$t2->setVar("id", $row['id']);
			$t2->setVar("name", $row['name']);
			mysql_free_result($result);

			$current_roles_query = "
				SELECT
					`users_groups_privileges`.role
				FROM
					`users_groups_privileges`
				WHERE `users_groups_privileges`.`group` = '".$id."'
			";
			$current_roles_result = mysql_query($current_roles_query);
			$privileges = array();
			while($current_row = mysql_fetch_array($current_roles_result)) {
				$privileges[] = $current_row['role'];
			}
			mysql_free_result($current_roles_result);

			$all_roles_query = "
				SELECT
					`users_roles`.*,
					CONCAT( REPEAT('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', COUNT(parent.`title`) - 1), `users_roles`.`title`) AS title
				FROM
					`users_roles`,
					`users_roles` AS parent
				WHERE `users_roles`.`left` BETWEEN parent.`left` AND parent.`right`
				GROUP BY `users_roles`.`name`
				ORDER BY `users_roles`.`left`
			";
			$all_roles_result = mysql_query($all_roles_query);
			while($row = mysql_fetch_array($all_roles_result)) {
				$t2->gotoNext("privileges");
				$t2->setVar("privileges.id", $row['id']);
				$t2->setVar("privileges.name", $row['name']);
				$t2->setVar("privileges.title", $row['title']);
				if(in_array($row['id'],$privileges)) {
					$t2->setVar("privileges.checked", " checked=\"checked\"");
				}
			}
			mysql_free_result($all_roles_result);

			$content .= $t2->toString();
		} else {
			CheckPrivileges("users_groups");
			if($_POST['submit']) {
				CheckPrivileges("users_groups_add");
				$_POST = makepost($_POST);
				field_validator("Name", $_POST['name'], "string", 0, 100, 1);
				if(count($messages)>0) {
					$t->setVar("error", show_array($messages));
				} else {
					$insert_query = "	INSERT INTO
											`users_groups` (`name`)
										VALUES (
											'".mysql_real_escape_string($_POST['name'])."'
										)
					";
					if(!mysql_query($insert_query)) {
						$_SESSION['message']['type'] = "error";
						$_SESSION['message']['msg'] = $MSG[$lang]['MSG_ERROR_ADD'];
					} else {
						Logger("User groups","User group","add", array('Group name'=>$_POST['name']));
						$_SESSION['message']['type'] = "success";
						$_SESSION['message']['msg'] = $MSG[$lang]['MSG_SUCCESS_ADD'];
						header("Location: index.php?act=users_groups");
						exit;
					}
				}
			}
			$t2 = new Template("inc/users_groups.html");
			// there may be a POST request here, let's populate some fields...
			foreach($_POST as $field=>$string) {
				$t2->setVar($field, $string);
			}
			$query ="SELECT * FROM `users_groups`";
			$result = mysql_query($query);
			while ($row = mysql_fetch_array($result)) {
				$t2->gotoNext("users_groups");
				$t2->setVar("users_groups.id", $row['id']);
				$t2->setVar("users_groups.name", $row['name']);
				$t2->setVar("users_groups.token", $_SESSION['token']);
				// print allowed menues
				foreach($privileges as $key=>$privilege) {$t2->setVar("users_groups.menu_".$privilege, " ");}
			}
			mysql_free_result($result);
			$content .= $t2->toString();
		}
		$t->setVar("content", $content);
	} elseif($act=="statistics") {
		CheckPrivileges("statistics");
		$t->setVar("where", "Statistics");
		$t2 = new Template("inc/statistics.html");

		if(isset($_POST['date_from']) || isset($_POST['date_to'])) {
			$_POST = makepost($_POST);
			$t2->setVar("date_from", $_POST['date_from']);
			$t2->setVar("date_to", $_POST['date_to']);

			$date_from = strtotime($_POST['date_from']);
			$date_to = strtotime($_POST['date_to']);
			$date_to += 60*60*24;
			if($date_from && $date_to && $date_to > 60*60*24) {
				if($date_from==$date_to) {
					$where = "AND UNIX_TIMESTAMP(orders.added) BETWEEN ".$date_from." AND ".$date_to;
				} else {
					$where = "AND UNIX_TIMESTAMP(orders.added) BETWEEN ".$date_from." AND ".$date_to;
				}
			} elseif($date_from) {
				$where = "AND UNIX_TIMESTAMP(orders.added) >= ".$date_from;
			} elseif($date_to > 60*60*24) {
				$where = "AND UNIX_TIMESTAMP(orders.added) <= ".$date_to;
			}
		}

		$users_query = "	SELECT
								orders.color,
								COUNT(orders.color) AS cnt,
								users.name
							FROM orders
							LEFT JOIN users ON orders.added_user = users.id
--							INNER JOIN orders_details ON orders.id = orders_details.order_id
							WHERE 1
							".$where."
							GROUP BY orders.color,users.id
		";
		$users_result = mysql_query($users_query);
		$users = array();
		while($users_row = mysql_fetch_assoc($users_result)) {
			$users[$users_row['name']][$users_row['color']] = $users_row['cnt'];
		}
		foreach($users as $key=>$arr) {
			$t2->gotoNext("users");
			$t2->setVar("users.name", $key);
			$t2->setVar("users.green", intval($arr['green']));
			$t2->setVar("users.red", intval($arr['red']));
			$t2->setVar("users.gray", intval($arr['gray']));
			$t2->setVar("users.purple", intval($arr['purple']));
		}

		$quantity_query = "	SELECT
								SUM(orders_details.quantity) AS cnt,
								users.name
							FROM orders_details
							LEFT JOIN orders ON orders_details.order_id = orders.id
							LEFT JOIN users ON orders.added_user = users.id
							WHERE
								orders.color = 'green'
								".$where."
							GROUP BY users.id
		";
		$quantity_result = mysql_query($quantity_query) or die(mysql_error());
		while($quantity_row = mysql_fetch_assoc($quantity_result)) {
			$t2->gotoNext("user_quantities");
			$t2->setVar("user_quantities.name", $quantity_row['name']);
			$t2->setVar("user_quantities.cnt", $quantity_row['cnt']);
		}

		$orders_query = "	SELECT
								orders.id,
								CONCAT_WS(' ',countries.name,countries.currency) AS country,
								users.name
							FROM orders
							LEFT JOIN users ON orders.added_user = users.id
							LEFT JOIN countries ON orders.country = countries.id
							WHERE 1
							".$where."
							AND orders.color = 'green'
		";
		$orders_result = mysql_query($orders_query) or die(mysql_error());
		$user_sales = array();
		while($orders_row = mysql_fetch_assoc($orders_result)) {
			$user_sales[$orders_row['name']][$orders_row['country']] += GetTotal($orders_row['id'],false);
		}
		foreach($user_sales as $user=>$countries) {
			$t2->gotoNext("user_sales");
			$t2->setVar("user_sales.name", $user);
			foreach($countries as $country=>$total) {
				$t2->gotoNext("user_sales.countries");
				$t2->setVar("user_sales.countries.country", $country);
				$t2->setVar("user_sales.countries.total", $total);
			}
		}

		$content .= $t2->toString();
		$t->setVar("content", $content);
	} elseif($act=="home") {
//		CheckPrivileges("home");
		$t->setVar("where", "Home");
		$t2 = new Template("inc/home.html");

		$count_query = "	SELECT COUNT(*) AS `count`
							FROM `users_actions`
							WHERE users_actions.user = '".intval($_SESSION['user_id'])."'
		";
		$count_result = mysql_query($count_query);
		$count_row = mysql_fetch_assoc($count_result);

		$pages = new Paginator;
		$pages->items_total = $count_row['count'];
		$pages->items_per_page = $results_per_page;
		$pages->paginate();

		$user_actions_query = "
			SELECT
				*,
				DATE_FORMAT(date,'%m-%d-%Y %H:%i') AS date_str,
				users.name
			FROM users_actions
			LEFT JOIN users ON users_actions.user = users.id
			WHERE users_actions.user = '".intval($_SESSION['user_id'])."'
			ORDER BY date DESC
		".$pages->limit;
		$user_actions_result = mysql_query($user_actions_query);
		while($user_actions_row = mysql_fetch_assoc($user_actions_result)) {
			$user_actions_row['details'] = unserialize(base64_decode($user_actions_row['details']));
			$t2->gotoNext("users_actions");
			$t2->setVar("users_actions.name", $user_actions_row['name']);
			$t2->setVar("users_actions.date_str", $user_actions_row['date_str']);
			$t2->setVar("users_actions.location", $user_actions_row['location']);
			$t2->setVar("users_actions.operation", $user_actions_row['operation']);
			foreach($user_actions_row['details'] as $field=>$value) {
				$t2->gotoNext("users_actions.details");
				$t2->setVar("users_actions.details.field", $field);
				$t2->setVar("users_actions.details.value", $value);
			}
		}

		$t2->setVar("pagination", $pages->display_pages());
		$content .= $t2->toString();
		$t->setVar("content", $content);
	} elseif($act=="logout") {
		Logout();
		header("Location: index.php");
		exit;
	} else {
		$t->setVar("where", "N/A");
		$content = "Feeling lost??";
		$t->setVar("content", $content);
	}
}

$t->setVar("scroll", $scroll);
$t->setVar("title", $title_cp);
$t->setVar("title_full", $title_full);
print $t->toString();

$DB->CloseDB();

?>