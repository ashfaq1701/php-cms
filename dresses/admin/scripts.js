$(document).ready(function() {
	var status = $('.toggle').attr("title");
	if(status!="show") {
		$('.toggle legend').siblings().hide();
	}
	$('.toggle legend').click(function(){
		$('.toggle legend').siblings().slideToggle("slow");
	});

	//Default Action
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).show(); //Fade in the active content
		return false;
	});

	$(document).on("click",".size_name",function(){
		var quantity = $(this).parent().find("input").val();
		quantity++;
		$(this).parent().find("input").val(quantity);
	}); 

	$(document).on("change",".product_select",function(){
		var current_product = $(this);
		var country = $("select[name=country] option:selected").val();
		var title = $(this).find("option:selected").attr("title");
		var name = $(this).find("option:selected").attr("rel");
		var prices = $(this).find("option:selected").attr("prices");
		var img = $('<img>'); //Equivalent: $(document.createElement('img'))
		$(this).parent().parent().parent().find('td .picture').html('');
		$(this).parent().find('.product_price span').html('');
		$(this).parent().find('.product_info span').html('');
		if(title.length!=0) {
			img.attr('src', '../upload/thumbs/'+title);
			img.appendTo($(this).parent().parent().parent().find('td .picture'));
		}
		if(name.length!=0) {
			$(this).parent().find('.product_info span').html(name);
		}
		var test = $(this).parent().find('.product_price span');

		$.each(JSON.parse(prices), function(idx, obj) {
			if(obj.country==country) {
				test.html(obj.price+' '+obj.currency);
			}
		});

	});

	$( "#search_autocomplete" ).autocomplete({
		source: "index.php?act=products&ajax",
		minLength: 2
	});

	var lastSel = $("select[name=country] option:selected");

	$("select[name=country]").change(function(){
		if(lastSel.val()!="0") {
			lastSel.attr("selected", true);
			alert('To change the country please create a new order.');
		}
	});
	$("select[name=country]").click(function(){
		lastSel = $("select[name=country] option:selected");
	});

	var product_form_index=0;
	$("#add_product").click(function(){
		var country = $("select[name=country] option:selected").val();
		if(country=="0") {
			alert('Please select country first.');
			return false;
		}
		product_form_index++;
		$(this).parent().before($("#product_form").clone().attr("id","product_form" + product_form_index));
		//Make the clone visible by changing CSS
		$("#product_form" + product_form_index).css("display","inline");
		$("#product_form" + product_form_index).find("legend span").html("Product "+product_form_index);

		$( ".search_autocomplete" ).autocomplete({
			mustMatch: true,
			source: "index.php?act=products&ajax",
			minLength: 2,
			select: function( event, ui ) {
				$(this).parent().find('.product_info span').html(ui.item.description);
				$(this).parent().find('.product_code span').html(ui.item.code);
				$(this).parent().find('.product_instock span').html(ui.item.in_stock);
				var country = $("select[name=country] option:selected").val();
				var img = $('<img>'); //Equivalent: $(document.createElement('img'))
				var test = $(this).parent().find('.product_price span');
				var test2 = $(this).parent().find('.product_quantities span');
				var quantities = '';
				img.attr('src', '../upload/thumbs/'+ui.item.picture);
				img.appendTo($(this).parent().parent().parent().find('td .picture'));
				$(this).val( ui.item.id );
				$.each(JSON.parse(ui.item.prices), function(idx, obj) {
					if(obj.country==country) {
						test.html(obj.price+' '+obj.currency);
					}
				});
				test2.html();
				$.each(JSON.parse(ui.item.quantities), function(idx, obj) {
					quantities += '<br><b>'+obj.size+'</b>: '+obj.quantity;
				});
				test2.html(quantities);
				$(this).attr('type', "hidden");
				$(this).attr('readonly', true);
				return false;
			}
		});
		return false;
	});

	$( ".search_autocomplete" ).autocomplete({
		mustMatch: true,
		source: "index.php?act=products&ajax",
		minLength: 2,
		select: function( event, ui ) {
			$(this).parent().find('.product_info span').html(ui.item.description);
			$(this).parent().find('.product_code span').html(ui.item.code);
			$(this).parent().find('.product_instock span').html(ui.item.in_stock);
			var country = $("select[name=country] option:selected").val();
			var img = $('<img>'); //Equivalent: $(document.createElement('img'))
			var test = $(this).parent().find('.product_price span');
			var test2 = $(this).parent().find('.product_quantities span');
			var quantities = '';
			img.attr('src', '../upload/thumbs/'+ui.item.picture);
			img.appendTo($(this).parent().parent().parent().find('td .picture'));
			$(this).val( ui.item.id );
			$.each(JSON.parse(ui.item.prices), function(idx, obj) {
				if(obj.country==country) {
					test.html(obj.price+' '+obj.currency);
				}
			});
			test2.html();
			$.each(JSON.parse(ui.item.quantities), function(idx, obj) {
				quantities += '<br><b>'+obj.size+'</b>: '+obj.quantity;
			});
			$(this).attr('type', "hidden");
			$(this).attr('readonly', true);
			return false;
		}
	});

	$(document).on("click",".remove_product",function(){
		//Remove the whole cloned div
		$(this).closest("div").remove();
		return false;
	});
	$("#check_in_google").click(function(){
		var country = $("select[name=country] option:selected").text();
		var post_code = $("input[name=post_code]").val();
		var city = $("input[name=city]").val();
		var address = $("input[name=address]").val();
		var query = country + ' ' + post_code + ' ' + city + ' ' + address;
		window.open("https://www.google.bg/maps/search/"+query+"/",null,"height=600,width=800,status=yes,toolbar=no,menubar=no,location=no");
		return false;
	}); 

	$( ".datepicker" ).datepicker({
		inline: true,
		dateFormat: "yy-mm-dd",
		firstDay: 1,
		monthNamesShort: [ "Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември" ],
		dayNamesMin: [ "Н", "П", "В", "С", "Ч", "П", "С" ],
		showWeek: false
	});

});




