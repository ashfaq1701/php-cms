<?php
class MySQL {
	function Connect2DB($host, $name, $pass, $db){
		global $link;
		$link = mysql_connect("$host", "$name", "$pass");
		mysql_select_db("$db", $link);
		mysql_query("SET NAMES utf8");
	}
	function CloseDB(){
		global $link;
		mysql_close($link);
	}
	function FetchRow($query){
		$rows = mysql_fetch_row($query);
		return $rows;
	}
	function FetchArray($query){
		$array = mysql_fetch_array($query);
		return $array;
	}
	function FetchNum($query){
		$num = mysql_num_rows($query);
		return $num;
	}
	function Query($sql){
		$query = mysql_query($sql) or die(mysql_error());
		return $query;
	}
}

function field_validator($field_descr, $field_data, 
  $field_type, $min_length="", $max_length="", 
  $field_required=1) {
	# array for storing error messages
	global $messages, $lang, $MSG;
	
	# first, if no data and field is not required, just return now:
	if(!$field_data && !$field_required){ return; }

	# initialize a flag variable - used to flag whether data is valid or not
	$field_ok=false;

	# this is the regexp for email validation:
	$email_regexp="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|";
	$email_regexp.="(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

	# a hash array of "types of data" pointing to "regexps" used to validate the data:
	$data_types=array(
		"email"=>$email_regexp,
		"digit"=>"^[0-9]$",
		"number"=>"^[0-9]+$",
		"alpha"=>"^[a-zA-Z]+$",
		"alpha_space"=>"^[a-zA-Z ]+$",
		"alphanumeric"=>"^[a-zA-Z0-9]+$",
		"alphanumeric_space"=>"^[a-zA-Z0-9 ]+$",
		"string"=>""
	);
	
	# check for required fields
	if ($field_required && empty($field_data)) {
		$messages[] = str_replace("{field_descr}", $field_descr, $MSG[$lang]['MSG_ERROR_IS_OBLIGATORY']);
		return;
	}
	
	# if field type is a string, no need to check regexp:
	if ($field_type == "string") {
		$field_ok = true;
	} else {
		# Check the field data against the regexp pattern:
		$field_ok = ereg($data_types[$field_type], $field_data);
	}
	
	# if field data is bad, add message:
	if (!$field_ok) {
		$messages[] = str_replace("{field_descr}", $field_descr, $MSG[$lang]['MSG_ERROR_FIELD_INVALID']);
		return;
	}
	
	# field data min length checking:
	if ($field_ok && ($min_length > 0)) {
		if (strlen($field_data) < $min_length) {
			$tmp_message = str_replace("{field_descr}", $field_descr, $MSG[$lang]['MSG_ERROR_FIELD_MINIMUM']);
			$tmp_message = str_replace("{min_length}", $min_length, $tmp_message);
			$messages[] = $tmp_message;
			return;
		}
	}
	
	# field data max length checking:
	if ($field_ok && ($max_length > 0)) {
		if (strlen($field_data) > $max_length) {
			$tmp_message = str_replace("{field_descr}", $field_descr, $MSG[$lang]['MSG_ERROR_FIELD_MAXIMUM']);
			$tmp_message = str_replace("{max_length}", $max_length, $tmp_message);
			$messages[] = $tmp_message;
			return;
		}
	}
}

function makepost($in) {
	if(is_array($in)) {
		foreach($in as $key=>$val) {
			$in[$key] = makepost($val);
		}
	} else {
		$in = mysql_real_escape_string($in);
	}
	return $in;
}

function P($arr, $where = "debug") {
	global $t;
	$t->setVar($where, "<pre>".print_r($arr, true)."</pre>");
}


function createDateRangeArray($strDateFrom,$strDateTo)
{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}

/**
 * create_time_range 
 * 
 * @param mixed $start start time, e.g., 9:30am or 9:30
 * @param mixed $end   end time, e.g., 5:30pm or 17:30
 * @param string $by   1 hour, 1 mins, 1 secs, etc.
 * @access public
 * @return void
 */ 
function create_time_range($start, $end, $by='30 mins') {

    $start_time = strtotime($start);
    $end_time   = strtotime($end);

    $current    = time();
    $add_time   = strtotime('+'.$by, $current);
    $diff       = $add_time-$current;

    $times = array();
    while ($start_time < $end_time) {
        $times[] = $start_time;
        $start_time += $diff;
    }
    $times[] = $start_time;
    return $times;
} 









function GetValidTimes($id,$date,$from,$to) {
	// tursim vsichki zaeti chasove za tazi data
	if($date!=false) {
		$date_q = "AND DATE_FORMAT(`date`,'%Y-%m-%d') = '".$date."'";
	}
	$times_query = "SELECT
						DATE_FORMAT(`date`,'%k:%i') AS `date`
					FROM schedule
					WHERE
						schedule.doctor ='".$id."'
						".$date_q."
						
	";
	$times_result = mysql_query($times_query);
	$busy_times = array();
	while($times_row = mysql_fetch_assoc($times_result)) {
		$busy_times[] = $times_row['date'];
	}
	// tursim kolko vreme prodaljava edin pregled
	$doctor_query = "	SELECT
							doctors.timeslotsPerHour
						FROM doctors
						WHERE doctors.id ='".$id."'
						LIMIT 1
	";
	$doctor_result = mysql_query($doctor_query);
	$doctor_row = mysql_fetch_assoc($doctor_result);
	$AppointmentLength = (60/$doctor_row['timeslotsPerHour']); // in minutes

	// create array of time ranges
	$times = create_time_range($from.":00", $to.":00", $AppointmentLength.' mins');
	array_pop($times);
	foreach ($times as $key => $time) {
		$times[$key] = date('G:i', $time);
	}
	// generirame vsichki vazmojni chasove
	$valid_times = array_diff($times,$busy_times);
	return $valid_times;
}




function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}


function show_array($arr) {
	$out = "<ul>";
	foreach($arr as $key=>$message) {
		$out .= "<li>".$message."</li>";
	}
	$out .= "</ul>";
	return $out;
}

function GenToken() {
	return sha1(uniqid(mt_rand(), true));
}

function validateDate($date, $format = 'Y-m-d H:i:s') {
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}

class imaging
{
	// Variables
	private $img_input;
	private $img_output;
	private $img_src;
	private $format;
	private $quality = 80;
	private $x2 = 0;
	private $y2 = 0;
	private $x_input = 0;
	private $y_input = 0;
	private $x_output = 0;
	private $y_output = 0;
	private $resize;

	public function set_x2($x2)
	{
		if(is_int($x2))
		{
			$this->src_x = $x2;
		}
	}
	public function set_y2($y2)
	{
		if(is_int($y2))
		{
			$this->src_y = $y2;
		}
	}
	public function set_x_input($x_input)
	{
		if(is_int($x_input))
		{
			$this->x_input = $x_input;
		}
	}
	public function set_y_input($y_input)
	{
		if(is_int($y_input))
		{
			$this->y_input = $y_input;
		}
	}
	public function set_x_output($x_output)
	{
		if(is_int($x_output))
		{
			$this->x_output = $x_output;
		}
	}
	public function set_y_output($y_output)
	{
		if(is_int($y_output))
		{
			$this->y_output = $y_output;
		}
	}
	// Set image
	public function set_img($img)
	{
		global $valid_image_extensions;
		// Find format
		$ext = strtoupper(pathinfo($img, PATHINFO_EXTENSION));
		if(!in_array($ext,$valid_image_extensions)) {
			$ext = "jpg";
		}
		// PNG image
		if($ext == "PNG")
		{
			$this->format = $ext;
			$this->img_input = ImageCreateFromPNG($img);
			$this->img_src = $img;
		}
		// GIF image
		elseif($ext == "GIF")
		{
			$this->format = $ext;
			$this->img_input = ImageCreateFromGIF($img);
			$this->img_src = $img;
		}
		// JPEG image
		elseif($ext == "JPG" OR $ext == "JPEG")
		{
			$this->format = $ext;
			$this->img_input = ImageCreateFromJPEG($img);
			$this->img_src = $img;
		}
		// Make the rest JPEG images too... (hopefully there will be no problems here ;))
		else
		{
			$this->format = $ext;
			$this->img_input = ImageCreateFromJPEG($img);
			$this->img_src = $img;
		}
		// Get dimensions
		$this->x_input = imagesx($this->img_input);
		$this->y_input = imagesy($this->img_input);

	}

	// Set maximum image size (pixels)
	public function set_size($size = 100)
	{
		// Resize
		if($this->x_input > $size && $this->y_input > $size)
		{
			// Wide
			if($this->x_input >= $this->y_input)
			{
				$this->x_output = $size;
				$this->y_output = ($this->x_output / $this->x_input) * $this->y_input;
			}
			// Tall
			else
			{
				$this->y_output = $size;
				$this->x_output = ($this->y_output / $this->y_input) * $this->x_input;
			}
			// Ready
			$this->resize = TRUE;
		}
		// Don't resize
		else { $this->resize = FALSE; }
	}

	// Set image quality (JPEG only)
	public function set_quality($quality)
	{
		if(is_int($quality))
		{
			$this->quality = $quality;
		}
	}
	// Save image
	public function save_img($path)
	{
		// Resize
		if($this->resize)
		{
			$this->img_output = ImageCreateTrueColor($this->x_output, $this->y_output);
			ImageCopyResampled($this->img_output, $this->img_input, 0, 0, $this->src_x, $this->src_y, $this->x_output, $this->y_output, $this->x_input, $this->y_input);
		}
		// Save PNG
		if($this->format == "PNG")
		{
			if($this->resize) { imagePNG($this->img_output, $path); }
			else { copy($this->img_src, $path); }
		}
		// Save GIF
		elseif($this->format == "GIF")
		{
			if($this->resize) { imageGIF($this->img_output, $path); }
			else { copy($this->img_src, $path); }
		}
		// Save JPEG, and others too (pray the gods)
		else
		{
			if($this->resize) { imageJPEG($this->img_output, $path, $this->quality); }
			else { copy($this->img_src, $path); }
		}
	}
	// Get width
	public function get_width()
	{
		return $this->x_input;
	}
	// Get height
	public function get_height()
	{
		return $this->y_input;
	}
	// Clear image cache
	public function clear_cache()
	{
		@ImageDestroy($this->img_input);
		@ImageDestroy($this->img_output);
	}
}

// a combination of various methods
// we don't want to convert html entities, or do any url encoding
// we want to retain the "essence" of the original file name, if possible
// char replace table found at:
// http://www.php.net/manual/en/function.strtr.php#98669
function sanitizeFilename($f) {
	$replace_chars = array(
		'S'=>'S', 's'=>'s', '?'=>'Dj','Z'=>'Z', 'z'=>'z', 'A'=>'A', 'A'=>'A', 'A'=>'A', 'A'=>'A', 'A'=>'A',
		'A'=>'A', '?'=>'A', 'C'=>'C', 'E'=>'E', 'E'=>'E', 'E'=>'E', 'E'=>'E', 'I'=>'I', 'I'=>'I', 'I'=>'I',
		'I'=>'I', 'N'=>'N', 'O'=>'O', 'O'=>'O', 'O'=>'O', 'O'=>'O', 'O'=>'O', 'O'=>'O', 'U'=>'U', 'U'=>'U',
		'U'=>'U', 'U'=>'U', 'Y'=>'Y', '?'=>'B', '?'=>'Ss','a'=>'a', 'a'=>'a', 'a'=>'a', 'a'=>'a', 'a'=>'a',
		'a'=>'a', '?'=>'a', 'c'=>'c', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'i'=>'i', 'i'=>'i', 'i'=>'i',
		'i'=>'i', '?'=>'o', 'n'=>'n', 'o'=>'o', 'o'=>'o', 'o'=>'o', 'o'=>'o', 'o'=>'o', 'o'=>'o', 'u'=>'u',
		'u'=>'u', 'u'=>'u', 'y'=>'y', 'y'=>'y', '?'=>'b', 'y'=>'y', '?'=>'f'
	);
	$f = strtr($f, $replace_chars);
	// convert & to "and", @ to "at", and # to "number"
	$f = preg_replace(array('/[\&]/', '/[\@]/', '/[\#]/'), array('-and-', '-at-', '-number-'), $f);
	$f = preg_replace('/[^(\x20-\x7F)]*/','', $f); // removes any special chars we missed
	$f = str_replace(' ', '-', $f); // convert space to hyphen 
	$f = str_replace('\'', '', $f); // removes apostrophes
	$f = preg_replace('/[^\w\-\.]+/', '', $f); // remove non-word chars (leaving hyphens and periods)
	$f = preg_replace('/[\-]+/', '-', $f); // converts groups of hyphens into one
	return strtolower($f);
}


/**
 * Return non-escaping string, depends on "Magic Quotes" parameter of PHP configuration
 *
 * @param string $string String to be not escaped
 * @param bool $scaping True for escaping, normal operation otherwise
 * @return string Escaped string
 */
function SlashString($string, $escaping = false)
{
	if ($escaping == true) {
		if (get_magic_quotes_gpc()) return $string;
		else return addslashes($string);
	} else {
		if (get_magic_quotes_gpc()) return stripslashes($string);
		else return $string;
	}
}

/**
 * Generate Salt for encrypted passwords
 *
 * @param integer $size Length of generated string
 * @return string Password salt
 */
function GenSalt($size = 6)
{
	$possible = "!@#$%^&*()_+=123456789abcdfghjkmnpqrstvwxyzACDEFGHJKLMNPQRSTVWXYZ";
	$code = '';

	for ($i = 0; $i < $size; $i++) {
		$code .= substr($possible, mt_rand(0, strlen($possible) - 1), 1);
	}
	return $code;
}

/**
 * Custom algorith for salting passwords
 *
 * @param string $password Password to salting
 * @param string $salt Salt string
 * @return string MD5 Crypted password with salt
 */
function SaltPassword($password, $salt)
{
	$salt_a = substr($salt, 3, 7);
	$salt_b = substr($salt, 0, 3);
	$password = SlashString($password);
	return md5($salt_a.$password.$salt_b);
}


/**
 * Update time and other settings of login user
 * @global object $skin Smarty Template object
 */
function UpdateLogin()
{
	global $t, $login_admin_time;

	$_SESSION['timeout'] = time(); // Updates auto-logout time
	$t->setVar("LOGIN", 'ok');
	$t->setVar("LOGIN_TIME_LEFT", round($login_admin_time / 60));
	$t->setVar('LOGIN_USERNAME', $_SESSION['name']);
}


function Logout($timeout = 0) {
	global $absolute_admin;
	$_SESSION = array();
	session_destroy();
	unset($_COOKIE[session_name()]);
	if ($timeout == 0) {
		header('Location: '.$absolute_admin);
		exit;
	} else {
		header('Refresh: '.abs(intval($timeout)).'; url='.$absolute_admin);
	}
}


/**
 * Check User Privileges
 *
 * @global string $MSG Error messages
 * @global object $skin Smarty instance
 * @global array $privileges Array of existing privileges
 *
 * @param string $privilege
 * @return bool True on permission granted, False - otherwise
 */
function CheckPrivileges($privilege)
{
	global $lang, $t, $privileges, $MSG;

	if (in_array($privilege, $privileges)) {
		return true;
	} else {
		$t->setVar('error', $MSG[$lang]['MSG_ACCESS_DENIED']);
		print $t->toString();
		$DB->CloseDB();
		exit;
	}
}

function GetTotal($order,$include_delivery = true) {
	$order_query = "	SELECT
							orders.country,
							orders.delivery_costs,
							orders.discount,
							countries.currency
						FROM `orders`
						LEFT JOIN countries ON countries.id = orders.country
						WHERE
							orders.id = '".$order."'
						LIMIT 1
	";
	$order_result = mysql_query($order_query);
	$order_row = mysql_fetch_assoc($order_result);

	$query = "	SELECT
					products_prices.price,
					orders_details.quantity
				FROM `orders_details`
				LEFT JOIN products ON orders_details.product = products.id
				LEFT JOIN products_prices ON orders_details.product = products_prices.product
				WHERE
					orders_details.order_id = '".$order."'
					AND products_prices.country = '".$order_row['country']."'
	";
	$result = mysql_query($query);
	$total = 0;
	while($row = mysql_fetch_assoc($result)) {
		$total += round( ($row['price'] * $row['quantity']), 2);
	}
	$total = round( ($total - ($total * $order_row['discount']) / 100 ), 2);
	if($include_delivery) {
		$total += $order_row['delivery_costs'];
	}
	$total .= " ".$order_row['currency'];
	return $total;
}


/**
 * Logger user actions to database
 *
 * @param string $page Page Name
 * @param string $group Group of categories
 * @param string $cat Category Name
 * @param string $op Operation type name
 * @param array $misc Details for user actions
 */
function Logger($page = 'N/A', $subpage = 'N/A', $op = 'N/A', $misc = array())
{
	$page = mysql_real_escape_string($page);
	$subpage = mysql_real_escape_string($subpage);
	$op = mysql_real_escape_string($op);


	$location = $page . ' » ' . $subpage;

	// fix some potential problems with unserialize
	$misc = base64_encode(serialize($misc));

	$query = "
		INSERT INTO `users_actions`	(
			`user`,
			`date`,
			`location`,
			`operation`,
			`details`
		) VALUES (
			'".intval($_SESSION['user_id'])."',
			NOW(),
			'".$location."',
			'".$op."',
			'".$misc."'
		)
	";

	mysql_query($query);

}

